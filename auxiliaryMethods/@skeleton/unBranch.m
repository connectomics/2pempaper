function [obj, branchIds, branchDegrees ] = unBranch( ...
    obj, ...
    treeIdcs, ...
    startNodeIdcs)
% skeleton.unBranch( ... )
%   Goes through [treeIdcs] trees starting from [startNodeIdcs] in 
%   breadthfirst manner splitting at branchpoints (branchpoint coordinates 
%   are duplicated) into separate trees; resulting trees are in linear
%   format (as returned by skeleton.sortNodesLinear(...))
% 
% INPUT treeIdcs: (Optional) 
%                Integers specifying the trees to process.
%                (Default: 1:obj.numTrees ; i.e. all trees)
%       startNodeIdcs: (Optional)
%                Integers specifying the nodeIdcs where to start
%                breadthfirst search
% Author: Martin Schmdit <martin.schmidt@brain.mpg.de>

if ~exist('treeIdcs', 'var') || isempty(treeIdcs)
    treeIdcs = 1:obj.numTrees;
end

% Get all branchpoints for trees in treeIdcs
allBps = arrayfun( ...
        @(x) obj.getBranchpoints(x), ...
        treeIdcs, ...
        'uni', false);

% Pull out endpoints to check/set startNodeIdcs
allEps = arrayfun( ...
    @(x) obj.getEndpoints(x), ...
    treeIdcs, ...
    'uni', false);

if ~exist('startNodeIdcs', 'var') || isempty(startNodeIdcs)
    startNodeIdcs = cellfun( ...
        @(x) x(1), ...
        allEps);
else
    assert(isnumeric(startNodeIdcs));
    assert(numel(startNodeIdcs) == numel(treeIdcs));
    
    startNodeIdcs = reshape(startNodeIdcs, size(allEps));
    isStartNode = cellfun( ...
        @(curEps, curBps, curStartP) ...
        ismember(curStartP, curEps) | ismember(curStartP, curBps), ...
        allEps, ...
        allBps, ...
        num2cell(startNodeIdcs));
    assert(all(isStartNode));
end

branchIds = cell(numel(treeIdcs), 1);
branchDegrees = cell(numel(treeIdcs), 1);

for k = 1:numel(treeIdcs)
    treeIdx = treeIdcs(k);
    curStartNodeIdx = startNodeIdcs(k);
    curBPs = allBps{k}(~ismember(allBps{k}, curStartNodeIdx));
    curEPs = allEps{k}(~ismember(allEps{k}, curStartNodeIdx));
    
    % Get all shortest paths from the startNode to all endpoints
    G = obj.getGraph(treeIdx, false);
    curSPs = arrayfun( ...
        @(EPs) shortestpath(G,curStartNodeIdx, EPs)', ...
        curEPs, ...
        'uni', false);
    
    % Split at branchpoints (bps are duplicated)
    % (note that some branches are contained multiple times, will be sorted
    % out later)
    curSPsSplit = {};
    curBranchDeg = [];
    for l = 1:numel(curSPs)
        curSP = curSPs{l};
        curBPsInSP = find(ismember(curSP, curBPs));
        idxRanges = cat(...
            2, ...
            cat(1, 1, curBPsInSP), ...
            cat(1, curBPsInSP, numel(curSP)));
        for m = 1:size(idxRanges, 1)
            curSPsSplit{end+1} = curSP(idxRanges(m, 1):idxRanges(m, 2));
            curBranchDeg(end+1) = m;
        end
    end
    
    % Remove duplicate paths
    curSPsSplitUnique = {};
    curBranchDegUnique = [];
    for l = 1:numel(curSPsSplit)
        curSameSize = cellfun( ...
            @(x) numel(x) == numel(curSPsSplit{l}), ...
            curSPsSplitUnique);
        curIdentical = cellfun( ...
            @(x) all(x == curSPsSplit{l}), ...
            curSPsSplitUnique(curSameSize));
        if ~any(curIdentical)
            curSPsSplitUnique{end+1} = curSPsSplit{l};
            curBranchDegUnique(end+1) = curBranchDeg(l);
        end
    end
    
    % Sort by branchdegree
    [curBDegUniqueSrtd, curSrtIdcs] = sort(curBranchDegUnique);
    curPathNodeIdcs = curSPsSplitUnique(curSrtIdcs);
    
    % Add linear paths as separate trees
    curColor = obj.colors{treeIdx};
    curGroupId = obj.groupId(treeIdx);
    [tmpCurAllComments, ~, curCommentedNodeIdcs] = obj.getAllComments(treeIdx);
    curAllComments = cell(obj.numNodes(treeIdx), 1);
    curAllComments(curCommentedNodeIdcs) = tmpCurAllComments;
    
    numDigitsBranchId = numel(num2str(numel(curPathNodeIdcs)));
    numDigitsBranchDegree = numel(num2str(max(curBDegUniqueSrtd)));
    
    branchIds{k} = (1:numel(curPathNodeIdcs))';
    branchDegrees{k} = curBDegUniqueSrtd';
    
    for l = 1:numel(curPathNodeIdcs)
        curNodeIdcs = curPathNodeIdcs{l};
        curNodes = obj.nodes{treeIdx}(curNodeIdcs, 1:3);
        curComments = curAllComments(curNodeIdcs);
        curTreeName = [ ...
            obj.names{treeIdx} ...
            '_branchId_' num2str( ...
                l, ...
                ['%0' num2str(numDigitsBranchId) 'd']) ...
            '_branchDegree_' num2str( ...
                curBDegUniqueSrtd(l), ...
                ['%0' num2str(numDigitsBranchDegree) 'd'])];
        
        obj = obj.addTree( ...
            curTreeName, ...
            curNodes, [], ...
            curColor, ...
            [], ...
            curComments, ...
            curGroupId);
    end
end

% Remove original trees
obj = obj.deleteTrees(treeIdcs);

end

