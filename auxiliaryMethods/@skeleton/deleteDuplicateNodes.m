function obj = deleteDuplicateNodes(obj, treeIdcs)
% Delete connected nodes that are at the same position of a tree.
%INPUT (optional) treeIdcs: int
%           Index (not id!) of tree in skel. Default: all trees.
% Author: Marcel Beining <marcel.beining@brain.mpg.de>
% Author: Martin Schmidt <martin.schmidt@brain.mpg.de>

if nargin < 2
    treeIdcs = 1:obj.numTrees;
end

for t = 1:numel(treeIdcs)
    % Extract 2nd column of edge list, where the position of (connected)
    % nodes matches
    idcsToDelete = obj.edges{treeIdcs(t)}( ...
        ~any( ... ~any(x, 2) is same as all(x == 0, 2)
                obj.nodes{treeIdcs(t)}(obj.edges{treeIdcs(t)}(:,1), 1:3) - ...
                obj.nodes{treeIdcs(t)}(obj.edges{treeIdcs(t)}(:,2), 1:3), ...
            2), ...
        2);
    
    % Make sure to go through unique node idcs in reverse order to not 
    % invalidate idcs due to deletion
    idcsToDelete = unique(idcsToDelete, 'sorted');
    idcsToDelete = idcsToDelete(end:-1:1);
    
    for i = 1:length(idcsToDelete)
        % delete duplicate nodes and close gaps
        obj = obj.deleteNode( ... 
            treeIdcs(t), ...
            idcsToDelete(i), ...
            1);
    end
end

end