function skel = setDescription( skel, descr, varargin )
%SETDESCRIPTION Set the tracing description.
% INPUT descr: string
%           The description text. The description can contain %s to
%           substitute the old description.
%       varargin: Name-value pairs for additional options.
%           'append': logical to append descr to the current description
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

opts.append = false;

if ~isempty(varargin)
    uopts = cell2struct(varargin(2:2:end), varargin(1:2:end), 2);
    opts = Util.setUserOptions(opts, uopts);
end

if ~isfield(skel.parameters, 'experiment') ...
        || ~isfield(skel.parameters.experiment, 'description')
    skel.parameters.experiment.description = '';
end

old_descr = skel.getDescription();
if opts.append && ~isempty(old_descr)
    % NOTE(amotta): At the time of writing, the description is interpreted
    % as Markdown. Paragraphs are separated by **two** linebreaks.
    descr = strjoin({old_descr, descr}, [newline, newline]);
end

skel.parameters.experiment.description = descr;

end
