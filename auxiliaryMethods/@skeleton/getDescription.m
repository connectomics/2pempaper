function str = getDescription(obj)
%GETDESCRIPTION Get the current description of the skeleton.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

try
    % NOTE(amotta): This errors if any of the fields doesn't exist.
    str = obj.parameters.experiment.description;
catch
    str = '';
end

end

