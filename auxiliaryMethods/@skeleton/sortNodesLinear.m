function [ obj ] = sortNodesLinear( ...
    obj, ...
    treeIdcs, ...
    startNodeIdcs, ...
    resetNodeIdcs )
% skeleton.sortNodesLinear( ... )
%   Sort all nodes within specified trees to form a linear chain
%   assuming they don't have branchpoints - otherwise assertion triggers
%   Additional adaptions applied to skeleton:
%       * sorts edges to [1, 2; 2, 3; 3, 4; ...] format
%         (makes linear chain easily recognizable)
%       * resets nodeIdcs to match linear chain sorting 
%         (if turned on - as is by default)
%
%   use case: e.g. allows to easily extract a correctly ordered list of
%             coordinates via 'obj.nodes{treeIdcs(k)}(:, 1:3)' that starts 
%             at startNodeIdcs(k)
%
% INPUT treeIdcs: (Optional) 
%                Integers specifying the trees to process.
%                (Default: 1:obj.numTrees ; i.e. all trees)
%       startNodeIdcs: (Optional)
%                Integers specifying the nodeIdcs that should be the first
%                of the linear chain after this fct was applied
%       resetNodeIdcs: (Optional)
%                Additionally runs obj.resetNodeIDs(); => nodeIdcs match
%                linear chain format
% Author: Martin Schmdit <martin.schmidt@brain.mpg.de>

if ~exist('treeIdcs', 'var') || isempty(treeIdcs)
    treeIdcs = 1:obj.numTrees;
end

% Assert that there are no branchpoints for trees in treeIdcs
allBps = arrayfun( ...
        @(x) obj.getBranchpoints(x), ...
        treeIdcs, ...
        'uni', false);
numBps = cellfun(@(x) numel(x), allBps);
assert(all(numBps == 0));

% Pull out endpoints to check/set startNodeIdcs
allEps = arrayfun( ...
    @(x) obj.getEndpoints(x), ...
    treeIdcs, ...
    'uni', false);
numEps = cellfun( ...
    @(x) numel(x), ...
    allEps);
assert(all(numEps == 2));

if ~exist('startNodeIdcs', 'var') || isempty(startNodeIdcs)
    startNodeIdcs = cellfun( ...
        @(x) x(1), ...
        allEps);
else
    assert(isnumeric(startNodeIdcs));
    assert(numel(startNodeIdcs) == numel(treeIdcs));
    
    startNodeIdcs = reshape(startNodeIdcs, size(allEps));
    isStartNode = cellfun( ...
        @(curEps, curStartP) ismember(curStartP, curEps), ...
        allEps, ...
        num2cell(startNodeIdcs));
    assert(all(isStartNode));
end

if ~exist('resetNodeIdcs', 'var') || isempty(resetNodeIdcs)
    resetNodeIdcs = true;    
end
assert(islogical(resetNodeIdcs) & numel(resetNodeIdcs) == 1);

% Get endNodeIdcs
endNodeIdcs = cellfun( ...
    @(curEps, curStartP) curEps(~ismember(curEps, curStartP)), ...
    allEps, ...
    num2cell(startNodeIdcs));

% Run sorting
for k = 1:numel(treeIdcs)
    treeIdx = treeIdcs(k);
    G = obj.getGraph(treeIdx, false);
    order = shortestpath(G, startNodeIdcs(k), endNodeIdcs(k));
    obj = obj.sortNodes(treeIdx, order);
    % Sort edge list to [1, 2; 2, 3; 3, 4; ...] format
    % (by first sorting the columns, each row individually, 
    %  and then run sortrows)
    obj.edges{treeIdx} = sort(obj.edges{treeIdx}')';
    obj.edges{treeIdx} = sortrows(obj.edges{treeIdx});    
end

if resetNodeIdcs
    obj.resetNodeIDs();
end

end






