function [obj, deletedTrees] = mergeTrees( obj, varargin )
%MERGETREES Merge two trees of a skeleton.
% USAGE
%   obj.mergeTrees(id1, id2)
%       Connect the nodes with id1 and id2 and merge the corresponding
%       trees.
%   obj.mergeTrees(tree1, node1, tree2, node2)
%       Merge the trees with linear indices tree1 and tree2 by connecting
%       the nodes with linear indices node1 and node2. The node indices are
%       linear indices w.r.t. to the corresponding tree.
%   obj.mergeTrees(..., 'saveMerge')
%       Run breadth-first search on graph defined by tree edges (i.e. tree1
%       and tree2) and merge in reverse breadth-first order (i.e. from 
%       leaves towards the start tree (which is tree1(1)). 
%       This ensures that trees are collapsed as far as possible and no
%       trees are duplicated by merging multiple times or in the wrong 
%       order.
%   obj.mergeTrees(treeIndices)
%       cell array of tree indices that should be merged together (no
%       edge concatenation)
%       (Note that calling obj.mergeTrees(treeIndices) only reorders trees 
%        and does not perform what the name of this function implies. Hence
%        it might be removed at some point.)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Marcel Beining <marcel.beining@brain.mpg.de>
%         Martin Schmidt <martin.schmidt@brain.mpg.de>

% Check if 'saveMerge' was passed, and delete it from varargin (otherwise
% switch case based on length(varargin) won't work)
saveMerge = false;
saveMergeMask = cellfun( ...
    @(x) isequal(x, 'saveMerge') | isequal(x, "saveMerge"), ...
    varargin);
if any(saveMergeMask)
    saveMerge = true;
    varargin = varargin(~saveMergeMask);
end

%add all nodes of second tree to first tree
switch length(varargin)
    case 1
        % merge trees according to list of treeIndices (equivalenceClass)
        % without concatenation
        
        warning(sprintf([ 'Deprecation Warning: \n' ...
            'The usage of obj.mergeTrees with 1 input argument seems to be' ...
            ' flawed and merely does a reordering of trees, instead of ' ...
            'merging trees.\nHence, it might be deleted (or rewritten once' ...
            ' the exact use case is apparent) in the near future. ' ...
            ]));
        
        if iscell(varargin{1})
            eqClass = varargin{1}(:);
            
            % add treeIdx that are not yet part of the treeIndices list
            eqClass = cat( ...
                1, ...
                num2cell(setdiff( ...
                    1:obj.numTrees, ...
                    cell2mat(eqClass)...
                    ))', ...
                eqClass);

            numNodes = obj.numNodes;
            numEdges = cellfun(@numel, obj.edges)/2;
            numTrees = obj.numTrees;
            fNames = fieldnames(obj);
            for f = 1:numel(fNames)
                if size(obj.(fNames{f}),1) == numTrees
                    if isnumeric(obj.(fNames{f}))
                        if strcmp(fNames{f}, 'thingIDs')
                            obj.thingIDs = (1:numel(eqClass))';
                        else
                            obj.(fNames{f}) = obj.(fNames{f})( ...
                                cellfun(@(x) x(1), eqClass) ...
                            );
                        end
                    else
                        try
                            obj.(fNames{f}) = cellfun( ...
                                @(x) ...
                                    cat( ...
                                        1, ...
                                        obj.(fNames{f}){x} ...
                                    ), ...
                                eqClass, ...
                                'uni',0);
                        catch
                            obj.(fNames{f}) = cellfun( ...
                                @(x) cat(2,obj.(fNames{f}){x}), ...
                                eqClass, ...
                                'uni',0);
                        end
                    end
                elseif strcmp(fNames{f}, 'edges')
                    % Note mschmidt:
                    % This branch is usually never reached, as skel.edges
                    % usually is of size(skel.edges, 1) == skel.numTrees
                    
                    % concatenate the edges of different skels by adding number of
                    % nodes of agglo 1:n to the edge indices of agglo n+1
                    obj.edges = cellfun( ...
                        @(x) cat(1, ...
                            obj.edges{x}) + ...
                            repmat( ...
                                reshape( ...
                                    repelem( ...
                                        cumsum(cat( ...
                                            1, ...
                                            0, ...
                                            numNodes(x(1:end-1)) ...
                                        )), ...
                                    numEdges(x)), ...
                                sum(numEdges(x)), 1), ...
                            1, 2), ...
                        eqClass, ...
                        'uni', 0);
                end
            end            
        else
            error('Wrong input parameter specified (should be cell array).');
        end
    case 4
        [tree1, node1, tree2, node2] = varargin{:};
        
        % For a save merge run breadth first search on the tree graph and
        % merge in reverse order of bf
        if saveMerge
            tree1 = reshape(tree1, [], 1);
            tree2 = reshape(tree2, [], 1);
            node1 = reshape(node1, [], 1);
            node2 = reshape(node2, [], 1);
            
            % Matlab sorts edges when creating a graph; in order to keep
            % track of the original indices, we supply them as weights to
            % the graph constructor
            mergeTree = graph(tree1, tree2, 1:numel(tree1));
            graphEdgeReorderedIdcs = mergeTree.Edges{:, 'Weight'};
            
            % Do breadth-first search over all connected components and 
            % extract edges to new nodes
            [bfEdges, bfEdgeIdcs] = bfsearch( ...
                mergeTree, ...
                tree1(1), 'edgetonew', ...
                'Restart', true);
            
            % Reverse edges and edge indices
            bfEdges = bfEdges(end:-1:1, :);
            bfEdgeIdcs = bfEdgeIdcs(end:-1:1);
            
            % swap node1 and node2 if corresponding edge was reversed by
            % breadth first search
            nodes12 = cat( ...
                2, ...
                node1(graphEdgeReorderedIdcs), ...
                node2(graphEdgeReorderedIdcs));
            notSameOrder =  ...
                ~all(mergeTree.Edges{bfEdgeIdcs, 'EndNodes'} == bfEdges, 2);
            assert(all(all( ...
                mergeTree.Edges{bfEdgeIdcs(notSameOrder), 1} == ...
                bfEdges(notSameOrder, 2:-1:1), 2), 1));
            
            % Extract nodes and trees in reverse breadth first order
            linIdxNode1 = sub2ind( ...
                size(nodes12), ...
                bfEdgeIdcs, ...
                1+notSameOrder);
            node1 = nodes12(linIdxNode1);
            linIdxNode2 = sub2ind( ...
                size(nodes12), ...
                bfEdgeIdcs, ...
                2-notSameOrder);
            node2 = nodes12(linIdxNode2);
            tree1 = bfEdges(:, 1);
            tree2 = bfEdges(:, 2);
            
            % If everything went fine, there shouldn't be any duplicates in
            % tree2! (Note: we are merging tree2 trees onto tree1 trees, so 
            % merging the same tree multiple times would lead to
            % duplicates)
            assert(numel(tree2) == unique(numel(tree2)));
        end
        
        for n = 1:numel(tree1)
            numNodes1 = obj.numNodes(tree1(n));
            obj.nodes{tree1(n)} = ...
                [obj.nodes{tree1(n)}; obj.nodes{tree2(n)}];
            obj.nodesAsStruct{tree1(n)} = [ ...
                obj.nodesAsStruct{tree1(n)}, ...
                obj.nodesAsStruct{tree2(n)} ...
            ];
            obj.nodesNumDataAll{tree1(n)} = [ ...
                obj.nodesNumDataAll{tree1(n)}; ...
                obj.nodesNumDataAll{tree2(n)} ...
            ];
            obj.edges{tree1(n)} = [ ...
                obj.edges{tree1(n)}; ....
                obj.edges{tree2(n)} + numNodes1; ...
                node1(n), node2(n) + numNodes1 ...
            ];
        end
        obj = obj.deleteTrees(tree2);
        deletedTrees = tree2;
    case 2
        [tree1, node1] = obj.getNodesWithIDs(varargin{1});
        [tree2, node2] = obj.getNodesWithIDs(varargin{2});
        if tree1 == tree2
            error('Both ids belong to the same tree.');
        end
        fnArgs = {tree1, node1, tree2, node2};
        if saveMerge
            fnArgs{end+1} = 'saveMerge';
        end
        obj = obj.mergeTrees(fnArgs{:});
    otherwise
        error('Wrong number of input parameters specified.');
end



end

