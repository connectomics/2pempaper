function writeSTL(inMesh, outFile)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if isstruct(inMesh)
        % Convert format returned by `isosurface` to `triangulation`.
        inMesh = triangulation(inMesh.faces, inMesh.vertices);
    end
    
    assert(isa(inMesh, 'triangulation'));
    stlwrite(inMesh, outFile);
end
