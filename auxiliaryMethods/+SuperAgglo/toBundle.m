function bundle = toBundle(maxSegId, agglos)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    aggloCount = numel(agglos);
   [nodeOff, edgeOff] = arrayfun(@(a) deal( ...
        size(a.nodes, 1), size(a.edges, 1)), agglos);
    
    nodeOff = uint64(cat(1, [0; 0], cumsum(nodeOff(:))));
    edgeOff = uint64(cat(1, [0; 0], cumsum(edgeOff(:))));
    
    nodes = zeros(3, nodeOff(end), 'uint32');
    edges = zeros(2, edgeOff(end), 'uint64');
    segIds = zeros(nodeOff(end), 1, 'uint64');
    aggloLUT = zeros(maxSegId + 1, 1, 'uint64');
    
    for curIdx = 1:aggloCount
        curAgglo = agglos(curIdx);
        
        curNodeOff = nodeOff(curIdx + [1; 2]) + uint64([1; 0]);
        curEdgeOff = edgeOff(curIdx + [1; 2]) + uint64([1; 0]);
        
        if isfield(curAgglo, 'segIds')
            assert(size(curAgglo.nodes, 2) == 3);
            curSegIds = curAgglo.segIds;
        else
            assert(size(curAgglo.nodes, 2) == 4);
            curSegIds = curAgglo.nodes(:, 4);
        end
        
       [curSegIds, curSortIds] = sort(curSegIds(:));
        curSegIds = cast(curSegIds(:), 'uint64');
        
        segIds(curNodeOff(1):curNodeOff(2)) = curSegIds;
        aggloLUT(curSegIds + 1) = curIdx;
        
        curNodes = curAgglo.nodes(curSortIds, 1:3) - 1;
        curNodes = transpose(cast(curNodes, 'like', nodes));
        nodes(:, curNodeOff(1):curNodeOff(2)) = curNodes;
        
        curEdges = curAgglo.edges;
       [~, curEdges] = ismember(curEdges, curSortIds);
        assert(all(curEdges(:)));
       
        curEdges = sortrows(sort(curEdges, 2)) - 1;
        curEdges = transpose(cast(curEdges, 'like', edges));
        edges(:, curEdgeOff(1):curEdgeOff(2)) = curEdges;
    end
    
    bundle = struct;
    bundle.segment_to_agglomerate = aggloLUT;
    bundle.agglomerate_to_segments_offsets = nodeOff;
    bundle.agglomerate_to_segments = segIds;
    bundle.agglomerate_to_positions = nodes;
    bundle.agglomerate_to_edges_offsets = edgeOff;
    bundle.agglomerate_to_edges = edges;
end
