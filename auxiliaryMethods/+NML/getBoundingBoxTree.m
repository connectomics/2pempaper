function box = getBoundingBoxTree(nml)
    % box = getBoundingBoxTree(nml)
    %   Returns the bounding box specified the 'bbox' tree.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    things = nml.things;
    thingNames = things.name;
    
    % find bounding box
    boxThingId = cellfun( ...
        @(s) strcmpi(s, 'bbox'), thingNames);
    assert(sum(boxThingId) == 1);
    
    boxNodes = nml.things.nodes{boxThingId};
    boxNodes = struct2table(boxNodes);
    
    % find lower and upper limit of box
    boxNodes.coord = [ ...
        boxNodes.x, boxNodes.y, boxNodes.z];
    boxMinVec = min(boxNodes.coord, [], 1);
    boxMaxVec = max(boxNodes.coord, [], 1);
    
    box = nan(3, 2);
    box(:, 1) = boxMinVec(:);
    box(:, 2) = boxMaxVec(:);
end
