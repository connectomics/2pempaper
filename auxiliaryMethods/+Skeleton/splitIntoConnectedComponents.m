function [outSkels, parentIds] = splitIntoConnectedComponents(inSkels)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    outSkels = cell(numel(inSkels), 1);
    for curIdx = 1:numel(inSkels)
        outSkels{curIdx} = doIt(inSkels(curIdx));
    end
    
    parentIds = repelem(1:numel(outSkels), cellfun(@numel, outSkels));
    parentIds = reshape(parentIds, [], 1);
    
    outSkels = cat(1, outSkels{:});
end

function skels = doIt(skel)
    compIds = conncomp(graph( ...
        skel.edges(:, 1), skel.edges(:, 2), ...
        [], size(skel.nodes, 1)));
    
    compCount = max(compIds(:));
    if isempty(compCount); compCount = 0; end
    
   [~, nodeIds] = sort(compIds);
    segCounts = accumarray(compIds(:), 1, [compCount, 1]);
    nodeIds = mat2cell(nodeIds(:), segCounts, 1);
    
    nodes = cellfun( ...
        @(ids) skel.nodes(ids, :), ...
        nodeIds, 'UniformOutput', false);
    
    edgeCounts = compIds(skel.edges(:, 1));
   [~, edges] = sort(edgeCounts);
    edges = skel.edges(edges, :);
    
    edgeCounts = accumarray(edgeCounts(:), 1, [compCount, 1]);
    edges = mat2cell(edges, edgeCounts, size(edges, 2));
   [~, edges] = cellfun(@ismember, edges, nodeIds, 'UniformOutput', false);
    
    skels = struct( ...
        'nodes', nodes, ...
        'edges', edges, ...
        'nodeIds', nodeIds);
end
