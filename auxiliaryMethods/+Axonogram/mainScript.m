% Example script demonstrating how to use +Axonogram module
% Author: ??
% Modified by: Sahil Loomba <sahil.loomba@brain.mpg.de>
% see auxiliaryMethods/axonogram/

skel = skeleton('path to skeleton');
skel = skel.keepTreeWithName('your tree name'); % note this function only works for skel object with one tree for now
scale = [11.24, 11.24, 30];
startpoint = skel.getNodesWithComment('_root',1,'regexp'); % comment where the tree will start

%% attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
comments = {skel.nodesAsStruct{1}.comment}';
assert(startpoint == find(contains(comments,'_root')))
% find all comments you need
idx_sy_all = find(cellfun(@(x) any(regexp(x,'^sp p \d+$')),comments));

% Plot axonogram
TREE = Axonogram.axonogram(skel,'',startpoint,scale);

% Plot location of synapses found
if ~isempty(idx_sy_all)
    XY = Axonogram.findPointsInAxonogram(idx_sy_all, TREE, skel, scale);
    gcf, plot(XY(:,1),XY(:,2), 'ok', 'MarkerSize', 7)
end

% Make tree vertical
camroll(90) 
