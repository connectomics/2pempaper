function [points, segIds] = calcSegmentPoint(seg, borderSegId, borderVxIds)
    % points = calcSegmentPoint(seg, segIds)
    %   This function returns for each segment the coordinates of a point
    %   that is guaranteed to lie within the segment.
    %
    % seg
    %   Segmentation volume.
    %
    % borderSegId (default: 0)
    %   Segment ID used to indicate borders between segments. If segments
    %   are guaranteed to be separated by border voxels, the segment point
    %   calculation can be speed up significantly.
    %     If segments may be immediately adjacent to each other (e.g., in
    %   case of a watershed segmentation without watershed ridges, or when
    %   working with volume tracings), set `borderSegId` to empty.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('borderSegId', 'var')
        borderSegId = 0;
    end
    
    if ~exist('borderVxIds', 'var')
        borderVxIds = [];
    end
    
    if ~isempty(borderSegId)
        assert(isscalar(borderSegId));
        borderSegId = cast(borderSegId, 'like', seg);
        borderVxIds = union(borderVxIds, find(seg == borderSegId));
    end
    
    % find voxels for each segment
   [segIds, ~, segVxIds] = unique(seg(:));
    segVxIds = accumarray(segVxIds, 1:numel(seg), [], @(ids) {ids});
    
    segVxIds = segVxIds(segIds > 0);
    segIds = segIds(segIds > 0);
    
    dist = false(size(seg));
    dist(borderVxIds) = true;
    
    % NOTE(amotta): bwdist will calculate the Euclidean distance to the
    % closest non-zero voxel. But bwdist is not aware of the matrix
    % borders. Let's temporarily add artificial border voxels.
    dist = padarray(dist, [1, 1, 1], true, 'both');
    dist = bwdist(dist);
    dist = dist(2:(end - 1), 2:(end - 1), 2:(end - 1));
    
    % Find maximum in relative indices
   [~, maxIdx] = cellfun(@(vxIds) max(dist(vxIds)), segVxIds);
    maxIdx = arrayfun(@(vxIds, idx) vxIds{1}(idx), segVxIds, maxIdx);
    
    % build output
    segSize = size(seg);
    points = nan(numel(segIds), 3);
   [points(:, 1), points(:, 2), points(:, 3)] = ind2sub(segSize, maxIdx);
end
