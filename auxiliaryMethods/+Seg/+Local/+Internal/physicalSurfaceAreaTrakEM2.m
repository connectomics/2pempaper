function [surfArea, capArea] = ...
        physicalSurfaceAreaTrakEM2(~, scale, mask)
    % [surfArea, capArea] = physicalSurfaceAreaTrakEM2(opts, scale, mask)
    %   Measures the physical surface area of a volume using the algorithm
    %   of TrakEM2¹. The input may be either a binary or label volume.
    %
    % References
    % ¹ ini.trakem2.display.AreaList.measure method
    %   written by Albert Cardona and Rodney Douglas
    %   https://github.com/trakem2/TrakEM2/blob/e2c46b0851a090310542e58ee2d7afc895b57a1d/src/main/java/ini/trakem2/display/AreaList.java#L1308
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    zIds = mask;
    zIds = shiftdim(any(zIds, 1), 1);
    zIds = shiftdim(any(zIds, 1), 1);
    zIds = reshape(find(zIds), 1, []);
    
    surfArea = 0;
    capArea = 0;
    
    prevSurfArea = 0;
    prevPerim = 0;
    prevZ = 0;
    
    for curZ = zIds
        curMask = mask(:, :, curZ);
        curSurfArea = bwarea(curMask) * prod(scale(1:2));
        curPerim = regionprops(curMask, 'Perimeter');
        curPerim = sum([curPerim.Perimeter]) * scale(1);
        
        if prevZ == 0
            % Very first slice
            surfArea = curSurfArea;
            capArea = curSurfArea;
        elseif curZ > prevZ + 1
            % Close last contiguous set
            surfArea = surfArea + prevSurfArea + prevPerim * scale(3);
            capArea = capArea + prevSurfArea;
            
            % Start new contiguous set
            surfArea = surfArea + curSurfArea;
            capArea = capArea + curSurfArea;
        else
            % Continue contiguous set
            surfArea = surfArea ...
                + (prevPerim + curPerim) / 2 * scale(3) ...
                + abs(prevSurfArea - curSurfArea);
        end
        
        prevSurfArea = curSurfArea;
        prevPerim = curPerim;
        prevZ = curZ;
    end
end
