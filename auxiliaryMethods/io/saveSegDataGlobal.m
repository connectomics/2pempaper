function saveSegDataGlobal(param, offset, data)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~isfield(param, 'backend') || isempty(param.backend)
        % backward compatibility
        param.backend = 'wkcube';
    end
    
    % NOTE(amotta): Default to uint32 in the absence of a `dtype`
    % parameter. This is to ensure backward compatibility.
    dtype = 'uint32';
    if isfield(param, 'dtype')
        dtype = param.dtype;
    end
    
    % Sanity check
    assert(ismember(dtype, {'uint32', 'uint64'}));
    
    if ~isa(data, dtype)
        % make sure we don't write nonsense to files
        error('Data has class ''%s''. Expected %s.', class(data), dtype);
    end
    
    switch param.backend
        case 'wkcube'
            writeKnossosRoi( ...
                param.root, param.prefix, offset, data, dtype);
        case 'wkwrap'
            wkwSaveRoi(param.root, offset, data);
        otherwise
            error('Unknown backend ''%s''', param.backend);
    end
end
