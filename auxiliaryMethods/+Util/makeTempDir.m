function [tempDir, tempCleanup] = makeTempDir(tempRoot)
    % [tempDir, tempCleanup] = makeTempDir(tempRoot)
    %   Creates a temporary directory does is guaranteed not to have
    %   existed before calling this function. The temporary directory is
    %   created in `tempRoot`, if specified, or in `Util.getTempDir()`
    %   otherwise.
    %
    %   The second output, `tempCleanup`, is optional. If requested, it
    %   contains a `onCleanup` object that automatically removes the
    %   temporary directory once `tempCleanup` goes out of scope.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('tempRoot', 'var') || isempty(tempRoot)
        tempRoot = Util.getTempDir();
    end
    
    while true
        tempDir = tempname(tempRoot);
        % NOTE(amotta): If mkdir a directory that already exists, then okay
        % is 1 and the error message ID is non-empty. To make sure that a
        % new directory has been created we also have to check said ID.
        [okay, ~, errMsgId] = mkdir(tempDir);
        if okay && isempty(errMsgId); break; end
    end
    
    if nargout >= 2
        % Create onCleanup object that will nuke the temporary directory
        tempCleanup = onCleanup(@() removeTempDir(tempDir));
    end
end

function removeTempDir(tempDir)
    assert(rmdir(tempDir, 's'));
end
