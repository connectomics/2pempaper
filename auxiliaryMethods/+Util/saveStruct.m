function saveStruct(fileName, toSave, varargin)
    % Utility function for saving a structure to MAT file. It automatically
    % changes to V7.3 MAT files, if necessary.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.append = false;
    opts.compression = false;
    opts = Util.modifyStruct(opts, varargin{:});

    args = {};
    sizes = structfun(@byteSizeOf, toSave);
    
    if opts.append
        args{end + 1} = '-append';
    end

    if any(sizes >= 2 * 1024 * 1024 * 1024)
        % only v7.3 supports variables larger than 2 GB
        args{end + 1} = '-v7.3';
        
        if ~opts.compression
            % NOTE(amotta): Compression is on by default. MATLAB does not
            % (as far as I can tell) provide a flag to explicitly
            % requestion compression. We can only disable it.
            args{end + 1} = '-nocompression';
        end
    else
        % NOTE(amotta): Compression cannot be disable for file version 7.
        args{end + 1} = '-v7';
    end

    save(fileName, '-struct', 'toSave', args{:});
end

function byteSize = byteSizeOf(in) %#ok
    byteSize = whos('in');
    byteSize = byteSize.bytes;
end
