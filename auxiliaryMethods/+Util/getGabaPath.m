function gabaPath = getGabaPath(...
    paths, ...
    getUnixPath, ...
    filesep2use, ...
    gabaHomePrefix)
% gabaPath = getGabaPath(paths)
%   Transforms absolute path(s) to platform (OS) dependent absolute path 
%   to make Windows Paths used in conjunction with WinSshFS work under
%   Linux and vice versa
%   Relative paths (not beginning with \ or / are unaffected
%
%   INPUT paths     : cell array of char arrays with absolute paths 
%                        or char array with absolute path 
%         getUnixPath
%                   : bool; whether to return a unixPath even on windows
%                     system; defaults to false 
%                     (might be desirable, if path is next processed on 
%                      unix system without access to this getGabaPath fct.)
%         filesep2use
%                   : char - the file separator that should be used to
%                     replace the file separators in the paths; 
%                     defaults to '/' 
%                     (which also works under current Windows systems)
%         gabaHomePrefix  
%                   : char array (optional) - where to find gaba folders
%                     by default calls Util.getWinSshFSGabaDrivePrefix and
%                     therby automatically detects which DrivePrefix to use
%
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('getUnixPath', 'var') || isempty(getUnixPath)
    getUnixPath = false;
end

if ~exist('filesep2use', 'var') || isempty(filesep2use)
    filesep2use = '/'; 
    % by default all fileseps are replaced by this one, 
    % which should work under all platforms
end

if ~exist('gabaHomePrefix', 'var')
    gabaHomePrefix = '';
end

if ~getUnixPath 
    % determine where gaba is mounted under winsshfs
    gabaHomePrefix = Util.getWinSshFSGabaDrivePrefix();
end

fileseps_regexp = ['[/\' filesep ']'];

% Transform single path to linearly iterable cell array and store original
% shape info
single_path = false;
if ischar(paths)
    single_path = true;
    paths = {paths};
end
origShape = size(paths);
paths = reshape(paths, [], numel(paths));

% Iterate over paths, 
for k = 1:numel(paths)
    curr_path = paths{k};
    [startIdx, endIdx] = regexp(curr_path(1:2), '([A-Z,a-z]:){1}');
    
    % there should be maximally one drive letter prefix
    assert(numel(startIdx) <= 1);
    if numel(startIdx) == 1
        assert(startIdx == 1);
        curr_path = curr_path(endIdx+1:end);
    elseif isempty(regexp(curr_path(1), fileseps_regexp))
        % if curr_path does not start with /\ or any other filesep, 
        % it is (presumably) a relative path and is not processed further
        continue;
    end
    
    % split curr_path at fileseps and join with filesep2use to make sure
    % that there are no mixed fileseps
    curr_path = regexp(curr_path, fileseps_regexp, 'split');
    
    % rejoin curr_path and add gabaHomePrefix
    curr_path = strjoin(curr_path, filesep2use);
    if ~getUnixPath
        curr_path = strjoin({gabaHomePrefix, curr_path}, '');
    end
    paths{k} = curr_path;
end


% revert process of packing paths into linearly iterable cell array
paths = reshape(paths, origShape);
if single_path
    paths = paths{1};
end

gabaPath = paths;

end