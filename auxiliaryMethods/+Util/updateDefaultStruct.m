function [strctOut] = updateDefaultStruct(defaultStrct, strctIn)
% [strctOut] = updateDefaultStruct(defaultStrct, strctIn)
%   
% Takes in a defaultStrct that is overwritten by fields from strctIn.
% Additional fields from strctIn are added to the output struct strctOut.
% If a field is a struct and is present in defaultStrct, the function is 
% called recursively.
%
% Input arguments
%    defaultStrct
%        struct with some default values/fields
%     
%    strctIn
%        struct with a subset/superset of of defaultStrct field.
%
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

strctOut = defaultStrct;
fnames = fieldnames(strctIn);

% recurseMask: isstruct & is in defaultStrct
recurseMask = structfun(@isstruct, strctIn);
recurseMask = recurseMask & ismember(fnames, fieldnames(defaultStrct));


% Handle nonstructs / structs that are not in defaultStrct:
fnamesNoRecurse = fnames(~recurseMask);
for k = 1:numel(fnamesNoRecurse)
    strctOut.(fnamesNoRecurse{k}) = strctIn.(fnamesNoRecurse{k});
end

% Handle structs (recursively):
fnamesRecurse = fnames(recurseMask);
for k = 1:numel(fnamesRecurse)
    strctOut.(fnamesRecurse{k}) = Util.updateDefaultStruct( ...
        defaultStrct.(fnamesRecurse{k}), strctIn.(fnamesRecurse{k}));
end

end

