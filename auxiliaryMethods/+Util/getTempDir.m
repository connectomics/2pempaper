function tempDir = getTempDir()
    % tempDir = getTempDir()
    %   Gets the path to the directory of temporary files.
    %   On GABA or Cobra, it uses the scratch directory.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % default
    tempDir = tempdir();
    
    % use default if not UNIX
    if ~isunix; return; end
    
    % check for GABA
    [code, host] = runShell('hostname');
    if code ~= 0; return; end

    % remove trailing whitespaces (line breaks, nulls, etc.)
    host = deblank(host);
    
    if strncmpi(host, 'gaba', 4) % gaba
        tempDir = ['/tmpscratch/', getenv('USER'), '/'];
    elseif not(isempty(regexp(host, '^co(bra)?\d*$', 'once'))) % cobra
        % See also regular expression in +Cluster/config.m
        tempDir = ['/ptmp/', getenv('USER'), '/'];
    end
end
