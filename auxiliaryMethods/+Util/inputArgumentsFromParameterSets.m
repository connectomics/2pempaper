function inputArguments = inputArgumentsFromParameterSets(varargin)
%   Generate grid from a set of input arguments.
%   Hack: Use eval for generating dynamic variables
%   Input: Variables comma separated
%   Output: Grid of all combinations of input variables' values
%   Author: Sahil Loomba <sahil.loomba@brain.mpg.de>
%   Usage: 
%         ia = inputArgumentsFromParameterSets([1,2],[3,4],[5,6],[5],[22],[343])
%         ia =
% 
%              1     3     5     5    22   343
%              2     3     5     5    22   343
%              1     4     5     5    22   343
%              2     4     5     5    22   343
%              1     3     6     5    22   343
%              2     3     6     5    22   343
%              1     4     6     5    22   343
%              2     4     6     5    22   343

    % generate dynamic variable to store arguments
    outputArgs = struct();
    for i=1:nargin
        eval(['outputArgs.A' num2str(i) '=0;']);
    end
    
    % generate string to collect outputs from ndgrid function
    leftSideString = '';
    for i=1:nargin
        leftSideString = [leftSideString, ['outputArgs.A' num2str(i) ',']];
    end
    leftSideString = leftSideString(1:end-1); % remove last comma
    eval(['[' leftSideString '] = ndgrid(varargin{:});']);
    
    % catenate outputs to make a grid
    rightSideString = '';
    for i=1:nargin
        rightSideString = [rightSideString, ['outputArgs.A' num2str(i) '(:),']];
    end
    rightSideString = rightSideString(1:end-1); % remove last comma
    eval(['inputArguments = cat(2, ' rightSideString ');']);
end

