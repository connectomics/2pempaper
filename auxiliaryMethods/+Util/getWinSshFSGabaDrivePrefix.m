function [drivePrefix] = getWinSshFSGabaDrivePrefix(dirs2match)
% drivePrefix = getWinSshFSGabaDrivePrefix(dirs2match)
%   Windows: returns drive letter, under which WinSshFS mounts gaba dirs
%            in case of no match         : throws error
%            in case of multiple matches : prints warning and takes
%                                          alphabetically "smallest" letter
%            (make sure to set 'Directory' to '/' in WinSshFS)
%
%   non Windows: returns empty character array compatible with fullfile
%                (i.e. fullfile(getWinSshFSGabaDrivePrefix(), 'tmpscratch')
%                 also works under non Windows platforms)
%
%   INPUT dirs2match: [N] optional cell array with dirs that should
%                            be available; defaults to
%                            {'gaba', 'u', 'tmpscratch', 'wKlive'}
%
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

drivePrefix = '';
if ~ispc
    return;
end

if ~exist('dirs2match', 'var') || isempty(dirs2match)
    dirs2match = {'gaba', 'u', 'tmpscratch', 'wKlive'};
end

driveLetters = char(65:90); % Capital Letters A-Z
match = arrayfun(... Run all driveLetters through arrayfun
    @(dL) ...
    all(... Check all dirs in dirs2match for each driveLetter for existence
        cellfun(@(dir) exist([dL ':' filesep dir], 'dir'), dirs2match) ...
        ), ...
    driveLetters...
    );

num_matches = sum(match);
matched_driveLetters = driveLetters(match);

if num_matches == 0
    error(['Could not find mounted Gaba drive.' newline ...
        'Are you sure WinSshFS is running?']);
elseif num_matches > 1
    drivePrefix = [matched_driveLetters(1) ':'];
    warning(['Found Gaba mounted under multiple drives: ' newline ...
        matched_driveLetters newline ...
        'Taking drive ' drivePrefix])
else
    drivePrefix = [matched_driveLetters ':'];
end
end

