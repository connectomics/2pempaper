function makeWKAggloFileMappingFromLUT(segLUT, name, folder, zeroBasedLUT, outType, gitInfo)
% makeWKAggloFileMappingFromLUT(segLUT, name, folder)
%
% Create an agglo file for a webknossos mapping of segments.
% (cf.
% https://discuss.webknossos.org/t/mapped-segmentation-using-on-disk-lookup-table/1565/18)
%
% INPUT segLUT: [Nx1] array. 
%           segLUT(segId) = aggloId if not zeroBaseLUT, else
%           segLUT(segId + 1) = aggloId
%       name: String specifying the mapping name (without .hdf5 extension).
%       folder: string/char
%          The folder where the output file is saved.
%       zeroBasedLUT: logical (optional).
%          Whether provided segLUT is 0-based (i.e.
%          segLUT(1) contains mapping for segId 0. Note that webknossos 
%          expects this mapping, which is why we have to convert segLUT, if 
%          zeroBasedLUT is set to false (default: false).
%       outType: string/char (optional). Default: uint64
%       gitInfo: struct (Output of Util.runInfo, optional). If not
%          provided, Util.runInfo(false) will be called in the context of
%          the caller.
%

if ~exist('zeroBasedLUT', 'var')
    zeroBasedLUT = false;
end


if ~exist('outType', 'var') || isempty(outType)
    outType = 'uint64';
end

if ~exist('gitInfo', 'var') || isempty(gitInfo)
    gitInfo = evalin('caller', 'Util.runInfo(false)');
end


segLUT = reshape(segLUT, [], 1);
if ~zeroBasedLUT
    segLUT = vertcat(0, segLUT);
end

maxSegId = size(segLUT, 1);

% Store to aggFile
mapFPath = fullfile(folder, name);
curFile = [mapFPath '.hdf5'];

seg2agg = '/segment_to_agglomerate';
h5create(curFile, seg2agg, maxSegId, 'Datatype', outType);
h5write(curFile, seg2agg, segLUT);
Util.protect(curFile);

% Store gitinfo
curFile = [name '_gitInfo.mat'];
curFile = fullfile(folder, curFile);
Util.save(curFile, gitInfo);
Util.protect(curFile);

end

