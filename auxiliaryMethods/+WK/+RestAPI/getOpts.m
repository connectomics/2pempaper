function [opts, limit] = getOpts(limit)

if ~exist('limit', 'var')
    limit = 500; % default pagination
end

% url to check for verification of access token
url_base = 'https://webknossos.brain.mpg.de';
url_api = '/api/projects/%s/tasks';
full_url_pattern = [url_base url_api];
full_url = sprintf(...
    full_url_pattern, ...
    'MS_AM_scMS109_spines_volume_12_02_2019' ...
    );

persistent access_token
printBuildInfo = false;

% If we don't have an access_token yet, kindly ask the user to provide one
if isempty(access_token)
    ask_user_str = [ ...
        'Please enter your wK authentication token ' ...
        '(can be found in wK menu directly above logout):\n'];
    access_token = input(ask_user_str);
    printBuildInfo = true;
end

% If we have an access_token, briefly check, whether it's valid and ask
% user until we get a valid access tokenL
valid_access_token = false;
while ~valid_access_token
    try
        if printBuildInfo
            disp('Checking access token...');
        end

        headerFields = {'X-Auth-Token', access_token};
        opts = weboptions(...
            'HeaderFields', headerFields, ...
            'ContentType', 'auto', ...
            'Timeout', 10*60);
        url_base = 'https://webknossos.brain.mpg.de';

        % Get buildinfo as test
        full_url_buildinfo = [url_base '/api/buildinfo'];
        buildinfo = webread(full_url_buildinfo, opts);
        
        % Access project taskdef as test
        tmp = webread(full_url, opts, 'limit', 20, 'pageNumber', 0);

        valid_access_token = true;

        if printBuildInfo
            disp('Access token verified. Build Info:');
            disp(buildinfo.webknossos);
        end

    catch ME
        disp('Access token not verified. Failed with following error message: ');
        warning(ME.message);
        ask_user_str = char("Check again your access token and " + ...
            "provide a valid one (surround access token by ''):\n");
        access_token = input(ask_user_str);
    end
end

headerFields = {'X-Auth-Token', access_token};
opts = weboptions(...
    'HeaderFields', headerFields, ...
    'ContentType', 'auto', ...
    'Timeout', 10*60);

end

