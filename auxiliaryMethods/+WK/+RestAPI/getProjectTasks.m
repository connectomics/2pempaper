function [taskDefs] = getProjectTasks(projectName)

[opts, limit] = WK.RestAPI.getOpts();

taskDefs = [];

url_base = 'https://webknossos.brain.mpg.de';
url_api = '/api/projects/%s/tasks';
full_url_pattern = [url_base url_api];
full_url = sprintf(full_url_pattern, projectName);

try
    l = 0; % 0-based indexing for pagenumber of Backend REST API
    while true
        taskDefsTmp = webread(...
            full_url, ...
            'limit', limit,...
            'pageNumber', l, ...
            opts);
        
        if isempty(taskDefsTmp)
            break;
        end
        
        taskDefs = cat(1, taskDefs, taskDefsTmp);
        l = l + 1;
        
    end
    disp('Successful GET requests.')
    disp(['Loaded ' num2str(size(taskDefs, 1)) ' task defs' ...
        ' with ' ...
        num2str(sum([taskDefs.tracingTime])./1e3./60./60) ...
        ' tracing hours']);
catch ME
    errors = ME;
    warning(ME.message);
end
end

