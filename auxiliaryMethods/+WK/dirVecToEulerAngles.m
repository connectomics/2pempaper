function eulerA = dirVecToEulerAngles(diffM)
    % NOTE(amotta): Copied here (and renamed) from
    % https://gitlab.mpcdf.mpg.de/connectomics/pipeline/blob/1ff4dca6a2d675f9d644bb34aac29d308d11e3ed/+connectEM/diffToEulerAngle.m
    %
    % Kevin M. Boergens, 2016
    rotationMatrix = angle_fudge2(diffM);
    rotationMatrixT = reshape((reshape(rotationMatrix,4,4)'),1, 16);
    eulerA = mod(my2E(rotationMatrixT) * 180 / pi + 360, 360);
end

function euler = my2E(rotation_matrix)
    % NOTE(amotta): Copied here from
    % https://gitlab.mpcdf.mpg.de/connectomics/pipeline/blob/1ff4dca6a2d675f9d644bb34aac29d308d11e3ed/+connectEM/my2E.m
    %
    % generate euler angles from rotation matrix (intermediate as quaternion)
    % adapted from THREEjs setFromQuaternion and setFromRotationMatrix
    % Kevin M. Boergens, 2016
    sx = norm(rotation_matrix(1 : 3));
    sy = norm(rotation_matrix(5 : 7));
    sz = norm(rotation_matrix(9 : 11));

    if (det(reshape(rotation_matrix, 4, 4)) < 0)
        sx = -sx;
    end

    invSX = 1 / sx;
    invSY = 1 / sy;
    invSZ = 1 / sz;

    rotation_matrix(1 : 3) = rotation_matrix(1 : 3) * invSX;
    rotation_matrix(5 : 7) = rotation_matrix(5 : 7) * invSY;
    rotation_matrix(9 : 11) = rotation_matrix(9 : 11) * invSZ;

    m11 = rotation_matrix(1);
    m12 = rotation_matrix(5);
    m13 = rotation_matrix(9);
    m21 = rotation_matrix(2);
    m22 = rotation_matrix(6);
    m23 = rotation_matrix(10);
    m31 = rotation_matrix(3);
    m32 = rotation_matrix(7);
    m33 = rotation_matrix(11);

    trace = m11 + m22 + m33;

    if (trace > 0)
        s = 0.5 / sqrt(trace + 1.0);
        
        w = 0.25 / s;
        x = (m32 - m23) * s;
        y = (m13 - m31) * s;
        z = (m21 - m12) * s;
    elseif (m11 > m22 && m11 > m33)
        s = 2.0 * sqrt(1.0 + m11 - m22 - m33);

        w = (m32 - m23) / s;
        x = 0.25 * s;
        y = (m12 + m21) / s;
        z = (m13 + m31) / s;
    elseif (m22 > m33)
        s = 2.0 * sqrt(1.0 + m22 - m11 - m33);

        w = (m13 - m31) / s;
        x = (m12 + m21) / s;
        y = 0.25 * s;
        z = (m23 + m32) / s;
    else
        s = 2.0 * sqrt(1.0 + m33 - m11 - m22);

        w = (m21 - m12) / s;
        x = (m13 + m31) / s;
        y = (m23 + m32) / s;
        z = 0.25 * s;
    end
    
    sqx = x * x;
    sqy = y * y;
    sqz = z * z;
    sqw = w * w;
    
    clamp = @(v)max(min(v, 1), -1);

    euler = [ ...
        atan2(2 * (x * w - y * z), (sqw - sqx - sqy + sqz))
        asin(clamp(2 * (x * z + y * w)))
        atan2(2 * (z * w - x * y), (sqw + sqx - sqy - sqz)) - pi];
end

% NOTE(amotta): Copied here from
% https://gitlab.mpcdf.mpg.de/connectomics/pipeline/blob/1ff4dca6a2d675f9d644bb34aac29d308d11e3ed/+connectEM/angle_fudge2.m
function matrix4 = angle_fudge2(direction_vector)
    % generate rotation matrix from direction vector
    % discontinuous in [0, 0, z]
    % Kevin M. Boergens, 2016

    assert(norm(direction_vector) > 0)
    matrix4 = [-1, 0, 0, 0, 0, 0, 0.4014285632542201, 0, 0, 1, 0, 0, 0, 0, 0, 1];
    firstComp = -asin(direction_vector(3) / norm(direction_vector));
    if norm(direction_vector(1 : 2))
        secondComp = feval(@(x)x(4)*x(3), vrrotvec([0, 1, 0], [direction_vector(1 : 2), 0]));
    else
        secondComp = 0;
    end

    pitchVec = [1, 0, 0];
    yawVec = [0, 1, 0];
    matrix4 = M4x4_rotate(secondComp, yawVec, matrix4, matrix4);
    matrix4 = M4x4_rotate(firstComp, pitchVec, matrix4, matrix4);
end

%adatped from webgl-mjs
function r = M4x4_rotate(angle, axis, m, r)
    a0 = axis(1);
    a1 = axis(2);
    a2 = axis(3);
    l = sqrt(a0 * a0 + a1 * a1 + a2 * a2);
    x = a0;
    y = a1;
    z = a2;
    if (l ~= 1.0)
        im = 1.0 / l;
        x = x * im;
        y = y * im;
        z = z * im;
    end
    c = cos(angle);
    c1 = 1 - c;
    s = sin(angle);
    xs = x * s;
    ys = y * s;
    zs = z * s;
    xyc1 = x * y * c1;
    xzc1 = x * z * c1;
    yzc1 = y * z * c1;

    m11 = m(1);
    m21 = m(2);
    m31 = m(3);
    m41 = m(4);
    m12 = m(5);
    m22 = m(6);
    m32 = m(7);
    m42 = m(8);
    m13 = m(9);
    m23 = m(10);
    m33 = m(11);
    m43 = m(12);

    t11 = x * x * c1 + c;
    t21 = xyc1 + zs;
    t31 = xzc1 - ys;
    t12 = xyc1 - zs;
    t22 = y * y * c1 + c;
    t32 = yzc1 + xs;
    t13 = xzc1 + ys;
    t23 = yzc1 - xs;
    t33 = z * z * c1 + c;

    r(1) = m11 * t11 + m12 * t21 + m13 * t31;
    r(2) = m21 * t11 + m22 * t21 + m23 * t31;
    r(3) = m31 * t11 + m32 * t21 + m33 * t31;
    r(4) = m41 * t11 + m42 * t21 + m43 * t31;
    r(5) = m11 * t12 + m12 * t22 + m13 * t32;
    r(6) = m21 * t12 + m22 * t22 + m23 * t32;
    r(7) = m31 * t12 + m32 * t22 + m33 * t32;
    r(8) = m41 * t12 + m42 * t22 + m43 * t32;
    r(9) = m11 * t13 + m12 * t23 + m13 * t33;
    r(10) = m21 * t13 + m22 * t23 + m23 * t33;
    r(11) = m31 * t13 + m32 * t23 + m33 * t33;
    r(12) = m41 * t13 + m42 * t23 + m43 * t33;
    if (r ~= m)
        r(13) = m(13);
        r(14) = m(14);
        r(15) = m(15);
        r(16) = m(16);
    end
end
