% comibne tc training task results of Hiwis with GT
% Nml contains correct_synapse_types + hiwi tracings downloaded from the project
% this script appends the comments in correct answers to the hiwi answers and generates individual nmls for each Hiwi's feedback
setConfig;

nmlFile = fullfile(outDir,'data','hiwiProjects','2pEM_syn_types_results_with_GT_v04.nml');

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'input_syns', 'resultsWithGT_v04');
mkdir(outputDir)

skel = skeleton(nmlFile);

% skel GT
skelGT = skel.keepTreeWithName('correct_synapse_types','partial');

skel = skel.deleteTreeWithName('correct_synapse_types','partial');
for curTree = 1:skel.numTrees
   curSkel = skel.deleteTrees(curTree, true);
   
   idxNodes = curSkel.getNodesWithComment('(syn-|tc)',1,'regexp');
   for curNode  = idxNodes(:)'
        curNodePos = curSkel.nodes{1}(curNode,1:3);
        curComment = curSkel.nodesAsStruct{1}(curNode).comment;
        idxClosestNode = skelGT.getClosestNode(curNodePos,1,false);
        appendComment = skelGT.nodesAsStruct{1}(idxClosestNode).comment;
        newComment = sprintf('%s / answer: %s',curComment, appendComment);
        curSkel = curSkel.setComments(1,curNode, newComment);
        
        % filename
        treeName = curSkel.names{1};
        t = regexp(treeName,'YH_st126_MT4_updated_1708__609402280100005f00d8a94d__(\w*)__(\w*)','tokens');
        curSkel.groups(:,:) = [];
        curSkel.write(fullfile(outputDir,sprintf('YH_st126_MT4_updated_1708__%s.nml',t{1}{1})))
   end
end

Util.log('Finished writing tasks to %s',outputDir)
