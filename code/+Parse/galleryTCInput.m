% use proofread dendrites to readout tc input 'tc_readout_vxx.nml'
setConfig;

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);

somaColors = Util.getSomaColors;

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

Util.log('Loading nml with axon and dend tracings...')
nml = config.skelINCombined;
skel = skeleton(nml);
skel = skel.replaceComments('soma','cellbody','exact','complete');
skel = skel.replaceComments('_in','cellbody','partial','complete');

% remove extra nodes
skel = skel.deleteNodesWithComment('-','exact');
skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019
skel.scale = [11.24,11.24,30];

% remove dendrites to add dendrites to newly annoated ones
idxDel = contains(skel.names, 'cell');
skel = skel.deleteTrees(idxDel);

Util.log('Remove extra nodes and add dendrites from: %s', config.skelMerged)
skelMerged = skeleton(config.skelMerged);
for curTree = 1:skelMerged.numTrees
    curSkel = skelMerged.deleteTrees(curTree, true);
    idxDel = curSkel.getNodesWithCommentAndDegree('(syn|tc)','','regexpi','',1);
    curSkel = curSkel.deleteNodes(1, idxDel, true);
    skel = skel.addTreeFromSkel(curSkel);
end
sprintf('Found %d IN dendrites',sum(contains(skel.names,'cell')))

%scale all spheres
scaleEM = [11.24; 11.24; 30];
scale = skel.scale./1000;
nodesPC = skel.setScale(nodesPC,scale);

% filenames of nmls already sent for proofread
skelAll = skeleton(config.skel_tc_input_complete); % skelAll is with tc input
skelAll.groups(:,:) = [];

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout', 'gallery');
mkdir(outputDir)

outT = struct;
for curTree = 1:skelAll.numTrees
    % IN dendrite partial
    curSkel = skelAll.deleteTrees(curTree, true);

    % dend name
    outT(curTree).dendName = curSkel.names(1);

    % ID
    id = regexp(curSkel.names{1}, 'cell i(\d+)' , 'tokens');
    id = str2num(id{1}{1});
    outT(curTree).cellId = id;

    % IN type
    switch cellTypesAll(id)
        case 1
            type = 'WBIN';
        case 2
            type = 'LBIN';
        case 3
            type = 'L4'; 
        case 4
            type = 'NGFC'; 
        case 5
            type = 'Inv-pyr';
        case 6
            type = 'na';
        case 7
            type = 'non-L4';
        case 8
            type = 'other-barrel';
        case 9
            type = 'no-class';          
        otherwise
            error('IN class not found!')
    end
    outT(curTree).cellType = {type};
end

outT = struct2table(outT);
uniqueIds = unique(outT.cellId);

for i = 1:numel(uniqueIds)
    curId = uniqueIds(i);
    idxKeep = contains(outT.dendName, sprintf('cell i%02d', curId));
    curSkel = skelAll.deleteTrees(idxKeep, true);
    
    sprintf('Trees for cell %02d:',curId)
    disp(curSkel.names)

    % non-tc syns
    synCoords = Util.getNodeCoordsWithComment(curSkel, '(syn-|-syn|syan)', 'regexp');

    % tc syns
    tcCoords =  Util.getNodeCoordsWithComment(curSkel, '(tc\+|tc\?\+|tc\d+)', 'regexp'); % tc+ tc?+ tc12
 
    % find cellbody node
    idxPlot = contains(skel.names, sprintf('cell i%02d', curId));
    skelToPlot = skel.deleteTrees(idxPlot, true);
    somaCoord = Util.getNodeCoordsWithComment(skelToPlot, 'cellbody', 'exact');
    
    % keep dend + axon
    clear idxPlot skelToPlot
    idxPlot = contains(skel.names, sprintf('cell i%02d', curId)) | contains(skel.names, sprintf('IN axon %02d', curId));
    skelToPlot = skel.deleteTrees(idxPlot, true);

    % scale
    somaCoord = skel.setScale(somaCoord, scale);
    synCoords = skel.setScale(synCoords, scale);
    if ~isempty(tcCoords)
        tcCoords = skel.setScale(tcCoords, scale);
    end

    % plot gallery
    sprintf('Now plotting:')
    disp(skelToPlot.names)
    doPlotting(skelToPlot, curId, cellTypesAll, somaColors, somaCoord, nodesPC, synCoords, tcCoords, outputDir);
end

%% point cloud of all tc synapses
tcCoords = Util.getNodeCoordsWithComment(skelAll, '(tc\+|tc\?\+|tc\d+)', 'regexp'); % tc+ tc?+ tc12
tcCoords = skel.setScale(tcCoords, scale);

Util.log('Now plotting figure....');
colorPC = [0.5,0.5,0.5];
synColor = [146 36 40]./255; %[0,0,0];
%tcColor = [230,159,0] ./255; % orange
tcColor = [0,1,1]; % cyan

scaleEM = [11.24; 11.24; 30];
somaSize = 150; tubeSize = 2; dotSize= 3;
somaBallSize = 50;

% define layer 4 borders in z
bboxLine = [-3000,30000;1000,30000;-6000,14000];
bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);

layer4_down = 2781; % older %3205; % [103, 46, 325]
layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas

bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);

f = figure();
f.Color = 'white';
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
for k = 1:4
    a(k) = subplot(2, 2, k);
    hold on
    switch k
       case 1
           objectPC = ...
                scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                'MarkerFaceColor', colorPC(1:3));
           objectTC = ...
                scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3)...
                ,somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                'MarkerFaceColor', 'none', ...
                'Marker','v');
           %xy
           Util.setView(6); % same as fig2
           %{
           set(gca,'xtick',[])
           set(gca,'xticklabel',[])
           set(gca,'ytick',[])
           set(gca,'yticklabel',[])
           set(gca,'ztick',[])
           set(gca,'zticklabel',[])
           %}
           axis equal; axis off; box off
           a(k).XLim = bbox(1,:);
           a(k).YLim = bbox(2,:);
           a(k).ZLim = bbox(3,:);
           Util.setPlotDefault(gca);
       case 3
           objectPC = ...
                scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                'MarkerFaceColor', colorPC(1:3), ...
                'Marker','.', 'MarkerEdgeAlpha', 0.8, 'LineWidth', 1);
           objectTC = ...
                scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3)...
                ,somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                'MarkerFaceColor', 'none', ...
                'Marker','o','MarkerEdgeAlpha', 1, 'LineWidth',2);
           %yz
           Util.setView(5); % same as fig2
            % border of L4 upper and down
           line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
               [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
               'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
           line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
               [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
               'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
           %{
           set(gca,'xtick',[])
           set(gca,'xticklabel',[])
           set(gca,'ytick',[])
           set(gca,'yticklabel',[])
           set(gca,'ztick',[])
           set(gca,'zticklabel',[])
           %}
           axis equal; axis off; box off
           a(k).XLim = bbox(1,:);
           a(k).YLim = bbox(2,:);
           a(k).ZLim = bbox(3,:);
           Util.setPlotDefault(gca);
        case 2
            colorMap = jet(64); colorMap(1,:) = [1,1,1]; climits = [0.1, 150];
            hist3([tcCoords(:,2), tcCoords(:,1)], 'CDataMode','auto','FaceColor','interp');
            colormap(colorMap);
            cb = colorbar('Location','east');
            cb.Limits  = climits;
            cb.Position(4) = cb.Position(4) * 0.3;
            %title(num2str(k)); xlabel('y'); ylabel('x');
            axis equal;  axis off;  box off
            set(gca, 'XLim', bbox(2,:), 'YLim', bbox(1,:), 'XDir', 'reverse', 'YDir','reverse');
            Util.setPlotDefault(gca);
        case 4
            colorMap = jet(64); colorMap(1,:) = [1,1,1]; climits = [0.1, 150];
            hist3([tcCoords(:,2), tcCoords(:,3)], 'CDataMode','auto','FaceColor','interp');
            colormap(colorMap);
            cb = colorbar('Location','east');
            cb.Limits  = climits;
            cb.Position(4) = cb.Position(4) * 0.3;
            %title(num2str(k));  xlabel('y'); ylabel('z');
            view([0,-90])
            axis equal; axis off; box off
            set(gca, 'XLim', bbox(2,:), 'YLim', bbox(3,:),'XDir', 'reverse', 'YDir','normal');
            Util.setPlotDefault(gca);
    end
end
%Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
Visualization.Figure.removeWhiteSpaceInSubplot(f,a);
% scalebar
scaleBarLength = 50; % um
line(a(3),[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis
outDirPdf = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout');
outfile = fullfile(outDirPdf,['pointcloud_all_TC.eps']);
export_fig(outfile,'-q101', '-nocrop', '-transparent');
outfile = fullfile(outDirPdf,['pointcloud_all_TC.png']);
export_fig(outfile,'-q101', '-nocrop', '-m8');
Util.log('Saving file %s.', outfile);
close(f);


function doPlotting(skel, i, cellTypesAll, somaColors, somaLocs, nodesPC, synCoords, tcCoords, outDirPdf)
    singleColor = [0, 158, 115]./255;
    colorPC = [0.5,0.5,0.5];    
    synColor = [146 36 40]./255; %[0,0,0];
    %tcColor = [230,159,0] ./255; % orange
    tcColor = [0,1,1]; % cyan
    
    scaleEM = [11.24; 11.24; 30];
    somaSize = 150; tubeSize = 4; dotSize= 3;
    somaBallColorStp = [1, 0, 1, 1]; % magenta
    somaBallColorSps = [0,0,1,1]; %blue
    somaBallSize = 50;

    % define layer 4 borders in z
    bboxLine = [-3000,30000;1000,30000;-6000,14000];
    bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);
    
    layer4_down = 2781; % older %3205; % [103, 46, 325]
    layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas
        
    fig = figure;
    fig.Color = 'white';
    
    if i==32
        warning('Skipping i 32')
        return; % was moves to s177
    end
    somaColor = somaColors{cellTypesAll(i)};
    
    Util.log('Now plotting figure....');
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    for k = 1:2
        a(k) = subplot(2, 1, k);
        hold on
        if skel.numTrees>1 % both dend, axon exist
            idxDend = find(contains(skel.names,'cell'));
            skel.plot(idxDend, 'k', true, tubeSize);
            idxAxon = find(contains(skel.names,'axon'));
            skel.plot(idxAxon, singleColor, true, tubeSize);
        else % only dend
            idxDend = find(contains(skel.names,'cell'));
            skel.plot(idxDend, 'k', true, tubeSize);
        end
        objectPC = ...
                    scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                    ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                    'MarkerFaceColor', colorPC(1:3));
        objectSomas = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,somaSize,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', somaColor(1:3));
        objectSyns = ...
                    scatter3(synCoords(:,1),synCoords(:,2),synCoords(:,3)...
                    ,somaBallSize, 'MarkerEdgeColor', synColor(1:3), ...
                    'MarkerFaceColor', 'none', ...
                    'Marker','.', 'MarkerEdgeAlpha', 0.8, 'LineWidth', 0.3);
        if ~isempty(tcCoords)
        objectTC = ...
                    scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3)...
                    ,somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                    'MarkerFaceColor', 'none', ...
                    'Marker','o', 'MarkerEdgeAlpha', 1, 'LineWidth',0.5);
        end

        switch k
           case 1
               %xy
               Util.setView(6); % same as fig2
           case 2
               %yz
               Util.setView(5); % same as fig2
                % border of L4 upper and down
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
        end
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
        axis equal
        axis off
        box off
        a(k).XLim = bbox(1,:);
        a(k).YLim = bbox(2,:);
        a(k).ZLim = bbox(3,:);
    end
    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
    
    % scalebar
    scaleBarLength = 50; % um
    line(a(k),[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis

    annotation( ...
            f,...
            'textbox', [0, 0.01,1,0.1], ...
            'String', sprintf('i%02d.png',i), ...
            'EdgeColor', 'none',...
            'Color', [0,0,0], ...
            'Interpreter','none',...
            'HorizontalAlignment', 'center')

    outfile = fullfile(outDirPdf,['IN_cell',num2str(i),'.eps']);
    export_fig(outfile,'-q101', '-nocrop', '-transparent');
    outfile = fullfile(outDirPdf,['IN_cell',num2str(i,'%02d'),'.png']);
    export_fig(outfile,'-q101', '-nocrop', '-transparent');
    Util.log('Saving file %s.', outfile);
    close(f);
end
