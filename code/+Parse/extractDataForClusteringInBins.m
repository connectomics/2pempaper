% use proofread dendrites to pick random TC synapses on BINs and ge the data table
setConfig;

outputDir = fullfile(outDir,'data');

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);


skelMerged = skeleton(config.skelMerged);
idsKeep = find(ismember(cellTypesAll, [3])); % Keep WB, LB

skel= skeleton(fullfile(outDir, 'outData','tracings','hiwiProjects','inspect_clustering_in_bins_complete_SL.nml'));
numTrees = skel.numTrees;

% output 
t = struct;
t.allIds = nan(numTrees,1);
t.cellType = cell(numTrees,1);
t.checked = nan(numTrees,1);
t.SASDyes = nan(numTrees,1);
t.SASD_yes_fraction = nan(numTrees,1);
t.SASDno = nan(numTrees,1);
t.SASD_no_fraction = nan(numTrees,1);

for i = 1:skel.numTrees
    curSkel = skel.deleteTrees(i, true);
    
    curId = regexp(curSkel.names{1}, 'cell i(\d+)', 'tokens');
    curId = str2num(curId{1}{1});
    sprintf('Trees for cell %02d:',curId)
    disp(curSkel.names)

    switch cellTypesAll(curId)
        case 1
            curType = 'WB';
        case 2
            curType = 'LB';
        otherwise
            error('Type not defined');
    end

    % parse
    allC = curSkel.getAllComments;

    % save
    t.allIds(i) = curId;
    t.cellType{i} = curType;
    t.checked(i) = sum(contains(allC, 'sasd'));
    t.SASDyes(i) = sum(contains(allC, 'sasd yes'));
    t.SASDno(i) = sum(contains(allC, 'sasd no'));
end

t.SASD_yes_fraction = t.SASDyes ./ t.checked;
t.SASD_no_fraction = t.SASDno ./ t.checked;
outTable = struct2table(t);
disp(outTable)

writetable(outTable, fullfile(outputDir, 'tc_clustering_bins_complete_SL.xlsx'))
