% IN gallery figures combined overlaid
%{
setConfig;

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout', 'galleryCombinedINs');
mkdir(outputDir)

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);

rng(0)
somaColors = Util.getSomaColors;

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

%scale all spheres
scaleEM = [11.24; 11.24; 30];
scale = skelPC.scale./1000;
nodesPC = skelPC.setScale(nodesPC,scale);

% skel with tc input
skelAll = skeleton(config.skel_tc_input_complete); % skelAll is with tc input
skelAll.groups(:,:) = [];

Util.log('Loading nml with axon and dend tracings...')
nml = config.skelINCombined;
skel = skeleton(nml);
skel = skel.replaceComments('soma','cellbody','exact','complete');
skel = skel.replaceComments('_in','cellbody','partial','complete');

% remove extra nodes
skel = skel.deleteNodesWithComment('-','exact');
skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019
skel.scale = [11.24,11.24,30];

% remove dendrites to add dendrites to newly annoated ones (53)
idxDel = contains(skel.names, 'cell');
skel = skel.deleteTrees(idxDel);

Util.log('Remove extra nodes and add dendrites from: %s', config.skelMerged)
skelMerged = skeleton(config.skelMerged);
for curTree = 1:skelMerged.numTrees
    curSkel = skelMerged.deleteTrees(curTree, true);
    idxDel = curSkel.getNodesWithCommentAndDegree('(syn|tc)','','regexpi','',1);
    curSkel = curSkel.deleteNodes(1, idxDel, true);
    skel = skel.addTreeFromSkel(curSkel);
end
sprintf('Found %d IN dendrites',sum(contains(skel.names,'cell')))

% remove axons of those that are not 53 axons
dendNames = skel.names(contains(skel.names, sprintf('cell i')));
dn = cellfun(@(x) x(end-1:end),dendNames, 'uni',0);
idxDel = ~contains(skel.names, dn);
skel = skel.deleteTrees(idxDel);

% soma Coords
idxPlot = contains(skel.names, sprintf('cell i'));
skelToPlot =  skel.deleteTrees(idxPlot, true); 
dendNames = skelToPlot.names;
somaCoords = Util.getNodeCoordsWithComment(skelToPlot, 'cellbody', 'exact');
somaCoords = skel.setScale(somaCoords, scale);
%}

f = figure;
f.Color = 'none';
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];


%% plot BP dendrites
idsBP = [1,7,18,19,23]; % from MH + [38,48]
idxStrongTC = arrayfun(@(x) sprintf('%02d',x), idsBP,'uni',0);
idxPlot = contains(skel.names, sprintf('cell'));
skelToPlot =  skel.deleteTrees(idxPlot, true);
idxPlot = contains(skelToPlot.names, idxStrongTC);
skelToPlot =  skelToPlot.deleteTrees(idxPlot, true);
names = skelToPlot.names;
dn = cellfun(@(x) x(end-1:end),dendNames, 'uni',0);
an = cellfun(@(x) x(end-1:end),names, 'uni',0);
curSomaCoords = [];
for i=1:numel(an)
    curAn = an(i);
    curSomaCoords(i,:) = somaCoords(ismember(dn,curAn),:);
end
numColors = size(curSomaCoords,1);
%curColors = randperm(255,numColors)' .* ones(numColors,1) * ([1,1,1]/255); % gradient of grey colors
curColors = Util.getRangedColors(0,0,0.7,1,0.7,1, numColors); %repmat([0,1,1], numColors,1);
outfile = fullfile(outputDir, 'gallery_IN_dendrites_BPs.eps');
doPlotting(skelToPlot, curColors, curSomaCoords, outfile);

%% plot MP dendrites
idxStrongTC = arrayfun(@(x) sprintf('%02d',x), [idsBP, 35],'uni',0);
idxPlot = contains(skel.names, sprintf('cell'));
skelToPlot =  skel.deleteTrees(idxPlot, true);
idxPlot = ~contains(skelToPlot.names, idxStrongTC); % non bin
skelToPlot =  skelToPlot.deleteTrees(idxPlot, true);
names = skelToPlot.names;
dn = cellfun(@(x) x(end-1:end),dendNames, 'uni',0);
an = cellfun(@(x) x(end-1:end),names, 'uni',0);
curSomaCoords = [];
for i=1:numel(an)
    curAn = an(i);
    curSomaCoords(i,:) = somaCoords(ismember(dn,curAn),:);
end
numColors = size(curSomaCoords,1);
%curColors = randperm(255,numColors)' .* ones(numColors,1) * ([1,1,1]/255); % gradient of grey colors
curColors = Util.getRangedColors(0.5,0.8, 0,0.2,0.4,0.6, numColors); % repmat([0.75, 0, 0.75], numColors,1);
outfile = fullfile(outputDir, 'gallery_IN_dendrites_MPs.eps');
doPlotting(skelToPlot, curColors, curSomaCoords, outfile);

outfile = fullfile(outputDir, 'gallery_IN_dendrites_BP_MP.fig');
export_fig(outfile,'-q101', '-nocrop', '-transparent');
close all

function doPlotting(skel, somaColors, somaCoords, outfile, synCoords, tcCoords, isPC, skelPreSynAxons)
    scaleEM = [11.24; 11.24; 30];
    somaSize = 150; tubeSize = 4; dotSize= 3;
    somaBallSize = 50;
    synColor = [146 36 40]./255; %[0,0,0];
    %tcColor = [230,159,0] ./255; % orange
    tcColor = [0,1,1]; % cyan
    

    if ~exist('isPC','var') | isempty(isPC)
        isPC = false;
    end
    if isPC
       somaSize = dotSize;
       somaBallSize = dotSize; 
    end
    % define layer 4 borders in z
    bboxLine = [-3000,30000;1000,30000;-6000,14000];
    bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);
    
    layer4_down = 2781; % older %3205; % [103, 46, 325]
    layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas
        
    Util.log('Now plotting figure....');
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
%    f = figure();
%    f.Units = 'centimeters';
%    f.Position = [1 1 21 29.7];
%    for k = 1:2
%       a(k) = subplot(2, 1, k);
        hold on
        if exist('skel','var') & ~isempty(skel)
            skel.plot('', somaColors, true, tubeSize);
        end

        if exist('skelPreSynAxons','var') & ~isempty(skelPreSynAxons)
            skelPreSynAxons.plot('', repmat(tcColor, skelPreSynAxons.numTrees,1), true, tubeSize);
        end
        if exist('somaCoords','var') & ~isempty(somaCoords)
            objectSomas = ...
                        scatter3(somaCoords(:,1),somaCoords(:,2),somaCoords(:,3),...
                        somaSize, somaColors, 'filled');
        end
        if exist('synCoords','var') & ~isempty(synCoords)
            objectSyns = ...
                        scatter3(synCoords(:,1),synCoords(:,2),synCoords(:,3),...
                        somaBallSize, 'MarkerEdgeColor', synColor(1:3), ...
                        'MarkerFaceColor', 'none', ...
                    'Marker','.', 'MarkerEdgeAlpha', 0.8, 'LineWidth', 1);
        end
        if exist('tcCoords','var') & ~isempty(tcCoords)
            objectTC = ...
                        scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3),...
                        somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                        'MarkerFaceColor', 'none', ...
                        'Marker','.', 'MarkerEdgeAlpha', 1, 'LineWidth',2);
        end

%        switch k
%           case 1
%               %xy
%               Util.setView(6); % same as fig2
%           case 2
%               %yz
               Util.setView(5); % same as fig2
                % border of L4 upper and down
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
%        end

%{
        % do 3D
        camorbit(-10,0,'camera');
        camorbit(0,-15,'data',[0 1 0]);
      
        view(0,0) 
        camorbit(-50,0,'camera');
        camorbit(0,5,'camera');
%}

        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
        axis equal
        axis off
        box off

        ax = gca;
        ax.XLim = bbox(1,:);
        ax.YLim = bbox(2,:);
        ax.ZLim = bbox(3,:);
%    end
%    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
    
    % scalebar
    scaleBarLength = 50; % um
    line(ax,[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis

%    export_fig(outfile,'-q101', '-nocrop', '-transparent');
%    Util.log('Saving file %s.', outfile);
%    close(f);
end
