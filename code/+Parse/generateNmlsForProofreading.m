% use Hiwi zip files to generate nmls for proofreading by Yunfeng
setConfig;

% zip dir
zipDir = '/tmpscratch/sahilloo/barrel/outData/tracings/hiwiProjects/2pEM_tc_tasks_finished_05_05_2021/';

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_05_05_2021_for_proofreading_test');
mkdir(outputDir)

files = dir(fullfile(zipDir,'*.zip'));

for curFile = 1:numel(files)

    [tempDir, cleanupFlag] = Util.makeTempDir; % get temp dir and clean it later
    
    % unzip
    nmlFile = unzip(fullfile(zipDir, files(curFile).name), tempDir);

    % skel
    curSkel = skeleton(nmlFile{:});

    % delete all groups
    curSkel.groups(:,:) = [];

    % tree name with 'cell'
    idxKeep = cellfun(@(x) any(regexpi(x, 'cell')), curSkel.names);
    assert(sum(idxKeep) == 1); % only one main tree with cell ID

    curSkel.write(fullfile(outputDir,sprintf('%s.nml',curSkel.names{idxKeep})));
    clear curSkel cleanupFlag
end

Util.log('Finished writing nmls to %s',outputDir)
