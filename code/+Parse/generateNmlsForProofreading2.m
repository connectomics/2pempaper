% use Hiwi nml files and discard those already proofread
setConfig;

% filenames of nmls already sent for proofread
pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_05_05_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = arrayfun(@(x) x.name(1:end-4), pf, 'uni',0);

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_17_05_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_25_05_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_04_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_11_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_18_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_22_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_24_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_28_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_29_06_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

pfDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_02_07_2021_for_proofreading');
pf = dir(fullfile(pfDir,'*.nml'));
pfNames = cat(1, pfNames, arrayfun(@(x) x.name(1:end-4), pf, 'uni',0));

% nml tasks dir
nmlDir = '/tmpscratch/sahilloo/barrel/outData/tracings/hiwiProjects/2pEM_tc_tasks_finished_08_07_2021/';

% new dir for files to proofread
outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread', '2pEM_tc_tasks_finished_08_07_2021_for_proofreading');
mkdir(outputDir)

files = dir(fullfile(nmlDir,'*.nml'));
for curFile = 1:numel(files)

    % skel
    curSkel = skeleton(fullfile(nmlDir, files(curFile).name));

    % delete all groups
    curSkel.groups(:,:) = [];

    % tree name with 'cell'
    idxKeep = cellfun(@(x) any(regexpi(x, 'cell')), curSkel.names);
    assert(sum(idxKeep) == 1); % only one main tree with cell ID, sometimes tree name is updated with taskID then search for group name manually

    % check this this nml was not proofread
    outName = curSkel.names{idxKeep};
    if any(contains(pfNames, outName))
        sprintf('Skipping file %s',outName)
        continue;
    end
    curSkel.write(fullfile(outputDir,sprintf('%s.nml',outName)));
    clear curSkel
end

Util.log('Finished writing nmls to %s',outputDir)
