% use split dendrites of INs to measure input synapses with Hiwis
setConfig;

nmlFile = fullfile(outDir,'data','hiwiProjects','2pEM_project_input_synapses_v03_b.nml');

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'input_syns', 'round3b');
mkdir(outputDir)

skelMain = skeleton(nmlFile);

%skel = skelMain.keepTreeWithName('_start_','partial');
%skel = skelMain.keepTreeWithName('Hiwi','partial');
skel = skelMain.keepTreeWithName('_start','partial');

[~,idxSort] = sort(skel.names);
skel  = skel.reorderTrees(idxSort);

for curTree = 1:skel.numTrees
    curSkel = skel.deleteTrees(curTree, true);
    % delete all groups
    curSkel.groups(:,:) = [];
    curSkel.write(fullfile(outputDir,sprintf('%s.nml',curSkel.names{:})));
end
Util.log('Finished writing tasks to %s',outputDir)
