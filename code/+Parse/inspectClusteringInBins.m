% use proofread dendrites to pick random TC synapses on BINs
setConfig;

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout');
mkdir(outputDir)

skelMerged = skeleton(config.skelMerged);
idsKeep = find(ismember(cellTypesAll, [3])); % Keep WB, LB

skelOut= skeleton();
skelOut.parameters = skelMerged.parameters;
skelOut.scale = [11.24, 11.24, 30]; % nm
rng(0)
countFraction  = 0.10;

for i = 1:numel(idsKeep)
    curId = idsKeep(i);
    curTreeName =  sprintf('cell i%02d', curId)
    idxKeep = contains(skelMerged.names, curTreeName);
    curSkel = skelMerged.deleteTrees(idxKeep, true);
    
    sprintf('Trees for cell %02d:',curId)
    disp(curSkel.names)

    % tc syns
    idxComments =  curSkel.getNodesWithComment('(tc\+|tc\?\+|tc\d+)', 1, 'regexp'); % tc+ tc?+ tc12
    count = ceil(countFraction * numel(idxComments));
    idxComments = idxComments( randperm(numel(idxComments), count)); % inspect random tc syns

    for j=1:numel(idxComments)
        curIdx = idxComments(j);
        curComment = curSkel.nodesAsStruct{1}(curIdx).comment;
        newComment = sprintf('%s sasd',curComment);
        curSkel.nodesAsStruct{1}(curIdx).comment = newComment;
    end

    skelOut = skelOut.addTreeFromSkel(curSkel);
    clear curSkel
end
skelOut.write(fullfile(outputDir, sprintf('%s_inspectClustering.nml', skelMerged.filename)));
