% use proofread dendrites to readout tc input 'tc_readout_vxx.nml'
setConfig;

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);


% filenames of nmls already sent for proofread
skelAll = skeleton(config.skel_tc_input_complete);
skelAll.groups(:,:) = [];

% remove those dendrites that are only partially done
idxDel = contains(skelAll.names, 'update');
Util.log('Removing trees to be updated: %d', sum(idxDel))
skelAll = skelAll.deleteTrees(idxDel);

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout');
mkdir(outputDir)

outT = struct;
for curTree = 1:skelAll.numTrees
    % IN dendrite partial
    curSkel = skelAll.deleteTrees(curTree, true);
    curComments = curSkel.getAllComments;

    % remove syn comment nodes to measure accurate pathlength
    idxDel = curSkel.getNodesWithCommentAndDegree('(syn|tc)','','regexpi','',1);
    curSkel = curSkel.deleteNodes(1, idxDel, true);

    % dend name
    outT(curTree).dendName = curSkel.names(1);

    % ID
    id = regexp(curSkel.names{1}, 'cell i(\d+)' , 'tokens');
    id = str2num(id{1}{1});
    outT(curTree).cellId = id;

    % IN type
    switch cellTypesAll(id)
        case 1
            type = 'WBIN';
        case 2
            type = 'LBIN';
        case 3
            type = 'L4'; 
        case 4
            type = 'NGFC'; 
        case 5
            type = 'Inv-pyr';
        case 6
            type = 'na';
        case 7
            type = 'non-L4';
        case 8
            type = 'other-barrel';
        case 9
            type = 'no-class';          
        otherwise
            error('IN class not found!')
    end
    outT(curTree).cellType = {type};

    % path lengths
    pL = curSkel.pathLength ./ 1e3; % um
    outT(curTree).pathlength = pL;

    % num of synapses
    numSyn = sum(cellfun(@(x) any(regexpi(x,'(syn-|-syn|tc|syan)')), curComments));
    outT(curTree).numSyn = numSyn;

    % num of tc syns
    numTCSyn = sum(cellfun(@(x) any(regexp(x, '(tc\+|tc\?\+|tc\d+)')), curComments)); % tc+ tc?+ tc12
    outT(curTree).numTCSyn = numTCSyn;

    % num of tc syns identified with axons
    numTCSynWithAxons = sum(cellfun(@(x) any(regexp(x, 'tc\d+')), curComments)); %tc12
    outT(curTree).numTCSynWithAxons = numTCSynWithAxons;

    % num of non-tc syns
    outT(curTree).numNonTCSyn = outT(curTree).numSyn  - outT(curTree).numTCSyn;
    
    % fraction of tc syns, densities
    outT(curTree).fractionTC = outT(curTree).numTCSyn / outT(curTree).numSyn;
    outT(curTree).fractionTCWithAxons = outT(curTree).numTCSynWithAxons / outT(curTree).numTCSyn;
    outT(curTree).synDensity = outT(curTree).numSyn / outT(curTree).pathlength;
    outT(curTree).tcSynDensity = outT(curTree).numTCSyn / outT(curTree).pathlength;
end

outT = struct2table(outT);
[~,idxSort] = sort(outT.dendName);
outT = outT(idxSort,:);

% write output
outfile = fullfile(outputDir, sprintf('readout_tc_%s',datestr(clock,30)));
writetable(outT, strcat(outfile, '.xlsx')); %data

% write output pooled over dendrites per IN
outTPooled = struct;
uniqueIds = unique(outT.cellId);
for i = 1:numel(uniqueIds)
    curId = uniqueIds(i);
    
    idxCurId = outT.cellId == curId;
    curT = outT(idxCurId,:);

    outTPooled(i).name = sprintf('cell i%02d',curId);
    outTPooled(i).id = curId;
    outTPooled(i).cellType = curT.cellType(1);
    outTPooled(i).pathlength = sum(curT.pathlength);
    outTPooled(i).numSyn = sum(curT.numSyn);
    outTPooled(i).numTCSyn = sum(curT.numTCSyn);
    outTPooled(i).numTCSynWithAxons = sum(curT.numTCSynWithAxons);
    outTPooled(i).numNonTCSyn = sum(curT.numNonTCSyn);
    % measurements
    outTPooled(i).fractionTC = outTPooled(i).numTCSyn / outTPooled(i).numSyn;
    outTPooled(i).fractionTCWithAxons = outTPooled(i).numTCSynWithAxons / outTPooled(i).numTCSyn;
    outTPooled(i).synDensity = outTPooled(i).numSyn / outTPooled(i).pathlength;
    outTPooled(i).tcSynDensity = outTPooled(i).numTCSyn / outTPooled(i).pathlength;
end
outTPooled = struct2table(outTPooled);
[~,idxSort] = sort(outTPooled.name);
outTPooled = outTPooled(idxSort,:);

% add barrel fraction to IN data
Util.log('Adding data for barrel fraction:')
outTPooled = addBarrelFractionData(outDir, outTPooled);

% export pooled table
Util.log('Saving IN table %s_pooled:', outfile)
writetable(outTPooled, strcat(outfile, '_pooled.xlsx'));

% plot
Util.log('Scatter plots for input tc:')
outTPooled = doPlotting(outTPooled, cellTypesAll, strcat(outfile, '_pooled'));
%doPlotting(outT, cellTypesAll, strcat(outfile, '_per_dendrite'));
disp(outTPooled)

% export combined dendrites skeleton
Util.log('Generaing merged skeleton:')
colors = Util.getSomaColors;
colors = cell2mat(colors');

skelMerged = skeleton();
skelMerged.parameters = skelAll.parameters;
skelMerged.scale = [11.24, 11.24, 30]; % nm
uniqueIds = unique(outTPooled.id);
for i = 1:numel(uniqueIds)
    curId = uniqueIds(i);
    curName = sprintf('cell i%02d',curId);
    curColor = colors(cellTypesAll(curId),1:3);
    % cur soma
    curSomaLoc = outTPooled.somaLocs(outTPooled.id == curId,:);

    % add soma node to each primary dendrite
    curTreesToAdd = contains(skelAll.names, sprintf('%02d',curId));
    curSkel = skelAll.deleteTrees(curTreesToAdd, true);
    for j = 1:curSkel.numTrees
        connect_to = curSkel.getClosestNode( curSomaLoc, j, false);
        curSkel = curSkel.addNode(j, curSomaLoc, connect_to, '', 'cellbody');
    end
    
    % merge trees 
    curSkel  = curSkel.mergeTreesSimple(1:curSkel.numTrees);
    assert(curSkel.numTrees == 1)
    assert(contains(curSkel.names{:}, curName))

    skelMerged = skelMerged.addTreeFromSkel(curSkel); % add all prim dendrites to this tree
    % update tree name
    skelMerged.names{i} = curName;
end
skelMerged.write(strcat(outfile, '.nml')); % sanity
Util.log('Finished writing skelMerged to %s',outfile)

%% add soma targeting fraction
[somaSynFrac, tempAxonIds] = Figures.Fig2.IN.getSomaSynFraction;
idxKeep = ismember(outTPooled.id, tempAxonIds);
assert(isequal(tempAxonIds, outTPooled.id(idxKeep)))

somaSynFracAll = nan(height(outTPooled),1);
somaSynFracAll(idxKeep) = somaSynFrac;
outTPooled.somaSynFraction = somaSynFracAll;

%% measure 'tc' input dependence on distance from soma
Util.log('Measuring tc and non-tc syn distances from soma:')
numTrees = skelMerged.numTrees;
tcDistFromSoma = cell(numTrees,1);
for curTree=1:numTrees
    curSkel = skelMerged.deleteTrees(curTree, true);
    soma = curSkel.getNodesWithComment('cellbody',1,'partial');
    soma_id = str2num(curSkel.nodesAsStruct{1}(soma).id);
    tc_input = curSkel.getNodesWithComment('(tc\+|tc\?\+|tc\d+)',1,'regexp');
    L = [];
    for n=1:numel(tc_input)
        cur_tc_input_id = str2num(curSkel.nodesAsStruct{1}(tc_input(n)).id);
        [~,~,l] = curSkel.getShortestPath(soma_id,cur_tc_input_id);
        l = l/1e3; % um
        L = [L,l];
    end
    tcDistFromSoma{curTree} = L;
end
outTPooled.tcDistFromSoma = tcDistFromSoma;

% measure non-tc synapse distances from soma
nonTcDistFromSoma = cell(numTrees,1);
tic;
for curTree=1:numTrees
    curSkel = skelMerged.deleteTrees(curTree, true);
    soma = curSkel.getNodesWithComment('cellbody',1,'partial');
    soma_id = str2num(curSkel.nodesAsStruct{1}(soma).id);
    non_tc_input = curSkel.getNodesWithComment('(syn-|-syn|tc|syan)',1,'regexp');
    L = [];
    for n=1:numel(non_tc_input)
        cur_non_tc_input_id = str2num(curSkel.nodesAsStruct{1}(non_tc_input(n)).id);
        [~,~,l] = curSkel.getShortestPath(soma_id,cur_non_tc_input_id);
        l = l/1e3;% um
        L = [L,l];
    end
    nonTcDistFromSoma{curTree} = L;
    Util.progressBar(curTree,numTrees);
end
outTPooled.nonTcDistFromSoma = nonTcDistFromSoma;

% plot
Util.log('Plotting tc and non-tc syn distance dependence on soma:')
out = plotDistFromSoma(outTPooled, cellTypesAll)
export_fig(strcat(outfile, '_tc_dist_from_soma.png'), '-q101', '-nocrop', '-m8');
close all

% export pooled table with distance dependence data, soma syn fraction
Util.log('Saving IN table %s_pooled_distance_data:', outfile)
writetable(outTPooled, strcat(outfile, '_pooled_distance_data.xlsx'));
Util.save(strcat(outfile, '_pooled_distance_data.mat'), outTPooled)

function out = plotDistFromSoma(outTPooled, cellTypesAll)
    % plot tc dist from somata
    somaColors = Util.getSomaColors;
    fontSize = 8;
    binLimits = [0,300]; %Manually check
    binWidth = 10; %um

    fig = figure;
    fig.Color = 'white';
    ax = subplot(3,2,1);
    hold on
    y_all = [];
    for i=1:height(outTPooled)
        curTable = outTPooled(i,:);
        curColor = somaColors{cellTypesAll(curTable.id)}(1:3);
    
        x = curTable.tcDistFromSoma{:};
        h = histogram(x,'BinWidth', binWidth, 'BinLimits', binLimits, 'DisplayStyle','stairs','LineWidth',1,'EdgeColor',curColor);
        y_all(i,:) = h.BinCounts;
    end
    x = h.BinEdges;
    x = x(1:end-1);
    n_den = sum(~isnan(y_all),1);
    plot(x, nanmean(y_all,1), 'k', 'LineWidth', 2, 'LineStyle', '-');
    plot(x, nanmean(y_all,1) + nanstd(y_all,0,1) ./ sqrt(n_den), 'k','LineWidth',1, 'LineStyle', '--');
    plot(x, nanmean(y_all,1) - nanstd(y_all,0,1) ./ sqrt(n_den), 'k','LineWidth',1, 'LineStyle', '--');

    %cosmetics
    xlabel('Distance from IN cellbody (um)')
    ylabel('Number of tc synapses')
    %ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:10:50];
    ax.YAxis.TickValues = [0:10:50];
    ax.YAxis.Limits = [0,50];
    %ax.XAxis.MinorTick = 'on';
    %ax.XAxis.MinorTickValues = [0:5:20];
    ax.XAxis.TickValues = [0:50:300];
    ax.XAxis.Limits = [0,300];
    ax.LineWidth = 1;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    
    % plot all tc input
    ax = subplot(3,2,2);
    hold on
    x = horzcat(outTPooled.tcDistFromSoma{:});
    h1 = histogram(x,'BinWidth', binWidth, 'BinLimits', binLimits,'DisplayStyle','stairs','LineWidth',1,'EdgeColor','k','Normalization','probability')
    x = horzcat(outTPooled.nonTcDistFromSoma{:});
    h2 = histogram(x,'BinWidth', binWidth, 'BinLimits', binLimits,'DisplayStyle','stairs','LineWidth',1,'EdgeColor',[0.5,0.5,0.5],'Normalization','probability')    

    h3Data = h1.Values ./ sum([h1.Values;h2.Values]); 
    bar(h1.BinEdges(1:end-1)+h1.BinWidth/2,h3Data,'FaceColor','none','EdgeColor','cyan')

    %cosmetics
    xlabel('Distance from IN cellbody (um) (pooled)')
    ylabel(sprintf('Number of synapses \n pooled over all INs'))
    ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:50:300];
    %ax.YAxis.TickValues = [0:100:300];
    %ax.YAxis.Limits = [0,300];
    %ax.XAxis.MinorTick = 'on';
    %ax.XAxis.MinorTickValues = [0:50:250];
    ax.XAxis.TickValues = [0:50:250];
    ax.XAxis.Limits = [0,250];
    ax.LineWidth = 1;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    legend({'tc','non-tc', 'tc/all'})

    % plot non tc input
    ax = subplot(3,2,3);
    hold on
    for i=1:height(outTPooled)
        curTable = outTPooled(i,:);
        curColor = somaColors{cellTypesAll(curTable.id)}(1:3);

        x = curTable.nonTcDistFromSoma{:};
        histogram(x,'BinWidth', binWidth, 'BinLimits', binLimits, 'DisplayStyle','stairs','LineWidth',1,'EdgeColor',curColor)
    end

    %cosmetics
    xlabel('Distance from IN cellbody (um)')
    ylabel('Number of non-tc synapses')
%{
    %ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:10:50];
    ax.YAxis.TickValues = [0:10:50];
    ax.YAxis.Limits = [0,50];
    %ax.XAxis.MinorTick = 'on';
    %ax.XAxis.MinorTickValues = [0:5:20];
    ax.XAxis.TickValues = [0:50:250];
    ax.XAxis.Limits = [0,250];
%}
    ax.LineWidth = 1;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    % plot all syn input
    ax = subplot(3,2,4);
    hold on
    for i=1:height(outTPooled)
        curTable = outTPooled(i,:);
        curColor = somaColors{cellTypesAll(curTable.id)}(1:3);

        x = horzcat(curTable.tcDistFromSoma{:}, curTable.nonTcDistFromSoma{:});
        histogram(x,'BinWidth', binWidth, 'BinLimits', binLimits, 'DisplayStyle','stairs','LineWidth',1,'EdgeColor',curColor)
    end

    %cosmetics
    xlabel('Distance from IN cellbody (um)')
    ylabel('Number of all synapses')
%{
    %ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:10:50];
    ax.YAxis.TickValues = [0:10:50];
    ax.YAxis.Limits = [0,50];
    %ax.XAxis.MinorTick = 'on';
    %ax.XAxis.MinorTickValues = [0:5:20];
    ax.XAxis.TickValues = [0:50:250];
    ax.XAxis.Limits = [0,250];
%}
    ax.LineWidth = 1;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    % plot tc / non-tc ratio
    ax = subplot(3,2,5);
    hold on
    y_all = []; tc_all = []; nontc_all = [];
    for i=1:height(outTPooled)
        curTable = outTPooled(i,:);
        curColor = somaColors{cellTypesAll(curTable.id)}(1:3);

        x = curTable.tcDistFromSoma{:};
        h = histogram(x,'BinWidth',binWidth, 'BinLimits', binLimits, ...
                    'DisplayStyle','stairs','LineWidth',1,'EdgeColor',curColor);
        tc = h.BinCounts;
        h.Visible = 'off';

        x = curTable.nonTcDistFromSoma{:};
        h = histogram(x,'BinWidth',10,'BinLimits', binLimits, ...
                    'DisplayStyle','stairs','LineWidth',1,'EdgeColor',curColor);
        nontc = h.BinCounts;
        h.Visible = 'off';

        y = tc ./ (tc+nontc); % fraction of tc in this bin
        x = h.BinEdges;
        x = x(1:end-1); assert(numel(x) == numel(y))
        stairs(x, y, 'color', curColor, 'LineWidth',1);

        % save
        y_all(i,:) = y; clear y
        tc_all(i,:) = tc; clear tc
        nontc_all(i,:) = nontc; clear nontc
    end

    % export
    out = struct;
    out.y_all = y_all; out.tc_all = tc_all; out.nontc_all = nontc_all;

    x = h.BinEdges;
    x = x(1:end-1);
    n_den = sum(~isnan(y_all),1);
    plot(x, nanmean(y_all,1), 'k', 'LineWidth', 2, 'LineStyle', '-');
    plot(x, nanmean(y_all,1) + nanstd(y_all,0,1) ./ sqrt(n_den), 'k','LineWidth',1, 'LineStyle', '--');
    plot(x, nanmean(y_all,1) - nanstd(y_all,0,1) ./ sqrt(n_den), 'k','LineWidth',1, 'LineStyle', '--');

    %cosmetics
    xlabel('Distance from IN cellbody (um)')
    ylabel('Fraction of tc synapses')
%{
    %ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:10:50];
    ax.YAxis.TickValues = [0:10:50];
    ax.YAxis.Limits = [0,50];
    %ax.XAxis.MinorTick = 'on';
    %ax.XAxis.MinorTickValues = [0:5:20];
    ax.XAxis.TickValues = [0:50:250];
    ax.XAxis.Limits = [0,250];
%}
    ax.LineWidth = 1;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

end

function outTPooled = addBarrelFractionData(outDir, outTPooled)
    koelblFile = fullfile(outDir,'outData/figures/koelbl_v04_07_2019_LM','barrelStateAxonFraction_koelblModified.mat');
    Util.log('Reading from: %s', koelblFile)
    m = load(koelblFile);
    axonBarrelRatio = m.plfi;
    axonHomeColRatio = m.plfiCol;
    axonIds = cellfun(@(x) str2num(x), m.axonIds); % IN IDs

    % do LM space barrel intra/outer check for dendrite pathlength
    koelblFile = fullfile(outDir,'outData/figures/koelbl_v04_07_2019_LM','barrelStateDendFraction_koelblModified.mat');
    m = load(koelblFile);
    dendBarrelRatio =  m.plfi;
    dendIds = cellfun(@(x) str2num(x), m.dendIds); % IN IDs

    % append to outTPooled
    plfi = nan(numel(outTPooled.id),1);
    plfiCol = nan(numel(outTPooled.id),1);
    plDendInBarrel = nan(numel(outTPooled.id),1);
    for i = 1:numel(outTPooled.id)
        curId = outTPooled.id(i);
        curType = outTPooled.cellType{i};
        idxFound = ismember(axonIds, curId);
        if contains(curType,{'no-class','other-barrel'}) 
            % no axon
            plfi(i) = NaN;
            plfiCol(i) = NaN;
        else
            assert(sum(idxFound)==1)
            plfi(i) = axonBarrelRatio(idxFound);
            plfiCol(i) = axonHomeColRatio(idxFound);
        end

        % dendrites are more than axons
        idxFound = ismember(dendIds, curId);
        assert(sum(idxFound)==1)
        plDendInBarrel(i) = dendBarrelRatio(idxFound) * outTPooled.pathlength(i); 
    end
    
    outTPooled.plfi = plfi;
    outTPooled.plfiCol = plfiCol;
    outTPooled.plDendInBarrel = plDendInBarrel;
end

function rawData = doPlotting(rawData, cellTypesAll, outfile)
    markerSize = 75;
    fontSize = 5;

    % plot for pooled cells which have >70% dendrites annotated
    if contains(outfile, 'pooled')
        Util.log('Loading nml with axon and dend tracings...')
        setConfig
        nml = config.skelINCombined;
        skel = skeleton(nml);
        skel = skel.replaceComments('soma','cellbody','exact','complete');
        skel = skel.replaceComments('_in','cellbody','partial','complete');
        
        % remove extra nodes
        skel = skel.deleteNodesWithComment('-','exact');
        skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019

        % Keep those with >70% pl of parent dendrites
        idxKeep = false(height(rawData),1);
        somaLocs = nan(height(rawData),3);
        for i=1:height(rawData)
            curId = rawData(i,:).id;
            curPl = rawData(i,:).pathlength;
            idxPlot = find(contains(skel.names, sprintf('cell i%02d', curId)));
            assert(numel(idxPlot) == 1)
            plPlot = skel.pathLength(idxPlot) ./ 1e3; % um

            if curPl >= 0.70*plPlot
                idxKeep(i) = true;
            end

            % get soma location
%            curSomaLoc = skel.getNodesWithComment('cellbody',find(idxPlot),'exact',false);
            curSomaLoc = Util.getNodeCoordsWithComment(skel,'cellbody','exact',idxPlot);
            if isempty(curSomaLoc)
                error(sprintf('Soma node not found %d', curId))
            end
            somaLocs(i,:) = curSomaLoc;
        end
        % append to table
        rawData.somaLocs = somaLocs;    

        Util.log('Keeping %d cells out of %d with >70%% dendrite covered', sum(idxKeep), numel(idxKeep))
        sprintf('Removed:')
        disp(rawData(~idxKeep,:))
        rawData = rawData(idxKeep,:);
    end
    fig = figure;
    fig.Color = 'white';
    
    ax = subplot(5,2,1);
    hold on
    
    idxWB = contains(rawData.cellType, 'WBIN');
    idxLB = contains(rawData.cellType, 'LBIN');
    idxL4 = contains(rawData.cellType, 'L4') & ~contains(rawData.cellType, 'non-L4');
    idxNGFC = contains(rawData.cellType, 'NGFC');
    idxInvPyr = contains(rawData.cellType, 'Inv-pyr');
    idxNonL4 = contains(rawData.cellType, 'non-L4');
    idxNC = contains(rawData.cellType, {'no-class'});

    d = rawData.fractionTC;
    x = [d(idxWB); d(idxLB); d(idxL4); d(idxNGFC); d(idxInvPyr); d(idxNonL4); d(idxNC)];
    
    d = rawData.cellType;
    g = [d(idxWB); d(idxLB); d(idxL4); d(idxNGFC); d(idxInvPyr); d(idxNonL4); d(idxNC)];
    
    curIdxWB = 1:sum(idxWB);
    curIdxLB = curIdxWB(end) + (1:sum(idxLB));
    curIdxL4 = curIdxLB(end) + (1:sum(idxL4));
    curIdxNGFC = curIdxL4(end) + (1:sum(idxNGFC));
    curIdxInvPyr = curIdxNGFC(end) + (1:sum(idxInvPyr));
    curIdxNonL4 = curIdxInvPyr(end) + (1:sum(idxNonL4));
    curIdxNC = curIdxNonL4(end) + (1:sum(idxNC));

    rng(0)
    colors = Util.getSomaColors;
    colors = cell2mat(colors');
    
    startS = 1; widthS = 2;
    [pos, posS] = Util.getBoxPlotPos(numel(unique(g)),startS);
    scatter(widthS.*rand(numel(curIdxWB),1)+posS(1), x(curIdxWB),...
        markerSize, colors(1,1:3),'o');
    scatter(widthS.*rand(numel(curIdxLB),1)+posS(2), x(curIdxLB),...
        markerSize, colors(2,1:3),'o');
    scatter(widthS.*rand(numel(curIdxL4),1)+posS(3), x(curIdxL4),...
        markerSize, colors(3,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNGFC),1)+posS(4), x(curIdxNGFC),...
        markerSize, colors(4,1:3),'o');
    scatter(widthS.*rand(numel(curIdxInvPyr),1)+posS(5), x(curIdxInvPyr),...
        markerSize, colors(5,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNonL4),1)+posS(6), x(curIdxNonL4),...
        markerSize, colors(7,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNC),1)+posS(7), x(curIdxNC),...
        markerSize, colors(9,1:3),'o');

    h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
    set(h,{'linew'},{2});
    
    %cosmetics
    ylabel('Fraction of TC synapses')
    ax.YAxis.TickValues = [0:0.05:0.1];
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.01:0.1];
    ax.YAxis.Limits = [0,0.1];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    
    % box plot: fraction of tc identified with axons for clustering
    ax = subplot(5,2,2);
    hold on
    
    idxWB = contains(rawData.cellType, 'WBIN');
    idxLB = contains(rawData.cellType, 'LBIN');
    idxL4 = contains(rawData.cellType, 'L4') & ~contains(rawData.cellType, 'non-L4');
    idxNGFC = contains(rawData.cellType, 'NGFC');
    idxInvPyr = contains(rawData.cellType, 'Inv-pyr');
    idxNonL4 = contains(rawData.cellType, 'non-L4');
    idxNC = contains(rawData.cellType, {'no-class'});
    
    d = rawData.fractionTCWithAxons;
    x = [d(idxWB); d(idxLB); d(idxL4); d(idxNGFC); d(idxInvPyr); d(idxNonL4); d(idxNC)];
    
    d = rawData.cellType;
    g = [d(idxWB); d(idxLB); d(idxL4); d(idxNGFC); d(idxInvPyr); d(idxNonL4); d(idxNC)];
    
    curIdxWB = 1:sum(idxWB);
    curIdxLB = curIdxWB(end) + (1:sum(idxLB));
    curIdxL4 = curIdxLB(end) + (1:sum(idxL4));
    curIdxNGFC = curIdxL4(end) + (1:sum(idxNGFC));
    curIdxInvPyr = curIdxNGFC(end) + (1:sum(idxInvPyr));
    curIdxNonL4 = curIdxInvPyr(end) + (1:sum(idxNonL4));
    curIdxNC = curIdxNonL4(end) + (1:sum(idxNC));
    
    rng(0)
    colors = Util.getSomaColors;
    colors = cell2mat(colors');
    
    startS = 1; widthS = 2;
    [pos, posS] = Util.getBoxPlotPos(numel(unique(g)),startS);
    scatter(widthS.*rand(numel(curIdxWB),1)+posS(1), x(curIdxWB),...
        markerSize, colors(1,1:3),'o');
    scatter(widthS.*rand(numel(curIdxLB),1)+posS(2), x(curIdxLB),...
        markerSize, colors(2,1:3),'o');
    scatter(widthS.*rand(numel(curIdxL4),1)+posS(3), x(curIdxL4),...
        markerSize, colors(3,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNGFC),1)+posS(4), x(curIdxNGFC),...
        markerSize, colors(4,1:3),'o');
    scatter(widthS.*rand(numel(curIdxInvPyr),1)+posS(5), x(curIdxInvPyr),...
        markerSize, colors(5,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNonL4),1)+posS(6), x(curIdxNonL4),...
        markerSize, colors(7,1:3),'o');
    scatter(widthS.*rand(numel(curIdxNC),1)+posS(7), x(curIdxNC),...
        markerSize, colors(9,1:3),'o');
    
    h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
    set(h,{'linew'},{2});
    
    %cosmetics
    ylabel(sprintf('Fraction of TC synapses \n accounted for by TC axons'))
    ax.YAxis.TickValues = [0:0.1:0.4];
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.05:4]; 
    ax.YAxis.Limits = [0,0.4];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    
    % scatter plot num TC vs num syn
    txt = arrayfun(@(x) sprintf('%d',x),rawData.id, 'uni',0); % cell Ids labels
    ax = subplot(5,2,3);
    hold on
    x1 = rawData.numSyn;
    x2 = rawData.numTCSyn;

    scatter(x1(idxWB), x2(idxWB),...
        markerSize, colors(1,1:3),'o');
    scatter(x1(idxLB), x2(idxLB),...
        markerSize, colors(2,1:3),'o');
    scatter(x1(idxL4), x2(idxL4),...
        markerSize, colors(3,1:3),'o');
    scatter(x1(idxNGFC), x2(idxNGFC),...
        markerSize, colors(4,1:3),'o');
    scatter(x1(idxInvPyr), x2(idxInvPyr),...
        markerSize, colors(5,1:3),'o');
    scatter(x1(idxNonL4), x2(idxNonL4),...
        markerSize, colors(7,1:3),'o');
    scatter(x1(idxNC), x2(idxNC),...
        markerSize, colors(9,1:3),'o');

    text(x1(idxWB), x2(idxWB), txt(idxWB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxLB), x2(idxLB), txt(idxLB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxL4), x2(idxL4), txt(idxL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNGFC), x2(idxNGFC), txt(idxNGFC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxInvPyr), x2(idxInvPyr), txt(idxInvPyr), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNonL4), x2(idxNonL4), txt(idxNonL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNC), x2(idxNC), txt(idxNC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

    %cosmetics
    ylabel('Number of TC synapses')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:50:300];
    ax.YAxis.TickValues = [0:100:300];
    ax.YAxis.Limits = [0,300];
    xlabel('Number of synapses');
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:1000:7000]; % Manually
    ax.XAxis.MinorTickValues = [0:500:7000];
    ax.XAxis.Limits = [0,7000];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    
    % scatter plot
    ax = subplot(5,2,4);
    hold on
    x1 = rawData.synDensity;
    x2 = rawData.tcSynDensity;
    
    scatter(x1(idxWB), x2(idxWB),...
        markerSize, colors(1,1:3),'o');
    scatter(x1(idxLB), x2(idxLB),...
        markerSize, colors(2,1:3),'o');
    scatter(x1(idxL4), x2(idxL4),...
        markerSize, colors(3,1:3),'o');
    scatter(x1(idxNGFC), x2(idxNGFC),...
        markerSize, colors(4,1:3),'o');
    scatter(x1(idxInvPyr), x2(idxInvPyr),...
        markerSize, colors(5,1:3),'o');
    scatter(x1(idxNonL4), x2(idxNonL4),...
        markerSize, colors(7,1:3),'o');
    scatter(x1(idxNC), x2(idxNC),...
        markerSize, colors(9,1:3),'o');

    text(x1(idxWB), x2(idxWB), txt(idxWB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxLB), x2(idxLB), txt(idxLB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxL4), x2(idxL4), txt(idxL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNGFC), x2(idxNGFC), txt(idxNGFC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxInvPyr), x2(idxInvPyr), txt(idxInvPyr), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNonL4), x2(idxNonL4), txt(idxNonL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNC), x2(idxNC), txt(idxNC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    
    %cosmetics
    ylabel('TC syn density\n (per um)')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.TickValues = [0:0.1:0.2];
    ax.YAxis.Limits = [0,0.2];
    
    xlabel('Syn density\n (per um)');
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:0.5:3]; % Manually
    %ax.XAxis.MinorTickValues = [0:01:1];
    ax.XAxis.Limits = [0,3];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    if contains(outfile, 'pooled')
    % scatter plot
    ax = subplot(5,2,5);
    hold on
    x1 = mean([rawData.plfi, rawData.plfiCol], 2);
    x2 = rawData.numTCSyn;

    scatter(x1(idxWB), x2(idxWB),...
        markerSize, colors(1,1:3),'o');
    scatter(x1(idxLB), x2(idxLB),...
        markerSize, colors(2,1:3),'o');
    scatter(x1(idxL4), x2(idxL4),...
        markerSize, colors(3,1:3),'o');
    scatter(x1(idxNGFC), x2(idxNGFC),...
        markerSize, colors(4,1:3),'o');
    scatter(x1(idxInvPyr), x2(idxInvPyr),...
        markerSize, colors(5,1:3),'o');
    scatter(x1(idxNonL4), x2(idxNonL4),...
        markerSize, colors(7,1:3),'o');
    scatter(x1(idxNC), x2(idxNC),...
        markerSize, colors(9,1:3),'o');

    text(x1(idxWB), x2(idxWB), txt(idxWB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxLB), x2(idxLB), txt(idxLB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxL4), x2(idxL4), txt(idxL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNGFC), x2(idxNGFC), txt(idxNGFC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxInvPyr), x2(idxInvPyr), txt(idxInvPyr), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNonL4), x2(idxNonL4), txt(idxNonL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNC), x2(idxNC), txt(idxNC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

    %cosmetics
    ylabel('Number of TC syn')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:50:300];
    ax.YAxis.TickValues = [0:100:300];
    ax.YAxis.Limits = [0,300];
    xlabel(sprintf('Avg. axonal pathlength \n fraction inside barrel and L4'));
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:0.5:1]; % Manually
    ax.XAxis.MinorTickValues = [0:0.1:1];
    ax.XAxis.Limits = [0,1];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)    

    % 3d scatter plot: PL inside barrel, numTCSyn,, fractionTC
    ax = subplot(5,2,6);
    hold on
    x1 = rawData.plDendInBarrel;
    x2 = rawData.fractionTC;
    x3 = rawData.numTCSyn;

    scatter3(x1(idxWB), x2(idxWB), x3(idxWB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(1,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxLB), x2(idxLB), x3(idxLB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(2,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxL4), x2(idxL4), x3(idxL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(3,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNGFC), x2(idxNGFC), x3(idxNGFC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(4,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxInvPyr), x2(idxInvPyr), x3(idxInvPyr), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(5,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNonL4), x2(idxNonL4), x3(idxNonL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(7,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNC), x2(idxNC), x3(idxNC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(9,1:3), 'MarkerFaceColor','none');

    text(x1(idxWB), x2(idxWB), x3(idxWB), txt(idxWB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxLB), x2(idxLB), x3(idxLB), txt(idxLB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxL4), x2(idxL4), x3(idxL4), txt(idxL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNGFC), x2(idxNGFC), x3(idxNGFC), txt(idxNGFC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxInvPyr), x2(idxInvPyr), x3(idxInvPyr), txt(idxInvPyr), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNonL4), x2(idxNonL4), x3(idxNonL4), txt(idxNonL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNC), x2(idxNC), x3(idxNC), txt(idxNC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

    %cosmetics
    xlabel(sprintf('Dendrite pathlength\n inside barrel (um)'));
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:500:2500]; % Manually
    ax.XAxis.MinorTickValues = [0:500:2500];
    ax.XAxis.Limits = [0,2500];

    ylabel('Fraction of TC syn')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.01:0.1];
    ax.YAxis.TickValues = [0:0.05:0.1];
    ax.YAxis.Limits = [0,0.1];

    zlabel('Number of TC syn');
    ax.ZAxis.MinorTick = 'on';
    ax.ZAxis.TickValues = [0:100:300]; % Manually
    ax.ZAxis.MinorTickValues = [0:50:300];
    ax.ZAxis.Limits = [0,300];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    %view(3) % 3d
    view([0,1]); ax.YAxis.Color = 'none';

    % 3d scatter plot: PL inside barrel, numTCSyn,, fractionTC
    ax = subplot(5,2,7);
    hold on
    x1 = rawData.plDendInBarrel;
    x2 = rawData.fractionTC;
    x3 = rawData.numTCSyn;

    scatter3(x1(idxWB), x2(idxWB), x3(idxWB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(1,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxLB), x2(idxLB), x3(idxLB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(2,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxL4), x2(idxL4), x3(idxL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(3,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNGFC), x2(idxNGFC), x3(idxNGFC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(4,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxInvPyr), x2(idxInvPyr), x3(idxInvPyr), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(5,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNonL4), x2(idxNonL4), x3(idxNonL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(7,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNC), x2(idxNC), x3(idxNC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(9,1:3), 'MarkerFaceColor','none');

    text(x1(idxWB), x2(idxWB), x3(idxWB), txt(idxWB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxLB), x2(idxLB), x3(idxLB), txt(idxLB), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxL4), x2(idxL4), x3(idxL4), txt(idxL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNGFC), x2(idxNGFC), x3(idxNGFC), txt(idxNGFC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxInvPyr), x2(idxInvPyr), x3(idxInvPyr), txt(idxInvPyr), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNonL4), x2(idxNonL4), x3(idxNonL4), txt(idxNonL4), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    text(x1(idxNC), x2(idxNC), x3(idxNC), txt(idxNC), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

    %cosmetics
    xlabel(sprintf('Dendrite pathlength\n inside barrel (um)'));
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:500:2500]; % Manually
    ax.XAxis.MinorTickValues = [0:500:2500];
    ax.XAxis.Limits = [0,2500];

    ylabel('Fraction of TC syn')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.01:0.1];
    ax.YAxis.TickValues = [0:0.05:0.1];
    ax.YAxis.Limits = [0,0.1];

    zlabel('Number of TC syn');
    ax.ZAxis.MinorTick = 'on';
    ax.ZAxis.TickValues = [0:100:300]; % Manually
    ax.ZAxis.MinorTickValues = [0:50:300];
    ax.ZAxis.Limits = [0,300];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    view([0,90]); ax.ZAxis.Color = 'none';

    % hitogram plot without colors
    ax = subplot(5,2,8);
    binWidth = 0.01;
    x = rawData.fractionTC;
    h = histogram(x,'BinWidth', binWidth, 'DisplayStyle','stairs','LineWidth',2,'EdgeColor','k');
    %cosmetics
    xlabel('Fraction of tc syn')
    ylabel('Number of INs')
    %ax.YAxis.MinorTick = 'on';
    %ax.YAxis.MinorTickValues = [0:10:50];
    %ax.YAxis.TickValues = [0:10:50];
    %ax.YAxis.Limits = [0,50];
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = [0:0.01:0.1];
    ax.XAxis.TickValues = [0:0.05:0.1];
    ax.XAxis.Limits = [0,0.1];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    % fractionTC vs somaTargetFraction
    ax = subplot(5,2,9);
    hold on
    [somaSynFrac, tempAxonIds] = Figures.Fig2.IN.getSomaSynFraction;
    idxKeep = ismember(rawData.id, tempAxonIds);
    assert(isequal(tempAxonIds, rawData.id(idxKeep)))
    c = colors(cellTypesAll(rawData.id(idxKeep),:),1:3); 
    x = somaSynFrac;
    y = rawData.fractionTC(idxKeep);
    
    scatter(x, y,...
        markerSize, c,'o');
    text(x, y, txt(idxKeep), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
    %cosmetics
    ylabel('Fraction of tc syn')
    xlabel('Soma target fraction')
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = [0:0.1:0.5];
    ax.XAxis.TickValues = [0:0.1:0.5];
    ax.XAxis.Limits = [0,0.5];
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.01:0.1];
    ax.YAxis.TickValues = [0:0.05:0.1];
    ax.YAxis.Limits = [0,0.1];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)

    end
    % export
    export_fig(strcat(outfile, '.png'), '-q101', '-nocrop', '-transparent', '-m8');
    close all

    if contains(outfile, 'pooled')
    % export 3d plot as fig
    fig = figure;
    fig.Color = 'white';
    ax = axes(fig);
    hold on
    x1 = rawData.plDendInBarrel;
    x2 = rawData.fractionTC;
    x3 = rawData.numTCSyn;

    scatter3(x1(idxWB), x2(idxWB), x3(idxWB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(1,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxLB), x2(idxLB), x3(idxLB), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(2,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxL4), x2(idxL4), x3(idxL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(3,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNGFC), x2(idxNGFC), x3(idxNGFC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(4,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxInvPyr), x2(idxInvPyr), x3(idxInvPyr), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(5,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNonL4), x2(idxNonL4), x3(idxNonL4), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(7,1:3), 'MarkerFaceColor','none');
    scatter3(x1(idxNC), x2(idxNC), x3(idxNC), ...
        markerSize, 'filled', 'MarkerEdgeColor', colors(9,1:3), 'MarkerFaceColor','none');

    view(3)
    %cosmetics
    xlabel(sprintf('Dendrite pathlength\n inside barrel (um)'));
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.TickValues = [0:1000:2500]; % Manually
    ax.XAxis.MinorTickValues = [0:500:2500];
    ax.XAxis.Limits = [0,2500];

    ylabel('Fraction of TC synapses')
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = [0:0.1:1];
    ax.YAxis.TickValues = [0:0.5:1];
    ax.YAxis.Limits = [0,1];

    zlabel('Number of TC synapses');
    ax.ZAxis.MinorTick = 'on';
    ax.ZAxis.TickValues = [0:100:300]; % Manually
    ax.ZAxis.MinorTickValues = [0:50:300];
    ax.ZAxis.Limits = [0,300];
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    set(gca,'FontSize',fontSize)
    saveas(gcf, strcat(outfile, '_3d.fig'))
    close all
    end
end
