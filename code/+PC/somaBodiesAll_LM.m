if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'),'tform');
tform = m.tform;

Util.log('load soma bodies inside barrel')
somaNml = fullfile(config.outDir,'data/skelSomaAll.nml'); % all somas in MT4 EM
somaSkel = skeleton(somaNml); % in EM scale
nodesEM = somaSkel.getNodes;
Util.log('apply EM to LM tform')
nodesLM = TFM.applyTformToVertices(tform, nodesEM, 'forward'); %EM to LM dataset
points = nodesLM;
scaleEM = [11.24,11.24,30];
scaleLM = [1172 1172 1000];
Util.log('point cloud');
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(points,'','',repelem([0,0,1,1],size(points,1),1)); % blue color
skel.write(fullfile(outDir,'outData/pointClouds/skelSomaAll_LM.nml'));
Util.log('mesh from points')
pointsDT = delaunayTriangulation(points);
edgesMesh = edges(pointsDT);
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addTree('barrel',pointsDT.Points,edgesMesh,[0,0,1,1]); % blue color
skel.write(fullfile(outDir,['outData/pointClouds/skelSomaAll_LM_mesh.nml']));

