% Description:
% find the soma nodes that are outside the barrel
% Date: vMay. dont use the EM mode, LM mode is better
% apply tilt correction to EM nodes and check Inside with LM Iso

thisVersion = 'vMay';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;
mkdir(fullfile(config.outDir,'outData/pointClouds/'));

m=load(fullfile(outDir,'barrelStateTformIso_vOct.mat'));
iso = m.iso;
isoT = m.isoT;
tform = m.tform;
scaleEM = m.scaleEM;
scaleLM = m.scaleLM;
DT = m.DT;
DTLM = m.DTLM;

Util.log('Loading nml with soma nodes in one tree...')
somaNml = fullfile(config.outDir,'data/cell_type_0426_vSept.nml');
skelSoma = skeleton(somaNml);
c = skelSoma.getAllComments;
qPts = skelSoma.nodes{1}(:,1:3);
% tilt correction hack
qPtsTemp = qPts;
qPtsTemp(:,2) = round(qPtsTemp(:,2)-.449*qPtsTemp(:,3));
qPtsLM = TFM.applyTformToVertices(tform, qPtsTemp, 'forward'); % EM to LM

Util.log('Finding skel nodes inside the iso...')
tic;
% Method: pointsLocation. Fast and accurate
IN = ~isnan(pointLocation(DTLM,qPtsLM));
toc;
Util.log(['Found ' num2str(sum(IN)) ' somas inside barrel!'])
Util.log('Delete nodes that are not inside')
skelIn = skeleton();
skelIn = skelIn.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelIn = skelIn.addNodesAsTrees(qPts(~IN,:),'',c(~IN));
skelIn.write(fullfile(config.outDir,['outData/pointClouds/skelSomaOut_' thisVersion '.nml']));
%{
skelInSingle = skeleton();
skelInSingle = skelInSingle.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelInSingle = skelInSingle.addTree('cell_type_L4_wo_uc_2203_explorative_2017-03-20_yunfeng_hua_807',...
                                qPts(IN,:),'','','',c(IN));
skelInSingle.write(fullfile(config.outDir,['outData/pointClouds/skelSomaInSingle_' thisVersion '.nml']));

Util.log('Delete nodes that are inside')
tic;
skelOutSingle = skeleton();
skelOutSingle = skelOutSingle.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelOutSingle = skelOutSingle.addTree('cell_type_L4_wo_uc_2203_explorative_2017-03-20_yunfeng_hua_807',...
                                qPts(~IN,:),'','','',c(~IN));
skelOutSingle.write(fullfile(config.outDir,['outData/pointClouds/skelSomaOutSingle_' thisVersion '.nml']));
toc;
%}
% write IN somas to LM space
points = qPtsLM(~IN,:);
Util.log('point cloud');
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(points,'','',repelem([0,0,1,1],size(points,1),1)); % blue color
skel.write(fullfile(outDir,['outData/pointClouds/skelSomaOut_LM_' thisVersion '.nml']));
Util.log('mesh from points')
pointsDT = delaunayTriangulation(points);
edgesMesh = edges(pointsDT);
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addTree('barrel',pointsDT.Points,edgesMesh,[0,0,1,1]); % blue color
skel.write(fullfile(outDir,['outData/pointClouds/skelSomaOut_LM_mesh_' thisVersion '.nml']));

Util.log('Saving data')
save(fullfile(outDir,['barrelStateSomaFraction_' thisVersion '.mat']),...
    'skelSoma','IN','isoT','iso','DT');

%% for grouping all types colored
nodesEM = skelSoma.getNodes;
skelPC = skeleton();
skelPC = skelPC.setParams('YH_st126_MT4_updated_1708',[12,12,30],[1,1,1]);
%skelPC = skelPC.addNodesAsTrees(nodesEM(IN,:),'',c(IN));

% set colors: ss: spiny stellate-b, sp: pyramid-m
ss = cellfun(@(x) any(regexpi(x,'ss')),c);
sp = cellfun(@(x) any(regexpi(x,'sp')),c);
in = cellfun(@(x) any(regexpi(x,'in')),c);
as = cellfun(@(x) any(regexpi(x,'as')),c);

skelPC = addSomasAsGroup(skelPC, nodesEM(~IN & ss,:),'ss',[0,0,1,1.0000], c(~IN & ss));
skelPC = addSomasAsGroup(skelPC, nodesEM(~IN & sp,:),'sp',[1,0,1,1.0000], c(~IN & sp));
skelPC = addSomasAsGroup(skelPC, nodesEM(~IN & in,:),'in',[0.5,0.5,0.5,1.0000], c(~IN & in));
skelPC = addSomasAsGroup(skelPC, nodesEM(~IN & as,:),'as',[0,1,0,1.0000], c(~IN & as));
skelPC.write(fullfile(config.outDir,['outData/pointClouds/skelSomaOutGroups_EM_' thisVersion '.nml']));

%%
Util.log('apply EM to LM tform and repeat all')
points =  qPtsLM;
Util.log('point cloud');
skelPC = skeleton();
skelPC  = skelPC.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skelPC = addSomasAsGroup(skelPC, points(~IN & ss,:),'ss',[0,0,1,1.0000], c(~IN & ss));
skelPC = addSomasAsGroup(skelPC, points(~IN & sp,:),'sp',[1,0,1,1.0000], c(~IN & sp));
skelPC = addSomasAsGroup(skelPC, points(~IN & in,:),'in',[0.5,0.5,0.5,1.0000], c(~IN & in));
skelPC = addSomasAsGroup(skelPC, points(~IN & as,:),'as',[0,1,0,1.0000], c(~IN & as));
skelPC.write(fullfile(outDir,['outData/pointClouds/skelSomaOutGroups_LM_' thisVersion '.nml']));

function skel = addSomasAsGroup(skel, points, groupName, color , comments)
    [skel, id] = skel.addGroup(groupName);
    skel = skel.addNodesAsTrees(points,'',comments,repelem(color, size(points,1),1));
    treesNew = skel.numTrees - size(points,1) + 1 : skel.numTrees;
    skel = skel.addTreesToGroup(treesNew , id);
end

