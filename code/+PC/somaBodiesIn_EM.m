% Description:
% find the soma nodes that are inside the barrel
% Date: vOct
thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;
mkdir(fullfile(config.outDir,'outData/pointClouds/'));

m=load(fullfile(outDir,'barrelStateTformIso_vOct.mat')); % correct iso that gets #533
iso = m.iso;
isoT = m.isoT;
tform = m.tform;
scaleEM = m.scaleEM;
scaleLM = m.scaleLM;
DT = m.DT;

Util.log('Loading nml with soma nodes in one tree...')
somaNml = fullfile(config.outDir,'data/cell_type_0426_vSept.nml');
skelSoma = skeleton(somaNml);
c = skelSoma.getAllComments;
qPts = skelSoma.nodes{1}(:,1:3);

Util.log('Finding skel nodes inside the iso...')
tic;
% Method: pointsLocation. Fast and accurate
IN = ~isnan(pointLocation(DT,qPts));
toc;
Util.log(['Found ' num2str(sum(IN)) ' somas inside barrel!'])
Util.log('Delete nodes that are not inside')
skelIn = skeleton();
skelIn = skelIn.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelIn = skelIn.addNodesAsTrees(qPts(IN,:),'',c(IN));
skelIn.write(fullfile(config.outDir,['outData/pointClouds/skelSomaIn_' thisVersion '.nml']));

skelInSingle = skeleton();
skelInSingle = skelInSingle.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelInSingle = skelInSingle.addTree('cell_type_L4_wo_uc_2203_explorative_2017-03-20_yunfeng_hua_807',...
                                qPts(IN,:),'','','',c(IN));
skelInSingle.write(fullfile(config.outDir,['outData/pointClouds/skelSomaInSingle_' thisVersion '.nml']));

Util.log('Delete nodes that are inside')
tic;
skelOutSingle = skeleton();
skelOutSingle = skelOutSingle.setParams('YH_st126_MT4_updated_1708',scaleEM,[1,1,1]);
skelOutSingle = skelOutSingle.addTree('cell_type_L4_wo_uc_2203_explorative_2017-03-20_yunfeng_hua_807',...
                                qPts(~IN,:),'','','',c(~IN));
skelOutSingle.write(fullfile(config.outDir,['outData/pointClouds/skelSomaOutSingle_' thisVersion '.nml']));
toc;

Util.log('Saving data')
save(fullfile(outDir,['barrelStateSomaFraction_' thisVersion '.mat']),...
    'skelSoma','IN','skelInSingle','skelOutSingle','isoT','iso','DT');

% added from another separate function here
Util.log('load soma bodies inside barrel')
nodesEM = skelIn.getNodes;
Util.log('apply EM to LM tform')
nodesLM = TFM.applyTformToVertices(tform, nodesEM, 'forward'); %EM to LM dataset
points = nodesLM;
scaleEM = [11.24,11.24,30];
scaleLM = [1172 1172 1000];
Util.log('point cloud');
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(points,'','',repelem([0,0,1,1],size(points,1),1)); % blue color
skel.write(fullfile(outDir,['outData/pointClouds/skelSomaIn_LM_' thisVersion '.nml']));
Util.log('mesh from points')
pointsDT = delaunayTriangulation(points);
edgesMesh = edges(pointsDT);
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addTree('barrel',pointsDT.Points,edgesMesh,[0,0,1,1]); % blue color
skel.write(fullfile(outDir,['outData/pointClouds/skelSomaIn_LM_mesh_' thisVersion '.nml']));

% added from another separate function here for grouping all types colored
nodesEM = skelSoma.getNodes;
skelPC = skeleton();
skelPC = skelPC.setParams('YH_st126_MT4_updated_1708',[12,12,30],[1,1,1]);
skelPC = skelPC.addNodesAsTrees(nodesEM(IN,:),'',c(IN));

% set colors: ss: spiny stellate-b, sp: pyramid-m
ss = cellfun(@(x) any(regexpi(x,'ss')),c(IN));
sp = cellfun(@(x) any(regexpi(x,'sp')),c(IN));
in = cellfun(@(x) any(regexpi(x,'in')),c(IN));
as = cellfun(@(x) any(regexpi(x,'as')),c(IN));

%b=[0,0,1], m=[1,0,1]
skelPC.colors(ss) = repelem({[0,0,1,1.0000]},sum(ss),1);
skelPC.colors(sp) = repelem({[1,0,1,1.0000]},sum(sp),1);
skelPC.colors(in) = repelem({[0.5,0.5,0.5,1.0000]},sum(in),1);
skelPC.colors(as) = repelem({[0,1,0,1.0000]},sum(as),1);

for i =1:skelPC.numTrees
    skelPC = skelPC.setComments(i,1,'somaPoint');
end
colors = vertcat(skelPC.colors{:});
colors = colors(:,1:3);
skelPC.write(fullfile(config.outDir,['outData/pointClouds/skelSomaInGroups_EM_' thisVersion '.nml']));

%%
Util.log('apply EM to LM tform and repeat all')
nodesLM = TFM.applyTformToVertices(tform, nodesEM(IN,:), 'forward'); %EM to LM dataset
points = nodesLM;
scaleEM = [12,12,30];
scaleLM = [1172 1172 1000];
Util.log('point cloud');
skelPC = skeleton();
skelPC  = skelPC.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skelPC = skelPC.addNodesAsTrees(points,'',c); % blue color
%b=[0,0,1], m=[1,0,1]
skelPC.colors(ss) = repelem({[0,0,1,1.0000]},sum(ss),1);
skelPC.colors(sp) = repelem({[1,0,1,1.0000]},sum(sp),1);
skelPC.colors(in) = repelem({[0.5,0.5,0.5,1.0000]},sum(in),1);
skelPC.colors(as) = repelem({[0,1,0,1.0000]},sum(as),1);
skelPC.write(fullfile(outDir,['outData/pointClouds/skelSomaInGroups_LM_' thisVersion '.nml']));
