% Description:
% find the exc soma nodes that are inside the baREL
setConfig;

Util.log('Parsing cell type skeleton...')
skelCellType = skeleton(config.cellTypesL4Fixed);
skelCellType = skelCellType.extractLargestTree;
c = skelCellType.getAllComments;

allClassNames = {'s','p'};
for idx = 1:numel(allClassNames)
    className = allClassNames{idx};
    Util.log(sprintf('Restricting to barrel for class: %s \n', className))
    switch className
        case 's'
            ids = cellfun(@(x) regexp(x,'s(?<id>\d+)','names') ,c,'uni',0);
            ids = cellfun(@(x) str2double(x.id), ids(~cellfun('isempty',ids)));
            count = max(ids);
            ids = sort(ids,'ascend');
    
            somaLocs = [];
            for i = 1:numel(ids)
                curId = ids(i);
                somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['s' num2str(curId,'%02d')] ,'exact');
            end
            comments = arrayfun(@(x) ['s' num2str(x,'%02d')],1:numel(ids), 'uni',0);
        case 'p'
            ids = cellfun(@(x) regexp(x,'p(?<id>\d+)','names') ,c,'uni',0);
            ids = cellfun(@(x) str2double(x.id), ids(~cellfun('isempty',ids)));
            count = max(ids);
            ids = sort(ids,'ascend');
    
            somaLocs = [];
            for i = 1:numel(ids)
                curId = ids(i);
                somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
            end
            comments = arrayfun(@(x) ['p' num2str(x,'%02d')],1:numel(ids), 'uni',0);
        case 'in'
    
    end
    [IN, somaLocsIn, somaLocsOut] = Tform.insideOutsideBarrel(somaLocs,'EM');
    
    % write skel
    skelIn = skeleton();
    skelIn = skelIn.setParams('YH_st126_MT4_updated_1708',config.scaleEM,[1,1,1]);
    skelIn = skelIn.addTree(sprintf('%s_exc_cells_in_barrel',className), somaLocs(IN,:), '','','',comments(IN));
    skelIn = skelIn.addTree(sprintf('%s_exc_cells_out_barrel',className), somaLocs(~IN,:), '','','',comments(~IN));
    skelIn.write(fullfile(config.outDir,'outData','pointClouds', sprintf('skel_%s_exc_cells_in_barrel.nml',className)));
end
