setConfig;

somaColor = {[230/255, 159/255, 0/255, 1],... % 1a orange
              [213/255, 94/255, 0/255, 1],... %1b vermillion
              [0.75, 0.75, 0, 1],... % 3 dark yellow
              [204/255, 121/255, 167/255, 1],... % 4 reddish purple
              [0.3010, 0.7450, 0.933,1],...
              [182/255, 219/255, 255/255 ,1]}; % 
                
Util.log('Loading PC from MTs')
skelMT4_EM = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAll_LM.nml']));% without glia but 300um
skelMT3_EM_with_glia = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAllMT3_LM.nml'])); % with glia
skelMT2_EM_with_glia = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAllMT2_LM.nml'])); % with glia

% new v nov 2020 without glia
skelMT4_EM_360 = skeleton(fullfile(outDir,'data','wholecells','somas_annotated','cell_counts_type_MT4.nml')); % without glia 360um
skelMT2_EM = skeleton(fullfile(outDir,'data','wholecells','somas_annotated','cell_counts_MT2.nml')); % without glia
skelMT3_EM = skeleton(fullfile(outDir,'data','wholecells','somas_annotated','cell_counts_MT3.nml')); % without glia

somaMT4_EM = skelMT4_EM.getNodes;
somaMT2_EM = skelMT2_EM.getNodes;
somaMT3_EM = skelMT3_EM.getNodes;

m=load(fullfile(outDir,'barrelStateTformIso_vOct.mat'),'DT'); % correct iso that gets #509
DT = m.DT; % corresponds to isoT EM space barrel

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'),'tform');
tformMT4 = m.tform;
m=load(fullfile(outDir,'barrelStateTformMT3_vOct.mat'),'tform');
tformMT3 = m.tform;
m=load(fullfile(outDir,'barrelStateTformMT2_vOct.mat'),'tform');
tformMT2 = m.tform;

%somaSkel = skeleton(fullfile(config.outDir,'data/skelSomaAll.nml'));
%somaMT4_EM = somaSkel.getNodes;
somaMT4_LM = TFM.applyTformToVertices(tformMT4, somaMT4_EM, 'forward'); %EM to LM dataset

%somaSkel = skeleton(fullfile(config.outDir,'data/skelSomaAllMT3.nml'));
%somaMT3_EM = somaSkel.getNodes;
somaMT3_LM = TFM.applyTformToVertices(tformMT3, somaMT3_EM, 'forward'); %EM to LM dataset

%somaSkel = skeleton(fullfile(config.outDir,'data/skelSomaAllMT2.nml'));
%somaMT2_EM = somaSkel.getNodes;
somaMT2_LM = TFM.applyTformToVertices(tformMT2, somaMT2_EM, 'forward'); %EM to LM dataset

Util.log('combined somas into LM');
scaleEM = [11.24,11.24,30];
scaleLM = [1172 1172 1000];
%{
%%
Util.log('point clouds...');
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = addSomasAsGroup(skel, somaMT4_LM,'MT4_LM', somaColor{1});
skel = addSomasAsGroup(skel, somaMT3_LM,'MT3_LM', somaColor{2});
skel = addSomasAsGroup(skel, somaMT2_LM,'MT2_LM', somaColor{3});
skel.write(fullfile(outDir,'outData/pointClouds/allMTSomas_LM.nml'));

Util.log('combined somas into MT4 space')
somaMT3InMT4_EM =  TFM.applyTformToVertices(tformMT4, somaMT3_LM, 'backward'); %LM to EM dataset
somaMT2InMT4_EM =  TFM.applyTformToVertices(tformMT4, somaMT2_LM, 'backward'); %LM to EM dataset

skel = skeleton();
skel  = skel.setParams('YH_st126_MT4_updated_1708',scaleEM,[1 1 1]);
skel = addSomasAsGroup(skel, somaMT4_EM,'MT4_EM', somaColor{1});
skel = addSomasAsGroup(skel, somaMT3InMT4_EM,'MT3_EM', somaColor{2});
skel = addSomasAsGroup(skel, somaMT2InMT4_EM,'MT2_EM', somaColor{3});
skel.write(fullfile(outDir,'outData/pointClouds/allMTSomas_EM.nml'));

Util.log('combined somas in MT4 space with INTRA/EXTRA barrel distinction')
skel = skeleton();
skel  = skel.setParams('YH_st126_MT4_updated_1708',scaleEM,[1 1 1]);
qPts = somaMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, qPts(IN,:),'MT4_IN_EM', somaColor{1});
skel = addSomasAsGroup(skel, qPts(~IN,:),'MT4_OUT_EM', somaColor{4});

qPts = somaMT3InMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, qPts(IN,:), 'MT3_IN_EM', somaColor{2});
skel = addSomasAsGroup(skel, qPts(~IN,:),'MT3_OUT_EM', somaColor{5});

qPts = somaMT2InMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, qPts(IN,:),'MT2_IN_EM', somaColor{3});
skel = addSomasAsGroup(skel, qPts(~IN,:),'MT2_OUT_EM', somaColor{6});

skel.write(fullfile(outDir,'outData/pointClouds/allMTSomasIntra_EM.nml'));

tform = tformMT4;
skel = skeleton;
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
qPts = somaMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(IN,:)),'MT4_IN_LM', somaColor{1});
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(~IN,:)),'MT4_OUT_LM', somaColor{4});
qPts = somaMT3InMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(IN,:)), 'MT3_IN_LM', somaColor{2});
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(~IN,:)),'MT3_OUT_LM', somaColor{5});
qPts = somaMT2InMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(IN,:)),'MT2_IN_LM', somaColor{3});
skel = addSomasAsGroup(skel, TFM.applyTformToVertices(tform,qPts(~IN,:)),'MT2_OUT_LM', somaColor{6});

skel.write(fullfile(outDir,'outData/pointClouds/allMTSomasIntra_LM.nml'));
%}
%% counts
sprintf('Orignal: MT4: %d, MT2: %d, MT3: %d. Total: %d', size(somaMT4_LM,1), size(somaMT2_LM,1), ...
    size(somaMT3_LM,1), sum([size(somaMT4_LM,1),size(somaMT2_LM,1),size(somaMT3_LM,1)]))
voxelSize = scaleLM;
distThr = 5; %um
mt4 = voxelSize .* somaMT4_LM; %nm
mt2 = voxelSize .* somaMT2_LM; %nm
mt3 = voxelSize .* somaMT3_LM; %nm

% delete mt4 overlap with mt2
[k42, d42] = dsearchn(mt4, mt2);
d42 = d42./1e3; %um
idx = d42<=distThr;
mt4(idx,:) = []; % delete overlap with mt2
sprintf('Deleted %d MT4 somas overlapping with MT2', sum(idx))

% delete mt4 overlap with mt3
[k43, d43] = dsearchn(mt4, mt3);
d43 = d43./1e3; %um
idx = d43<=distThr;
mt4(idx,:) = []; 
sprintf('Deleted %d MT4 somas overlapping with MT3', sum(idx))

% delete mt2 overlap with mt3
[k23, d23] = dsearchn(mt2, mt3);
d23 = d23./1e3; %um
idx = d23<=distThr;
mt2(idx,:) = [];
sprintf('Deleted %d MT2 somas overlapping with MT3', sum(idx))

sprintf('After %dum distThr, MT4: %d, MT2: %d, MT3: %d. Total: %d', distThr, size(mt4,1), size(mt2,1), ...
        size(mt3,1), sum([size(mt4,1),size(mt2,1),size(mt3,1)]))

%% extrapolating neuron numbers for text
volMT = [229   217   300];%um
volDataset = [453.2,430.4,360];% um
count4 = size(mt4,1); % 10000 vs 12000 slices
count2 = size(mt2,1); % 10000 slices, wo glia
count3 = size(mt3,1); % 10000 slices, wo glia
count4_extended = size(skelMT4_EM_360.nodes{1},1);
count2_with_glia = skelMT2_EM_with_glia.numTrees;
count3_with_glia = skelMT3_EM_with_glia.numTrees;

avgSomaDensity = mean([count2, count3, count4]/prod(volMT))*1e9; % per mm3
sprintf('Soma density per mm3 in first 300um on avg in MT2,3,4: %.02f', avgSomaDensity)

fracInc = count4_extended/count4;
sprintf('Fractional increase from 60um: %.03f', fracInc)

sprintf('Entire imaged volume (4 MTs) contained %d somas (dataset vol x avg soma density) ', avgSomaDensity*prod(volDataset)/1e9)
sprintf('Of these %d somas were explicity counted', sum(count2+count3+count4_extended))

mt2_neurons = count2;
mt2_glia = count2_with_glia - count2;
mt3_neurons = count3;
mt3_glia = count3_with_glia - count3;

sprintf('Neuron fraction in MT2: %.03f (N:%d, G:%d) and MT3: %.03f (N: %d, G:%d)',...
                    mt2_neurons/(mt2_neurons+mt2_glia), mt2_neurons, mt2_glia,...
                    mt3_neurons/(mt3_neurons+mt3_glia), mt3_neurons, mt3_glia )

%% debug dist cutoff
fig = figure;
ax = axes(fig);
hold on
h = histogram(d42,'BinWidth', 2);
ax.XAxis.TickValues = 0:5:150;
xlabel('Distance between nearest pairs (um)')
ylabel('Frequency')

%%
function skel = addSomasAsGroup(skel, points, groupName, color )
    [skel, id] = skel.addGroup(groupName);
    skel = skel.addNodesAsTrees(points,'','',repelem(color, size(points,1),1));
    treesNew = skel.numTrees - size(points,1) + 1 : skel.numTrees;
    skel = skel.addTreesToGroup(treesNew , id);
end
