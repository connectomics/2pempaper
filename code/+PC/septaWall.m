if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;
somaColor = {[230/255, 159/255, 0/255, 1],... % 1a orange
              [213/255, 94/255, 0/255, 1],... %1b vermillion
              [0.75, 0.75, 0, 1],... % 3 dark yellow
              [204/255, 121/255, 167/255, 1],... % 4 reddish purple
              [0.3010, 0.7450, 0.933,1],...
              [182/255, 219/255, 255/255 ,1]}; %

Util.log('Loading PC from MTs')
%skelMT4 = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAll_LM.nml']));
%skelMT3 = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAllMT3_LM.nml']));
%skelMT2 = skeleton(fullfile(outDir,['outData/pointClouds/skelSomaAllMT2_LM.nml']));

m=load(fullfile(outDir,'barrelStateTformIso_vOct.mat')); % correct iso that gets #509
isoT = m.isoT;
DT = m.DT; % corresponds to isoT EM space barrel

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'),'tform');
tformMT4 = m.tform;

somaSkel = skeleton(fullfile(config.outDir,'data/skelSomaAll.nml'));
somaMT4_EM = somaSkel.getNodes;
somaMT4_LM = TFM.applyTformToVertices(tformMT4, somaMT4_EM, 'forward'); %EM to LM dataset

Util.log('combined somas into LM');
scaleEM = [11.24,11.24,30];
scaleLM = [1172 1172 1000];

Util.log('combined somas in MT4 space with INTRA/EXTRA barrel distinction')
skel = skeleton();
skel  = skel.setParams('YH_st126_MT4_updated_1708',scaleEM,[1 1 1]);
qPts = somaMT4_EM; IN = ~isnan(pointLocation(DT,qPts));
skel = addSomasAsGroup(skel, qPts(IN,:),'MT4_IN_EM', somaColor{1});
skel = addSomasAsGroup(skel, qPts(~IN,:),'MT4_OUT_EM', somaColor{4});
skel.write(fullfile(outDir,'outData/pointClouds/septaWall_EM.nml'));

somasIN = qPts(IN,:);
somasOUT = qPts(~IN,:);

fileID = '/tmpscratch/sahilloo/barrel/allForHeiko/septaWallSomasIN.txt';
dlmwrite(fileID,somasIN);
fileID = '/tmpscratch/sahilloo/barrel/allForHeiko/septaWallSomasOUT.txt';
dlmwrite(fileID,somasOUT);



function skel = addSomasAsGroup(skel, points, groupName, color )
    [skel, id] = skel.addGroup(groupName);
    skel = skel.addNodesAsTrees(points,'','',repelem(color, size(points,1),1));
    treesNew = skel.numTrees - size(points,1) + 1 : skel.numTrees;
    skel = skel.addTreesToGroup(treesNew , id);
end

