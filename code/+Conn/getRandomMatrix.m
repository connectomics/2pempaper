function [bconn, conn] = getRandomMatrix(preLength, postLength, varargin )
% bconn: binarized connectome
% conn: not binarized connectome
% get random connectome based on defined pre and post classes lengths
rng(0)
opts = struct;
opts.distribution = 'bernoulli';
opts.N = 1;
opts.p = 0.05;
opts.lambda = 5;
opts.a = 2;
opts.b = 4;
opts.plotFlag = false;
opts = Util.modifyStruct(opts, varargin{:});

switch opts.distribution
    case 'bernoulli'
        pd = makedist('binomial','N',1,'p',opts.p);
        conn = random(pd,[preLength,postLength]); 
        bconn = conn;
    case 'binomial'
        pd = makedist('binomial','N',opts.N,'p',opts.p);
        conn = random(pd,[preLength,postLength]); 
        conn_norm = (conn - pd.mean)./pd.std;
        bconn = double(conn_norm>0);
    case 'poisson'
        pd = makedist('poisson','lambda',opts.lambda);
        conn = random(pd,[preLength,postLength]); 
        conn_norm = (conn - pd.mean)./pd.std;
        bconn = double(conn_norm>0);
    case 'beta'
        pd = makedist('Beta','a',opts.a,'b',opts.b);
        conn = random(pd,[preLength,postLength]); 
        conn_norm = (conn - pd.mean)./pd.std;
        bconn = double(conn_norm>0);
    otherwise
        error('Unknown probability distribution %s', opts.distribution)
end
if opts.plotFlag
    figure
    subplot(2,1,1)
    imagesc(conn)
    daspect([preLength,1,1])
    colorbar
    subplot(2,1,2)
    imagesc(bconn)
    daspect([preLength,1,1])
    colorbar
    mean(bconn(:)) 
end
end
