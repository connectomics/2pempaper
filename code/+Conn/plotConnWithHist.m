function plotConnWithHist(data,yL,xL,preIds, postIds,titleName, daspectFlag,histFlag, outfile, flagLabels )

if ~exist('flagLabels','var') || isempty(flagLabels)
    flagLabels= false;
end
Util.log('Plotting conn...')
dataY = sum(data,2);
dataX = sum(data,1);
minSynCount = 0;

fig = figure();
fig.Color = 'white';
fig.Position(1:2) = 50;
fig.Position(3:4) = 900;

ax = axes(fig);

maxSyn = max(data(:));
colorMap = colormap(jet);
colorMap(1,:) = [0,0,0];
climits = [0.0001, maxSyn];
h = imagesc(ax, data);
if daspectFlag
    daspect([1 1 1])
end
colormap(colorMap)
set(gca,'CLim',climits, 'TickDir','out')
set(gca,'ytick',1:numel(preIds), 'yticklabels',preIds,...
            'xtick',1:numel(postIds), 'xticklabels',postIds)
if flagLabels
    title(titleName)
end

colorbar('Location','westoutside')
% set(gca,'xtick',[max(idxPreIN), max(idxPreTC)],'xticklabels','',...
%         'ytick',[max(idxPostIN), max(idxPostStp), max(idxPostSps)],'yticklabels','')
ax.Color = 'black';
ax.Position = [0.15, 0.15, 0.7, 0.7];

set(ax,'XAxisLocation','top')

if histFlag
    %% Histogram over incoming synapses
    Util.log('Plotting histogram post...')
    axPost = axes(fig);
    axPost.Position = ax.Position;
    axPost.Position(2:2:4) = [ax.Position(2)+ax.Position(4), 0.05]; % top
    axPost.Position(2:2:4) = [0.05, ax.Position(2)-0.05]; % bottom

    postSynCount = dataX;

    histogram(axPost, ...
        'BinCount', postSynCount, ...
        'BinEdges', 0:1:numel(postSynCount), ...
        'EdgeColor', ax.ColorOrder(1, :), ...
        'FaceAlpha', 1);

    axPost.YDir = 'reverse';
    axPost.TickDir = 'out';
    axPost.YMinorTick = 'off';
    % axPost.YScale = 'log';
    % axPost.YLim(2) = max(postSynCount);
    % yTicks = 0:axPost.YLim(end);
    % yticks(axPost, yTicks);
    % yticklabels(axPost, arrayfun( ...
    %     @(d) sprintf('%d', d), ...
    %     yTicks, 'UniformOutput', false));

    xlim(axPost, [0, numel(postSynCount)]);
    if axPost.XLim(2) > max(xticks(axPost))
        xticks(axPost, [xticks(axPost), axPost.XLim(2)]);
    end
    ylim(axPost, [0, max(postSynCount)]);
    if axPost.YLim(2) > max(yticks(axPost))
        yticks(axPost, [yticks(axPost), axPost.YLim(2)]);
    end
    ylabel(axPost, 'Synapses');

    %% Histogram over outgoing synapses
    Util.log('Plotting histogram pre...')
    axPre = axes(fig);
    axPre.Position = ax.Position;
    axPre.Position(1) = sum(ax.Position(1:2:end));
    axPre.Position(3) = 0.95 - axPre.Position(1);

    axonSynCount = dataY;
    histogram(axPre, ...
        'BinCounts', axonSynCount, ...
        'BinEdges', 0:1:numel(axonSynCount), ...
        'Orientation', 'horizontal', ...
        'EdgeColor', ax.ColorOrder(1, :), ...
        'FaceAlpha', 1);

    axPre.TickDir = 'out';
    % axPre.XScale = 'log';
    axPre.XAxisLocation = 'top';
    axPre.XMinorTick = 'off';
    axPre.YDir = 'reverse';
    axPre.YAxisLocation = 'right';

    % axPre.XLim(1) = minSynCount;
    % axPre.XLim(2) = max(axonSynCount);
    % xTicks = log10(axPre.XLim);
    % xTicks = 10 .^ (xTicks(1):xTicks(2));
    % xticks(axPre, xTicks);
    % xticklabels(axPre, arrayfun( ...
    %     @(d) sprintf('%d', d), ...
    %     xTicks, 'UniformOutput', false));

    xlim(axPre, [0, max(axonSynCount)]);
    if axPre.XLim(2) > max(xticks(axPre))
        xticks(axPre, [xticks(axPre), axPre.XLim(2)]);
    end
    ylim(axPre, [0, numel(axonSynCount)]);
    if axPre.YLim(2) > max(yticks(axPre))
        yticks(axPre, [yticks(axPre), axPre.YLim(2)]);
    end
    xlabel('Synapses');

end
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all
end
