function synMap = doShaftSpine(skel,preIds,preIdx, postIds, searchStr)
   synMap = zeros(numel(preIds),numel(postIds));
   for i=1:numel(preIds)
       thisPreIdx = preIdx{i};
       if isempty(thisPreIdx)
           disp(['tree ' preIds{i} ' is not present in pre']);
           continue;
       else
           axonComments = skel.getAllComments(thisPreIdx);
          % super slow
          % for j=1:numel(postIds)
          %     thisPost = sum(cellfun(@(x) strcmp([searchStr num2str(j,'%02d')],postIds(j)),axonComments));
          %     synMap(i,j) = thisPost;
          % end
            for j=1:numel(axonComments)
                thisComment = axonComments{j};
                id = regexp(thisComment,[searchStr '(?<id>\w+)'],'names');
                if isempty(id)
                    clear id
                    continue;
                end
                id = str2double(id.id);
                synMap(i,id) = synMap(i,id)+1;
            end
       end
   end
end

