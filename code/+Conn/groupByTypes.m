function outMap = groupByTypes(map,cellTypesAll,direction, preIds, postIds, yL, xL, synType, outDirPdf)
    titleName = [synType '-' yL '-to-' xL '-grouped'];
    ct = unique(cellTypesAll);
    outMap = [];
    switch direction
        case 'x'
            for curCT = ct(:)'
                curIdx = cellTypesAll == curCT;
                outMap = [outMap, sum(map(:,curIdx),2)];
            end
        case 'y'
            for curCT = ct(:)'
                curIdx = cellTypesAll == curCT;
                outMap = [outMap; sum(map(curIdx,:),1)];
            end 
        case 'both'
            tempMap = [];
            for curCT = ct(:)'
                curIdx = cellTypesAll == curCT;
                tempMap = [tempMap, sum(map(:,curIdx),2)];
            end
            for curCT = ct(:)'
                curIdx = cellTypesAll == curCT;
                outMap = [outMap; sum(tempMap(curIdx,:),1)];
            end           
    end
    % make plot
    Conn.plotConnWithHist(outMap,yL,xL,preIds, postIds, titleName, false, true,...
        fullfile(outDirPdf,['heatmap_' titleName '.png']));
end
