function [synMap, shaftMap, somaMap] = getPostConnectome(skel, preIds, postIds, postIdx, yL, xL, outDirPdf, plotFlag)
    if ~exist('plotFlag','var') || isempty(plotFlag)
        plotFlag = false;
    end
    % this function is meant to look from the post side for all possible
    % inputs
    % pre ids: string to search: 'tc1' cell-array of strings
    % shaft map is syn map for tc as no 'tc1 so' comments
    outfile = [yL '-to-' xL];
    somaMap = zeros(numel(postIds),numel(preIds));
    shaftMap = zeros(numel(postIds),numel(preIds));
    for i=1:numel(postIds)
        thisPostIdx = postIdx(i);
        dendComments = skel.getAllComments(thisPostIdx);
        for j=1:numel(preIds)
            thisSh = sum(cellfun(@(x) strcmp(x,preIds{j}),dendComments));
            shaftMap(i,j) = thisSh;
            thisSo = sum(cellfun(@(x) strcmp(x,[preIds{j} ' so']),dendComments));
            somaMap(i,j) = thisSo;
        end
    end
    % transpose: pre on y, post on x
    somaMap = somaMap';
    shaftMap = shaftMap';
    synMap = somaMap + shaftMap;
    
    % make plot
    Conn.plotConnWithHist(synMap,yL,xL,preIds, postIds,[outfile '-allSyn'], false, true,...
        fullfile(outDirPdf,['heatmap_full_' outfile '.png']));
    if nargout>1 & plotFlag
        Conn.plotConnWithHist(somaMap,yL,xL,preIds, postIds,[outfile '-somaSyn'], false, true,...
            fullfile(outDirPdf,['heatmap_soma_' outfile '.png']));
    end
    if nargout>2 & plotFlag
        Conn.plotConnWithHist(shaftMap,yL,xL,preIds, postIds,[outfile '-shaftSyn'], false, true,...
            fullfile(outDirPdf,['heatmap_shaft_' outfile '.png']));
    end
end
