function [synMapStp, synMapSps] = doSoma(skel, skelCellType, preIds,preIdx, countStp, countSps)
    synMapStp = zeros(numel(preIds),countStp);
    synMapSps = zeros(numel(preIds),countSps);
    for i=1:numel(preIds)
        thisPreIdx = preIdx{i};
        if isempty(thisPreIdx)
            disp(['tree ' preIds{i} ' is not present in pre']);
            continue;
        else
            soSynIdx = skel.getNodesWithComment('-',thisPreIdx);
            for j = 1:length(soSynIdx)
                synPos = skel.nodes{thisPreIdx}(soSynIdx(j),1:3);
                somaIdx = skelCellType.getClosestNode(synPos,1,false);
                postName = skelCellType.nodesAsStruct{1}(somaIdx).comment;% s%% or p%% or py3 or py5 or in or uc
                postType = postName(1); % s or p
                postId = str2double(postName(2:end)); % will be NaN for non s or p comments
                if isnan(postId)
                    continue;
                end
                switch postType
                    case 'p'
                        synMapStp(i,postId) = synMapStp(i,postId)+1;
                    case 's'
                        synMapSps(i,postId) = synMapSps(i,postId)+1;
                    case 'i'
                        disp('This syn was on IN, skipped')
                    case 'u'
                        disp('This syn was on UC, skipped')
                    otherwise
                        error('Soma syn at non s or p cellbody!'); % if interneuron, skip this.
                end
            end
        end
    end
end
