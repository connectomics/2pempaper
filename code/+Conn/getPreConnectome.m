function [synMapStp, synMapSps, somaMapStp, somaMapSps] = getPreConnectome(skel, skelCellType, preIds, preIdx, countStp, countSps, outDirPdf)
    % IN to Stp and Sps input
    % skel: skel cffi
    % skelCellType: skel with all L4 cells and their types and IDs in comments
    % pre Ids: TC axon 01
    % pre Idx: tc axon index in trees
    
    spineMapStp = Conn.doShaftSpine(skel, preIds, preIdx, 1:countStp, 'sp p ');    
    spineMapSps = Conn.doShaftSpine(skel, preIds, preIdx, 1:countSps, 'sp s ');
    shaftMapStp = Conn.doShaftSpine(skel, preIds, preIdx, 1:countStp, 'sh p ');
    shaftMapSps = Conn.doShaftSpine(skel, preIds, preIdx, 1:countSps, 'sh s ');
    [somaMapStp, somaMapSps] = Conn.doSoma(skel, skelCellType, preIds, preIdx, countStp, countSps);

    synMapStp = shaftMapStp + spineMapStp + somaMapStp;
    synMapSps = shaftMapSps + spineMapSps + somaMapSps;

    postIdsStp = arrayfun(@(x) ['p' x],1:countStp,'uni',0);
    postIdsSps = arrayfun(@(x) ['s' x],1:countSps,'uni',0);

    % make plot
    Conn.plotConnWithHist(synMapStp,'IN','Stp',preIds, postIdsStp,'IN-Stp', false, true,...
        fullfile(outDirPdf,['heatmap_full_' 'IN-Stp' '.png']));
    Conn.plotConnWithHist(synMapSps,'IN','Sps',preIds, postIdsSps,'IN-Sps', false, true,...
        fullfile(outDirPdf,['heatmap_full_' 'IN-Sps' '.png']));

    if nargout>4
        Conn.plotConnWithHist(somaMapStp,'IN','Stp',preIds, postIdsStp,'IN-Stp', false, true,...
            fullfile(outDirPdf,['heatmap_soma_' outfile '.png']));
        Conn.plotConnWithHist(somaMapSps,'IN','Sps',preIds, postIdsSps,'IN-Sps', false, true,...
            fullfile(outDirPdf,['heatmap_soma_' outfile '.png']));
        Conn.plotConnWithHist(shaftMapStp,'IN','Stp',preIds, postIdsStp,'IN-Stp', false, true,...
            fullfile(outDirPdf,['heatmap_shaft_' outfile '.png']));
        Conn.plotConnWithHist(shaftMapSps,'IN','Sps',preIds, postIdsSps,'IN-Sps', false, true,...
            fullfile(outDirPdf,['heatmap_shaft_' outfile '.png']));
        Conn.plotConnWithHist(spineMapStp,'IN','Stp',preIds, postIdsStp,'IN-Stp', false, true,...
            fullfile(outDirPdf,['heatmap_spine_' outfile '.png']));
        Conn.plotConnWithHist(spineMapSps,'IN','Sps',preIds, postIdsSps,'IN-Sps', false, true,...
            fullfile(outDirPdf,['heatmap_spine_' outfile '.png']));
    end

end
