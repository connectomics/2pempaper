function synMapSorted = sortAndFixColumns(synMap)
% create block structure
    synMapSorted = [];
    idxFix = []; % empty for first row
    for curRowIdx = 1:size(synMap,1)
        [~, idxSort] = sort(synMap(curRowIdx,:),'descend');
        idxNonFixed = setdiff(idxSort,idxFix,'stable');% important to keep order
  
        % updated row entries for sorted map
        synMapSorted(curRowIdx,:) = [synMap(curRowIdx,idxFix), ...
                        synMap(curRowIdx,idxNonFixed)];
        % fix column indices for next row
        idxFix = [idxFix,...
            idxNonFixed(synMap(curRowIdx,idxNonFixed)~=0)];
    end
end
