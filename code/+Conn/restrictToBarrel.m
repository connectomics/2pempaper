function [synMapOut, IN] = restrictToBarrel(synMap, className, pos)
% Description: Restrict class cell types to those within barrel
% Assuming that synMap post and pre ids are sorted eg. column 3 is s03 or row 5 is IN05

setConfig;
if ~exist('pos','var') || isempty(pos)
    pos = 2; % postsynaptic
end

Util.log('Parsing cell type skeleton...')
skelCellType = skeleton(config.cellTypesL4Fixed);
skelCellType = skelCellType.extractLargestTree;
c = skelCellType.getAllComments;

Util.log(sprintf('Restricting to barrel for class: %s \n', className))
switch className
    case 's'
        ids = cellfun(@(x) regexp(x,'s(?<id>\d+)','names') ,c,'uni',0);
        ids = cellfun(@(x) str2double(x.id), ids(~cellfun('isempty',ids)));
        count = max(ids);
        ids = sort(ids,'ascend');

        assert(size(synMap,pos) == numel(ids))
        somaLocs = [];
        for i = 1:numel(ids)
            curId = ids(i);
            somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['s' num2str(curId,'%02d')] ,'exact');
        end
    case 'p'
        ids = cellfun(@(x) regexp(x,'p(?<id>\d+)','names') ,c,'uni',0);
        ids = cellfun(@(x) str2double(x.id), ids(~cellfun('isempty',ids)));
        count = max(ids);
        ids = sort(ids,'ascend');

        assert(size(synMap,pos) == numel(ids))
        somaLocs = [];
        for i = 1:numel(ids)
            curId = ids(i);
            somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
        end
    case 'in'

end
[IN, somaLocsIn, somaLocsOut] = Tform.insideOutsideBarrel(somaLocs,'EM');
assert(numel(IN) == size(synMap,pos))
switch pos
    case 1 % pre
        synMapOut = synMap(IN,:);
    case 2 % post
        synMapOut = synMap(:,IN);
end
end
