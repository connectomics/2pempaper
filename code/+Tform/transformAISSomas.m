% use tform from MT4LM to MT4_EM and apply to iso(LM)
% vOct
thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'));
tform = m.tform;
scaleLM = m.scaleLM;
scaleEM = m.scaleEM;

outDirForHeiko = [outDir 'allForHeiko' filesep];
if ~exist(outDirForHeiko,'dir')
    mkdir(outDirForHeiko)
end

nmlDir = '/tmpscratch/sahilloo/barrel/data/example_ais/';
nmls = dir(fullfile(nmlDir,'*.nml'));
skels = arrayfun(@(x) skeleton(fullfile(nmlDir,x.name)),nmls);

somaLocsPyr = [];
for i=1:numel(skels)
thisSkel = skels(i);
soma = Util.getNodeCoordsWithComment(thisSkel,'pyr','regexp');
somaLocsPyr = vertcat(somaLocsPyr,soma);
end

somaLocsStp = [];
for i=1:numel(skels)
thisSkel = skels(i);
soma = Util.getNodeCoordsWithComment(thisSkel,'stp','regexp');
somaLocsStp = vertcat(somaLocsStp,soma);
end

somaLocsPyrLM = TFM.applyTformToVertices(tform,somaLocsPyr,'forward'); %EM to LM
somaLocsStpLM = TFM.applyTformToVertices(tform,somaLocsStp,'forward'); %EM to LM
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(somaLocsPyrLM); % blue color
skel.write(fullfile(outDirForHeiko,'aisSomaPositions_Pyr.nml'));
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(somaLocsStpLM); % blue color
skel.write(fullfile(outDirForHeiko,'aisSomaPositions_Stp.nml'));

