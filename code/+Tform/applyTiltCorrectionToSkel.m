function skelOut = applyTiltCorrectionToSkel(skel)
skelOut = skel;

for i = 1:skelOut.numTrees
    curNodes = skelOut.nodes{i};
    curNodes = curNodes(:,1:3);
    
    % apply tilt correction to each node of skeleton
    curNodes(:,2) = round(curNodes(:,2)-.449*curNodes(:,3));
    
    % update in skel
    skelOut = skelOut.setNodes(i, curNodes);
end
end
