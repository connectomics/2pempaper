% compare EM->LM tformed points with manually labelled LM points
setConfig;

thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

% do for MT4
load(fullfile(outDir,['barrelStateTform_' thisVersion '.mat']))

% ignore vessel points
cpEM = cpEM(1:end-2,:);
cpLM = cpLM(1:end-2,:);

% use tform to convert cps EM to LM
cpEMtoLM = TFM.applyTformToVertices(tform, cpEM, 'forward');

mt4 = struct;
mt4.cpEM = cpEM;
mt4.cpLM = cpLM;
mt4.cpEMtoLM = cpEMtoLM;

% do for MT2
load(fullfile(outDir,['barrelStateTformMT2_' thisVersion '.mat']))

% use tform to convert cps EM to LM
cpEMtoLM = TFM.applyTformToVertices(tform, cpEM, 'forward');

mt2 = struct;
mt2.cpEM = cpEM;
mt2.cpLM = cpLM;
mt2.cpEMtoLM = cpEMtoLM;

% save data
outDirPdf = fullfile(config.outDir,'outData/figures/fig6/',config.version);
Util.save(fullfile(outDirPdf,'somaCoordsForSupp.mat'), mt4, mt2)

%load('E:\tmpscratch\sahilloo\barrel\outData\figures\fig6\04_07_2019\somaCoordsForSupp.mat')

%%
fig = figure;
hold all
LM = mt4.cpLM;
EMtoLM = mt4.cpEMtoLM;

scatter3sph(LM(:,1)*1.172,LM(:,2)*1.172,LM(:,3)*1,'size',5,'color',[.3 .3 .3],'transp',.3);
scatter3sph(EMtoLM(:,1)*1.172,EMtoLM(:,2)*1.172,EMtoLM(:,3)*1,'size',5,'color',[1 0 0],'transp',.2);

LM = mt2.cpLM;
EMtoLM = mt2.cpEMtoLM;

scatter3sph(LM(:,1)*1.172,LM(:,2)*1.172,LM(:,3)*1,'size',5,'color',[.3 .3 .3],'transp',.3);
scatter3sph(EMtoLM(:,1)*1.172,EMtoLM(:,2)*1.172,EMtoLM(:,3)*1,'size',5,'color',[1 0 0],'transp',.2);

