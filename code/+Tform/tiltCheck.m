% Description:
% find the tilt one gets between EM and LM-To-EM CPs
% vOct
thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/tiltCheck/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

xlsfile = fullfile(outDir,'data/tform_map_MT4.xlsx');

Util.log('Loading excel file with soma positions');
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);

pointsEMFromTable = xlTable.xl1(2:end); % EM
pointsLMFromTable = xlTable.xl2(2:end); % LM

% extract x,y,z coordinates as a Nx3 matrix
cpEM = cell2mat(cellfun(@(x) regexp(x,...
    '(?<x>\d+),\s(?<y>\d+),\s(?<z>\d+)','names'),...
    pointsEMFromTable,'uni',0));
cpEM = arrayfun(@(x) structfun(@str2double,x),cpEM,'uni',0);
cpEM = cell2mat(cellfun(@(x) x',cpEM,'uni',0));

cpLM = cell2mat(cellfun(@(x) regexp(x,...
    '(?<x>\d+),\s(?<y>\d+),\s(?<z>\d+)','names'),...
    pointsLMFromTable,'uni',0));
cpLM = arrayfun(@(x) structfun(@str2double,x),cpLM,'uni',0);
cpLM = cell2mat(cellfun(@(x) x',cpLM,'uni',0));

% tilt correction hack
cpEM(:,2) = round(cpEM(:,2)-.449*cpEM(:,3));

%% calclulate and apply tform to transform EM skeletons to LM
Util.log('Calculating transformation EM to LM');
scaleEM = [11.24,11.24,30];
scaleLM = [1172,1172,1000];
relativeSearchRange = 0.1; % very important for z control points compression

Util.log('Doing manual reflection in xz plane')
tformRef = [1,0,0,0;0,-1,0,0;0,0,1,0;0,0,0,1];
cpEMRef = TFM.applyTformToVertices(tformRef,cpEM);

[ tformHorn, regParams, stat ] = TFM.computeOptimalAbsorTform(...
    cpEMRef, cpLM, scaleEM, scaleLM, relativeSearchRange );
Util.log(['Horns: lsqs: ' num2str(stat.lsqsBefore) ' ,lsqsOpt: ' num2str(stat.lsqsOpt)]);

% combine tforms
tform = tformHorn*tformRef;

% use tform to convert LM to EM 
cpLMToEM = TFM.applyTformToVertices(tform, cpLM, 'backward');
cpEMToLM = TFM.applyTformToVertices(tform, cpEM, 'forward');

Util.log('Saving data')
save(fullfile(outDir,['tiltCheckState.mat']),...
    'tform','regParams', 'cpLM','cpEM','scaleEM','scaleLM','cpLMToEM');

skelOut = skeleton;
skelOut = skelOut.setParams('m150517_2p_03',scaleLM,[0,0,0]);
skelOut = skelOut.addNodesAsTrees(cpLM, '', '', repelem([1,0,1,1],size(cpLM,1),1));
skelOut = skelOut.addNodesAsTrees(cpEMToLM, '', '', repelem([0.9290, 0.6940, 0.1250,1],size(cpEMToLM,1),1));
skelOut.write(fullfile(outDirPdf,'CPs_LM_EMToLM.nml'));


% get pca of cpEM and cpLMToEM
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];

for j=1:4
    subplot(2,2,j);
    data = cpEM;
    data(:,1) = data(:,1)-mean(data(:,1));
    data(:,2) = data(:,2)-mean(data(:,2));
    data(:,3) = data(:,3)-mean(data(:,3));

    % axes('LineWidth',0.6,...
    %     'FontName','Helvetica',...
    %     'FontSize',8,...
    %     'XAxisLocation','Origin',...
    %     'YAxisLocation','Origin');
    line(data(:,1),data(:,2),data(:,3),...
        'LineStyle','None',...
        'Marker','+','MarkerFaceColor','r','MarkerEdgeColor','r');
    axis equal
    hold on;
    [coeff,newdata,latend,tsd,variance] = pca(data);

    origin = [0,0,0];
    points1 = [origin;coeff(1,:)];
    plot3(range(data(:,1)) .* points1(:,1),...
        range(data(:,2)) .* points1(:,2),...
        range(data(:,3)) .* points1(:,3),'r-');
    points2 = [origin;coeff(2,:)];
    plot3(range(data(:,1)) .* points2(:,1),...
        range(data(:,2)) .* points2(:,2),...
        range(data(:,3)) .* points2(:,3),'r--');
    points3 = [origin;coeff(3,:)];
    plot3(range(data(:,1)) .* points3(:,1),...
        range(data(:,2)) .* points3(:,2),...
        range(data(:,3)) .* points3(:,3),'r-.');

    % LM to EM data
    uint32(cpLMToEM);
    data = cpLMToEM;
    data(:,1) = data(:,1)-mean(data(:,1));
    data(:,2) = data(:,2)-mean(data(:,2));
    data(:,3) = data(:,3)-mean(data(:,3));
    % axes('LineWidth',0.6,...
    %     'FontName','Helvetica',...
    %     'FontSize',8,...
    %     'XAxisLocation','Origin',...
    %     'YAxisLocation','Origin');
    line(data(:,1),data(:,2),data(:,3),...
        'LineStyle','None',...
        'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
    axis equal
    [coeff,newdata,latend,tsd,variance] = pca(data);

    origin = [0,0,0];
    points1 = [origin;coeff(1,:)];
    plot3(range(data(:,1)) .* points1(:,1),...
        range(data(:,2)) .* points1(:,2),...
        range(data(:,3)) .* points1(:,3),'k-');
    points2 = [origin;coeff(2,:)];
    plot3(range(data(:,1)) .* points2(:,1),...
        range(data(:,2)) .* points2(:,2),...
        range(data(:,3)) .* points2(:,3),'k--');
    points3 = [origin;coeff(3,:)];
    plot3(range(data(:,1)) .* points3(:,1),...
        range(data(:,2)) .* points3(:,2),...
        range(data(:,3)) .* points3(:,3),'k-.');
    hold off
    
    xlabel('x ');
    ylabel('y ');
    zlabel('z ');
    grid off

    switch j
            case 1
                %xy
                view([0, -90]);
                camroll(-90);
            case 2
                %xz
                view([0, 180]);
                camroll(-90);
            case 3
                %yz
                view([90, 0]);
                camroll(-90);
                camroll(270);
            %case 4
            %    %yz
            %    view([90,0]);
            %    camroll(-90);
            %    camroll(270);
        end
    axis equal 
 %   axis off
    box on

end
legend({'EM points','eig1','eig2','eig3','LMtoEM'}); % needs manual placement

outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'tilt_check_allViews'));
export_fig(outfile,'-q101', '-nocrop', '-transparent');
close all

%
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];

data = cpEM;
data(:,1) = data(:,1)-mean(data(:,1));
data(:,2) = data(:,2)-mean(data(:,2));
data(:,3) = data(:,3)-mean(data(:,3));

% axes('LineWidth',0.6,...
%     'FontName','Helvetica',...
%     'FontSize',8,...
%     'XAxisLocation','Origin',...
%     'YAxisLocation','Origin');
line(data(:,1),data(:,2),data(:,3),...
    'LineStyle','None',...
    'Marker','+','MarkerFaceColor','r','MarkerEdgeColor','r');
axis equal
hold on;
[coeff,newdata,latend,tsd,variance] = pca(data);
coeffEM = coeff;

origin = [0,0,0];
points1 = [origin;coeff(1,:)];
plot3(range(data(:,1)) .* points1(:,1),...
    range(data(:,2)) .* points1(:,2),...
    range(data(:,3)) .* points1(:,3),'r-');
points2 = [origin;coeff(2,:)];
plot3(range(data(:,1)) .* points2(:,1),...
    range(data(:,2)) .* points2(:,2),...
    range(data(:,3)) .* points2(:,3),'r--');
points3 = [origin;coeff(3,:)];
plot3(range(data(:,1)) .* points3(:,1),...
    range(data(:,2)) .* points3(:,2),...
    range(data(:,3)) .* points3(:,3),'r-.');

% LM to EM data
data = cpLMToEM;
data(:,1) = data(:,1)-mean(data(:,1));
data(:,2) = data(:,2)-mean(data(:,2));
data(:,3) = data(:,3)-mean(data(:,3));
% axes('LineWidth',0.6,...
%     'FontName','Helvetica',...
%     'FontSize',8,...
%     'XAxisLocation','Origin',...
%     'YAxisLocation','Origin');
line(data(:,1),data(:,2),data(:,3),...
    'LineStyle','None',...
    'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
axis equal
[coeff,newdata,latend,tsd,variance] = pca(data);
coeffLM = coeff;
origin = [0,0,0];
points1 = [origin;coeff(1,:)];
plot3(range(data(:,1)) .* points1(:,1),...
    range(data(:,2)) .* points1(:,2),...
    range(data(:,3)) .* points1(:,3),'k-');
points2 = [origin;coeff(2,:)];
plot3(range(data(:,1)) .* points2(:,1),...
    range(data(:,2)) .* points2(:,2),...
    range(data(:,3)) .* points2(:,3),'k--');
points3 = [origin;coeff(3,:)];
plot3(range(data(:,1)) .* points3(:,1),...
    range(data(:,2)) .* points3(:,2),...
    range(data(:,3)) .* points3(:,3),'k-.');
hold off

xlabel('x ');
ylabel('y ');
zlabel('z ');
grid on
box on
axis equal 

legend({'EM points','eig1','eig2','eig3','LM points transformed to EM'},'location','best');

outfile = fullfile(outDirPdf, ...
    sprintf('%s.fig', 'tilt_check_3D'));
export_fig(outfile,'-q101', '-nocrop', '-transparent');
close all


%% calculate angles' difference
angle = [];
for i =1:3
    x = coeffEM(i,:);
    y = coeffLM(i,:);
    angle(i) = atan2d(norm(cross(x,y)),dot(x,y));
end
angle_rad = deg2rad(angle);
rotm = eul2rotm(angle_rad,'XYZ')
rotmNew = [rotm,[0;0;0];[0,0,0,1]]
cpLMToEM_new = TFM.applyTformToVertices(rotmNew,cpLMToEM);

save(fullfile(outDir,['tiltCheckState.mat']),...
    'rotm','cpLMToEM_new','-append');








