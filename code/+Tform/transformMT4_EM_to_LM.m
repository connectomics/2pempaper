% Description:
% find the transformation between MT4_EM and MT4_LM
% vOct
thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

xlsfile = fullfile(outDir,'data/tform_map_MT4.xlsx');

Util.log('Loading excel file with soma positions');
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);

pointsEMFromTable = xlTable.xl1(2:end); % EM
pointsLMFromTable = xlTable.xl2(2:end); % LM

% extract x,y,z coordinates as a Nx3 matrix
cpEM = cell2mat(cellfun(@(x) regexp(x,...
    '(?<x>\d+),\s(?<y>\d+),\s(?<z>\d+)','names'),...
    pointsEMFromTable,'uni',0));
cpEM = arrayfun(@(x) structfun(@str2double,x),cpEM,'uni',0);
cpEM = cell2mat(cellfun(@(x) x',cpEM,'uni',0));

cpLM = cell2mat(cellfun(@(x) regexp(x,...
    '(?<x>\d+),\s(?<y>\d+),\s(?<z>\d+)','names'),...
    pointsLMFromTable,'uni',0));
cpLM = arrayfun(@(x) structfun(@str2double,x),cpLM,'uni',0);
cpLM = cell2mat(cellfun(@(x) x',cpLM,'uni',0));

% tilt correction hack
cpEM(:,2) = round(cpEM(:,2)-.449*cpEM(:,3));

%% calclulate and apply tform to transform EM skeletons to LM
Util.log('Calculating transformation EM to LM');
scaleEM = [11.24,11.24,30];
scaleLM = [1172,1172,1000];
relativeSearchRange = 0.1; % very important for z control points compression

Util.log('Doing manual reflection in xz plane')
tformRef = [1,0,0,0;0,-1,0,0;0,0,1,0;0,0,0,1];
cpEMRef = TFM.applyTformToVertices(tformRef,cpEM);

[ tformHorn, regParams, stat ] = TFM.computeOptimalAbsorTform(...
    cpEMRef, cpLM, scaleEM, scaleLM, relativeSearchRange );
Util.log(['Horns: lsqs: ' num2str(stat.lsqsBefore) ' ,lsqsOpt: ' num2str(stat.lsqsOpt)]);

% combine tforms
tform = tformHorn*tformRef;

Util.log('Saving data')
save(fullfile(outDir,['barrelStateTform_' thisVersion '.mat']),...
    'tform','regParams', 'cpLM','cpEM','scaleEM','scaleLM');

