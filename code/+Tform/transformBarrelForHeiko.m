% use tform from MT4LM to MT4_EM and apply to iso(LM)
% vOct
thisVersion = 'vOct';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

Util.log('Loading barrel iso');
m=load(fullfile(outDir,'barrelState_vJuly.mat'));
iso= m.iso;

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'));
tform = m.tform;
scaleLM = m.scaleLM;
scaleEM = m.scaleEM;

outDirForHeiko = [outDir 'allForHeiko' filesep];
if ~exist(outDirForHeiko,'dir')
    mkdir(outDirForHeiko)
end

Util.log('Transform barrel iso and generate AM file for Heiko...')
isoT = TFM.applyTformToIso(tform,iso,'backward'); %LM to EM
isoT = smoothpatch(isoT,1,5,1);

vertices = isoT.vertices;
% hack for removing nans
verticesFixed = vertices;
idxDel = isnan(vertices(:,1)) | isnan(vertices(:,2)) | isnan(vertices(:,3));
verticesFixed(idxDel,:) = [];
DT = delaunayTriangulation(verticesFixed);
[K,v] = convexHull(DT);

%{
figure;
trisurf(K,DT.Points(:,1),DT.Points(:,2),DT.Points(:,3),'FaceColor','cyan','EdgeColor','none');
camlight
lighting gouraud
saveas(gcf,fullfile(outDirForHeiko,['barrelConvexHullIso_' thisVersion '.fig']))
%}
Util.log('Exporting iso for AM')
issf = isoT;
issf.vertices = isoT.vertices * [scaleEM(1),0,0; 0,scaleEM(2),0;0,0,scaleEM(3)];
issf = {issf};
Util.KLEEv4_exportSurfaceToAmira_v2(issf,fullfile(outDirForHeiko,['isoEMForHeiko_' thisVersion '.am']),[0.5,0.5,0.5]);

Util.log('Export iso for AM in LM')
issf = iso;
issf.vertices = iso.vertices * [scaleLM(1),0,0; 0,scaleLM(2),0;0,0,scaleLM(3)];
issf = {issf};
Util.KLEEv4_exportSurfaceToAmira_v2(issf,fullfile(outDirForHeiko,['isoLMForHeiko_' thisVersion '.am']),[0.5,0.5,0.5]);

% added: 8 may 2019
% additionally save DT of isoEM
DTLM =  delaunayTriangulation(iso.vertices);

Util.log('Saving data')
save(fullfile(outDir,['barrelStateTformIso_' thisVersion '.mat']),...
    'tform','iso','isoT','scaleEM','scaleLM','DT','K','DTLM');

