function [IN, nodes_in, nodes_out] = insideOutsideBarrel(nodes, space)
% Description: Function to divide input nodes to whether they are inside
%   or outside the barrel isosurface
setConfig;
if ~exist('space','var') || isempty(space)
    space = 'LM';
end
m = load(config.barrelStateTform);
iso = m.iso;
tform = m.tform;
DTLM = delaunayTriangulation(iso.vertices);

Util.log('Switching space to LM')
switch space
    case 'EM'
        nodes_temp = nodes;
        nodes_temp(:,2) = round(nodes_temp(:,2)-.449*nodes_temp(:,3));
        nodesLM = TFM.applyTformToVertices(tform, nodes_temp, 'forward');
    case 'LM'

end
IN = ~isnan(pointLocation(DTLM,nodesLM));
nodes_in = nodes(IN,:); 
nodes_out = nodes(~IN,:);
end
