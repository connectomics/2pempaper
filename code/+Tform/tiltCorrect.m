% use tform from MT4LM to MT4_EM and apply to iso(LM)
% vOct
thisVersion = 'vNov';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
end
outDir = config.outDir;

Util.log('Loading barrel iso');
m=load(fullfile(outDir,'barrelState_vJuly.mat'));
iso= m.iso;

m=load(fullfile(outDir,'tiltCheckState.mat'));
tform = m.tform;
scaleLM = m.scaleLM;
scaleEM = m.scaleEM;
rotmNew = m.rotmNew;

outDirForHeiko = [outDir 'allForHeiko' filesep];
if ~exist(outDirForHeiko,'dir')
    mkdir(outDirForHeiko)
end

Util.log('Transform barrel iso and generate AM file for Heiko...')
isoT = TFM.applyTformToIso(tform,iso,'backward'); %LM to EM
isoT = TFM.applyTformToIso(rotmNew,isoT,'backward'); %LM to EM
isoT = smoothpatch(isoT,1,5,1);
vertices = isoT.vertices;
DT = delaunayTriangulation(vertices);
[K,v] = convexHull(DT);
figure;
trisurf(K,DT.Points(:,1),DT.Points(:,2),DT.Points(:,3),'FaceColor','cyan','EdgeColor','none');
camlight
lighting gouraud
saveas(gcf,fullfile(outDirForHeiko,['barrelConvexHullIso_tiltCorr' thisVersion '.fig']))

Util.log('Exporting iso for AM')
issf = isoT;
issf.vertices = isoT.vertices * [scaleEM(1),0,0; 0,scaleEM(2),0;0,0,scaleEM(3)];
issf = {issf};
Util.KLEEv4_exportSurfaceToAmira_v2(issf,fullfile(outDirForHeiko,['isoEMForHeiko_tiltCorrBackward' thisVersion '.am']),[0.5,0.5,0.5]);

Util.log('Saving data')
save(fullfile(outDir,['barrelStateTformIso_tiltCorr' thisVersion '.mat']),...
    'tform','iso','isoT','scaleEM','scaleLM','DT','K');

