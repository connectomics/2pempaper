setConfig;
skel = skeleton(config.skelTCCombined);
skel = skel.keepTreeWithName('TC axon 1');
scale = config.scaleEM;
startpoint = skel.getNodesWithComment('_root',1,'regexp');

%% attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
comments = {skel.nodesAsStruct{1}.comment}';
assert(startpoint == find(contains(comments,'_root')))
% find all comments you need
idx_sy_all = find(cellfun(@(x) any(regexp(x,'^sp p \d+$')),comments));

%%
TREE = Axonogram.axonogram(skel,'',startpoint,scale);
if ~isempty(idx_sy_all)
    XY = Axonogram.findPointsInAxonogram(idx_sy_all, TREE, skel, scale);
    gcf, plot(XY(:,1),XY(:,2), 'ok', 'MarkerSize', 7)
end
camroll(90)