function plotColorbar(outfile,maxSyn)
    colorMap = parula(maxSyn);
    fig = figure();
    fig.Color = 'white';
    hold on
    rectangle('Position',[0.5,0.5,maxSyn,1],'LineWidth',2,'FaceColor','k')
    h = scatter(1:1:maxSyn,repelem(1,1,maxSyn),500,'o','Filled');
    h.CData = colorMap;
    set(gca,'xlim',[0,10],'ylim',[0,10])
    set(gca,'box','off','xcolor','none','ycolor','none')
    export_fig(outfile, '-q101', '-nocrop', '-transparent');
    close all
end