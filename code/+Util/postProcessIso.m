function [iso, DT, K, v] = postProcessIso(iso,config,plotFlag)
if ~exist('plotFlag','var') || isempty(plotFlag)
   plotFlag = false; 
end
% do smoothing, delanuay triangulation on the iso
outDir = config.outDir;

% smooth iso
Util.log('Smoothing isosurface...')
iso = smoothpatch(iso,1,5,1);
%isoPost = smoothpatch(isoPost,1,5);

% extract faces and vertices from iso
vertices = iso.vertices;

% make triangulation object
tic;
Util.log('Calculating DT...')
DT = delaunayTriangulation(vertices);
%{
Util.log('Creating tetramesh of DT...')
figure;
tetramesh(DT,'FaceAlpha',0.3);
saveas(gcf,fullfile(outDir,'delaunayTriangulation.fig'))
close all
%}
% convexHull
Util.log('Calculating convexHull...')
[K,v] = convexHull(DT);
if plotFlag
    Util.log('Creating trisurf of conv hull...');
    figure;
    trisurf(K,DT.Points(:,1),DT.Points(:,2),DT.Points(:,3),'FaceColor','cyan','EdgeColor','none');
    camlight 
    lighting gouraud
    saveas(gcf,fullfile(outDir,'barrelConvexHullIso.fig'))
    close all
    toc;
end
end
% 
% return
% % extract faces and vertices from iso
% faces = isoPost.faces;
% vertices = isoPost.vertices;                                                                                                                
% % make triangulation object
% tic;
% Util.log('Calculating DT...')
% DT = delaunayTriangulation(vertices);
% %{
% Util.log('Creating tetramesh of DT...')
% figure;
% tetramesh(DT,'FaceAlpha',0.3);
% saveas(gcf,fullfile(outDir,'delaunayTriangulation.fig'))
% close all
% %}
% % convexHull
% Util.log('Calculating convexHull...')
% [K,v] = convexHull(DT);
% Util.log('Creating trisurf of conv hull...');
% figure;
% trisurf(K,DT.Points(:,1),DT.Points(:,2),DT.Points(:,3),'FaceColor','cyan','EdgeColor','none');
% camlight
% lighting gouraud
% saveas(gcf,fullfile(outDir,'barrelPostConvexHullIso.fig'))
% close all
% toc;


