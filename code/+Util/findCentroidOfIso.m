function comIso =  findCentroidOfIso(iso)
% iso: isosurface object FV
% com = 1x3 centroid

vertices = iso.vertices;
comIso = [mean(vertices(:,1)),mean(vertices(:,2)),mean(vertices(:,3))];

end