function figPlot(xlab,ylab)
set(gca,'TickDir','out');
set(gca,'LineWidth',1);
set(gca,'Box','off');
xlabel(xlab);
ylabel(ylab);
set(gca,'FontName','Arial');
set(gca,'FontSize',12)
end
