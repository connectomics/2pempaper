function [coords, treeInd] = getNodeCoordsWithComment(skel,comments,type, treeIdx)
% Description:
% get coordinates of the nodes with comment
%   Input: 
%       skel: skeleton
%       comments = cell-array of strings or a string eg. {'sp','sh'} or 'sp'
%       type: same as in getNodesWithComment function
%       treeIdx: tree in which to look. Default all trees
%   Output:
%       coords: Nx3 array of list of coordinates

coords = [];
if ~iscell(comments)
    comments = {comments};
end
if iscolumn(comments)
    comments = comments';
end
if  ~exist('treeIdx','var') || isempty(treeIdx)
    treeIdx = 1:skel.numTrees;
end
if iscolumn(treeIdx)
    treeIdx = treeIdx';
end
treeInd = [];
for comment = comments
    for i=treeIdx
        m = skel.getNodesWithComment(comment{1},i,type,true);
        mm = ~cellfun(@isempty,m); % in case that tree doesn't have the comment
        loc = find(mm);
        if ~isempty(loc)
            somaCoord = skel.nodes{i}(m{loc(1)},1:3);
            coords = vertcat(coords,somaCoord);
            treeInd = cat(1,treeInd,repelem(i,size(somaCoord,1),1));
        end
    end
end    
end

