function xd = generateDataFromStairs(yd, posBins, factor)
% if it is a stair plot with manually binned data, then artifically create
% the underlying data
% yd: Heights of your stait plot
% posBins: Bin positions of your stair plot
% factor: Value for scaling y data eg. 100 if y data is fractions
if ~exist('factor','var') || isempty(factor)
    factor = 1;
end

if numel(posBins) - numel(yd) == 1
    warning('Appending y data with its last entry!')
    yd = cat(1, yd(:), yd(end));
end
assert(numel(yd) == numel(posBins))
xd = [];
for i=1:numel(posBins)
    curData = repelem(posBins(i), ceil(yd(i)*factor));% take care of fractional values
    xd = [xd, curData];
end
end