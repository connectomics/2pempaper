function [synDensity, synCount, synLength] = calculateSynDensity(skel, comments, searchType, bbox, dim, binSize)
% Description:
% Calculate syn density along dim dimension in a bbox for a skeleton with comments
% specified in cell-array of str inputs in comments
% bbox: 3x2 array
% dim: 3 for z, 2 for y, 1 for x in the bbox
% binSize = 1000
% comments: 'sp' or'sh' or both {'sp,'sh}
% searchType : 'partial'
% Output:
%       synDensity: Cell array same size as number of bins formed. Each entry contains data of syn density per cut pieces/trees in that bin
%                  numeber of syn per um
%       synCount: each individual piece count of synapses
%       synLength: each individual piece length (in um)
binStart = bbox(dim,1):binSize:bbox(dim,2);

if ~iscell(comments)
    comments = {comments};
end
if iscolumn(comments)
    comments = comments';
end

[synDensity, synCount, synLength] = doForAllComments(skel, comments, searchType, bbox, dim, binStart);

end

function [synD, synC, synL] = doForAllComments(skel, comments, searchType,bbox, dim, binStart)
for i = 1 : numel(binStart)-1
    thisBBox = bbox;
    thisBBox(dim,:) = [binStart(i) binStart(i+1)];
    thisSkel = skel.restrictToBBox(thisBBox,'',false);
    for j = 1:numel(comments)
        curComment = comments{j};
        thisSyn{j} = thisSkel.getNodesWithComment(curComment,'',searchType,true);
        idx = ~cellfun(@isempty, thisSyn{j});
        thisSynCount{j} = zeros(size(thisSyn{j}));
        thisSynCount{j}(idx) = cellfun(@(x) numel(x),thisSyn{j}(idx));
    end

    thisSynCountAll = sum(horzcat(thisSynCount{:}),2);
    thisPl = thisSkel.pathLength./1e3; %um

    isValid = thisPl~=0; % only valid trees after cutting
    synD{i} = zeros(sum(isValid),1);
    synD{i} = thisSynCountAll(isValid) ./thisPl(isValid);
    synC{i} = thisSynCountAll(isValid);
    synL{i} = thisPl(isValid);
end
end





