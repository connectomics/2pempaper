function [hgram, pd] = fitGaussianNormalized(y,x,fitClass,color, binWidth)
% function to plot histgram and its fit
% y: values of heights
% x: bin locations
if ~exist('color','var') || isempty(color)
    color = 'k';
end

% do the plotting
hold on
% make histogram
hgram = histogram(y,numel(x),'BinWidth', binWidth,'DisplayStyle','stairs','LineWidth',3,'EdgeColor',color,...
        'Normalization', 'probability');

% fit class dist and make histogram invisible
h = histfit(y,numel(x),fitClass);
h(1).Visible = 'off';
h(2).Visible = 'off';
% h(2).Color = color;
% h(2).LineStyle = '-';

x_values = h(2).XData;
y_values = h(2).YData;
y_values = y_values./numel(y_values);
plot(x_values,y_values,'Color',color,'LineWidth',2);

pd = fitdist(reshape(y,'',1),fitClass);

end
