
function nuclei = fillNucleiHoles(nuclei, mask)
    % Dataset specific changes needed here, this function takes mask and
    % drill holes into 'outer hull' to be able to use imfill, change drill
    % location according to dataset :)
    vesselMasked = or(nuclei,mask);
%     vesselMasked(1:400, 800) = 0;
    nuclei = imfill(vesselMasked, 'holes') & ~mask;
end
