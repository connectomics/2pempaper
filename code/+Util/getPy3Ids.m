function [outIds, ids] = getPy3Ids()
setConfig;
% List ids of py3 in cffi nml
ids = [1,3,4,7,8,15,17,19,20,21,23,24,25,27,28,40,44,48,49,50,53,55,67,72,78,80,84,91];

% exclude intra barrel ones
Util.log('Parsing cell type skeleton...')
skelCellType = skeleton(config.cellTypesL4Fixed);
skelCellType = skelCellType.extractLargestTree;
c = skelCellType.getAllComments;

className = 'p';
Util.log(sprintf('Restricting to barrel for class: %s \n', className))
switch className
    case 'p'
     %   ids = cellfun(@(x) regexp(x,'p(?<id>\d+)','names') ,c,'uni',0);
     %   ids = cellfun(@(x) str2double(x.id), ids(~cellfun('isempty',ids)));
        ids = sort(ids,'ascend');

        somaLocs = [];
        for i = 1:numel(ids)
            curId = ids(i);
            somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
        end
end
assert(size(somaLocs,1) == numel(ids))
[IN, somaLocsIn, somaLocsOut] = Tform.insideOutsideBarrel(somaLocs,'EM');
% py3 ids to exclude that are outside barrel
outIds = ids(~IN);
end
