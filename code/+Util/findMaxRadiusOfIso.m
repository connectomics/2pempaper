function [radius,borderPoint] = findMaxRadiusOfIso(iso,centroid,inPlane)
% iso: isosurface object FV
% centroid: centroid of the iso
% radius: max distance of iso points from centroid
% inPlane: logical flag to measure planar in x-y radius or not

if ~exist('origin','var') || isempty(centroid);
    centroid = Util.findCentroidOfIso(iso);
end
if ~exist('inPlane','var') || isempty(inPlane);
    inPlane = true;
end

vertices = iso.vertices;
x = vertices(:,1);
y = vertices(:,2);
z = vertices(:,3);
if inPlane
    distFromCentroid = sqrt((x-centroid(1)).^ 2 + (y-centroid(2)) .^ 2);
else
    distFromCentroid = sqrt((x-centroid(1)).^ 2 + (y-centroid(2)) .^ 2 + ...
                            (z-centroid(3)).^ 2 ); 
end

[radius,idx] = max(distFromCentroid);
borderPoint = vertices(idx,:);

end