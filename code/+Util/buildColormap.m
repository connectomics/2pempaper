function cmap = buildColormap(n)
    c = 1 + (n - 1) / 2;
    alpha = linspace(0, 1, c);
    alpha = transpose(alpha);
    
    cmap = zeros(n, 3);
    cmap(1:c, :) = alpha .* [1, 1, 1];
    cmap(c:n, :) = sqrt(( ...
        alpha .* [0.301, 0.745, 0.933] .^ 2 ...
      + (1 - alpha) .* [1, 1, 1] .^ 2));
end
