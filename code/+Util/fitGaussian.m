function [hgram, h, pd] = fitGaussian(y,x,fitClass,color)
% function to plot histgram and its fit
% y: values of heights
% x: bin locations
if ~exist('color','var') || isempty(color)
    color = 'k';
end
% do the plotting
hold on
% make histogram
hgram = histogram(y,numel(x),'DisplayStyle','stairs','LineWidth',3,'EdgeColor',color);
% fit class dist and make histogram invisible
h = histfit(y,numel(x),fitClass);
h(1).Visible = 'off';
h(2).Color = color;
h(2).LineStyle = '-';

pd = fitdist(reshape(y,'',1),fitClass);
end
