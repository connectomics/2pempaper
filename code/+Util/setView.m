function a = setView(index,doMore, bbox, a)
% Author: Sahil Loomba <sahil.loomba@brain.mpg.de>
% set views for the barrel background
if ~exist('doMore', 'var') || isempty(doMore)
    doMore = false;
end
if ~exist('bbox', 'var') || isempty(bbox)
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
end
if ~exist('a', 'var') || isempty(a)
    a = gca;
end

switch index
    case 1
        %xy
        view([180, -90]);
    case 2
         %xz
         view([-90, 0]);
         camroll(-90);
    case 3
        %yz
        view([180, 180]);
    case 4
        %yz
        view([180, 180]);
    case 5 % old view
        view([90, 0]);
        camroll(-90);
        camroll(270);
    case 6 % old view fig2
        view([0, -90]);
        camroll(-90);
end

if doMore
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
set(gca,'ztick',[])
set(gca,'zticklabel',[])
axis equal
axis off
box off
set(gca,'XLim',bbox(1,:));
set(gca,'YLim',bbox(2,:));
set(gca,'ZLim',bbox(3,:));
end
end

