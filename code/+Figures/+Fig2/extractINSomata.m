function [somaCoordsOut,cellTypesOut, somaColorsOut] = extractINSomata(ver,numCellTypes)
% extract IN somata locations
setConfig;
outDir = config.outDir;

if ~exist('ver','var') | isempty(ver)
    ver = '04_07_2019'; % not used anymore but let it be
end

if ~exist('numCellTypes','var') | isempty(numCellTypes)
numCellTypes = 1:9; % edit if you only want to see specific cell types
end

% define soma colors and cell types for 32 IN axons
somaColors = Util.getSomaColors;
Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypes = cell2mat(xlTable1.xl16);
axonNamesInTable = vertcat(xlTable1.xl13);
dendNamesInTable = vertcat(xlTable1.xl12);
%numCellTypes = 1:9; % edit if you only want to see specific cell types

Util.log('Loading nml with axon and dend tracings...')
nml = config.skelINCombined;
skel = skeleton(nml);
skel = skel.replaceComments('soma','cellbody','complete');

% remove extra nodes
skel = skel.deleteNodesWithComment('-','exact');
skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019

skel.scale = [11.24,11.24,30];
totalTrees = skel.numTrees;
treeNames = skel.names;
idxAxon = cellfun(@(x) any(regexpi(x,'axon')),treeNames);
idxDend = cellfun(@(x) any(regexpi(x,'cell')),treeNames);

thisSkel = skel;
somaCoordsOut = [];
cellTypesOut = [];
somaColorsOut = [];
% loop over each IN cell type and plot the corresponding somas
for i = numCellTypes
    thisCellType = i;
    
    % extract axons tree names of this type
    idxCurCellIds = cellTypes==thisCellType;
    curDendNames = dendNamesInTable(idxCurCellIds);% names of dend trees of this i class

    curDendTrees = cellfun(@(x) getTreeWithName(skel,x,'exact'),curDendNames);
    if iscolumn(curDendTrees)
        curDendTrees = curDendTrees';
    end
    % extract soma coords from this dend tree indices
    somaLocs = [];
    for j = curDendTrees
        m = getNodesWithComment(skel,'cellbody',j,'exact',true);
        mm = ~cellfun(@isempty,m);
        loc = find(mm);
        somaCoord = skel.nodes{j}(m{loc(1)},1:3);
        somaLocs = vertcat(somaLocs,somaCoord);
    end
    assert(size(somaLocs,1)==numel(curDendNames));
    % prepare output
    somaCoordsOut = vertcat(somaCoordsOut, somaLocs);
    cellTypesOut = vertcat(cellTypesOut, repelem(i,size(somaLocs,1))');
    somaColorsOut = vertcat(somaColorsOut, repelem(somaColors{i}(1:3),size(somaLocs,1),1));
end

end
