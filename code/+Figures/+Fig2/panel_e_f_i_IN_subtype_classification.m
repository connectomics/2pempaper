% subtypes of INs with clustering
setConfig;
somaColors = Util.getSomaColors;
fontSize = 12;

cx_path = fullfile(outDir,'outData','figures','IN_subtypes');
cx_data = readtable(fullfile(cx_path,'readout_tc_20210729T073021_pooled_SL4.xlsx'));

% scatter plots
id = cx_data.id;
numTCSyn = cx_data.numTCSyn;
numSyn = cx_data.numSyn;
fractionTC = cx_data.fractionTC;
sasd_fraction = cx_data.SASD_yes_fraction;
stp_fraction = cx_data.stp_fraction;
fractionPlDendInBarrel = cx_data.plDendInBarrel ./ cx_data.pathlength; % fraction of pl inside barrel
fractionPlAxonInBarrel = cx_data.plfi;
txt = arrayfun(@num2str,id,'uni',0);
synCountStpHit = cx_data.synCountStpHit;
synCountSpsHit = cx_data.synCountSpsHit;
somaSynFrac = cx_data.somaSynFrac;

idStrongTC = [2,3,4,5,8,29,40,54];
idxStrongTC = ismember(id,idStrongTC);
idxWB = ismember(id, [2,5,8,54]);
idxLB = ismember(id,[3,4,29,40]);
idsNGFC = [12,14];
idxNGFC = ismember(id, idsNGFC);
idsSomaSynFracFull = [1,2,3,4,5,6,7,8,12,14,17,24,25,27,28,29,40,42];
idxSomaSynFracFull = ismember(id, idsSomaSynFracFull);

idsCutDendrites = [37, 57,44,48,51,13, 35]; % exclude from syn counts
idsCutDendrites = sort(idsCutDendrites);
idxCutDendrites = ismember(id,idsCutDendrites);

%% num TC vs num all syn
fontSize = 14;
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(numSyn, numTCSyn,...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text(numSyn, numTCSyn, txt, ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.XAxis.Label.String = 'Number of syns';
ax.YAxis.Label.String = 'Number of TC syns';
% export_fig(fullfile(cx_path,'panel_IN_subtypes_all_syn_vs_tc.eps'), '-q101', '-nocrop', '-m8');
% close all

%% num TC vs %TC
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(fractionTC,numTCSyn,...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text( fractionTC,numTCSyn, txt, ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = 'Number of TC syns';
ax.XAxis.Label.String = '%TC input';
ax.YAxis.Limits = [0,300];
ax.XAxis.Limits = [0,0.1];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_tc_percentage_vs_tc.eps'), '-q101', '-nocrop', '-m8');
% close all


% 1 Hierarchical clustering with %TC vs % num syn
x = fractionTC; y = numTCSyn; labels = txt;
idxDel = isnan(x) | isnan(y);
x(idxDel) = []; y(idxDel) = []; labels(idxDel) = [];
% normalize
x_mean = mean(x);
x_std = std(x);
x = arrayfun(@(data) (data-x_mean) / x_std,x);
y_mean = mean(y);
y_std = std(y);
y = arrayfun(@(data) (data-y_mean) / y_std,y);

pd = pdist([x,y],'euclidean');
lnk = linkage(pd,'ward');
H = dendrogram(lnk, 0, 'labels',labels); % set P=0
T = cluster(lnk,'maxclust',2);
labels(find(T==1))'
labels(find(T==2))'
title(sprintf('Using %%TC, num TC syns. Euclidean ward. Max 2 clusters'))
% export_fig(fullfile(cx_path,'panel_IN_subtypes_dendrogram_fractionTC_numTC.eps'), '-q101', '-nocrop', '-m8');

%% % TC vs %dend in barrel
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(fractionPlDendInBarrel, fractionTC,...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text(fractionPlDendInBarrel, fractionTC, txt, ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.XAxis.Label.String = '% dendrite inside barrel';
ax.YAxis.Label.String = '%TC input';
ax.XAxis.Limits = [0,1];
ax.YAxis.Limits = [0,0.1];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_tc_percentage_vs_dend_pl_percentage.eps'), '-q101', '-nocrop', '-m8');
% close all

%% % axon vs %dend in barrel
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(fractionPlDendInBarrel, fractionPlAxonInBarrel,...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text( fractionPlDendInBarrel, fractionPlAxonInBarrel, txt, ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.XAxis.Label.String = '% dendrite inside barrel';
ax.YAxis.Label.String = '% axon inside barrel';
ax.XAxis.Limits = [0,1];
ax.YAxis.Limits = [0,1];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_axon_pl_percentage_vs_dend_pl_percentage.eps'), '-q101', '-nocrop', '-m8');
% close all

%% % Stp targets vs % Tc input
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(fractionTC(idxStrongTC),stp_fraction(idxStrongTC),...
    'o','filled', 'MarkerFaceColor','c','MarkerEdgeColor','k');
text( fractionTC(idxStrongTC),stp_fraction(idxStrongTC), txt(idxStrongTC), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

% add line for barrel vs L4 stp fraction
m = load(fullfile(outDir,'allForHeiko/fig1_panelg_somaCountsLM.mat'));
stpIn = size(m.outTable.Var3{2},1); %Inside
spsIn = size(m.outTable.Var3{3},1); % Inside
yLineB = stpIn/(stpIn + spsIn);

synStpTotal = size(m.outTable.Var2{2},1);
synSpsTotal = size(m.outTable.Var2{3},1);
stpInL4 = synStpTotal;
spsInL4 = synSpsTotal;
yLineL4 = stpInL4/(stpInL4 + spsInL4);
temp = xlim;
line([temp(1), temp(2)],[yLineB yLineB],'LineWidth',2,'color',[0,0,0],'LineStyle','--');
line([temp(1), temp(2)],[yLineL4 yLineL4],'LineWidth',2,'color',[0,0,0],'LineStyle','--');

Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = '%Stp targets';
ax.XAxis.Label.String = '%TC input';
ax.YAxis.Limits = [0,0.40];
ax.XAxis.Limits = [0,0.1];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_stp_targets_perc_vs_tc_perc.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% % Stp targets vs number of axons
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
histogram(stp_fraction(idxStrongTC),'BinWidth',0.05,'DisplayStyle','stairs','EdgeColor','k', 'orientation','horizontal');
Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = '%Stp targets';
ax.XAxis.Label.String = 'Number of axons';
ax.YAxis.Limits = [0,0.4];
ax.XAxis.Limits = [0,4];
ax.XAxis.TickValues = 0:2:4;
% export_fig(fullfile(cx_path,'panel_IN_subtypes_stp_targets_vs_number_of_axons.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% stp targets vs sps targets
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(synCountSpsHit(idxStrongTC),synCountStpHit(idxStrongTC),...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text( synCountSpsHit(idxStrongTC), synCountStpHit(idxStrongTC), txt(idxStrongTC), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = 'Sps targets';
ax.XAxis.Label.String = 'Stp targets';
ax.YAxis.Limits = [0,50];
ax.XAxis.Limits = [0,100];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_stp_targets_vs_sps_targets.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% clustering on % stp targets vs % TC input
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
x = fractionTC(idxStrongTC); y = stp_fraction(idxStrongTC); labels = txt(idxStrongTC);
idxDel = isnan(x) | isnan(y);
x(idxDel) = []; y(idxDel) = []; labels(idxDel) = [];
pd = pdist([x,y],'euclidean');
lnk = linkage(pd,'ward');
H = dendrogram(lnk, 'labels',labels);
T = cluster(lnk,'maxclust',2);
labels(find(T==1))'
labels(find(T==2))'
title(sprintf('Using %%TC input, %%Stp targets (idxStrongTC only)'))
% export_fig(fullfile(cx_path,'panel_IN_subtypes_dendrogram_fractionTC_fractionStp.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% clustering on % stp targets vs % sasd

x = sasd_fraction(idxStrongTC); y = stp_fraction(idxStrongTC); labels = txt(idxStrongTC);
idxDel = isnan(x) | isnan(y);
x(idxDel) = []; y(idxDel) = []; labels(idxDel) = [];
pd = pdist([x,y],'euclidean');
lnk = linkage(pd,'ward');
H = dendrogram(lnk, 'labels',labels);
T = cluster(lnk,'maxclust',2);
labels(find(T==1))'
labels(find(T==2))'
title(sprintf('Using %%SASD, %%Stp targets (idxStrongTC only)'))
ax = gca;
ax.YAxis.TickValues = 0:0.1:0.4;
Util.setPlotDefault(gca,'','');
% export_fig(fullfile(cx_path,'panel_IN_subtypes_dendrogram_fractionSASD_fractionStp.eps'), '-q101', '-nocrop', '-transparent');
% close all

% histogram
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter(x,y,...
    'o','filled', 'MarkerFaceColor','c','MarkerEdgeColor','k');
text( x,y, txt(idxStrongTC), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = '% StP targets';
ax.XAxis.Label.String = {'% clustered' 'TC synapses'};
ax.YAxis.Limits = [0,0.40];
ax.XAxis.Limits = [0,0.40];
ax.YAxis.TickValues = 0:0.1:0.4;
ax.XAxis.TickValues = 0:0.1:0.4;
% export_fig(fullfile(cx_path,'panel_IN_subtypes_sasd_perc_vs_stp_targets_perc.eps'), '-q101', '-nocrop', '-transparent');
% close all


%% % Stp targets WB vs LB boxplot
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
pValueRS12 = ranksum(stp_fraction(idxWB), stp_fraction(idxLB));

x = [stp_fraction(idxWB); stp_fraction(idxLB)];
curTxt = [txt(idxWB); txt(idxLB)];

curIdxWB = 1:sum(idxWB);
curIdxLB = curIdxWB(end)+(1:sum(idxLB));
g = cell(numel(x),1);
g(curIdxWB) = {'WB'}; % first box
g(curIdxLB) = {'LB'}; % second box

widthS = 2; startS = 1;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
h = boxplot(x,g,'Colors','k','Widths',2,'Positions',pos);
set(h,{'linew'},{2})
x_pos = widthS.*rand(numel(curIdxWB),1)+posS(1);
scatter(x_pos, x(curIdxWB),...
    'o','filled', 'MarkerFaceColor','c','MarkerEdgeColor',somaColors{1}(1:3));
text(x_pos, x(curIdxWB), curTxt(curIdxWB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
x_pos = widthS.*rand(numel(curIdxLB),1)+posS(2);
scatter(x_pos, x(curIdxLB),...
    'o','filled', 'MarkerFaceColor','c','MarkerEdgeColor',somaColors{2}(1:3));
text(x_pos, x(curIdxLB), curTxt(curIdxLB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
title(sprintf('ranksum: %.3f',pValueRS12))
ax.YAxis.Limits = [0,0.4];
ax.YAxis.Label.String = '% Stp targets';
Util.setPlotDefault(gca)
% export_fig(fullfile(cx_path,'panel_IN_subtypes_stp_targets_perc_boxplot.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% % clustering of syns on WB vs LB
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on

% ranksum
pValueRS12 = ranksum(sasd_fraction(idxWB), sasd_fraction(idxLB));
x = [sasd_fraction(idxWB); sasd_fraction(idxLB)];
curTxt = [txt(idxWB); txt(idxLB)];
curIdxWB = 1:sum(idxWB);
curIdxLB = curIdxWB(end)+(1:sum(idxLB));
g = cell(numel(x),1);
g(curIdxWB) = {'WB'}; % first box
g(curIdxLB) = {'LB'}; % second box
widthS = 2; startS = 1;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
h = boxplot(x,g,'Colors','k','Widths',2,'Positions',pos);
set(h,{'linew'},{2})
x_pos = widthS.*rand(numel(curIdxWB),1)+posS(1);
scatter(x_pos, x(curIdxWB),...
    'o','filled', 'MarkerFaceColor',somaColors{1}(1:3),'MarkerEdgeColor',somaColors{1}(1:3));
text(x_pos, x(curIdxWB), curTxt(curIdxWB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
x_pos = widthS.*rand(numel(curIdxLB),1)+posS(2);
scatter(x_pos, x(curIdxLB),...
    'o','filled', 'MarkerFaceColor',somaColors{2}(1:3),'MarkerEdgeColor',somaColors{2}(1:3));
text(x_pos, x(curIdxLB), curTxt(curIdxLB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
title(sprintf('ranksum: %.3f',pValueRS12))
ax.YAxis.Limits = [0,0.4];
ax.YAxis.Label.String = '% clustered syns';
Util.setPlotDefault(gca)
% export_fig(fullfile(cx_path,'panel_IN_subtypes_clustering_bins_SL.eps'), '-q101', '-nocrop', '-m8');
% close all

% text for sasd fraction / clustering on BINs
curData1 = sasd_fraction(idxWB);
sprintf('WBs clustered synapses: %.2f+-%.2f %% (N=%d)', mean(curData1), std(curData1), numel(curData1))
curData2 = sasd_fraction(idxLB);
sprintf('LBs clustered synapses: %.2f+-%.2f %% (N=%d)', mean(curData2), std(curData2), numel(curData2))
sprintf('LBs fold lower than WBs: %.2f (p ranksum: %.3f)', mean(curData1)/mean(curData2), pValueRS12)


%% so-syn % vs axon pl inside barrel
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter( fractionPlAxonInBarrel(idxSomaSynFracFull), somaSynFrac(idxSomaSynFracFull),...
    'o','filled', 'MarkerFaceColor','k','MarkerEdgeColor','k');
text(  fractionPlAxonInBarrel(idxSomaSynFracFull), somaSynFrac(idxSomaSynFracFull), txt(idxSomaSynFracFull), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
scatter(fractionPlAxonInBarrel(~idxSomaSynFracFull), somaSynFrac(~idxSomaSynFracFull),...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text( fractionPlAxonInBarrel(~idxSomaSynFracFull), somaSynFrac(~idxSomaSynFracFull),txt(~idxSomaSynFracFull), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = 'Soma syn fraction';
ax.XAxis.Label.String = 'Axon pl in barrel';
ax.YAxis.Limits = [0,0.5];
ax.XAxis.Limits = [0,1];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_axon_pl_percentage_vs_soma_syn_fraction.eps'), '-q101', '-nocrop', '-transparent');
% close all

fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
histogram(somaSynFrac,'BinWidth',0.05,'DisplayStyle','stairs','EdgeColor','k', 'orientation','vertical');
Util.setPlotDefault(gca,'','');
ax.XAxis.Label.String = 'Soma syn fraction';
ax.YAxis.Label.String = 'Number of axons';
ax.XAxis.Limits = [0,0.5];
%ax.YAxis.Limits = [0,4];
%ax.YAxis.TickValues = 0:2:4;
set(gca,'XAxisLocation','top')
ax.YAxis.Direction = 'reverse';
% export_fig(fullfile(cx_path,'panel_IN_subtypes_soma_syn_fraction_hist.eps'), '-q101', '-nocrop', '-transparent');
% close all

%% soma syn % vs TC % input
fig = figure;
fig.Color = 'white';
ax = axes(fig)
hold on
scatter( fractionTC(idxSomaSynFracFull), somaSynFrac(idxSomaSynFracFull),...
    'o','filled', 'MarkerFaceColor','k','MarkerEdgeColor','k');
text( fractionTC(idxSomaSynFracFull), somaSynFrac(idxSomaSynFracFull), txt(idxSomaSynFracFull), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);
scatter(fractionTC(~idxSomaSynFracFull), somaSynFrac(~idxSomaSynFracFull), ...
    'o','filled', 'MarkerFaceColor','none','MarkerEdgeColor','k');
text( fractionTC(~idxSomaSynFracFull), somaSynFrac(~idxSomaSynFracFull), txt(~idxSomaSynFracFull), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

Util.setPlotDefault(gca,'','');
ax.YAxis.Label.String = 'Soma syn fraction';
ax.XAxis.Label.String = 'TC percentage';
ax.YAxis.Limits = [0,0.5];
ax.XAxis.Limits = [0,0.10];
% export_fig(fullfile(cx_path,'panel_IN_subtypes_tc_percentage_vs_soma_syn_fraction.eps'), '-q101', '-nocrop', '-transparent');
% close all


% text for soma innervation
d = somaSynFrac(idxStrongTC);
sprintf('strong TC INs: soma syn fraction: %.2f+-%.2f, n= %d, range: %.2f - %.2f', mean(d), std(d), numel(d), min(d), max(d))

% text for TC input on BINs subtypes
d = numTCSyn(idxWB);
sprintf('WBs tc input: %.2f+-%.2f (N=%d)', mean(d), std(d), numel(d))
d = numTCSyn(idxLB);
sprintf('LBs tc input: %.2f+-%.2f (N=%d)', mean(d), std(d), numel(d))

% text for WB vs LB to ExN innvervation
sprintf('WB -> Stp (%.2f +- %.2f)', mean(stp_fraction(idxWB)), std(stp_fraction(idxWB)))
sprintf('LB -> Stp (%.2f +- %.2f)', mean(stp_fraction(idxLB)), std(stp_fraction(idxLB)))

% text for num synapses
minSyn = min(numSyn(~idxCutDendrites));
maxSyn = max(numSyn(~idxCutDendrites));
sprintf('Range of input synapses excluding cut-ones: %d - %d (N=%d)', minSyn, maxSyn, sum(~idxCutDendrites))
sprintf('Ids cut dendrites or other barrel:')
disp(idsCutDendrites)

% report %TC for BINS and nonBINS with ? 70 dend in barrel
idxLargeDend = fractionPlDendInBarrel >= 0.70;

d = fractionTC(idxStrongTC);
sprintf('BINs: %%TC: %.2f +- %.2f (N=%d)', 100*mean(d), 100*std(d), numel(d))

d = fractionTC(~idxStrongTC & idxLargeDend);
sprintf('nonBINs with >=70%% intrabarrel dendrite: %%TC: %.2f +- %.2f (N=%d)', 100*mean(d), 100*std(d), numel(d))




