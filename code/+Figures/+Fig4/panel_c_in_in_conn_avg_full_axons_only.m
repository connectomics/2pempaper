% panel with in to in connectome for 4 major types only: Avg n of syn per cell pair per type pair
% keep full IN axons only in pre side
setConfig;
somaColors = Util.getSomaColors;
load(config.connmat_SLmanual,'synMapININ','cellTypesAll','preClassIds','postClassIds')

% main IN types
cellTypesKeep = [1,2,3,4, 5,7,8,9];

% sort index for IN types
[cellTypesAll,idxSort] = sort(cellTypesAll);
outMap = synMapININ(idxSort,idxSort);

% full pre axons
idxFull = postClassIds.i_full(idxSort);
cellTypesAllPre = cellTypesAll(idxFull);
outMap = outMap(idxFull,:);

% keep pre side main IN types only
idxKeep = ismember(cellTypesAllPre, cellTypesKeep);
outCellTypesPre = cellTypesAllPre(idxKeep);
outMap = outMap(idxKeep,:);

% keep post side main IN types only
idxKeep = ismember(cellTypesAll, cellTypesKeep);
outCellTypesPost = cellTypesAll(idxKeep);
outMap = outMap(:,idxKeep);

assert(size(outMap,1) == numel(outCellTypesPre))
assert(size(outMap,2) == numel(outCellTypesPost))

% avg n of syn per cell pair per type pair
outAvgSyn = zeros(numel(cellTypesKeep),numel(cellTypesKeep));

for i = 1:numel(cellTypesKeep)
    preType = cellTypesKeep(i);
    for j = 1:numel(cellTypesKeep)
        postType = cellTypesKeep(j);
        curMap = outMap(outCellTypesPre==preType, outCellTypesPost==postType);
        outAvgSyn(i,j) = averageSynPerPair(curMap);
    end
end

% remove class 9 NaNs
idxPre = isnan(outAvgSyn(:,1));
outAvgSyn(idxPre,:) = [];
cellTypesKeepPre = cellTypesKeep(~idxPre);

idxPost = isnan(outAvgSyn(1,:));
outAvgSyn(:,idxPost) = [];
cellTypesKeepPost = cellTypesKeep(~idxPost);

Util.log('Round off:')
format bank
outAvgSyn = round(outAvgSyn,2);

Util.log('plot heatmap from data')
colorMap = jet(64);
colorMap(1,:) = [0,0,0];
%climits = [0.0001, 13];
%colorMap = gray(64);
h = heatmap(cellTypesKeepPost, cellTypesKeepPre, outAvgSyn, 'Colormap',colorMap,...
            'FontSize',30, 'MissingDataColor','w');

outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_conn_avg_full_axons_only.eps');
%set(gcf,'Position',get(0, 'Screensize'))
%colorbar off
colorbar('Location','eastoutside')
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

function avgSyn = averageSynPerPair(synMap)
    % for a given conn, find avg number of synapses per cell pair
    totalSyn = sum(synMap(:));
    totalPairs = size(synMap,1) * size(synMap,2);
    avgSyn = totalSyn/totalPairs;
end
