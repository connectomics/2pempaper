% panel a with in1 to in 2
setConfig;
outDir = config.outDir;

curCellIdsToPlot = [2,40]; %[8, 29];% BIN1 and BIN2 examples

rng(0)
Rmin = 0; Rmax = 0.2;
Gmin = 0.6; Gmax = 0.7;
Bmin = 0.7; Bmax = 1;
singleColor = [0, 158, 115]./255;

%colorsForAxons = Util.getRangedColors( Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, numel(curCellIdsToPlot));
colorsForAxons = repmat(singleColor, numel(curCellIdsToPlot) ,1);

%c = 0.3 + (0.99-0.3)*rand(1,numel(curCellIdsToPlot)); %or these two lines for gray
%c(3,:) = c(1,:); c(2,:) = c(1,:);
%colorsForDendrites = c'; 
colorsForDendrites = repmat([0,0,0],numel(curCellIdsToPlot),1); 

somaColors = Util.getSomaColors;

tubeSize = 1; dotSize= 3;
somaBallSize = 100;
synSize = 80; synShape = 'v';

outDirPdf = fullfile(config.outDir,'outData/figures/fig5/', config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
Util.log('Loading data...')
skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];
clear skelIn

skel = skeleton(config.cffi); % tc axons and their outputs to SpS, StP, IN

idxTreesAxons = arrayfun(@(x) skel.getTreeWithName(['IN axon ' num2str(x,'%02d')],'exact'), curCellIdsToPlot);

idxTreesDend = arrayfun(@(x) skel.getTreeWithName(['cell i' num2str(x,'%02d')],'exact'), curCellIdsToPlot);

% extract soma locations
somaLocs = Util.getNodeCoordsWithComment(skel, {'soma','cellbody', '_in'}, 'partial',idxTreesDend);
curSomaColors = [somaColors{1}(1:3); somaColors{2}(1:3)];
% extract syn locations from each other
synLocs1_2 = Util.getNodeCoordsWithComment(skel,['IN' num2str(curCellIdsToPlot(1),'%02d')], 'partial',idxTreesDend(2));
synLocs2_1 = Util.getNodeCoordsWithComment(skel,['IN' num2str(curCellIdsToPlot(2),'%02d')], 'partial',idxTreesDend(1));
synLocs = synLocs2_1;
synColors = repelem([0,0,0], size(synLocs,1),1);
assert(size(synColors,1) == size(synLocs,1));

%scale all spheres
scale = skel.scale./1000;
somaLocs = skel.setScale(somaLocs,scale);
nodesPC = skel.setScale(nodesPC, scale);
if not(isempty(synLocs))
synLocs = skel.setScale(synLocs,scale);
end
toPlotTrees = [idxTreesAxons(2); idxTreesDend(1)]; 
toPlotColors = [colorsForAxons(2,:); colorsForDendrites(1,:)];

Util.log('Now plotting figure....');
bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;
% plot 
for i=1:numel(toPlotTrees)
    skel.plot(toPlotTrees(i), toPlotColors(i,:), true, tubeSize);
end
objectSomas = ...
            scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3),...
            somaBallSize,curSomaColors(:,1:3),'filled');
objectPC = ...
            scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3),...
            dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
            'MarkerFaceColor', colorPC(1:3),'MarkerFaceAlpha',0.5,'MarkerEdgeAlpha',0.5);
if not(isempty(synLocs))
synapseObject = ...
            scatter3(synLocs(:,1),synLocs(:,2),synLocs(:,3),...
            synSize, synColors(:,1:3), synShape, 'filled');
end
%Visualization.Figure.removeWhitespaceFromPlot();
%yz
Util.setView(5,true);
%% hacky to match other panels figures
%ax = gca; ax.XLim = [-96,420]; ax.YLim = [-96,420];
% outfile = fullfile(outDirPdf,'panel_cffi_sketch_right.eps');
% export_fig(outfile,'-q101', '-nocrop', '-transparent');
% Util.log('Saving file %s.', outfile);
% close(f);
