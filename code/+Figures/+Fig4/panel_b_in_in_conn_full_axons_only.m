% panel with in to in connectome sorted by IN cell types with full axons only + soma input
% Use full IN axons that are present in cffi file
setConfig;
somaColors = Util.getSomaColors;
load(config.connmat_SLmanual,'synMapININ','shaftMapININ', 'somaMapININ', 'cellTypesAll','preClassIds','postClassIds')

coloredFlag = true; % colors for IN types in circuit

% sort index for IN types
[~,idxSort] = sort(cellTypesAll);

% sort outMap
synMap = synMapININ(idxSort,idxSort);
shaftMap = shaftMapININ(idxSort,idxSort);
somaMap = somaMapININ(idxSort,idxSort);

% sort IN Ids
postIds = postClassIds.i(idxSort);
idxFull = postClassIds.i_full(idxSort);
postIds = cellfun(@(x) x(3:end),postIds,'uni',0);

% pre axon Ids
sprintf('Keeping %d full INs only:', sum(idxFull))
postIdsPre = postIds(idxFull);

% keep full pre axon rows only
synMap = synMap(idxFull,:);
shaftMap = shaftMap(idxFull,:);
somaMap = somaMap(idxFull,:);

assert(numel(postIdsPre) == size(synMap,1))
assert(numel(postIds) == size(synMap,2))

outMap = synMap;

% plot heatmap from data
colorMap = colormap(jet);
colorMap(1,:) = [0,0,0];
climits = [0.0001, 13];
h = heatmap(postIds, postIdsPre, outMap, 'ColorLimits',climits,'Colormap',colorMap,...
            'FontSize',30, 'MissingDataColor','w');
axp = struct(gca);
axp.Axes.XAxisLocation = 'bottom';
axp.XAxis.TickLabelRotation = 45;
outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_connectome_full_axon_only.eps');
set(gcf,'Position',get(0, 'Screensize'))
colorbar('Location','eastoutside')
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

Conn.plotConnWithHist(outMap,'IN','IN',postIdsPre, postIds,'IN-IN', false, true,...
        fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_connectome_full_axon_only_with_hist.eps'), false);
% close all


%% plot the IN IN circuit in 3d
Util.log('Loading nml with axon and dend tracings...')
nml = config.skelINCombined;
skel = skeleton(nml);
skel = skel.replaceComments('soma','cellbody','exact','complete');
skel = skel.replaceComments('_in','cellbody','partial','complete');

% remove extra nodes
skel = skel.deleteNodesWithComment('-','exact');
skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019
skel.scale = [11.24,11.24,30];
scale = skel.scale./1000;

% keep only dendrites
idxPlot = contains(skel.names, sprintf('cell i'));
skel =  skel.deleteTrees(idxPlot, true);

% get skel for postIds only
[somaCoordsPost, somaColorsPost] = getSomaCoordsForTheseIds(skel, postIds, config);
somaCoordsPost = skel.setScale(somaCoordsPost, scale);

% get skel for pre Ids only
[somaCoordsPre, somaColorsPre] = getSomaCoordsForTheseIds(skel, postIdsPre, config);
somaCoordsPre = skel.setScale(somaCoordsPre, scale);

% plot somata
numColors = size(somaCoordsPost,1);
if coloredFlag
    somataColors = somaColorsPost; % colors by types
    outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_connectome_full_axon_only_circuit_colored.eps');
else
    somataColors = randperm(255,numColors)' .* ones(numColors,1) * ([1,1,1]/255); % gradient of grey colors
    outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_connectome_full_axon_only_circuit.eps');
end
ax = doPlotting('', somataColors, somaCoordsPost)

% add arrows between INs based on syn counts between pre and post
maxThickness = 4;
maxAlpha = 1;
maxSyn = max(outMap(:));
for row = 1:size(outMap,1)
    curPre = somaCoordsPre(row,:);
    preColor = somaColorsPre(row,:);
    for col = 1:size(outMap,2)
        curPost = somaCoordsPost(col,:);
        curSyn = outMap(row,col);
        if curSyn>0
            lineThickness = curSyn * maxThickness/maxSyn;
            lineAlpha = curSyn * maxAlpha/maxSyn;

            lh = line(ax(1),[curPre(1), curPost(1)], [curPre(2), curPost(2)], [curPre(3), curPost(3)],...
                'LineStyle','-','LineWidth',lineThickness, 'Color',preColor);
            lh.Color(4) = lineAlpha;

            lh = line(ax(2),[curPre(1), curPost(1)], [curPre(2), curPost(2)], [curPre(3), curPost(3)],...
                'LineStyle','-','LineWidth',lineThickness, 'Color',preColor);
            lh.Color(4) = lineAlpha;

        end
    end
end

% export_fig(outfile,'-q101', '-nocrop', '-transparent');
% Util.log('Saving file %s.', outfile);
% close all

function [somaCoords, somaColors] = getSomaCoordsForTheseIds(skel, ids, config)
    % keep only main IN dendrites( including unclass ones that are part of the 43x43 connectome
    idxPlot = contains(skel.names, ids);
    skelToPlot =  skel.deleteTrees(idxPlot, true);
    assert(skelToPlot.numTrees == numel(ids))
    
    % sort trees of skel in same order as ids
    [~,idxSortAscendingIds] = sort(skelToPlot.names);
    skelToPlot = skelToPlot.reorderTrees(idxSortAscendingIds);

    % get existing sorting order of the ids
    [X,Y] = ismember(skelToPlot.names, cellfun(@(x) ['cell i' x],ids,'uni',0));
    [~,idxSort] = sort(Y(X));
    skelToPlot = skelToPlot.reorderTrees(idxSort); % sorted by cell types as in IN-IN connectome

    assert(all(cellfun(@(x,y) contains(x,y), skelToPlot.names, ids)))
    disp(table(skelToPlot.names, ids)) % check sortedness
    
    somaCoords = Util.getNodeCoordsWithComment(skelToPlot, 'cellbody', 'exact');

    % get corresponding colors of sorted somata in same order as ids
    allColors = Util.getSomaColors;
    Util.log('Read cell types manually assigned')
    xlsfile = config.cellTypesIN;
    [~,~,xl1] = xlsread(xlsfile);
    xlTable1 = cell2table(xl1);
    xlTable1(1,:) = [];
    cellTypesAll = cell2mat(xlTable1.xl16);
    
    somaColors = allColors(cellTypesAll(cellfun(@(x) str2double(x), ids)));
    somaColors = cell2mat(somaColors');
    somaColors = somaColors(:,1:3);
end


function a = doPlotting(skel, somaColors, somaCoords)
    scaleEM = [11.24; 11.24; 30];
    somaSize = 150; tubeSize = 2; dotSize= 3;
    somaBallSize = 50;
    synColor = [0,0,0];
    tcColor = [230,159,0] ./255;

    if ~exist('isPC','var') | isempty(isPC)
        isPC = false;
    end
    if isPC
       somaSize = dotSize;
    end

    % define layer 4 borders in z
    bboxLine = [-3000,30000;1000,30000;-6000,14000];
    bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);

    layer4_down = 2781; % older %3205; % [103, 46, 325]
    layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas

    fig = figure;
    fig.Color = 'none';

    Util.log('Now plotting figure....');
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    for k = 1:2
        a(k) = subplot(2, 1, k);
        hold on
        if exist('skel','var') & ~isempty(skel)
            skel.plot('', somaColors, true, tubeSize);
        end
        objectSomas = ...
                    scatter3(somaCoords(:,1),somaCoords(:,2),somaCoords(:,3),...
                    somaSize, somaColors, 'filled');
        if exist('synCoords','var') & ~isempty(synCoords)
            objectSyns = ...
                        scatter3(synCoords(:,1),synCoords(:,2),synCoords(:,3),...
                        somaBallSize, 'MarkerEdgeColor', synColor(1:3), ...
                        'MarkerFaceColor', 'none', ...
                    'Marker','v', 'MarkerEdgeAlpha', 0.8, 'LineWidth', 0.3);
        end
        if exist('tcCoords','var') & ~isempty(tcCoords)
            objectTC = ...
                        scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3),...
                        somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                        'MarkerFaceColor', 'none', ...
                        'Marker','v', 'MarkerEdgeAlpha', 1, 'LineWidth',0.5);
        end

        switch k
           case 1
               %xy
               Util.setView(6); % same as fig2
           case 2
               %yz
               Util.setView(5); % same as fig2
                % border of L4 upper and down
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
        end
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
        axis equal
        axis off
        box off
        a(k).XLim = bbox(1,:);
        a(k).YLim = bbox(2,:);
        a(k).ZLim = bbox(3,:);
    end
    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');

    % scalebar
    scaleBarLength = 50; % um
    line(a(k),[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis
end

