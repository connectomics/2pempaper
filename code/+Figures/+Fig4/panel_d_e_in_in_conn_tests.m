% panel with in to in connectome for 4 major types only: Do tests on innervation comparison WB - LB, LB - WB, Lb-LB
setConfig;
somaColors = Util.getSomaColors;
load(config.connmat_SLmanual,'synMapININ','cellTypesAll','preClassIds','postClassIds')

% main IN types
cellTypesKeep = [1,2,3,4, 5,7];

% sort index for IN types
[cellTypesAll,idxSort] = sort(cellTypesAll);
outMap = synMapININ(idxSort,idxSort);

% full pre axons
idxFull = postClassIds.i_full(idxSort);
cellTypesAllPre = cellTypesAll(idxFull);
outMap = outMap(idxFull,:);

% keep pre side main IN types only
idxKeep = ismember(cellTypesAllPre, cellTypesKeep);
outCellTypesPre = cellTypesAllPre(idxKeep);
outMap = outMap(idxKeep,:);

% keep post side main IN types only
idxKeep = ismember(cellTypesAll, cellTypesKeep);
outCellTypesPost = cellTypesAll(idxKeep);
outMap = outMap(:,idxKeep);

assert(size(outMap,1) == numel(outCellTypesPre))
assert(size(outMap,2) == numel(outCellTypesPost))

% post
idxWB = outCellTypesPost == 1;
idxLB = outCellTypesPost == 2;
idxL4 = outCellTypesPost == 3;
idxNG = outCellTypesPost == 4;
idxBP = outCellTypesPost == 5;
idxNonL4 = outCellTypesPost == 7;

% pre
idxWBPre = outCellTypesPre == 1;
idxLBPre = outCellTypesPre == 2;
idxL4Pre = outCellTypesPre == 3;
idxNGPre = outCellTypesPre == 4;
idxBPPre = outCellTypesPost == 5;
idxNonL4Pre = outCellTypesPost == 7;

% do statistical tests  on innervation matrices
allNames = {'WB','LB','L4','NG','BP','NonL4'};
allIdxPre = {idxWBPre, idxLBPre, idxL4Pre, idxNGPre, idxBPPre, idxNonL4Pre};
allIdxPost = {idxWB, idxLB, idxL4, idxNG, idxBP, idxNonL4};
data = struct;
for i=1:numel(allNames)
    for j=1:numel(allNames)
        curVar = sprintf('synData%s%s', allNames{i}, allNames{j});
        data.(curVar) = outMap(allIdxPre{i}, allIdxPost{j});
    end
end

%{
% for text
x = [data.synDataWBLB(:); data.synDataLBLB(:); data.synDataNGLB(:); data.synDataL4LB(:)];
curIdxWBLB = 1:numel(data.synDataWBLB(:));
curIdxLBLB = curIdxWBLB(end)+(1:numel(data.synDataLBLB(:)));
curIdxNGLB = curIdxLBLB(end)+(1:numel(data.synDataNGLB(:)));
curIdxL4LB = curIdxNGLB(end)+(1:numel(data.synDataL4LB(:)));

pVal_WBLB_LBWB = ranksum(x(curIdxWBLB),x(curIdxLBWB));
pVal_WBLB_LBLB = ranksum(x(curIdxWBLB),x(curIdxLBLB));
pVal_LBWB_LBLB = ranksum(x(curIdxLBLB),x(curIdxLBLB));
%}

Util.log('box plot')
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on;

% for box plot
x = [data.synDataWBLB(:); data.synDataLBWB(:); data.synDataWBWB(:); data.synDataLBLB(:); data.synDataNGLB(:); data.synDataL4LB(:)];
curIdxWBLB = 1:numel(data.synDataWBLB(:));
curIdxLBWB = curIdxWBLB(end)+(1:numel(data.synDataLBWB(:)));
curIdxWBWB = curIdxLBWB(end)+(1:numel(data.synDataWBWB(:)));
curIdxLBLB = curIdxWBWB(end)+(1:numel(data.synDataLBLB(:)));
curIdxNGLB = curIdxLBLB(end)+(1:numel(data.synDataNGLB(:)));
curIdxL4LB = curIdxNGLB(end)+(1:numel(data.synDataL4LB(:)));

g = cell(numel(x),1);
g(curIdxWBLB) = {'wb-lb'}; % 1st box
g(curIdxLBWB) = {'lb-wb'}; % 2nd box
g(curIdxWBWB) = {'wb-wb'}; % 3rd box
g(curIdxLBLB) = {'lb-lb'}; % 4th box
g(curIdxNGLB) = {'ng-lb'}; % 5th box
g(curIdxL4LB) = {'l4-lb'}; % 6th box

% directed inh difference
pVal_WBLB_LBWB = ranksum(x(curIdxWBLB),x(curIdxLBWB));

% self inh difference in each group
pVal_WBWB_LBLB = ranksum(x(curIdxWBWB),x(curIdxLBLB));

% others
pVal_WBLB_LBLB = ranksum(x(curIdxWBLB),x(curIdxLBLB));
pVal_LBWB_WBWB = ranksum(x(curIdxLBWB),x(curIdxWBWB)); 

% for text to show no significant inh into LBINs
pVal_NGLB_LBLB = ranksum(x(curIdxNGLB),x(curIdxLBLB));
pVal_L4LB_LBLB = ranksum(x(curIdxL4LB),x(curIdxLBLB));

% keep only first 4 boxes
x = x(1:curIdxLBLB(end));
g = g(1:curIdxLBLB(end));

%{
rng(0)
colors = cat(1,[0.5,0.5,0.5],... % WBLB
               [0.5,0.5,0.5],... % LBWB
               [0.5,0.5,0.5]); % LBLB

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(3, startS);
scatter(widthS.*rand(numel(curIdxWBLB),1)+posS(1), x(curIdxWBLB),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLBWB),1)+posS(2), x(curIdxLBWB),...
    100,colors(2,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLBLB),1)+posS(3), x(curIdxLBLB),...
    100,colors(3,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
set(h,{'linew'},{4});
%}

violinplot(x,g,'GroupOrder',{'wb-lb','lb-wb','wb-wb','lb-lb'},'ViolinColor',[0,0,0], 'MedianColor', [0,0,0], 'ViolinAlpha', 0.1, 'BoxColor',[0,0,0])
sprintf('p-value RS = WBLB-LBWB %f; WBWB-LBLB %f', pVal_WBLB_LBWB, pVal_WBWB_LBLB)
sprintf('p-value RS = WBLB-LBLB %f; LBWB-WBWB %f', pVal_WBLB_LBLB, pVal_LBWB_WBWB)
%sprintf('No sign inh onto LB \n p-value RS = WBLB-LBLB %f, NGLB-LBLB %f, L4LB-LBLB %f', pVal_WBLB_LBLB, pVal_NGLB_LBLB, pVal_L4LB_LBLB)

ylabel('# synapses')
yLimits = [0,14];
yTicks = [0:2:14];
yMinorTicks = [1:1:14];
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
%set(gcf,'Position',get(0, 'Screensize'))
set(gca,'FontSize',30)
Util.setPlotDefault(gca,'','');
%daspect([2,1,1])
outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_conn_tests.pdf');
% export_fig(outfile, '-q101', '-nocrop', '-m8');
% close all

% export to python
outData = struct;
outData(1).x = x;
outData(1).g = g;


%% for text
d = data.synDataWBLB;
sprintf('WB -> LB, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

d = data.synDataLBWB;
sprintf('LB -> WB, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

d = data.synDataNGWB;
sprintf('NG -> WB, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

d = data.synDataWBNG;
sprintf('WB -> NG, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

d = data.synDataNGLB;
sprintf('NG -> LB, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

d = data.synDataLBNG;
sprintf('LB -> NG, Mean, s.d., N: %f, %f, %d', mean(d(:)), std(d(:)), numel(d))

%% grouped inh
gWB = [data.synDataLBWB(:); data.synDataL4WB(:); data.synDataNGWB(:); data.synDataBPWB(:); data.synDataNonL4WB(:)];
gLB = [data.synDataWBLB(:); data.synDataL4LB(:); data.synDataNGLB(:); data.synDataBPLB(:); data.synDataNonL4LB(:)];
gL4 = [data.synDataWBL4(:); data.synDataLBL4(:); data.synDataNGL4(:); data.synDataBPL4(:); data.synDataNonL4L4(:)];
gNG = [data.synDataWBNG(:); data.synDataLBNG(:); data.synDataL4NG(:); data.synDataBPNG(:); data.synDataNonL4NG(:)]; 
gBP = [data.synDataWBBP(:); data.synDataLBBP(:); data.synDataL4BP(:); data.synDataNGBP(:); data.synDataNonL4BP(:)];
gNonL4 = [data.synDataWBNonL4(:); data.synDataLBNonL4(:); data.synDataL4NonL4(:); data.synDataNGNonL4(:); data.synDataBPNonL4(:)];

pWL = ranksum(gWB,gLB); 
p4L = ranksum(gL4, gLB);
pNL = ranksum(gNG, gLB);
pBL = ranksum(gBP, gLB);
pNL = ranksum(gNonL4, gLB);

sprintf('Grouped inh: p WB-LB %f, L4-LB %f, NG-LB %f, BP-LB %f, nonL4-LB %f', pWL, p4L, pNL, pBL, pNL)

% WBNG vs NG WB
pval_WBNG_NGWB = ranksum(data.synDataWBNG(:), data.synDataNGWB(:));
sprintf('p-value RS = WBNG-NGWB %f', pval_WBNG_NGWB)

% plot box to show group input inhibition
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on;

x = [gWB(:); gLB(:); gL4(:); gNG(:); gBP(:); gNonL4(:)];
curIdxWB = 1:numel(gWB(:));
curIdxLB = curIdxWB(end)+(1:numel(gLB(:)));
curIdxL4 = curIdxLB(end)+(1:numel(gL4(:)));
curIdxNG = curIdxL4(end)+(1:numel(gNG(:)));
curIdxBP = curIdxNG(end)+(1:numel(gBP(:)));
curIdxNonL4 = curIdxBP(end)+(1:numel(gNonL4(:)));

g = cell(numel(x),1);
g(curIdxWB) = {'wb'};
g(curIdxLB) = {'lb'};
g(curIdxL4) = {'l4'};
g(curIdxNG) = {'ng'};
g(curIdxBP) = {'BP'};
g(curIdxNonL4) = {'nonL4'};

%{
rng(0)
colors = repmat([0.5,0.5,0.5], 6,1);

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(6, startS);
scatter(widthS.*rand(numel(curIdxWB),1)+posS(1), x(curIdxWB),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLB),1)+posS(2), x(curIdxLB),...
    100,colors(2,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxL4),1)+posS(3), x(curIdxL4),...
    100,colors(3,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxNG),1)+posS(4), x(curIdxNG),...
    100,colors(4,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxBP),1)+posS(5), x(curIdxBP),...
    100,colors(5,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxNonL4),1)+posS(6), x(curIdxNonL4),...
    100,colors(6,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
set(h,{'linew'},{4});
%}
violinplot(x,g,'GroupOrder',{'wb','lb','l4','ng','BP','nonL4'},'ViolinColor',[0,0,0], 'MedianColor', [0,0,0], 'ViolinAlpha', 0.1, 'BoxColor',[0,0,0])
ylabel('# synapses')
yLimits = [0,14];
yTicks = [0:2:14];
yMinorTicks = [1:1:14];
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
set(gca,'FontSize',30)
Util.setPlotDefault(gca,'','');
%daspect([2,1,1])
outfile = fullfile(outDir,'outData/figures','fig5',config.version,'lbin_no_sign_inh.pdf');
% export_fig(outfile, '-q101', '-nocrop', '-m8');
% close all

% export
outData(2).x = x;
outData(2).g = g;
Util.save(fullfile(outDir, 'outData/figures','fig5',config.version,'data_in_in_conn_tests.mat'), outData) 

% sprintf('Mean output from WB')
% disp(mean(data.synDataWBLB,2))
% sprintf('Mean output from LB')
% disp(mean(data.synDataLBWB,2))
% 
% sprintf('Mean input onto WB')
% disp(mean(data.synDataLBWB,1))
% sprintf('Mean input onto LB')
% disp(mean(data.synDataWBLB,1))
