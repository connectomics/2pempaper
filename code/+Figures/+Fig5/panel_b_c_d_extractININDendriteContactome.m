% extract IN-IN dendrite contactome based on proximity
setConfig;

outDirPdf = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version]);
Util.log('Parsing cffi skeleton...')
%skel = skeleton(fullfile(outDir,'outData','figures','fig5',config.version,'skel_in_in_dendrite_proximities_1um.nml'));
skel = skeleton(config.skelDendContactome);

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);
%warning('Doing for 43 IN types only')
%cellTypesAll = cellTypesAll(1:43);
clear xlTable1 xlsfile xl1

treeNames = skel.names;
postClassNames = {'dend_IN'};
postClassNamesSearchStr = {'i'}; 
postClassIds = struct('i',[],'iLocs',[]);
INgroupNames = {'BIN1', 'BIN2','L4IN','NGFC','InvPyr','L4Other','nonL4','OtherBarrel','unclassified'};

% get dend Ids from cell names for each post class
for i=1:numel(postClassNames)
    dendLocs = find(cellfun(@(x) contains(x,['cell ' postClassNamesSearchStr{i}]),treeNames));
    dendIds = cellfun(@(x) regexp(x,['cell ' postClassNamesSearchStr{i} '(?<id>\w+)'],'names'),treeNames(dendLocs),'uni',0);
    dendIds = cellfun(@(x) x.id,dendIds,'uni',0);
    [dendIds, idxSort] = sort(dendIds);
    
    postClassIds.(postClassNamesSearchStr{i}) = cellfun(@(x) [postClassNames{i} x] ,dendIds,'uni',0); % dend_INxx?
    postClassIds.([postClassNamesSearchStr{i} 'Locs']) = dendLocs(idxSort);
    clear dendIds idxSort dendLocs
end

% extract cell types for INs that are present in cffi nml
postClassINIds = cellfun(@(x) regexp(x,'IN(\d+)','tokens'), postClassIds.i);
postClassINIds = cellfun(@(x) str2double(x{1}), postClassINIds);
cellTypesAll = cellTypesAll(postClassINIds);

Util.log('IN to IN')
[synMapININ, shaftMapININ,somaMapININ] = Conn.getPostConnectome(skel, postClassIds.i, postClassIds.i, postClassIds.iLocs, 'dend_IN','dend_IN', outDirPdf);
%shaftMapININ_grouped = Conn.groupByTypes(shaftMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'shaftSyn',outDirPdf);
%somaMapININ_grouped = Conn.groupByTypes(somaMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'somaSyn',outDirPdf);
synMapININ_grouped = Conn.groupByTypes(synMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'dend_IN', 'dend_IN', 'full', outDirPdf);

% do min to create symmetric matrix because of hiwi mistakes
u = triu(synMapININ,1);
l = tril(synMapININ,-1);
ul_min = min(u',l);
synMapININ = ul_min + ul_min';

% show only >=2 sites : see mh comments N=1
%synMapININ = synMapININ .* (synMapININ>1);

% sort index for IN types
[cellTypesAll,idxSort] = sort(cellTypesAll);

% sort outMap
synMap = synMapININ(idxSort,idxSort);

% sort IN Ids
postIds = postClassIds.i(idxSort);
postIds = cellfun(@(x) x(8:9), postIds,'uni',0);
postIdsPre = postIds;

% text
outMap = synMap;
outMap = outMap( triu(true(size(outMap))) ); % convert to upper triangular array
allPairs = numel(outMap);
sprintf('Mean +-std: %f +- %f (%d)', mean(outMap(:)), std(outMap(:)), allPairs)
sprintf('More than one apposition in IN pairs: %.2f %% (%d / %d)', 100*sum(outMap(:)>1)/allPairs, sum(outMap(:)>1), allPairs)
sprintf('Zero appositions in IN pairs: %.2f %% (%d / %d)', 100*sum(outMap(:)==0)/allPairs, sum(outMap(:)==0), allPairs)
sprintf('1 apposition in IN pairs: %.2f %% (%d / %d)', 100*sum(outMap(:)==1)/allPairs, sum(outMap(:)==1), allPairs)

% plot heatmap from data
outMap = triu(synMap);
colorMap = colormap(jet);
colorMap(1,:) = [1,1,1];
%climits = [0.0001, max(outMap(:))];
h = heatmap(postIds, postIdsPre, outMap, 'Colormap',colorMap,...
            'MissingDataColor', 'w', 'GridVisible', 'off', 'MissingDataLabel', " ", ...
            'FontSize',30);
axp = struct(gca);
axp.Axes.XAxisLocation = 'bottom';
axp.XAxis.TickLabelRotation = 45;
outfile = fullfile(outDir,'outData/figures/fig5',config.version,'in_in_sorted_dendrite_contactome.eps');
set(gcf,'Position',get(0, 'Screensize'))
colorbar('Location','eastoutside')
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

Conn.plotConnWithHist(outMap,'IN','IN',postIdsPre, postIds,'IN-IN', false, true,...
        fullfile(outDir,'outData/figures/fig5',config.version,'in_in_sorted_dendrite_contactome_with_hist.eps'), false);
% close all

% group by types main IN types
outMap = synMap; % symmetric
%cellTypesKeep = [1,2,3,4, 5,7]; 
cellTypesKeep = [1,2,3,7];  % remove BP and NGFC due to low N

% keep pre side main IN types only
idxKeep = ismember(cellTypesAll, cellTypesKeep);
outCellTypesPre = cellTypesAll(idxKeep);
outMap = outMap(idxKeep,:);

% keep post side main IN types only
idxKeep = ismember(cellTypesAll, cellTypesKeep);
outCellTypesPost = cellTypesAll(idxKeep);
outMap = outMap(:,idxKeep);

assert(size(outMap,1) == numel(outCellTypesPre))
assert(size(outMap,2) == numel(outCellTypesPost))

% plot grouped contactome avg n of syn per cell pair per type pair
outAvgSyn = zeros(numel(cellTypesKeep),numel(cellTypesKeep));
for i = 1:numel(cellTypesKeep)
    curPre = cellTypesKeep(i);
    for j = 1:numel(cellTypesKeep)
        curPost = cellTypesKeep(j);
        curMap = outMap(outCellTypesPre==curPre, outCellTypesPost==curPost); % Not symmetric

        if i==j
            curMap = curMap( triu(true(size(curMap)),1) ); % exclude diagonal and symmetric part
            outAvgSyn(i,j) = mean(curMap);
        else
            outAvgSyn(i,j) = mean(curMap(:)); % nonsym: avg number of touches per cell type pair
        end
    end
end

Util.log('Round off:')
format bank
outAvgSyn = round(outAvgSyn,2);
% upper triangular only
outAvgSynUT = triu(outAvgSyn);
Util.log('plot heatmap from data')
colorMap = jet(64);
colorMap(1,:) = [1,1,1];
%climits = [0.0001, 13];
%colorMap = gray(64);
h = heatmap(cellTypesKeep, cellTypesKeep, outAvgSynUT, 'Colormap',colorMap,...
            'MissingDataColor', 'w', 'GridVisible', 'off', 'MissingDataLabel', " ", ...
            'FontSize',30);
outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_dendritecontactome_avg.eps');
%set(gcf,'Position',get(0, 'Screensize'))
%colorbar off
colorbar('Location','eastoutside')
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

% text for manuscript
i = 2;
curMap1 = outMap(outCellTypesPre==i, outCellTypesPost==i);  % LB-LB
curMap1 = curMap1( triu(true(size(curMap1))) );
sprintf('2-2: %.2f+-2.f', mean(curMap1), std(curMap1))
for i = cellTypesKeep
    for j = cellTypesKeep
        if i==2 & j==2; continue; end
        if i~=j; continue; end
        curMap2 = outMap(outCellTypesPre==i, outCellTypesPost==j);
        curMap2 = curMap2( triu(true(size(curMap2))) );
        curP = ranksum(curMap1(:), curMap2(:));
        sprintf('%d-%d vs %d-%d: %.2f +- %.2f (p=%.4f)', 2,2, i,j, mean(curMap2), std(curMap2), curP)
    end
end


% plot grouped contactome fraction of 0 touches
outAvgSyn = zeros(numel(cellTypesKeep),numel(cellTypesKeep));
for i = 1:numel(cellTypesKeep)
    curPre = cellTypesKeep(i);
    for j = 1:numel(cellTypesKeep)
        curPost = cellTypesKeep(j);
        curMap = outMap(outCellTypesPre==curPre, outCellTypesPost==curPost); % Not symmetric

        if i==j
            curMap = curMap( triu(true(size(curMap)),1) ); % exclude diagonal and symmetric part
            outAvgSyn(i,j) = sum(curMap(:)==0)/numel(curMap);
        else
            outAvgSyn(i,j) = sum(curMap(:)==0)/numel(curMap(:)); % nonsym: binary contactome based only
        end
    end
end

Util.log('Round off:')
format bank
outAvgSyn = round(outAvgSyn,2);

% upper triangular only
outAvgSynUT = triu(outAvgSyn);

Util.log('plot heatmap from data')
colorMap = jet(64);
colorMap(1,:) = [1,1,1];
%climits = [0.0001, 13];
%colorMap = gray(64);
h = heatmap(cellTypesKeep, cellTypesKeep, outAvgSynUT, 'Colormap',colorMap,...
            'MissingDataColor', 'w', 'GridVisible', 'off', 'MissingDataLabel', " ", ...
            'FontSize',30);
outfile = fullfile(outDir,'outData/figures','fig5',config.version,'in_in_sorted_dendritecontactome_avg_touches.eps');
%set(gcf,'Position',get(0, 'Screensize'))
%colorbar off
colorbar('Location','eastoutside')
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

% Util.log('Saving data...')
% save(fullfile(outDirPdf,['extractININDendriteConnectome.mat']));
