% plot to quantify TC syn inputs for WB and LB from connectome
setConfig;
somaColors = Util.getSomaColors;
load(config.connmat_SLmanual,'synMapTCIN', 'cellTypesAll')
setConfig;
%{
Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);
warning('Doing for 43 IN types only')
cellTypesAll = cellTypesAll(1:43);
%}
% TC to IN: idx for WB, idx for LB
% keep post side main IN types only
cellTypesKeep = [1,2,3,4, 5,7];
idxKeep = ismember(cellTypesAll, cellTypesKeep);
outCellTypesPost = cellTypesAll(idxKeep);
synMapTCIN = synMapTCIN(:,idxKeep);
idxWB = outCellTypesPost == 1;
idxLB = outCellTypesPost == 2;


%idxWB = cellTypesAll == 1;
%idxLB = cellTypesAll == 2;

%% per group
synDataWB = reshape(synMapTCIN(:,idxWB),1,'');
synDataLB = reshape(synMapTCIN(:,idxLB),1,'');
rng(0)

%{
% keep it for pVal calculation
Util.log('box plot')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
x = [synDataWB(:); synDataLB(:)];
curIdxWB = 1:numel(synDataWB);
curIdxLB = curIdxWB(end)+(1:numel(synDataLB));
g = cell(numel(x),1);
g(curIdxWB) = {'wholeBarrel'}; % first box
g(curIdxLB) = {'lowerBarrel'}; % second box
pValINAnova = anova1(x(curIdxWB(1):end),g(curIdxWB(1):end),'off'); % WB vs LB
pValINRS = ranksum(x(curIdxWB),x(curIdxLB)); % WB vs LB

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
scatter(widthS.*rand(numel(curIdxWB),1)+posS(1), x(curIdxWB),...
    100,'o','filled', 'MarkerFaceColor',somaColors{1}(1:3),'MarkerEdgeColor',somaColors{1}(1:3));
scatter(widthS.*rand(numel(curIdxLB),1)+posS(2), x(curIdxLB),...
    100,'o','filled', 'MarkerFaceColor',somaColors{2}(1:3),'MarkerEdgeColor',somaColors{2}(1:3));

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos,'Jitter',1);
set(h,{'linew'},{4})

sprintf('p-value BINs with zeros included :Anova-%f, RS=%f',pValINAnova,pValINRS)
%ylabel('# synapses per TC')
yLimits = [0,10];
yTicks = [0,5,10];
yMinorTicks = [1:4, 6:9];
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
ax.TickLength = [0.025,0.025];
% daspect([2,1,1])
set(gca,'FontSize',30)
Util.setPlotDefault(gca,'','');
export_fig(fullfile(outDir,'outData','figures','fig4',config.version,'panel_tc_to_bins.eps'),'-q101', '-nocrop','-transparent');
close all
%}
%% per cell overlaid in group
synDataWB = synMapTCIN(:,idxWB);
synDataLB = synMapTCIN(:,idxLB);

synDataWB = arrayfun(@(col) nonzeros(synDataWB(:,col)), 1:size(synDataWB,2),'uni',0); % remove zeros
synDataLB = arrayfun(@(col) nonzeros(synDataLB(:,col)), 1:size(synDataLB,2),'uni',0); % remove zeros

Util.log('box plot')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
x = [vertcat(synDataWB{:}); vertcat(synDataLB{:})];
curIdxWB1 = 1:size(synDataWB{1},1);
curIdxWB2 = curIdxWB1(end)+(1:size(synDataWB{2},1));
curIdxWB3 = curIdxWB2(end)+(1:size(synDataWB{3},1));
curIdxWB4 = curIdxWB3(end)+(1:size(synDataWB{4},1));

curIdxLB1 = curIdxWB4(end)+(1:size(synDataLB{1},1));
curIdxLB2 = curIdxLB1(end)+(1:size(synDataLB{2},1));
curIdxLB3 = curIdxLB2(end)+(1:size(synDataLB{3},1));
curIdxLB4 = curIdxLB3(end)+(1:size(synDataLB{4},1));

g = cell(numel(x),1);
g(curIdxWB1) = {'wb'}; % 1st box
g(curIdxWB2) = {'wb'}; % 1st box
g(curIdxWB3) = {'wb'};
g(curIdxWB4) = {'wb'}; 
g(curIdxLB1) = {'lb'}; % 2nd box
g(curIdxLB2) = {'lb'}; % 2nd box
g(curIdxLB3) = {'lb'}; % 2nd box
g(curIdxLB4) = {'lb'}; % 2nd box
rng(0)
colors = cat(1,Util.getRangedColors(0,0,0.4,0.8,0,0.4,4),... % WB
               Util.getRangedColors(0.9,1,0.4,0.8,0,0.3,4)); % LB

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(2, startS);
scatter(widthS.*rand(numel(curIdxWB1),1)+posS(1), x(curIdxWB1),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxWB2),1)+posS(1), x(curIdxWB2),...
    100,colors(2,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxWB3),1)+posS(1), x(curIdxWB3),...
    100,colors(3,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxWB4),1)+posS(1), x(curIdxWB4),...
    100,colors(4,1:3),'filled','o');

scatter(widthS.*rand(numel(curIdxLB1),1)+posS(2), x(curIdxLB1),...
    100,colors(5,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLB2),1)+posS(2), x(curIdxLB2),...
    100,colors(6,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLB3),1)+posS(2), x(curIdxLB3),...
    100,colors(7,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxLB4),1)+posS(2), x(curIdxLB4),...
    100,colors(8,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
set(h,{'linew'},{2})
% add mean line for two boxes
mu1 = mean(x([curIdxWB1(:);curIdxWB2(:); curIdxWB3(:); curIdxWB4(:)]));
mu2 = mean(x([curIdxLB1(:);curIdxLB2(:); curIdxLB3(:);curIdxLB4(:)]));
plot([posS(1), posS(1)+widthS], [mu1,mu1], '-r','LineWidth',4)
plot([posS(2), posS(2)+widthS], [mu2,mu2], '-r','LineWidth',4)

pValINRS = ranksum(x([curIdxWB1(:);curIdxWB2(:); curIdxWB3(:); curIdxWB4(:)]),...
                    x([curIdxLB1(:);curIdxLB2(:); curIdxLB3(:);curIdxLB4(:)])); % WB vs LB

sprintf('p-value BINs excluding zeros: RS=%f',pValINRS)

%ylabel('# synapses per TC')
yLimits = [0,10];
yTicks = [0:5:10];
yMinorTicks = 0:10;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
ax.TickLength = [0.025,0.025];
set(gca,'FontSize',30)
Util.setPlotDefault(gca,'','');
daspect([4,4,1])
% export_fig(fullfile(outDir,'outData','figures','fig4',config.version,'panel_tc_to_bins_per_cell.eps'),'-q101', '-nocrop','-transparent');
% close all

%% panel tc to bins per cell as histogram
synDataWB = synMapTCIN(:,idxWB);
synDataLB = synMapTCIN(:,idxLB);
Util.log('hist plot')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
% per IN per color
%rng(0)
%colors = cat(1,Util.getRangedColors(0,0,0.4,0.8,0,0.4,2),... % WB
%               Util.getRangedColors(0.9,1,0.4,0.8,0,0.3,4)); % LB
%d = horzcat(synDataWB,synDataLB);
%for i=1:size(d,2)
%    hgram = histogram(d(:,i), 'BinWidth', 1,...
%    'DisplayStyle','stairs','LineWidth',3,'EdgeColor',colors(i,:));
%end     
% per group per color
colors = cat(1,somaColors{1}(1:3),... % WB
               somaColors{2}(1:3)); % LB
d = {nonzeros(synDataWB(:)), nonzeros(synDataLB(:))};
for i=1:numel(d)
    hgram = histogram(d{i}, 'BinWidth', 1,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',3,'EdgeColor',colors(i,:));
end
set(gca,'XDir','reverse','TickDir','out','YAxisLocation','right')
camroll(-90);
ax.XAxis.TickValues = 0:5:10;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = 0:1:10;
ax.XAxis.Limits = [0,10];

ax.YAxis.TickValues = 0:0.5:1;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:0.1:1;
ax.YAxis.Limits = [0,1];

ax.LineWidth = 4;
ax.TickLength = [0.025,0.025];
set(gca,'FontSize',30)
Util.setPlotDefault(gca,'','');
daspect([5,1,1])
% export_fig(fullfile(outDir,'outData','figures','fig4',config.version,'panel_tc_to_bins_per_cell_hist.eps'),...
%     '-q101', '-nocrop','-transparent');
% close all

%% Histogram over tc to WB/LB conn inset
synDataWB = synMapTCIN(:,idxWB);
synDataLB = synMapTCIN(:,idxLB);
Util.log('hist of conn:')
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on;

% per group per color
colors = cat(1,somaColors{1}(1:3),... % WB
               somaColors{2}(1:3)); % LB
d = horzcat(sum(synDataWB,2), sum(synDataLB,2));
for i=1:size(d,2)
    barh(d(:,i),'FaceColor','none','EdgeColor',colors(i,:),'LineWidth',2);
end  
set(gca,'XDir','reverse','TickDir','out','YAxisLocation','right','XAxisLocation','top')
camroll(-180);
ax.XAxis.TickValues = 0:5:15;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = 0:1:15;
ax.XAxis.Limits = [0,15];

ax.YAxis.TickValues = [1,5,10,15,20];
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 1:1:20;
ax.YAxis.Limits = [0,21];
%set(gca,'YColor','none')
ax.LineWidth = 2;
set(gca,'FontSize',20)
Util.setPlotDefault(gca,'','');
daspect([1,0.5,1])
% export_fig(fullfile(outDir,'outData','figures','fig4',config.version,'panel_tc_to_bins_hist_conn_inset.pdf'),...
%     '-q101', '-nocrop');
% close all

%% Fraction of tc connections that exists for INs: Sanity check
data = synMapTCIN(:,idxWB);
fracDataWB =  sum(data>0,1) ./ repmat(size(data,1),1,size(data,2));
data = synMapTCIN(:,idxLB);
fracDataLB =  sum(data>0,1) ./ repmat(size(data,1),1,size(data,2));

Util.log('box plot')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
x1 = fracDataWB; x2 = fracDataLB;
x = [x1(:); x2(:)];
curIdxx1 = 1:numel(x1);
curIdxx2 = curIdxx1(end)+(1:numel(x2));

g = cell(numel(x),1);
g(curIdxx1) = {'x1'}; % 1st box
g(curIdxx2) = {'x2'}; % 2nd box

pValINAnova = anova1(x(curIdxx1(1):end),g(curIdxx1(1):end),'off'); % WB vs LB
pValINRS = ranksum(x(curIdxx1),x(curIdxx2)); % WB vs LB

rng(0)
colors = cat(1,somaColors{1}(1:3),... % WB
               somaColors{2}(1:3)); % LB

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(2, startS);
scatter(widthS.*rand(numel(curIdxx1),1)+posS(1), x(curIdxx1),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdxx2),1)+posS(2), x(curIdxx2),...
    100,colors(2,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','kx');
set(h,{'linew'},{2})
% add mean line for two boxes
mu1 = mean(x(curIdxx1(:)));
mu2 = mean(x(curIdxx2(:)));
plot([posS(1), posS(1)+widthS], [mu1,mu1], '-','Color',[1,0,0,0.5],'LineWidth',4)
plot([posS(2), posS(2)+widthS], [mu2,mu2], '-','Color',[1,0,0,0.5],'LineWidth',4)

sprintf('p-value BINs:Anova-%f, RS=%f',pValINAnova,pValINRS)
yLimits = [0,1];
yTicks = [0:0.5:1];
yMinorTicks = [0:0.1:1];
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
ax.TickLength = [0.025,0.025];
set(gca,'FontSize',20)
Util.setPlotDefault(gca,'','');
daspect([12,1,2])
% export_fig(fullfile(outDir,'outData','figures','fig4',config.version,'panel_tc_to_bins_fraction_tc_conns.eps'),'-q101', '-nocrop','-transparent');
% close all


%% Text output
synThr = 2;
sprintf('Fraction of inputs with >2 TC synapses:')
data = synDataWB;
sprintf('WB: %.02f',sum(data>synThr)/size(data,1))
data = synDataLB;
sprintf('LB: %.02f',sum(data>synThr)/size(data,1))

sprintf('Fraction of inputs with >2 TC synapses. Excluding zero syn connections:')
data = synDataWB(synDataWB>0);
sprintf('WB: %.02f',sum(data>synThr)/size(data,1))
data = synDataLB(synDataLB>0);
sprintf('LB: %.02f',sum(data>synThr)/size(data,1))

function [out, m, u, n] = doForMat(in)
% sum along columns for nonzero entries. Find per stp #syn/tc where tc connection exists
for curCol = 1:size(in,2)
    thisCol = in(:,curCol);
    idxNonZero = thisCol>0;
    count = sum(thisCol(idxNonZero));
    if any(idxNonZero)
        out(curCol) = count/sum(idxNonZero);
        assert(out(curCol) >= 1) % #syn per tc must be >=1 for nonzero connections only
    else
         out(curCol) = 0;
    end
    m(curCol) = mean(thisCol(idxNonZero));
    u(curCol) = std(thisCol(idxNonZero));
    n(curCol) = sum(idxNonZero);
end
end

