% panel g: One whole barrel IN with axon and its targetted soma locations.
close all

setConfig;
outDir = config.outDir;

curCellIdsToPlot = [2,4]; % BIN1, BIN2

restrictBarrel = true;
restrictSoma = true;

Rmin = 0;
Rmax = 0.2;
Gmin = 0.5;
Gmax = 0.9;
Bmin = 0.3;
Bmax = 0.75;
singleColor = [0, 158, 115]./255;

somaColors = Util.getSomaColors;
Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN; 
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);
axonNamesInTable = vertcat(xlTable1.xl13);
dendNamesInTable = vertcat(xlTable1.xl12);
numCellTypes = numel(unique(cellTypesAll));

somaSize = 150; tubeSize = 2; dotSize= 3; somaTargetSize = 150;

outDirPdf = fullfile(config.outDir,'outData/figures/fig2/', config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
% load connectome data
load(config.connmat_SLmanual,'synMapININ','synMapINStp','synMapINSps', 'somaMapINStp', 'somaMapINSps', 'somaMapININ')

if restrictSoma
    sprintf('Restricting analysis to soma synapses...\n')
    synMapINStp = somaMapINStp;
    synMapINSps = somaMapINSps;
    synMapININ = somaMapININ;
end

if restrictBarrel
    Util.log('Restrict to ExNs inside barrel')
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
    stpOut = find(~INStp);
    spsOut = find(~INSps);
else
    stpOut = []; % remove nothing
    spsOut = []; % remove nothing
end

Util.log('EXcluding py3 in conn')
idsPy3 = Util.getPy3Ids;

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

Util.log('Loading nml with axon and dend tracings...')
nml = config.skelINCombined;
skel = skeleton(nml);
skel = skel.replaceComments('soma','cellbody','complete');
skel = skel.replaceComments('_in','cellbody','partial','complete');

% remove extra nodes
skel = skel.deleteNodesWithComment('-','exact');
skel = skel.deleteNodesWithComment('IN\d\d so','regexp'); % extra nodes not to measure for path length. see YH email: 04:46 am 11.07.2019

scaleEM = [11.24; 11.24; 30];
skel.scale = [11.24,11.24,30];
totalTrees = skel.numTrees;
treeNames = skel.names;
idxAxon = cellfun(@(x) any(regexpi(x,'axon')),treeNames);
idxDend = cellfun(@(x) any(regexpi(x,'cell')),treeNames);

% change skel cell/axon colors to green/black
green = {[0,1,0,1]};% axon
black = {[0,0,0,1]};% dend
skel.colors(idxDend) = repelem(black,sum(idxDend),1);

% different colors
%{
colorsForAxons = Util.getRangedColors( Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, sum(idxAxon));
%}
% single colors
colorsForAxons = repmat(singleColor, sum(idxAxon),1);
colorsForAxons =  cat(2,colorsForAxons,ones(size(colorsForAxons,1),1));
colorsForAxons = mat2cell(colorsForAxons,ones(size(colorsForAxons,1),1),4);
skel.colors(idxAxon)  = colorsForAxons; % different shades of blue-green
colorsForAxons = vertcat(colorsForAxons{:});
colorsForAxons = colorsForAxons(:,1:3);

%scale all spheres
scale = skel.scale./1000;
nodesPC = skel.setScale(nodesPC,scale);

%% load skelCellTypeFixed
skelCellType = skeleton(config.cellTypesL4Fixed);

if iscolumn(curCellIdsToPlot)
    curCellIdsToPlot = curCellIdsToPlot';
end
for idx = 1:numel(curCellIdsToPlot)
    i = curCellIdsToPlot(idx);
    %somaColor = somaColors{cellTypesAll(i)}; % based on cell type
    somaColor = [0,0,0]; % black

    treeNamesToAdd = cat(2,axonNamesInTable(i)); % add only axon
    if iscolumn(treeNamesToAdd)
        treeNamesToAdd = treeNamesToAdd';
    end
    treesToAdd = [];
    for j = treeNamesToAdd
        thisTreeIdx = cellfun(@(x) contains(x,j),treeNames); % find in treeNames the occurence of this axon name
        thisTreeIdx = find(thisTreeIdx);
        treesToAdd = vertcat(treesToAdd,thisTreeIdx); % all axon trees for this cell type   
    end
    locOfAxonColor = ismember(find(idxAxon),treesToAdd);    

    % extract soma locations for IN to be plotted
    m = skel.getNodesWithComment('cellbody',treesToAdd,'exact',true);
    mm = ~cellfun(@isempty,m);
    loc = find(mm);
    somaLocs = skel.nodes{treesToAdd(loc(1))}(m{loc(1)},1:3);
    somaLocs = skel.setScale(somaLocs,scale);
   
    targetSomaLocs = [];
    targetSomaColors = []; 

    %{
    % extract IN hit from connectome
    idxINHit = find(synMapININ(i,:)>0);
    somaINHit = arrayfun(@(x) Util.getNodeCoordsWithComment(skel,{'soma','cellbody'},'exact', skel.getTreeWithName(['cell i' num2str(x,'%02d')]) ),idxINHit,'uni',0);
    thisSomaLocs =  cell2mat(somaINHit');
    assert(size(thisSomaLocs,1)==numel(idxINHit))
    targetSomaLocs = thisSomaLocs;
    targetSomaColors = vertcat(somaColors{cellTypesAll(idxINHit)});
    clear thisSomaLocs
    %}

    % extract stp/sps targets from connectome and skelCellType
    idxStpHit = find(synMapINStp(i,:)>0);
    idxDel = ismember(idxStpHit, idsPy3);
    idxStpHit(idxDel) = [];
    clear IdxDel

    idxDel = ismember(idxStpHit, stpOut);
    idxStpHit(idxDel) = [];
    clear idxDel

    somaStpHit = arrayfun(@(x) Util.getNodeCoordsWithComment(skelCellType,['p' num2str(x,'%02d')],'exact'),idxStpHit,'uni',0);
    thisSomaLocs =  cell2mat(somaStpHit');
    assert(size(thisSomaLocs,1)==numel(idxStpHit))
    targetSomaLocs = [targetSomaLocs; thisSomaLocs];
    targetSomaColors = [targetSomaColors; repelem(somaColors{end-1}, numel(idxStpHit),1)]; 

    idxSpsHit = find(synMapINSps(i,:)>0);
    idxDel = ismember(idxSpsHit, spsOut);
    idxSpsHit(idxDel) = [];
    clear idxDel

    somaSpsHit = arrayfun(@(x) Util.getNodeCoordsWithComment(skelCellType,['s' num2str(x,'%02d')],'exact'),idxSpsHit,'uni',0);
    thisSomaLocs =  cell2mat(somaSpsHit');
    assert(size(thisSomaLocs,1)==numel(idxSpsHit))
    targetSomaLocs = [targetSomaLocs; thisSomaLocs];
    targetSomaColors = [targetSomaColors; repelem(somaColors{end},numel(idxSpsHit),1)];


    % extract Stp Apical dendrites
    skelApicals = Figures.Fig1.extractApicalDendrites(somaStpHit);

    % scale soma locs
    targetSomaLocs = skel.setScale(targetSomaLocs,scale);

    % define layer 4 borders in z
    bboxLine = [-3000,30000;1000,30000;-6000,14000];
    bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);

    layer4_down = 2781; % older %3205; % [103, 46, 325]
    layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas

    Util.log('Now plotting figure....');
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    for k = 1:2
        a(k) = subplot(2, 1, k);
        hold on
        skel.plot(treesToAdd,cat(1,colorsForAxons(locOfAxonColor,:),[0,0,0]), true, tubeSize);
        objectSomas = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,somaSize,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', somaColor(1:3));
        objectTargetSomas = ...
                    scatter3(targetSomaLocs(:,1),targetSomaLocs(:,2),targetSomaLocs(:,3)...
                    ,somaTargetSize, targetSomaColors(:,1:3), 'filled');

        if exist('skelApicals','var') & ~isempty(skelApicals)
            skelApicals.plot('', repelem([1,0,1], skelApicals.numTrees,1), true, tubeSize);
        end
        switch k
           case 1
               %xy
               Util.setView(6); % same as fig2
           case 2
               %yz
               Util.setView(5); % same as fig2
                % border of L4 upper and down
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
        end
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
        axis equal
        axis off
        box off
        a(k).XLim = bbox(1,:);
        a(k).YLim = bbox(2,:);
        a(k).ZLim = bbox(3,:);
    end
    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');

    % scalebar
    scaleBarLength = 50; % um
    line(a(k),[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis

    outfile = fullfile(outDirPdf,['panel_example_targets_','_cell',num2str(i),'.eps']);
%     export_fig(outfile,'-q101', '-nocrop', '-transparent');
%     Util.log('Saving file %s.', outfile);
%     close(f);
end
