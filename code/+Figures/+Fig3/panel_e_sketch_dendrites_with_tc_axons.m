% Show WB and LB dendrites with input tc synapses and example axons
setConfig;

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);

somaColors = Util.getSomaColors;

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

%scale all spheres
scaleEM = [11.24; 11.24; 30];
scale = skelPC.scale./1000;
nodesPC = skelPC.setScale(nodesPC,scale);

% filenames of nmls wih dendrites and tc axons
cellIdsToPlot = [3,5]; % 3 / 5

outputDir = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished_readout', 'galleryCombinedINs');

for i = 1:numel(cellIdsToPlot)
    curId = cellIdsToPlot(i);

    switch curId
        case 3
            tcAxonName = 'tc_axon_3';
        case 5
            tcAxonName = 'tc_axon_1';
    end
    
    nml = fullfile(outDir,'data',sprintf('tc_clustering_example_i%02d.nml',curId));
    skelAll = skeleton(nml); % skel with tc input and tc axons
    skelAll.groups(:,:) = [];

    idxKeep = contains(skelAll.names, sprintf('cell i%02d', curId));
    curSkel = skelAll.deleteTrees(idxKeep, true);
    
    sprintf('Trees for cell %02d:',curId)
    disp(curSkel.names)

    % non-tc syns
    % synCoords = Util.getNodeCoordsWithComment(curSkel, '(syn-|-syn|syan)', 'regexp');
    synCoords = [];

    % tc syns
    tcCoords =  Util.getNodeCoordsWithComment(curSkel, '(tc\+|tc\?\+|tc\d+)', 'regexp'); % tc+ tc?+ tc12
 
    % find cellbody node
    somaCoord = Util.getNodeCoordsWithComment(curSkel, 'cellbody', 'exact');
    
    % keep tc axon
    idxKeep = contains(skelAll.names, tcAxonName);
    skelToPlot = skelAll.deleteTrees(idxKeep, true); % add tc axon
    skelToPlot = skelToPlot.addTreeFromSkel(curSkel); % add dendrite

    % scale
    somaCoord = skelAll.setScale(somaCoord, scale);
    if ~isempty(synCoords)
        synCoords = skelAll.setScale(synCoords, scale);
    end
    if ~isempty(tcCoords)
        tcCoords = skelAll.setScale(tcCoords, scale);
    end

    % plot gallery
    sprintf('Now plotting:')
    disp(skelToPlot.names)
    doPlotting(skelToPlot, curId, cellTypesAll, somaColors, somaCoord, nodesPC, synCoords, tcCoords, outputDir);
end

function doPlotting(skel, i, cellTypesAll, somaColors, somaLocs, nodesPC, synCoords, tcCoords, outDirPdf)
    singleColor = [0,1,1]; % cyan % [0, 158, 115]./255;
    colorPC = [0.5,0.5,0.5];    
    synColor = [146 36 40]./255; %[0,0,0];
    %tcColor = [230,159,0] ./255; % orange
    tcColor = [0,1,1]; % cyan
    
    scaleEM = [11.24; 11.24; 30];
    somaSize = 150; tubeSize = 4; dotSize= 3;
    somaBallColorStp = [1, 0, 1, 1]; % magenta
    somaBallColorSps = [0,0,1,1]; %blue
    somaBallSize = 25;

    % define layer 4 borders in z
    bboxLine = [-3000,30000;1000,30000;-6000,14000];
    bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);
    
    layer4_down = 2781; % older %3205; % [103, 46, 325]
    layer4_up = 9743; % z=500 % based on fig1 border [185, 89, 515] doesnt show extended data somas
        
%     fig = figure;
%     fig.Color = 'white';
    
    if i==32
        warning('Skipping i 32')
        return; % was moves to s177
    end
    somaColor = somaColors{cellTypesAll(i)};
    
    Util.log('Now plotting figure....');
    bbox = [-8000,35000;-8000,35000;-6000,14000];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    for k = 1:2
        a(k) = subplot(2, 1, k);
        hold on
        if skel.numTrees>1 % both dend, axon exist
            idxDend = find(contains(skel.names,'cell'));
            skel.plot(idxDend, 'k', true, tubeSize);
            idxAxon = find(contains(skel.names,'axon'));
            skel.plot(idxAxon, singleColor, true, tubeSize);
        else % only dend
            idxDend = find(contains(skel.names,'cell'));
            skel.plot(idxDend, 'k', true, tubeSize);
        end
        %{
        objectPC = ...
                    scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                    ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                    'MarkerFaceColor', colorPC(1:3));
        %}
        objectSomas = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,somaSize,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', somaColor(1:3));
        if ~isempty(synCoords)
        objectSyns = ...
                    scatter3(synCoords(:,1),synCoords(:,2),synCoords(:,3)...
                    ,somaBallSize, 'MarkerEdgeColor', synColor(1:3), ...
                    'MarkerFaceColor', 'none', ...
                    'Marker','.', 'MarkerEdgeAlpha', 0.8, 'LineWidth', 0.3);
        end
        if ~isempty(tcCoords)
        objectTC = ...
                    scatter3(tcCoords(:,1),tcCoords(:,2),tcCoords(:,3)...
                    ,somaBallSize, 'MarkerEdgeColor', tcColor(1:3), ...
                    'MarkerFaceColor', 'none', ...
                    'Marker','o', 'MarkerEdgeAlpha', 1, 'LineWidth',0.5);
        end

        switch k
           case 1
               %xy
               Util.setView(6); % same as fig2
           case 2
               %yz
               Util.setView(5); % same as fig2
                % border of L4 upper and down
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
               line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                   [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                   'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
        end
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
        axis equal
        axis off
        box off
        a(k).XLim = bbox(1,:);
        a(k).YLim = bbox(2,:);
        a(k).ZLim = bbox(3,:);
    end
    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
    
    % scalebar
    scaleBarLength = 50; % um
    line(a(k),[bboxLine(1,2)-scaleBarLength, bboxLine(1,2)], [bboxLine(2,2) - scaleBarLength, bboxLine(2,2)], [bboxLine(3,2), bboxLine(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis

    annotation( ...
            f,...
            'textbox', [0, 0.01,1,0.1], ...
            'String', sprintf('i%02d.png',i), ...
            'EdgeColor', 'none',...
            'Color', [0,0,0], ...
            'Interpreter','none',...
            'HorizontalAlignment', 'center')

    outfile = fullfile(outDirPdf,['tc_clustering_example_cell',num2str(i),'.eps']);
%     export_fig(outfile,'-q101', '-nocrop', '-transparent');
%     Util.log('Saving file %s.', outfile);
%     close(f);
end
