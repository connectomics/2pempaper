% panel h with % stp targets out of total exc targets for WB and LB BINs
setConfig;
somaColors = Util.getSomaColors;
load(config.connmat_SLmanual,'synMapINStp','synMapINSps', 'somaMapINStp','somaMapINSps',  'cellTypesAll', 'postClassINIds')

restrictToSoma = true;
if restrictToSoma
    sprintf('Restricting analysis to soma synapses...\n')
    synMapINStp = somaMapINStp;
    synMapINSps = somaMapINSps;
end

restrictBarrel = true;

if restrictBarrel
    Util.log('Restrict to ExNs inside barrel')
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
    stpOut = find(~INStp);
    spsOut = find(~INSps);
else
    stpOut = []; % remove nothing
    spsOut = []; % remove nothing
end

Util.log('EXcluding py3 in conn')
idsPy3 = Util.getPy3Ids;

idxDel = unique([stpOut(:); idsPy3(:)]);
synMapINStp(:,idxDel) = []; % soma locations are not important

% idx for WB, idx for LB
idxWB = cellTypesAll == 1;
idxLB = cellTypesAll == 2;
idxL4 = cellTypesAll == 3;

% find number of stp targets / all targetted
m = load(fullfile(outDir,'allForHeiko/fig1_panelg_somaCountsLM.mat'));

synCountStpHit = sum(synMapINStp>0,2);
synCountSpsHit = sum(synMapINSps>0,2);
synStpTotal = size(m.outTable.Var2{2},1);
synSpsTotal = size(m.outTable.Var2{3},1);

% fraction of Stp in exc target synapses per IN
synFracStp = synCountStpHit ./ (synCountStpHit + synCountSpsHit);

% write out cellIds and synFracStp in barrel for INs for subtype classification
outTable = table(postClassINIds, synFracStp, cellTypesAll, synCountSpsHit, synCountStpHit);
disp(outTable)
% writetable(outTable, fullfile(outDir,'outData','figures','IN_subtypes','synFracStpOfINs.xlsx'))

% plot
txt = arrayfun(@(x) sprintf('%d',x),postClassINIds, 'uni',0); % cell Ids labels
fontSize = 5;

Util.log('box plot')
rng(0)
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;

x = [synFracStp(idxWB);synFracStp(idxLB); synFracStp(idxL4)];
txt = [txt(idxWB); txt(idxLB); txt(idxL4)];

curIdxWB = 1:sum(idxWB);
curIdxLB = curIdxWB(end)+(1:sum(idxLB));
curIdxL4 = curIdxLB(end)+(1:sum(idxL4));
g = cell(numel(x),1);
g(curIdxWB) = {'wholeBarrel'}; % first box
g(curIdxLB) = {'lowerBarrel'}; % second box
g(curIdxL4) = {'L4-IN'}; % second box

widthS = 2; startS = 1;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
h = boxplot(x,g,'Colors','k','Widths',2,'Positions',pos);
set(h,{'linew'},{2})
x_pos = widthS.*rand(numel(curIdxWB),1)+posS(1);
scatter(x_pos, x(curIdxWB),...
    'o','filled', 'MarkerFaceColor',somaColors{1}(1:3),'MarkerEdgeColor',somaColors{1}(1:3));
text(x_pos, x(curIdxWB), txt(curIdxWB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

x_pos = widthS.*rand(numel(curIdxLB),1)+posS(2)
scatter(x_pos, x(curIdxLB),...
    'o','filled', 'MarkerFaceColor',somaColors{2}(1:3),'MarkerEdgeColor',somaColors{2}(1:3));
text(x_pos, x(curIdxLB), txt(curIdxLB), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

x_pos = widthS.*rand(numel(curIdxL4),1)+posS(3);
scatter(x_pos, x(curIdxL4),...
    'o','filled', 'MarkerFaceColor',somaColors{3}(1:3),'MarkerEdgeColor',somaColors{3}(1:3));
text(x_pos, x(curIdxL4), txt(curIdxL4), ...
    'horizontalalignment','center','verticalalignment','middle', 'Color',[0,0,0],'FontSize',fontSize);

% add line for %stp inside barrel
stpIn = size(m.outTable.Var3{2},1); %Inside
spsIn = size(m.outTable.Var3{3},1); % Inside
yLineB = stpIn/(stpIn + spsIn);

stpInL4 = synStpTotal;
spsInL4 = synSpsTotal;
yLineL4 = stpInL4/(stpInL4 + spsInL4);

temp = xlim;
line([temp(1), temp(2)],[yLineB yLineB],'LineWidth',2,'color',[0,0,0],'LineStyle','--');
line([temp(1), temp(2)],[yLineL4 yLineL4],'LineWidth',2,'color',[0,0,0],'LineStyle','--');

daspect([25,1,4])

% cosmetics
ax.YAxis.Label.String = '%Stp in all exc. targets';
ax.YAxis.Limits = [0,1];
ax.YAxis.TickValues = 0:0.1:1;
set(gca,'xcolor','none','LineWidth',2)

% ranksum
pValueRS12 = ranksum(synFracStp(idxWB), synFracStp(idxLB))
[~,pValueKS12] = kstest2(synFracStp(idxWB), synFracStp(idxLB))

pValueRS13 = ranksum(synFracStp(idxWB), synFracStp(idxL4))
[~,pValueKS13] = kstest2(synFracStp(idxWB), synFracStp(idxL4))

pValueRS23 = ranksum(synFracStp(idxLB), synFracStp(idxL4))
[~,pValueKS23] = kstest2(synFracStp(idxLB), synFracStp(idxL4))

title(sprintf('WB-LB (ranksum) %f, (ks): %f \n WB-L4 (ranksum) %f, (ks): %f \n LB-L4 (ranksum) %f, (ks): %f', ...
                pValueRS12, pValueKS12,  pValueRS13, pValueKS13,  pValueRS23, pValueKS23));
Util.setPlotDefault(gca,false,false);

% export_fig(fullfile(outDir,'outData','figures','fig2',config.version,'panel_stp_fraction.eps'),'-q101', '-nocrop','-transparent');
% close all

%% Numbers for text
sprintf('Fraction of Stp targets by \nLower barrel: mean+-s.d. %.02f +- %.02f \nWhole barrel: mean+-s.d.  %.02f +- %.02f \n',...
         mean(synFracStp(idxLB)), std(synFracStp(idxLB)), mean(synFracStp(idxWB)), std(synFracStp(idxWB)))
sprintf('#Stp / #Stp+#Sps hit for \nLower barrel: %.02f, n=%d \nWhole barrel: %.02f, n=%d \n',...
        sum(synCountStpHit(idxLB))/(sum(synCountStpHit(idxLB)) + sum(synCountSpsHit(idxLB))), sum(synCountStpHit(idxLB)),...
        sum(synCountStpHit(idxWB))/(sum(synCountStpHit(idxWB)) + sum(synCountSpsHit(idxWB))), sum(synCountStpHit(idxWB)))
sprintf(' #Sps / #Stp +#Sps hit by \nLower barrel: %.02f, n=%d \nWhole barrel: %.02f, n=%d \n',...
        sum(synCountSpsHit(idxLB))/(sum(synCountStpHit(idxLB)) + sum(synCountSpsHit(idxLB))), sum(synCountSpsHit(idxLB)),...
        sum(synCountSpsHit(idxWB))/(sum(synCountStpHit(idxWB)) + sum(synCountSpsHit(idxWB))), sum(synCountSpsHit(idxWB)))
sprintf('N hit connection of LBs: %d', sum(synCountStpHit(idxLB)) + sum(synCountSpsHit(idxLB)) )
