%% single TC axon with its post-syn targets sps/stp and IN and INs to Stp/Sps
% update idxTCPlot to 1, 3, 5 sequentially
setConfig;
[cx_data,cx_datatxt,cx_dataraw] = xlsread(config.cffi_excel_SLmanual);
somaColors = Util.getSomaColors;
colorTC = Util.getTCColor;
restrictFlag = true; % restrict to exc cells within barrel
sortINFlag = true; % sort INs by first-hit synapses
sortTCExcFlag = true; % srot TC output to ExN but first synapse
debugFlag = true; % debug open and closed triads for manual reconstruction
debugFlagPlot = false;
markerSize = 10;

%%
Util.log('Reading data...')
rng(0)
% 2019-08-12_cffi_v1.xlsx
% NOTE : defined ignoring the first row with variable names
cx_tcCols = find(cellfun(@(x) any(regexp(x,'t\d+')),cx_dataraw(1,:)));
cx_StpRows = reshape(find(cellfun(@(x) any(regexp(x,'^p$')),cx_dataraw(:,1))) - 1,1,'');% defined ignoring first row
cx_SpsRows = reshape(find(cellfun(@(x) any(regexp(x,'^s$')),cx_dataraw(:,1))) - 1,1,'');
cx_INrows = reshape(find(cellfun(@(x) any(regexp(x,'i')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row
cx_INCols = find(cellfun(@(x) any(regexp(x,'i\d+')),cx_dataraw(1,:))); % NOTE: Empty cols are thrown out
cx_INtypeCol = size(cx_dataraw,2); % col in cx_dataraw
cx_idsCol = 2;
cx_depthCol = 3;

rng(0)
cx_postsyn_depth = cx_data(:,cx_depthCol)+600;
cx_targetXPos = cx_postsyn_depth*0+1150+(rand(size(cx_postsyn_depth,1),1)-0.5)*50;
% cx_targetXPos(cx_StpRows) = cx_postsyn_depth(cx_StpRows)*0+1030+(rand(size(cx_StpRows,2),1)-0.5)*20;  % Stp
% cx_targetXPos(cx_SpsRows) = cx_postsyn_depth(cx_SpsRows)*0+1030+(rand(size(cx_SpsRows,2),1)-0.5)*20;  % Sps

% add jitter for visibility of stp and sps
r = -1+ (20)*rand(numel(cx_targetXPos),1); % scatter around TC axon axis

cx_targetXPos(cx_StpRows) = 1000;  % Stp
cx_targetXPos(cx_SpsRows) = 1000;  % Sps
cx_targetXPos(cx_INrows) = 980;  % INs
cx_INvertOffset = 0;% -50;%-200;
cx_tcXspacing = 0;
cx_tcXoffsetIN = 1000; %1400; TC to IN input from same TC start
cx_tcXoffsetExc = 1000;

%% load connectome and restrict to main IN types
load(config.connmat_SLmanual)
% NOTE must append zeros
Util.log('convert to binary connectome')
connTCIN = double(synMapTCIN>0);
% restrict post targets
curType = '';
curTypeId = [1,2,3]; % WB, LB, L4 related
idxKeep = reshape(ismember(cellTypesAll,curTypeId),1,'');
idxTCPlot = 5;% row vector 
tcName = '5'; % empty for main figure eps file
idxINPlot = find(connTCIN(idxTCPlot,:)>0 & idxKeep);

% remove those IN whose axons that are not reconstructed
inNames = {skel.names{:}};
inNames = inNames(contains(inNames,'IN axon'));
tempid = cellfun(@(x) regexp(x,'IN axon (?<id>\d+)','names'),inNames);
inNamesID = cellfun(@str2double,{tempid.id});  
idxINPlot = idxINPlot(ismember(idxINPlot, inNamesID));
clear tempid

if restrictFlag
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
else
    INStp = true(countStp,1);
    INSps = true(countSps,1);
end

%% plot stp, sps, INs
Util.log('Plotting somas...')
fig = figure;
fig.Color = 'white';
hold on
cx_postType = []; % 1 for Ins, 2 for Stp, 3 for Sps
i_post_exc_plot = []; % List of stp sps plotted here
i_tcc = idxTCPlot; % TC01, etc.
i_tc = cx_tcCols(i_tcc); % no for loop, works for just one TC
for i_post = 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);
    cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % synapse depths
    switch cx_datatxt{i_post+1,1}
        case 'i'
            curIN = str2double(cx_datatxt{i_post+1,2});
            if ismember(curIN,idxINPlot)      
                curINColor = somaColors{cellTypesAll(curIN)}(1:3);
                if sortINFlag
                    cx_post_pos = min(cx_thisPosVec);% lowest point of input synapse from tc
                    cx_postsyn_depth(i_post) = cx_post_pos; % update the IN square depth for later use
                end
                plot(cx_targetXPos(i_post),cx_post_pos+cx_INvertOffset,'o',...
                    'MarkerEdgeColor', curINColor, 'MarkerFaceColor', curINColor ,'MarkerSize',15,'LineWidth',2);
            end
            cx_postType(i_post) = 1;
        case 'p'
            if ~isempty(cx_thisPosVec)
%                plot(cx_targetXPos(i_post),cx_post_pos,'om','MarkerSize', markerSize);
                i_post_exc_plot(end+1) = i_post;
            end
            cx_postType(i_post) = 2;
        case 's'
            if ~isempty(cx_thisPosVec)
%                plot(cx_targetXPos(i_post),cx_post_pos,'ob','MarkerSize',markerSize);
                i_post_exc_plot(end+1) = i_post;
            end
            cx_postType(i_post) = 3;
    end
end

if restrictFlag % restrict exc to those within barrel
    idxStpHere = find(cx_postType(i_post_exc_plot)==2);
    stpInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxStpHere)+1, 2}});
    idxStpHereKeep = INStp(stpInnv);
    idxSpsHere = find(cx_postType(i_post_exc_plot)==3);
    spsInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxSpsHere)+1, 2}});
    idxSpsHereKeep = INSps(spsInnv);
 
    i_post_exc_plot = reshape(i_post_exc_plot([idxStpHereKeep(:); idxSpsHereKeep(:)]),1,'');
end

%% TC input
Util.log('Plotting tc input...')
cx_postColors = {[0,0,0],[1,0,1],[0,0,1]};
cx_TCsynPosVec_byType={[],[],[]};
cx_TCinputPerCell = {[],[],[]};
postExcTCSynCount = []; % number of TC to Exc synapses
for i_tcc = idxTCPlot
    i_tc = cx_tcCols(i_tcc);
    cx_thisXposIN = i_tcc*cx_tcXspacing+cx_tcXoffsetIN;
    cx_thisXposExc = i_tcc*cx_tcXspacing+cx_tcXoffsetExc;
    cx_thisTCmaxpos = 0;
    for i_post = 1:size(cx_dataraw,1)-1
        cx_post_pos = cx_postsyn_depth(i_post);           
        if strcmp(cx_datatxt{i_post+1,1},'i')
            cx_post_pos = cx_post_pos+cx_INvertOffset;
        end
        if isempty(cx_datatxt{i_post+1,i_tc}) %no tc synapse but empty string will be non empty cell-array
            cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
        else
            cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % empty string gives empty,  multiple numbers to array
        end
        if ~isempty(cx_thisPosVec)
%             cx_thisPosVec = min(cx_thisPosVec); % first syn position
            for iii=1:length(cx_thisPosVec)
                curLineWidth = 1;
                faceAlpha = 0.5;
                if ismember(i_post,cx_INrows) % if IN input
                    curLineWidth = 1; faceAlpha = 0.8;
                    if ismember(str2double(cx_datatxt{i_post+1,2}),idxINPlot)
                        plot([cx_thisXposIN cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
                            '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                        cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
%                         % < Marker at IN syn
%                         curMarkerSize = 5;
%                         plot(cx_thisXposIN,cx_thisPosVec(iii),'<','Color',...
%                             cx_postColors{cx_postType(i_post)},'MarkerSize',curMarkerSize);
                    end
                else % if exc input
                    if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
%                         plot([cx_thisXposExc cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
%                             '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                        cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
                        if iii==length(cx_thisPosVec)
                            postExcTCSynCount = vertcat(postExcTCSynCount,length(cx_thisPosVec) ); % add only if this exc is plotted
                        end
                    end
                end
                cx_TCsynPosVec_byType{cx_postType(i_post)} = [cx_TCsynPosVec_byType{cx_postType(i_post)},...
                    cx_thisPosVec(iii)];                
            end
        end
        cx_TCinputPerCell{cx_postType(i_post)} = [cx_TCinputPerCell{cx_postType(i_post)} ,length(cx_thisPosVec)];
    end
    % vertical TC axon lines
    plot(repmat(cx_thisXposIN,[1 2]),[0 max(cx_thisTCmaxpos,400)],'-','Color',colorTC,'LineWidth',4); % restrict TC to 420
    plot(cx_thisXposIN,0,'s','MarkerFaceColor',colorTC, 'MarkerEdgeColor',colorTC,'MarkerSize',15);
    plot(cx_thisXposIN,max(cx_thisTCmaxpos,400),'^', 'MarkerFaceColor',colorTC,'MarkerEdgeColor',colorTC,'MarkerSize',15)
end

%% get ranks for Exc inputs
allExcRankPos = [];

% map each col IN location to its row location
tempid = cellfun(@(x) regexp(x,'i(?<id>\d+)','names'),cx_dataraw(1,cx_INCols));
map_col_to_row = cellfun(@str2double,{tempid.id});  
for i_tcc = 1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_targetXPos(map_col_to_row(i_tcc));    
    cx_thisYpos = cx_postsyn_depth(map_col_to_row(i_tcc))+cx_INvertOffset;  
    curIN = str2double(cx_datatxt{map_col_to_row(i_tcc)+1,2});
    if ismember(curIN,idxINPlot)    % draw if this IN is in our list
        synCount = [];
        p = []; s = [];
        for i_post = i_post_exc_plot
            cx_post_pos = cx_postsyn_depth(i_post);
            if isempty(cx_datatxt{i_post+1,i_tc})
                cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
            else
                cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
            end
            if sortTCExcFlag % look at TC targeted ExNs only
               i_tc = cx_tcCols(idxTCPlot); % works for just one TC
               cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});% look for TCxx input min dist for this post
               cx_post_pos = min(cx_thisPosVec);
            end
            if ~isempty(cx_thisPosVec) % existing synapses to this post exc
                allExcRankPos = [allExcRankPos, cx_post_pos];
            end
        end
    end
end
allExcRankPosUnique = unique(allExcRankPos,'stable');
[allExcRankPosUniqueSorted,idxSort] = sort(allExcRankPosUnique, 'ascend');

yMap = (400/max(idxSort))*idxSort; % hardcoded map 0-400 to 1-20

%% IN inputs to Exc
Util.log('Plotting IN input...')
postExcToDebug = struct('p',[],'s',[]);
faceAlpha = 1;
postExcINSynCount = {};
% map each col IN location to its row location
tempid = cellfun(@(x) regexp(x,'i(?<id>\d+)','names'),cx_dataraw(1,cx_INCols));
map_col_to_row = cellfun(@str2double,{tempid.id});  
for i_tcc = 1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_targetXPos(map_col_to_row(i_tcc));    
    cx_thisYpos = cx_postsyn_depth(map_col_to_row(i_tcc))+cx_INvertOffset;  
    curIN = str2double(cx_datatxt{map_col_to_row(i_tcc)+1,2});
    if ismember(curIN,idxINPlot)    % draw if this IN is in our list
        synCount = [];
        p = []; s = [];
        for i_post = i_post_exc_plot
            cx_post_pos = cx_postsyn_depth(i_post);
            if strcmp(cx_datatxt{i_post+1,1},'i')
                cx_post_pos = cx_post_pos+cx_INvertOffset;
            end
            if isempty(cx_datatxt{i_post+1,i_tc})
                cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
            else
                cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
            end
            if ~isempty(cx_thisPosVec) % existing synapses to this post exc
    %             for iii=1:length(cx_thisPosVec)  
                if sortTCExcFlag % look at TC targeted ExNs only
                   i_tc_temp = cx_tcCols(idxTCPlot); % works for just one TC
                   cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc_temp});% look for TCxx input min dist for this post
                   cx_post_pos = min(cx_thisPosVec);
                end
                idx = yMap(cx_post_pos == allExcRankPosUnique);
                plot([cx_thisXpos cx_targetXPos(i_post)],[cx_thisYpos idx],'-',...
                    'Color',[0,1,0,faceAlpha],'LineWidth',1);
                synCount = [synCount,length(cx_thisPosVec)];
    %             end
            else % no synapses to this post exc
                switch cx_postType(i_post)
                    case 2 % stp
                        p = [p,str2double(cx_datatxt{i_post+1,2})];
                    case 3 % sps
                        s = [s,str2double(cx_datatxt{i_post+1,2})];
                end
                synCount = [synCount,0];
            end
        end
        % s and p not innervated but this IN
        temp  = false(1, countStp);
        temp(p) = true;
        postExcToDebug(end+1).p = temp;
        temp  = false(1, countSps);
        temp(s) = true;
        postExcToDebug(end).s = temp;
        postExcToDebug(end).i = curIN;
        clear p s temp
        postExcINSynCount{i_tcc} = synCount;
        clear synCount
    end
end
postExcToDebug(1) = []; % empty because of declaration

%% debug for exc that don't receive IN input
if debugFlag
    %% STP
    fprintf('Stp innervated by TC: \n')
    stpInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(cx_postType(i_post_exc_plot)==2)+1, 2}});
    disp(stpInnv)
    clear somaLocs INSIDE IN
    for i=1:numel(stpInnv)
        curId = stpInnv(i);
        somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
    end
    [IN, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');    
    fprintf('Stp INSIDE: \n')
    disp(stpInnv(IN))
    fprintf('Stp OUTSIDE: \n')
    disp(stpInnv(~IN))
    
    pDebug = find(all(vertcat(postExcToDebug(:).p)));
    fprintf('Stp not innervated by IN: \n')
    disp(pDebug)
    if ~isempty(pDebug)
        clear somaLocs INSIDE
        for i=1:numel(pDebug)
            curId = pDebug(i);
            somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
        end
        [INSIDE, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');    
        fprintf('Stp INSIDE: \n')
        disp(pDebug(INSIDE))
        pDebugInside = pDebug(INSIDE);
        fprintf('Stp OUTSIDE: \n')
        disp(pDebug(~INSIDE))
        pDebugOutside = pDebug(~INSIDE);
        stpTable = table(numel(stpInnv), numel(stpInnv(IN)),numel(stpInnv(~IN)),...
            numel(pDebug), sum(INSIDE), sum(~INSIDE),...
            (100*(numel(stpInnv(IN))-numel(pDebug(INSIDE)))/numel(stpInnv(IN))),...
        'VariableNames',{'stp_all','stp_all_in','stp_all_out','stp_nocffi','stp_nocffi_inside','stp_nocffi_outside','perc_cffi'})
        clear somaLocs INSIDE
    else
        fprintf('All Stp innervated! \n')
        stpTable = table(numel(stpInnv), numel(stpInnv(IN)),numel(stpInnv(~IN)),...
            0, NaN, NaN,...
            (100*1),...
        'VariableNames',{'stp_all','stp_all_in','stp_all_out','stp_nocffi','stp_nocffi_inside','stp_nocffi_outside','perc_cffi'})
    end
    
    %% SPS
    fprintf('Sps innervated by TC: \n')
    spsInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(cx_postType(i_post_exc_plot)==3)+1, 2}});
    disp(spsInnv)
    clear somaLocs INSIDE IN
    for i=1:numel(spsInnv)
        curId = spsInnv(i);
        somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['s' num2str(curId,'%02d')] ,'exact');
    end
    [IN, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');    
    fprintf('Sps INSIDE: \n')
    disp(spsInnv(IN))
    fprintf('Sps OUTSIDE: \n')
    disp(spsInnv(~IN))
    
    sDebug = find(all(vertcat(postExcToDebug(:).s)));
    fprintf('Sps not innervated by IN: \n')
    disp(sDebug)
    if ~isempty(sDebug)
        clear somaLocs
        for i=1:numel(sDebug)
            curId = sDebug(i);
            somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['s' num2str(curId,'%02d')] ,'exact');
        end
        [INSIDE, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');
        fprintf('Sps INSIDE: \n')
        sDebugInside = sDebug(INSIDE);
        disp(sDebug(INSIDE))
        fprintf('Sps OUTSIDE: \n')
        disp(sDebug(~INSIDE))
        sDebugOutside = sDebug(~INSIDE);
        spsTable = table(numel(spsInnv), numel(spsInnv(IN)),numel(spsInnv(~IN)),...
            numel(sDebug), sum(INSIDE), sum(~INSIDE),...
            (100*(numel(spsInnv(IN))-numel(sDebug(INSIDE)))/numel(spsInnv(IN))),...
        'VariableNames',{'sps_all','sps_all_in','sps_all_out','sps_nocffi','sps_nocffi_inside','sps_nocffi_outside','perc_cffi'})
        clear somaLocs INSIDE
    else
        fprintf('All Sps innervated! \n')
        spsTable = table(numel(spsInnv), numel(spsInnv(IN)),numel(spsInnv(~IN)),...
            0, NaN, NaN,...
            (100*1),...
        'VariableNames',{'sps_all','sps_all_in','sps_all_out','sps_nocffi','sps_nocffi_inside','sps_nocffi_outside','perc_cffi'})
    end
    
    % total number of IN to Exc synapses
    idxKeep = ~cellfun(@isempty, postExcINSynCount);
    fprintf('The %d INs make total synapses: \n',sum(idxKeep))
    disp(cellfun(@sum, postExcINSynCount(idxKeep)))
    fprintf('Total synapses: %d \n',sum(cellfun(@sum, postExcINSynCount(idxKeep))))
    
    %% plot gallery
    if debugFlagPlot
        
        somaSize = 100; dotSize= 3; tubeSize = 2;somaTargetSize=70;
        targetSomaLocs=[];
        targetSomaColors=[];
        inAxonsToPlot = arrayfun(@(x) ['IN axon ' num2str(x,'%02d')],idxINPlot,'uni',0);
        colorsToPlot = repmat([0,0.5,0],numel(inAxonsToPlot),-2);

        pToPlot = arrayfun(@(x) ['cell p' num2str(x,'%02d')],pDebugInside,'uni',0);
        colorsToPlot = cat(1,colorsToPlot,repmat([1,0,1],numel(pToPlot),1));

        somaStpHit = arrayfun(@(x) Util.getNodeCoordsWithComment(skelCellType,['p' num2str(x,'%02d')],'exact'),pDebugInside,'uni',0);
        thisSomaLocs =  cell2mat(somaStpHit');
        targetSomaLocs = [targetSomaLocs; thisSomaLocs];
        targetSomaColors = [targetSomaColors; repelem(somaColors{end-1},numel(pDebugInside),1)];

        sToPlot = arrayfun(@(x) ['cell s' num2str(x,'%02d')],sDebugInside,'uni',0);
        colorsToPlot = cat(1,colorsToPlot,repmat([1,0,1],numel(sToPlot),1));

        somaSpsHit = arrayfun(@(x) Util.getNodeCoordsWithComment(skelCellType,['s' num2str(x,'%02d')],'exact'),sDebugInside,'uni',0);
        thisSomaLocs =  cell2mat(somaSpsHit');
        targetSomaLocs = [targetSomaLocs; thisSomaLocs];
        targetSomaColors = [targetSomaColors; repelem(somaColors{end},numel(sDebugInside),1)];

        treeNamesToPlot = cat(2,inAxonsToPlot,pToPlot,sToPlot);
        treesToPlot = skel.getTreeWithName(treeNamesToPlot,'exact');

        skelPC = skeleton(config.skelIn);
        nodesPC = skelPC.getNodes;
        colorPC = [0.5,0.5,0.5];

        scale = skel.scale./1000;
        nodesPC = skel.setScale(nodesPC,scale);
        targetSomaLocs = skel.setScale(targetSomaLocs,scale);

        bbox = [-8000,35000;-8000,35000;-6000,14000];
        bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
        scaleEM = [11.24; 11.24; 30];
        % define layer 4 borders in z
        bboxLine = [-3000,30000;1000,30000;-6000,14000];
        bboxLine = bsxfun(@times, bboxLine, scaleEM./1000);

        layer4_down = 2781;
        layer4_up = 9440; % based on 2p borders 328 and 500 nm

        f = figure();
        f.Units = 'centimeters';
        f.Position = [1 1 21 29.7];
        for k=1:4
            a(k) = subplot(2, 2, k);
            hold on;
            skel.plot(treesToPlot, colorsToPlot, true, tubeSize);

            objectPC = ...
                    scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                    ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                    'MarkerFaceColor', colorPC(1:3));
            objectTargetSomas = ...
                        scatter3(targetSomaLocs(:,1),targetSomaLocs(:,2),targetSomaLocs(:,3)...
                        ,somaTargetSize, targetSomaColors(:,1:3), 'filled');
            alpha = 1;
            set(objectTargetSomas, 'MarkerEdgeAlpha', alpha, 'MarkerFaceAlpha', alpha);

            switch k
                   case 1
                       %xy
                       Util.setView(1);
                   case 2
                       %xz
                       Util.setView(2);
                        % border of L4 upper and down
                        line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                           [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                           'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
                        line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                           [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                           'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
                   case 3
                       %yz
                       Util.setView(5);
                        % border of L4 upper and down
                       line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                           [layer4_up*scaleEM(3)./1000 layer4_up*scaleEM(3)./1000],...
                           'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
                       line([bboxLine(1,1) bboxLine(1,2)],[bboxLine(2,1) bboxLine(2,2)],...
                           [layer4_down*scaleEM(3)./1000 layer4_down*scaleEM(3)./1000],...
                           'LineWidth',0.75,'color',[1,0,0],'LineStyle','--');
                   case 4
                       %yz
                       Util.setView(4);
            end
            set(gca,'xtick',[])
            set(gca,'xticklabel',[])
            set(gca,'ytick',[])
            set(gca,'yticklabel',[])
            set(gca,'ztick',[])
            set(gca,'zticklabel',[])
            axis equal
            axis off
            box off
            a(k).XLim = bbox(1,:);
            a(k).YLim = bbox(2,:);
            a(k).ZLim = bbox(3,:);
        end
        Visualization.Figure.removeWhiteSpaceInSubplot(f,a);
        outfile = fullfile(outDir, 'outData/figures','fig3',config.version, 'models',['panel_example_targets_','type_tc' num2str(idxTCPlot), '.png']);
%         export_fig(outfile,'-q101', '-nocrop', '-transparent','-m8');
%         Util.log('Saving file %s.', outfile);
%         close(f);
    end
end % end of debugFlag

%% overlay somas of exc
i_tcc = idxTCPlot; % TC01
i_tc = cx_tcCols(i_tcc);
for i_post = i_post_exc_plot % 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);  
    cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
    if sortTCExcFlag
        cx_post_pos = min(cx_thisPosVec);% lowest point of input synapse from tc
    end
    switch cx_datatxt{i_post+1,1}
        case 'p'
            if ~isempty(cx_thisPosVec)
                idx = yMap(cx_post_pos == allExcRankPosUnique);
                cx_post_pos = idx;
                plot(cx_targetXPos(i_post),cx_post_pos,'sm','MarkerSize', markerSize);
            end
        case 's'
            if ~isempty(cx_thisPosVec)
                idx = yMap(cx_post_pos == allExcRankPosUnique);
                cx_post_pos = idx;
                plot(cx_targetXPos(i_post),cx_post_pos,'sb','MarkerSize',markerSize);
            end
    end
end

set(gca,'YLim',[0, 400]); % set manually for TC axon 03
daspect([50,150,1])
set(gca,'TickDir','out','box','off','XColor','none','YColor','none')

%%
Util.log('Saving fig panel...')
set(gcf,'Position',get(0, 'Screensize'))
outfile = fullfile(outDir, 'outData/figures','fig3',config.version,['panel_cffi_example_with_syn' tcName '.pdf']);
% export_fig(outfile, '-q101', '-nocrop', '-transparent','-m8');
% close all

% TC to EXc syn counts
sprintf('Total %d synapses from this TC to %d ExNs',sum(postExcTCSynCount(:)),numel(postExcTCSynCount))
sprintf('TC to INs:')
synMapTCIN(idxTCPlot,idxINPlot)