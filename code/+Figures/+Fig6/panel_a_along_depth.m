% plot the distributions along Z
% number of synapses
% tc axon lengths
% real endigns
% branch points
setConfig;
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
nml = config.skelTCCombined; % annotated endings
skel = skeleton(nml);
skel = skel.keepTreeWithName('TC axon','regexp');
skel.scale = [11.24,11.24,30];

Util.log('find synapses...')
bboxWk = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bboxWk, [11.24; 11.24; 30]./1000);

Util.log('Find tc axon lengths...')
dimension = 3; binSize = 1000;
[synDensity, synCount, synLengths] = Util.calculateSynDensity(skel,{'sp','sh'},'partial', bboxWk, dimension, binSize);

Util.log('find real/artifical endings and synapses...')
nodesCross1 = Util.getNodeCoordsWithComment(skel,'out','partial');
nodesCross2 = Util.getNodeCoordsWithComment(skel,'gg','partial');
nodesCross = vertcat(nodesCross1,nodesCross2);
nodesCircle = Util.getNodeCoordsWithComment(skel,'end','partial');
nodesBP = [];
for i =1:skel.numTrees
    bp = getBranchpoints(skel,i);       
    nodesBP = vertcat(nodesBP, skel.nodes{i}(bp,1:3));
end

scale = skel.scale./1000;
nodesCross = skel.setScale(nodesCross,scale);
nodesCircle = skel.setScale(nodesCircle,scale);
nodesBP = skel.setScale(nodesBP,scale);

Util.log('Now plotting...')
dataZ = nodesCircle(:,3);
binWidth = 30;
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
l1 = histogram(dataZ,'DisplayStyle','stairs','BinWidth',binWidth,...
    'EdgeColor','g','LineWidth',2);
xlim([bbox(3,1) bbox(3,2)]);
axis on
box off
set(gca,'TickDir','out');
ax1 = gca;
set(ax1,'YAxisLocation','left',...
    'YColor','g','LineWidth',2,'TickDir','out');
ax1_pos = ax1.Position; % position of first axes
ax2_pos = ax1_pos;
ax2 = axes('Position', ax2_pos);
dataZ = nodesBP(:,3);
l2 = histogram(ax2,dataZ,'DisplayStyle','stairs','BinWidth',binWidth,...
    'EdgeColor',[240/255,228/255,66/255],'LineWidth',2);
set(ax2,'Color','none','YAxisLocation','left',...
    'YColor',[240/255,228/255,66/255],'LineWidth',2,'TickDir','out');
xlim([bbox(3,1) bbox(3,2)]);
axis on
box off
%% syn counts and lengths
ax3_pos = ax1_pos;
ax3 = axes('Position', ax3_pos);
dataType = synCount;
dataPoints  = cellfun(@sum,dataType);
l3 = plot(dataPoints,'b','LineWidth',2,'Parent',ax3);
set(ax3,'Color','none','YAxisLocation','right','xtick',[],'xticklabels',[],...
    'YColor','b','LineWidth',2,'TickDir','out');
axis on
box off
ax4_pos = ax3_pos;
ax4 = axes('Position', ax4_pos);
dataType = synLengths;
dataPoints  = cellfun(@sum,dataType);
l4 = plot(dataPoints,'r','LineWidth',2,'Parent',ax4);
set(ax4,'Color','none','YAxisLocation','right','xtick',[],'xticklabels',[],...
    'YColor','r','LineWidth',2,'TickDir','out');
axis on
box off
%% 
leg = legend([l1;l2;l3;l4],{'# real endings',...
    '# branchpoints','# synapses','Axon lengths (\mum)'},'Location','northwest'); 
legend boxoff


%% set limits to x axis
lineLim = [6,17]; % manual at first and last zero entry
histLim = [];
histLim(1) = ax1.XLim(1) + lineLim(1)*(ax1.XLim(2)-ax1.XLim(1))/(ax3.XLim(2)-ax3.XLim(1));
histLim(2) = ax1.XLim(1) + lineLim(2)*(ax1.XLim(2)-ax1.XLim(1))/(ax3.XLim(2)-ax3.XLim(1));

set(ax3, 'XLim',lineLim);
set(ax4, 'XLim',lineLim);
set(ax1,'XLim',histLim)
set(ax2,'XLim',histLim)

outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'panel_along_depth'));
% export_fig(outfile,'-q101','-transparent');

