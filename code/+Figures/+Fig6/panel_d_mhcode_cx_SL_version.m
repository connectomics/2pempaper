%% H data circuit sketch
setConfig;
[cx_data,cx_datatxt,cx_dataraw] = xlsread(config.cffi_excel);
separateFigures = true;
idsPy3 = Util.getPy3Ids;
py3Color = Util.getPy3Color;
somaColors = Util.getSomaColors;

restrictFlag = true; % restrict to exc cells within barrel
removePy3 = true;

%%
Util.log('Reading data...')
% 2019-08-12_cffi_v1.xlsx
% NOTE : defined ignoring the first row with variable names
cx_tcCols = find(cellfun(@(x) any(regexp(x,'t\d+')),cx_dataraw(1,:)));
cx_StpRows = reshape(find(cellfun(@(x) any(regexp(x,'^p$')),cx_dataraw(:,1))) - 1,1,'');% defined ignoring first row
cx_SpsRows = reshape(find(cellfun(@(x) any(regexp(x,'^s$')),cx_dataraw(:,1))) - 1,1,'');
cx_INrows = reshape(find(cellfun(@(x) any(regexp(x,'i')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row
cx_INCols = find(cellfun(@(x) any(regexp(x,'i\d+')),cx_dataraw(1,:)));
cx_INtypeCol = size(cx_dataraw,2); % col in cx_dataraw
cx_idsCol = 2;
cx_depthCol = 3;

rng(0)
cx_postsyn_depth = cx_data(:,cx_depthCol)+600;
cx_targetXPos = cx_postsyn_depth*0+1150+(rand(size(cx_postsyn_depth,1),1)-0.5)*50;
cx_targetXPos(cx_StpRows) = cx_postsyn_depth(cx_StpRows)*0+1050+(rand(size(cx_StpRows,2),1)-0.5)*75;  % Stp
cx_targetXPos(cx_SpsRows) = cx_postsyn_depth(cx_SpsRows)*0+1150+(rand(size(cx_SpsRows,2),1)-0.5)*75;  % Sps
cx_targetXPos(cx_INrows) = cx_postsyn_depth(cx_INrows)*0+1250+(rand(size(cx_INrows,2),1)-0.5)*75;  % INs
cx_INvertOffset = 0;% -50;%-200;
cx_tcXspacing = 5;
cx_tcXoffsetIN = 880; %1400; TC to IN input from same TC start
cx_tcXoffsetExc = 880;

%% load connectome and restrict to exN in barrel
idxTCPlot = 1:20;
load(config.connmat_SLmanual)

if restrictFlag
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
else
    INStp = true(countStp,1);
    INSps = true(countSps,1);
end

%% plot stp, sps, INs
Util.log('Plotting somas...')
fig = figure;
fig.Color = 'white';
hold on
if ~separateFigures
    subplot(1,4,1)
end
cx_postType = []; % 1 for Ins, 2 for Stp, 3 for Sps, 5 for Py3
i_post_exc_plot = []; % List of stp sps plotted here
for i_post = 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);  
    switch cx_datatxt{i_post+1,1}
        case 'i'
            cx_postType(i_post) = 1;
        case 'p'
            i_post_exc_plot(end+1) = i_post;
            id = str2double(cx_datatxt{i_post+1,2});
            if ismember(id, idsPy3) % ids of py3 to group 4
                cx_postType(i_post) = 4;
            else
                cx_postType(i_post) = 2;
            end
        case 's'
            i_post_exc_plot(end+1) = i_post;
            cx_postType(i_post) = 3;
    end
end

if restrictFlag % restrict exc to those within barrel
    idxStpHere = find(cx_postType(i_post_exc_plot)==2);
    stpInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxStpHere)+1, 2}});
    idxStpHereKeep = INStp(stpInnv);
    idxSpsHere = find(cx_postType(i_post_exc_plot)==3);
    spsInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxSpsHere)+1, 2}});
    idxSpsHereKeep = INSps(spsInnv);

    i_post_exc_plot = reshape(i_post_exc_plot([idxStpHereKeep(:); idxSpsHereKeep(:)]),1,'');
end

%% TC input
Util.log('Plotting tc input...')
cx_postColors = {[0,1,0],[1,0,1],[0,0,1], py3Color}; % IN, Stp, Sps, Py3
faceAlpha = 0.5;
cx_TCsynPosVec_byType = cell(1, numel(unique(cx_postType)));
cx_TCinputPerCell = cell(1, numel(unique(cx_postType)));
for i_tcc =1:length(cx_tcCols)
    i_tc = cx_tcCols(i_tcc);
    cx_thisXposIN = i_tcc*cx_tcXspacing+cx_tcXoffsetIN;
    cx_thisXposExc = i_tcc*cx_tcXspacing+cx_tcXoffsetExc;
    cx_thisTCmaxpos = 0;
    for i_post = 1:size(cx_dataraw,1)-1
        if removePy3
            if cx_postType(i_post) == 4
                continue;
            end
        end
        cx_post_pos = cx_postsyn_depth(i_post);           
        if strcmp(cx_datatxt{i_post+1,1},'i')
            cx_post_pos = cx_post_pos+cx_INvertOffset;
        end
        if isempty(cx_datatxt{i_post+1,i_tc}) %no tc synapse but empty string will be non empty cell-array
            cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
        else
            cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % empty string gives empty,  multiple numbers to array
        end
        if ~isempty(cx_thisPosVec)
            for iii=1:length(cx_thisPosVec)
                curLineWidth = 1;
                curMarkerSize = 5;
                % if IN input
                if ismember(i_post,cx_INrows)
                    plot([cx_thisXposIN cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
                        '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                    cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
                    plot(cx_thisXposIN,cx_thisPosVec(iii),'-','Color',...
                        cx_postColors{cx_postType(i_post)},'MarkerSize',curMarkerSize);
                else % if exc input
                    if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
                        plot([cx_thisXposExc cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
                            '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                        cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
                    end
                end
                cx_TCsynPosVec_byType{cx_postType(i_post)} = [cx_TCsynPosVec_byType{cx_postType(i_post)},...
                    cx_thisPosVec(iii)];                
            end
        end
        cx_TCinputPerCell{cx_postType(i_post)} = [cx_TCinputPerCell{cx_postType(i_post)} ,length(cx_thisPosVec)];
    end
    % vertical TC axon lines
    plot(repmat(cx_thisXposIN,[1 2]),[-50 cx_thisTCmaxpos],'-c','LineWidth',1);hold on
    plot(cx_thisXposIN,-10,'^k','MarkerSize',5);
%     plot(repmat(cx_thisXposExc,[1 2]),[-1000 cx_thisTCmaxpos],'-k','LineWidth',1);hold on
%     plot(cx_thisXposExc,-50,'^k','MarkerSize',5);

end

%% Stp+Py3 somas
for i_post = 1:size(cx_dataraw,1)-1
    if removePy3
        if cx_postType(i_post) == 4
            continue;
        end
    end
     cx_post_pos = cx_postsyn_depth(i_post);
     switch cx_datatxt{i_post+1,1}
         case 'p'
             if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
                 id = str2double(cx_datatxt{i_post+1,2});
                 if ismember(id, idsPy3) % ids of py3 to group 4
                     plot(cx_targetXPos(i_post),cx_post_pos,'.','color', cx_postColors{cx_postType(i_post)},'MarkerSize',20);
                 else
                     plot(cx_targetXPos(i_post),cx_post_pos,'.','color', cx_postColors{cx_postType(i_post)},'MarkerSize',20);
                 end
             end
     end
    
end

%% IN inputs to Stp+Py3
Util.log('Plotting in input...')
faceAlpha = 0.5;
% map each col IN location to its row location
tempid = cellfun(@(x) regexp(x,'i(?<id>\d+)','names'),cx_dataraw(1,cx_INCols));
map_col_to_row = cellfun(@str2double,{tempid.id});
for i_tcc =1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_targetXPos(map_col_to_row(i_tcc));    
    cx_thisYpos = cx_postsyn_depth(map_col_to_row(i_tcc))+cx_INvertOffset;    
    for i_post = 1:size(cx_dataraw,1)-1
        if removePy3
            if cx_postType(i_post) == 4
                continue;
            end
        end
        if strcmp(cx_datatxt{i_post+1,1},'p') % for stp + py3 only
            if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
                cx_post_pos = cx_postsyn_depth(i_post);
                if strcmp(cx_datatxt{i_post+1,1},'i')
                    cx_post_pos = cx_post_pos+cx_INvertOffset;
                end
                if isempty(cx_datatxt{i_post+1,i_tc})
                    cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
                else
                    cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
                end
                if ~isempty(cx_thisPosVec)
        %             for iii=1:length(cx_thisPosVec)
                        plot([cx_thisXpos cx_targetXPos(i_post)],[cx_thisYpos cx_post_pos],'-',...
                            'Color',[0,0,0,faceAlpha],'LineWidth',0.5);
        %             end
                end
            end
        end
    end
end

%% Sps somas
for i_post = 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);
    switch cx_datatxt{i_post+1,1}
        case 's'
            if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
                plot(cx_targetXPos(i_post),cx_post_pos,'.','color', cx_postColors{cx_postType(i_post)},'MarkerSize',20);
            end
    end
end

%% IN input to Sps
faceAlpha = 0.5;
% map each col IN location to its row location
tempid = cellfun(@(x) regexp(x,'i(?<id>\d+)','names'),cx_dataraw(1,cx_INCols));
map_col_to_row = cellfun(@str2double,{tempid.id});
for i_tcc =1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_targetXPos(map_col_to_row(i_tcc));
    cx_thisYpos = cx_postsyn_depth(map_col_to_row(i_tcc))+cx_INvertOffset;
    for i_post = 1:size(cx_dataraw,1)-1
        if strcmp(cx_datatxt{i_post+1,1},'s')% for sps only
            if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
                cx_post_pos = cx_postsyn_depth(i_post);
                if strcmp(cx_datatxt{i_post+1,1},'i')
                    cx_post_pos = cx_post_pos+cx_INvertOffset;
                end
                if isempty(cx_datatxt{i_post+1,i_tc})
                    cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
                else
                    cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
                end
                if ~isempty(cx_thisPosVec)
        %             for iii=1:length(cx_thisPosVec)
                        plot([cx_thisXpos cx_targetXPos(i_post)],[cx_thisYpos cx_post_pos],'-',...
                            'Color',[0,0,0,faceAlpha],'LineWidth',0.5);
        %             end
                end
            end
        end
    end
end
%% IN input to IN
faceAlpha = 0.5;
% map each col IN location to its row location
tempid = cellfun(@(x) regexp(x,'i(?<id>\d+)','names'),cx_dataraw(1,cx_INCols));
map_col_to_row = cellfun(@str2double,{tempid.id});
for i_tcc =1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_targetXPos(map_col_to_row(i_tcc));
    cx_thisYpos = cx_postsyn_depth(map_col_to_row(i_tcc))+cx_INvertOffset;
    for i_post = 1:size(cx_dataraw,1)-1
        if strcmp(cx_datatxt{i_post+1,1},'i')% for IN only
            cx_post_pos = cx_postsyn_depth(i_post);
            if strcmp(cx_datatxt{i_post+1,1},'i')
                cx_post_pos = cx_post_pos+cx_INvertOffset;
            end
            if isempty(cx_datatxt{i_post+1,i_tc})
                cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
            else
                cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc});
            end
            if ~isempty(cx_thisPosVec)
    %             for iii=1:length(cx_thisPosVec)
                    plot([cx_thisXpos cx_targetXPos(i_post)],[cx_thisYpos cx_post_pos],'-',...
                        'Color',[0,0,0,faceAlpha],'LineWidth',0.5);
    %             end
            end
    end
    end
end


%% IN somas
for i_post = 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);   
    switch cx_datatxt{i_post+1,1}
        case 'i'
            plot(cx_targetXPos(i_post),cx_post_pos+cx_INvertOffset,'s','color', 'k', 'MarkerSize',10);
    end
end

% set labels
set(gca,'YLim',[-80 400]) % changed from [-250,500] by SL
box off
set(gca,'TickDir','out');
%%
if separateFigures
    box off
    xlabel('')
    ylabel('')
%    set(gca,'xticklabels','','xcolor','none','ycolor','none')
    set(gcf,'Position',get(0, 'Screensize'));
    Util.log('Saving fig panel 1...')
    %outfile = fullfile(outDir, 'outData/figures','fig3',config.version,'panel_graph_conn.eps');
    %export_fig(outfile, '-q101', '-nocrop', '-transparent');
    outfile = fullfile(outDir, 'outData/figures','fig3',config.version,'panel_graph_conn.pdf');
%     export_fig(outfile, '-q101', '-nocrop');

%     close all
else 
    text(1310,150,'L4');
    text(1310,400,'L3');
    text(1310,-100,'L5A');
end
daspect([200,200,1])




keyboard
%% panel 2
Util.log('Plotting panel2 ...')
if separateFigures
    fig = figure;
    fig.Color = 'white';
else
    subplot(1,4,2)
end
cx_posBins = 0:25:500;
yd=cell(3,1);
pd = cell(3,1);
for cx_typeC=[2 3 1]
    yData = cx_TCsynPosVec_byType{cx_typeC};
    if cx_typeC==1 % IN
        somaColors = Util.getSomaColors;
        [~, ~, curpd] = Util.fitGaussian(yData, cx_posBins,'normal',somaColors{1}(1:3)); % IN axon green    
        clear somaColors
    else
        [~, ~, curpd] = Util.fitGaussian(yData, cx_posBins,'normal',cx_postColors{cx_typeC});
    end
    yd{cx_typeC} = yData;
    pd{cx_typeC} = curpd;
    sprintf('Prob dist for %d \n',cx_typeC)
    disp(curpd)
%     cx_TCsynPosVec_byType_hist = hist(cx_TCsynPosVec_byType{cx_typeC},cx_posBins);
%     cx_hist_link = stairs(cx_TCsynPosVec_byType_hist,cx_posBins);hold on;
% %     set(cx_hist_link,'FaceColor',cx_postCols{cx_typeC});
%     set(cx_hist_link,'Color',cx_postCols{cx_typeC},'LineWidth',3);
end
set(gca,'XLim',[-150 500],'XDir','reverse','TickDir','out','YAxisLocation','right')
camroll(-90);
% do tests: kmb: IN/Stp/Sps
[~,p1_km,~] = kstest2(yd{1},yd{2}); [p2_km,~,~] = ranksum(yd{1},yd{2});
[~,p1_kb,~] = kstest2(yd{1},yd{3}); [p2_kb,~,~] = ranksum(yd{1},yd{3});
[~,p1_mb,~] = kstest2(yd{2},yd{3}); [p2_mb,~,~] = ranksum(yd{2},yd{3});
if separateFigures
    title(sprintf(['IN-Stp: (KS)p=%.03e, (RS)p=%.03e\n ',...
        'IN-Sps: (KS)p=%.03e, (RS)p=%.03e\n ',...
        'Stp-Sps: (KS)p=%.03e, (RS)p=%.03e'],...
        p1_km,p2_km,p1_kb,p2_kb,p1_mb,p2_mb))
    % cosmetics
    box off
    set(gca,'ylim',[0,40],'yticklabel','','ytick',[0,20,40],...
        'LineWidth',2,...
        'Color','none')
%    set(gca,'XColor','none')
    xlabel('')
    ylabel('')
    Util.log('Saving fig panel 2...')
    outfile = fullfile(outDir, 'outData/figures','fig3',config.version,'panel_tc_syn_num_not_used.eps');
    export_fig(outfile, '-q101', '-nocrop', '-transparent');
    close all
else
    text(1310,150,'L4');
    text(1310,400,'L3');
    text(1310,-100,'L5A');
    sprintf(['IN-Stp: (KS)p=%.03e, (RS)p=%.03e\n ',...
        'IN-Sps: (KS)p=%.03e, (RS)p=%.03e\n ',...
        'Stp-Sps: (KS)p=%.03e, (RS)p=%.03e'],...
        p1_km,p2_km,p1_kb,p2_kb,p1_mb,p2_mb)
end

%% panel 3
Util.log('Plotting panel3...')
if separateFigures
    fig = figure;
    fig.Color = 'white';
else
    subplot(1,4,3)
end
for cx_typeC=[1 2 3]
    cx_TCsynPerCell_hist = hist(cx_TCinputPerCell{cx_typeC},0:10);
    cx_hist_link = stairs(0:10,cx_TCsynPerCell_hist);hold on;
    set(cx_hist_link,'Color',cx_postColors{cx_typeC},'LineWidth',3);
    mean(cx_TCinputPerCell{cx_typeC})
end
set(gca,'YScal','log','TickDir','out');
% [h,p,ci,stats]=ttest2(cx_TCinputPerCell{1},cx_TCinputPerCell{3})

if separateFigures
    box off;
    set(gca,'xtick',[0,5,10],'LineWidth',2,...
        'Color','none')
    Util.log('Saving fig panel 3...')
    outfile = fullfile(outDir, 'outData/figures','fig3',config.version,'panel_tc_syn_per_L4cell.eps');
    export_fig(outfile, '-q101', '-nocrop', '-transparent');
    close all
else
   xlabel('TC syn per L4 cell');
end
%% panel 4
Util.log('Plotting panel4...')
% IN to IN plot separately
if separateFigures
    fig = figure;
    fig.Color = 'white';
else
    subplot(1,4,4)
end
somaColors = Util.getSomaColors;
cx_postsyn_horiz = cx_data(:,1);
% cx_INtypeColors = {[1 0 0],[0.6 0 0],[1 1 0],[0.7 0.7 0.7],'c'};
cx_INtypeColors = somaColors;
for i_post = 1:size(cx_postType,2)
    cx_post_pos = cx_postsyn_depth(i_post);        
    if cx_postType(i_post) == 1
        plot(cx_postsyn_horiz(i_post),cx_post_pos+cx_INvertOffset,'sk','MarkerSize',10); hold on
        cx_thisINtype = str2double(cx_dataraw{i_post+1,cx_INtypeCol});
        if cx_thisINtype>0
            plot(cx_postsyn_horiz(i_post),cx_post_pos+cx_INvertOffset,...
                '+','MarkerSize',10,'Color',cx_INtypeColors{cx_thisINtype}); hold on
        end
    end
end

for i_tcc =1:length(cx_INCols)
    i_tc = cx_INCols(i_tcc);
    cx_thisXpos = cx_postsyn_horiz(i_tcc);    
    cx_thisYpos = cx_postsyn_depth(i_tcc)+cx_INvertOffset;    
    for i_post = cx_INrows
        cx_post_pos = cx_postsyn_depth(i_post);
        if strcmp(cx_datatxt{i_post+1,1},'i')
            cx_post_pos = cx_post_pos+cx_INvertOffset;
        end
        if isempty(cx_datatxt{i_post+1,i_tc})
            cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
        else
            cx_thisPosVec = str2double(cx_dataraw{i_post+1,i_tc});
        end
        if ~isempty(cx_thisPosVec)
            for iii=1:length(cx_thisPosVec)
                plot([cx_thisXpos cx_postsyn_horiz(i_post)],[cx_thisYpos cx_post_pos],'-g');
            end
        end
    end
end
daspect([1 1 1])
% if separateFigures
%     Util.log('Saving fig panel 4...')
%     outfile = fullfile(outDir, 'outData/figures','fig3',config.version,'panel_mh_4.eps');
%     export_fig(outfile, '-q101', '-nocrop', '-transparent');
%     close all
% else
%     saveas(gcf,fullfile(outDir, 'outData/figures','fig3',config.version,'panels_mh.png'))
%     export_fig(fullfile(outDir, 'outData/figures','fig3',config.version,'panels_mh.eps'));
% end
% %% soma targeting fraction
% 
% cx_dataSoma = xlsread(fullfile('/Users/mh/Documents/_mhlab/publications/2017 2P_SBEM','IN_full_reconstructed_mh_sheet2.xlsx'));
% 
% figure
% plot(cx_dataSoma(:,7),cx_dataSoma(:,9),'x');daspect([1 1 1])
% set(gca,'XLim',[0 0.5],'YLim',[0 0.5]);xlabel('soma synapse fraction');ylabel('spine synapse fraction');
