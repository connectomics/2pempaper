% make TC axon 01 axonogram with sp o, sh o, stp v, sps v outputs
% TC axons as black
setConfig;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

synSize = 50; tubeSize = 1; dotSize= 3;
somaColors = Util.getSomaColors;
synBallColorSh = somaColors{1}(1:3); % green open
synBallColorSp = [0,0,0]; % black
synBallColorIn = somaColors{1}(1:3); % green
synBallColorStp = somaColors{end-1}(1:3);
synBallColorSps = somaColors{end}(1:3);
synBallColorPy3 = Util.getPy3Color;
colorPC = [0.5, 0.5, 0.5];

restrictBarrel = true;

if restrictBarrel
    load(config.connmat_SLmanual,'synMapINStp','synMapINSps')
    Util.log('Restrict to ExNs inside barrel')
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
    stpOut = find(~INStp);
    spsOut = find(~INSps);
else
    stpOut = []; % remove nothing
    spsOut = []; % remove nothing
end

idsPy3 = Util.getPy3Ids;

idsPExclude = unique([idsPy3(:); stpOut(:)]);
idsSExclude = spsOut;

% all soma bodies as grey
skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;

nml = fullfile(config.skelTCCombined);
skel = skeleton(nml);
skel = skel.keepTreeWithName('TC axon 01','exact');

%% make axonogram (PLASS-like)
startpoint = skel.getNodesWithComment('_root',1,'regexp');

% attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
comments = {skel.nodesAsStruct{1}.comment}';
assert(startpoint == find(contains(comments,'_root')))

% find all comments you need
synLocsSp = find(cellfun(@(x) any(regexp(x,'^sp([-\s])?$')),comments)); % starts with sp, ends with sp or sp- or spspace optinally
synLocsSh = find(cellfun(@(x) any(regexp(x,'^sh([\s-])?(i)?([-\s])?$')),comments)); % sh i or sh
synLocsIn = find(cellfun(@(x) any(regexp(x,'^sh([\s-])?i([-\s])?(\d+)$')),comments)); % sh i xx

%synLocsStp = find(cellfun(@(x) any(regexp(x,'^s[ph](\s)?[p]([\s\d+])*?$')),comments));
idxP = cellfun(@(x) any(regexp(x,'^s[ph](\s)?[p]([\s\d+])*?$')),comments);
ids = cellfun(@(x) regexp(x,'^s[ph](\s)?p(\s)?(?<id>\d+)$','names'), comments(idxP),'uni',0);
ids = cellfun(@(x) str2double(x.id),ids);

isExclude = ismember(ids, idsPExclude);

idxP = find(idxP);
synLocsStp = idxP(~isExclude); 

% sps
idxS = cellfun(@(x) any(regexp(x,'^s[ph](\s)?[s]([\s\d+])*?$')),comments);
ids = cellfun(@(x) regexp(x,'^s[ph](\s)?s(\s)?(?<id>\d+)$','names'), comments(idxS),'uni',0);
ids = cellfun(@(x) str2double(x.id),ids);

isExclude = ismember(ids, idsSExclude);

idxS = find(idxS);
synLocsSps = idxS(~isExclude);
%synLocsSps = find(cellfun(@(x) any(regexp(x,'^s[ph](\s)?[s]([\s\d+])*?$')),comments));

tic;
Util.log('Now scatterting...')
[TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
xlabel(''); ylabel('');
set(gca,'xtick',[0,150,300],'xticklabel','')
% spines
XY = Axonogram.findPointsInAxonogram(synLocsSp, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',synBallColorSp, 'MarkerFaceColor','none');
clear XY
% shafts
XY = Axonogram.findPointsInAxonogram(synLocsSh, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),150, 'o','MarkerEdgeColor',synBallColorSh,'MarkerFaceColor', 'none');
clear XY
%{
% py3
XY = Axonogram.findPointsInAxonogram(synLocsPy3, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),150, 'o','MarkerEdgeColor',synBallColorPy3,'MarkerFaceColor', synBallColorPy3);
clear XY
%}
% IN
XY = Axonogram.findPointsInAxonogram(synLocsIn, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),150, 'o','MarkerEdgeColor',synBallColorIn,'MarkerFaceColor',synBallColorIn);
clear XY
% Stp
XY = Axonogram.findPointsInAxonogram(synLocsStp, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),150, 'o', 'MarkerEdgeColor', synBallColorStp,  'MarkerFaceColor', synBallColorStp);
clear XY
% Sps
XY = Axonogram.findPointsInAxonogram(synLocsSps, TREE, skel, config.scaleEM);
gcf, scatter(XY(:,1),XY(:,2),150, 'o', 'MarkerEdgeColor', synBallColorSps,  'MarkerFaceColor', synBallColorSps);
% cosmetics
daspect([80 100 1])
box off
camroll(90);
set(gca,'linewidth',2,'ycolor',[1 1 1]); % off xaxis
title('')
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'panel_axonogram'));
% export_fig(outfile,'-q101', '-nocrop', '-transparent');
% Util.log('Saving file %s', outfile);
% close(f);    
toc;

