% panel with Tc axon and sps, stp, IN dendrite
setConfig;
outDir = config.outDir;

do3D = true;
convHull = true;

Rmin = 0; Rmax = 0.2;
Gmin = 0.5; Gmax = 0.9;
Bmin = 0.3; Bmax = 0.75;

somaColors = Util.getSomaColors;

tubeSize = 1; dotSize= 3;
somaBallSize = 150;
somaStpColor = somaColors{end-1}(1:3);%magenta
somaSpsColor = somaColors{end}(1:3);%blue
skelTCColor =  Util.getTCColor; % 
somaInColor = [0,0,0]; % black
skelInColor = [0,0,0]; % dendrite
skelInAxonColor = somaColors{1}(1:3); % BIN1 green
synBallSize = 70; synShape = 'v';
%synBallColorTC = skelTCColor;
%synBallColorIN = skelInAxonColor;

outDirPdf = fullfile(config.outDir,'outData/figures/fig3/', config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];
clear skelIn

skel = skeleton(config.cffi); % tc axons and their outputs to SpS, StP, IN
skel.scale = config.scaleEM;

tcId = 1;
stpId = 9;
spsId =  25;
inId = 2;
% TC axon
skelTC = skel.keepTreeWithName(['TC axon ' num2str(tcId,'%02d')],'exact');

% cell i## with soma at comment '_in ##' in cell i01 tree name
skelIn = skel.keepTreeWithName(['cell i' num2str(inId,'%02d')],'partial');
somaIn =  Util.getNodeCoordsWithComment(skelIn, {'soma','cellbody','_in'}, 'partial');
synIn =  Util.getNodeCoordsWithComment(skelIn, ['tc' num2str(tcId)], 'exact');

% cell p## with soma at comment '_sp ##'
skelStp = skel.keepTreeWithName(['cell p' num2str(stpId,'%02d')], 'exact');
somaStp =  Util.getNodeCoordsWithComment(skelStp, '_sp', 'partial');
synStp = Util.getNodeCoordsWithComment(skelStp, ['tc' num2str(tcId)], 'exact');

% cell s## with soma postion at comment '_ss ##'
skelSps = skel.keepTreeWithName(['cell s' num2str(spsId,'%02d')], 'exact');
somaSps =  Util.getNodeCoordsWithComment(skelSps, '_ss', 'partial');
synSps = Util.getNodeCoordsWithComment(skelSps, ['tc' num2str(tcId)], 'exact');

% IN axon ## and its syn to p/s
skelInAxon = skel.keepTreeWithName(['IN axon ' num2str(inId,'%02d')],'partial');
% remove extra nodes
skelInAxon = skelInAxon.deleteNodesWithComment('-','exact');
synInAxonStp =  Util.getNodeCoordsWithComment(skelInAxon, ['s[hp]\s(p(\s)?' num2str(stpId,'%02d') ')$'],'regexp');
synInAxonSps =  Util.getNodeCoordsWithComment(skelInAxon, ['s[hp]\s(s(\s)?' num2str(spsId,'%02d') ')$'],'regexp');

% extract all synCoords
synCoordsTC = vertcat(synIn, synStp, synSps);
%synCoordsIN = vertcat(synInAxonStp, synInAxonSps);

synBallColorTC = vertcat(repmat(somaInColor,size(synIn,1),1),...
                        repmat(somaStpColor,size(synStp,1),1),...
                        repmat(somaSpsColor,size(synSps,1),1)); 

synBallColorIN = vertcat(repmat(somaStpColor,size(synInAxonStp,1),1),...
                        repmat(somaSpsColor,size(synInAxonSps,1),1)); 

%scale all spheres
scale = skel.scale./1000;
somaLocsStp = skel.setScale(somaStp,scale);
somaLocsSps = skel.setScale(somaSps,scale);
somaLocsIn = skel.setScale(somaIn,scale);
nodesPC = skel.setScale(nodesPC, scale);
somaLocsSynTC = skel.setScale(synCoordsTC,scale);
%somaLocsSynIN = skel.setScale(synCoordsIN,scale);

Util.log('Now plotting figure....');
f = figure();
ax = axes(f);
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;

%replace with gray area
if convHull
    DT = delaunayTriangulation(nodesPC);
    [K,v] = convexHull(DT);
    trisurf(K,DT.Points(:,1),DT.Points(:,2),DT.Points(:,3),'FaceColor',[0.5,0.5,0.5],'EdgeColor','none','FaceAlpha',.1);
    camlight 
    lighting gouraud
else
    %objectPC = ...
    %        scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
    %        ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
    %        'MarkerFaceColor', colorPC(1:3),'MarkerFaceAlpha',0.3,'MarkerEdgeAlpha',0.3);
end

skelStp.plot('', somaStpColor(1:3) ,true, tubeSize);
skelSps.plot('', somaSpsColor(1:3) ,true, tubeSize);
skelTC.plot('', skelTCColor(1:3) ,true, tubeSize);
skelIn.plot('',skelInColor(1:3), true, tubeSize);
%skelInAxon.plot('',skelInAxonColor(1:3), true, tubeSize);
objectStp = ...
             scatter3(somaLocsStp(:,1),somaLocsStp(:,2),somaLocsStp(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaStpColor(1:3), ...
            'MarkerFaceColor', somaStpColor(1:3));
objectSps = ...
             scatter3(somaLocsSps(:,1),somaLocsSps(:,2),somaLocsSps(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaSpsColor(1:3), ...
            'MarkerFaceColor', somaSpsColor(1:3));
objectIn = ...
             scatter3(somaLocsIn(:,1),somaLocsIn(:,2),somaLocsIn(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaInColor(1:3), ...
            'MarkerFaceColor', somaInColor(1:3));
synapseObjectTC = ...
        scatter3(somaLocsSynTC(:,1),somaLocsSynTC(:,2),somaLocsSynTC(:,3)...
        ,synBallSize,synBallColorTC, 'filled', synShape);
%synapseObjectIN = ...
%        scatter3(somaLocsSynIN(:,1),somaLocsSynIN(:,2),somaLocsSynIN(:,3)...
%        ,synBallSize,synBallColorIN, 'filled',synShape);

%Visualization.Figure.removeWhitespaceFromPlot();
%yz
Util.setView(3, true);
if do3D
    % do once for sanity check then remove
    % axis on
    % box on
    % ax.BoxStyle = 'full';
    camorbit(-10,0,'camera');
    camorbit(0,-15,'data',[0 1 0]);
end

%% hacky to match other panels figures
%ax = gca; ax.XLim = [-96,420]; ax.YLim = [-96,420];
if do3D
    outfile = fullfile(outDirPdf,['panel_cffi_sketch_left_3d.png']);
else
    outfile = fullfile(outDirPdf,['panel_cffi_sketch_left.eps']);
end
% export_fig(outfile,'-q101', '-nocrop', '-transparent','-m8');
% Util.log('Saving file %s.', outfile);
% close(f);
