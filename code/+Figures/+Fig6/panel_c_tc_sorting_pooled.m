%% data circuit sketch

% add shaft/ other spine curve as well (discussed 4.11.2020) "_pooled"
% NOTE: DoNOT USE FOR CFFI: this restrict TC but not the Exc that are not inside barrel

setConfig;
[cx_data,cx_datatxt,cx_dataraw] = xlsread(config.cffi_excel_SLmanual_pooled);
restrictTC = false; % do for all TCs
idsPy3 = Util.getPy3Ids;

somaColors = Util.getSomaColors;
py3Color = Util.getPy3Color;

%%
Util.log('Reading data...')
% NOTE : defined ignoring the first row with variable names
cx_tcCols = find(cellfun(@(x) any(regexp(x,'t\d+')),cx_dataraw(1,:)));
cx_StpRows = reshape(find(cellfun(@(x) any(regexp(x,'^p$')),cx_dataraw(:,1))) - 1,1,'');% defined ignoring first row
cx_SpsRows = reshape(find(cellfun(@(x) any(regexp(x,'^s$')),cx_dataraw(:,1))) - 1,1,'');
cx_INrows = reshape(find(cellfun(@(x) any(regexp(x,'^i$')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row
cx_INCols = find(cellfun(@(x) any(regexp(x,'i\d+')),cx_dataraw(1,:)));
cx_INtypeCol = size(cx_dataraw,2); % col in cx_dataraw
cx_ShaftRows = reshape(find(cellfun(@(x) any(regexp(x,'shaft')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row 
cx_SpineRows = reshape(find(cellfun(@(x) any(regexp(x,'spine')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row
cx_idsCol = 2;
cx_depthCol = 3;

rng(0)
cx_postsyn_depth = cx_data(:,cx_depthCol)+600;
cx_targetXPos = cx_postsyn_depth*0+1150+(rand(size(cx_postsyn_depth,1),1)-0.5)*50;
cx_targetXPos(cx_StpRows) = cx_postsyn_depth(cx_StpRows)*0+1050+(rand(size(cx_StpRows,2),1)-0.5)*75;  % Stp
cx_targetXPos(cx_SpsRows) = cx_postsyn_depth(cx_SpsRows)*0+1150+(rand(size(cx_SpsRows,2),1)-0.5)*75;  % Sps
cx_targetXPos(cx_INrows) = cx_postsyn_depth(cx_INrows)*0+1250+(rand(size(cx_INrows,2),1)-0.5)*75;  % INs

cx_targetXPos(cx_ShaftRows) = cx_postsyn_depth(cx_ShaftRows)*0+1150+(rand(size(cx_ShaftRows,2),1)-0.5)*75;  % Shaft
cx_targetXPos(cx_SpineRows) = cx_postsyn_depth(cx_SpineRows)*0+1250+(rand(size(cx_SpineRows,2),1)-0.5)*75;  % Spine

cx_INvertOffset = 0;% -50;%-200;
cx_tcXspacing = 5;
cx_tcXoffsetIN = 880; %1400; TC to IN input from same TC start
cx_tcXoffsetExc = 880;
cx_postType = []; % 1 for Ins, 2 for Stp, 3 for Sps, 4 Shaft, 5 Spine, 6 Py3
for i_post = 1:size(cx_dataraw,1)-1
    cx_post_pos = cx_postsyn_depth(i_post);  
    switch cx_datatxt{i_post+1,1}
        case 'i'
%             plot(cx_targetXPos(i_post),cx_post_pos+cx_INvertOffset,'sk','MarkerSize',10);
            cx_postType(i_post) = 1;
        case 'p'
%              plot(cx_targetXPos(i_post),cx_post_pos,'.m','MarkerSize',20);            
            id = str2double(cx_datatxt{i_post+1,2});
            if ismember(id, idsPy3) % ids of py3 to group 6
                cx_postType(i_post) = 6;
            else
                cx_postType(i_post) = 2;
            end
        case 's'
%             plot(cx_targetXPos(i_post),cx_post_pos,'.b','MarkerSize',20);
            cx_postType(i_post) = 3;
        case 'shaft'
%             plot(cx_targetXPos(i_post),cx_post_pos,'.r','MarkerSize',20);
            cx_postType(i_post) = 4;
        case 'spine'
%             plot(cx_targetXPos(i_post),cx_post_pos,'.g','MarkerSize',20);
            cx_postType(i_post) = 5;
    end
end

%% TC input
if restrictTC
    Util.log('Restricting to three TCs:')
    idxTCPlot = [1,3,5]; % limit analysis to three TC that we analyzed
    suffix = '_restrictTC';
else
    idxTCPlot = 1:length(cx_tcCols);
    suffix = '';
end
cx_postColors = {somaColors{1}(1:3), [1,0,1], [0,0,1], [138,43,226]./255, [0.5,0.5,0.5], py3Color}; % green, magenta, blue, purple, grey, yellow-orange
faceAlpha = 0.5;
cx_TCsynPosVec_byType= cell(numel(unique(cx_postType)),1);
cx_TCinputPerCell = cell(numel(unique(cx_postType)),1);
for i_tcc = idxTCPlot %1:length(cx_tcCols)
    i_tc = cx_tcCols(i_tcc);
    cx_thisXposIN = i_tcc*cx_tcXspacing+cx_tcXoffsetIN;
    cx_thisXposExc = i_tcc*cx_tcXspacing+cx_tcXoffsetExc;

%     cx_thisXpos = i_tc*cx_tcXspacing+cx_tcXoffset;
    cx_thisTCmaxpos = 0;
    for i_post = 1:size(cx_dataraw,1)-1
        cx_post_pos = cx_postsyn_depth(i_post);           
        if strcmp(cx_datatxt{i_post+1,1},'i')
            cx_post_pos = cx_post_pos+cx_INvertOffset;
        end
        if isempty(cx_datatxt{i_post+1,i_tc}) %no tc synapse but empty string will be non empty cell-array
            cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
        else
            cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % empty string gives empty,  multiple numbers to array
        end
        if ~isempty(cx_thisPosVec)
            for iii=1:length(cx_thisPosVec)
                curLineWidth = 1;
                curMarkerSize = 5;
                % if IN input
                if ismember(i_post,cx_INrows)
%                    plot([cx_thisXposIN cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
%                        '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                    cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
%                     plot(cx_thisXposIN,cx_thisPosVec(iii),'>','Color',...
%                         cx_postColors{cx_postType(i_post)},'MarkerSize',curMarkerSize);
                else % if exc input
%                   plot([cx_thisXposExc cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
%                        '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                    cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
                end
                cx_TCsynPosVec_byType{cx_postType(i_post)} = [cx_TCsynPosVec_byType{cx_postType(i_post)},...
                    cx_thisPosVec(iii)];                
            end
        end
        cx_TCinputPerCell{cx_postType(i_post)} = [cx_TCinputPerCell{cx_postType(i_post)} ,length(cx_thisPosVec)];
    end
end

%%
Util.log('Now plotting:')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on
cx_posBins = 0:25:500;
binWidth = 25;
yd = cell(numel(unique(cx_postType)),1);
pd = cell(numel(unique(cx_postType)),1);
for cx_typeC=[1 2 3 4 6]
    yData = cx_TCsynPosVec_byType{cx_typeC};
    [ ~, curpd] = Util.fitGaussianNormalized(yData, cx_posBins,'normal',cx_postColors{cx_typeC}, binWidth);
    yd{cx_typeC} = yData;
    pd{cx_typeC} = curpd;
    sprintf('Prob dist for %d \n',cx_typeC)
    disp(curpd)
end
box off
set(gca,'XDir','reverse','TickDir','out','YAxisLocation','right')
camroll(-90);
ax.XAxis.Limits = [-50 400];
ax.XAxis.TickValues = -50:50:500;

ax.YAxis.Limits = [0 0.4];
ax.YAxis.TickValues = 0:0.1:0.4;

ax.LineWidth = 2;
daspect([458, 1, 1]) % manual
%%
Util.log('Saving fig panel...')
outfile = fullfile(outDir, 'outData/figures','fig3',config.version,['panel_tc_syn_num' suffix '_pooled.eps']);
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all

% do tests: kmb: IN/Stp/Sps
[~,p1_km,~] = kstest2(yd{1},yd{2}); [p2_km,~,~] = ranksum(yd{1},yd{2});
[~,p1_kb,~] = kstest2(yd{1},yd{3}); [p2_kb,~,~] = ranksum(yd{1},yd{3});
[~,p1_mb,~] = kstest2(yd{2},yd{3}); [p2_mb,~,~] = ranksum(yd{2},yd{3});

[p42,~,~] = ranksum(yd{4},yd{2}); % sh with stp
[p43,~,~] = ranksum(yd{3},yd{3}); % sh with sps


sprintf(['IN-Stp: (KS)p=%.03e, (RS)p=%.03e\n ',...
    'IN-Sps: (KS)p=%.03e, (RS)p=%.03e\n ',...
    'Stp-Sps: (KS)p=%.03e, (RS)p=%.03e'],...
    p1_km,p2_km,p1_kb,p2_kb,p1_mb,p2_mb)

sprintf('Shaft-Stp: p=%.03e \n Shaft-Sps p=%.03e', p42,p43)

