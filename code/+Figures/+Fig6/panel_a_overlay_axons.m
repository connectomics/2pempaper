% make TC axons figure with cell bodies as grey dots: 5TC axons
% email from MH: Tuesday, October 09, 2018 5:39 PM
% https://webknossos.brain.mpg.de/annotations/Explorational/5bbd4593010000e91ed6a06a#9585,22931,8944,0,0.64,341985
% spine syn: orange inv triangle
% shaft syn: violet inv tirangle
% add black cross to "out" or "gg" endings
% open black circle when real endings
% TC axons as black
setConfig;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
somaColors = Util.getSomaColors;
synBallSize = 10; synShape = 'o'; tubeSize = 1; dotSize= 2; crossSize = 15;

rng(0)
% black
Rmin = 0; Rmax = 0;
Gmin = 0; Gmax = 0;
Bmin = 0; Bmax = 0;

singleColor = Util.getTCColor;
oneFigure= false; % false for the main fig3 panels

synBallColorSp = [0,0,0];
synBallColorSh = somaColors{1}(1:3);
colorPC = somaColors{6}(1:3); % grey
colorCross= [0,0,0];
colorRoot = singleColor;

% all soma bodies as grey
skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;

nml = fullfile(config.skelTCCombined);
skel = skeleton(nml);
skel = skel.keepTreeWithName('TC axon','regexp');
totalTrees = skel.numTrees;
treeNames = skel.names;
numTrees = skel.numTrees;

% get axon Ids from cell names
axonIds = cellfun(@(x) regexpi(x,'TC axon (?<id>\w+)','names'),treeNames,'uni',0);
axonIds = cellfun(@(x) x.id,axonIds,'uni',0);

% plot whole skel with different color ranges just with barrel dots
%{
colorsForAxons = Util.getRangedColors( Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, numTrees);
colorsForAxons =  cat(2,colorsForAxons,ones(size(colorsForAxons,1),1));
colorsForAxons = mat2cell(colorsForAxons,ones(size(colorsForAxons,1),1),4);

colorsForAxons = vertcat(colorsForAxons{:});
colorsForAxons = colorsForAxons(:,1:3);
%}
colorsForAxons = repmat(singleColor, numTrees,1);

skel.scale = config.scaleEM;

Util.log('Add cross at false endings')
thisSkel = skel;
nodesCross1 = Util.getNodeCoordsWithComment(skel,'out','partial');
nodesCross2 = Util.getNodeCoordsWithComment(skel,'gg','partial');
nodesCross = vertcat(nodesCross1,nodesCross2);
nodesCircle = Util.getNodeCoordsWithComment(skel,'end','partial');
nodesRoot = Util.getNodeCoordsWithComment(skel,'root','partial');

%{
steps = 4; mode = 'up_to';
comment = 'end'; % true ending
[thisSkel,skelEndingsEnd] = splitEndings(skel,comment,steps, mode, true);
comment = 'out'; % out of dataset
[thisSkel,skelEndingsOut] = splitEndings(thisSkel,comment,steps, mode, true);
% add ending trees
if ~isempty(skelEndingsEnd.nodes)
    thisSkel = thisSkel.addTreeFromSkel(skelEndingsEnd);
else
    warning('skelEndingsEnd was empty!')
end
if ~isempty(skelEndingsOut)
    thisSkel = thisSkel.addTreeFromSkel(skelEndingsOut);
else
    warning('skelEndingsOut was empty!')
end

colorsForEndings = vertcat(repelem(colorEnd(1:3), skelEndingsEnd.numTrees,1),...
                         repelem(colorEnd(1:3), skelEndingsOut.numTrees,1));
colors = vertcat(colorsForAxons, colorsForEndings);
%}
colors = colorsForAxons;

somaLocsSp = Util.getNodeCoordsWithComment(skel,'sp','partial');
somaLocsSh = Util.getNodeCoordsWithComment(skel,'sh','partial');
scale = skel.scale./1000;
somaLocsSp = skel.setScale(somaLocsSp,scale);
somaLocsSh = skel.setScale(somaLocsSh,scale);
nodesPC = skel.setScale(nodesPC, scale);
nodesCross = skel.setScale(nodesCross,scale);
nodesCircle = skel.setScale(nodesCircle,scale);
nodesRoot = skel.setScale(nodesRoot,scale);

bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
if oneFigure
    tic;
    Util.log('Now plotting...')
    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    %Visualization.Figure.adaptPaperPosition();
    for j = [1,2,3,4]
        a(j) = subplot(2,2, j);
        hold on
        thisSkel.plot('',colors, true, tubeSize);
        synapseObjectSp = ...
                scatter3(somaLocsSp(:,1),somaLocsSp(:,2),somaLocsSp(:,3)...
                ,synBallSize,synShape,'filled', 'MarkerEdgeColor', synBallColorSp(1:3), ...
                'MarkerFaceColor', synBallColorSp(1:3));
        synapseObjectSh = ...
                scatter3(somaLocsSh(:,1),somaLocsSh(:,2),somaLocsSh(:,3)...
                ,synBallSize,synShape,'filled', 'MarkerEdgeColor', synBallColorSh(1:3), ...
                'MarkerFaceColor', synBallColorSh(1:3));
        synapseObjectPC = ...
                scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                'MarkerFaceColor', colorPC(1:3));
        synapseObjectCross = ...
                scatter3(nodesCross(:,1),nodesCross(:,2),nodesCross(:,3)...
                ,crossSize,'x','MarkerEdgeColor', colorCross(1:3), ...
                'MarkerFaceColor', colorCross(1:3));
        synapseObjectCircle = ...
                scatter3(nodesCircle(:,1),nodesCircle(:,2),nodesCircle(:,3)...
                ,crossSize,'o','MarkerEdgeColor', colorCross(1:3));
        synapseObjectRoot = ...
                scatter3(nodesRoot(:,1),nodesRoot(:,2),nodesRoot(:,3)...
                ,crossSize,'*','MarkerEdgeColor', colorRoot(1:3));
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        set(gca,'ytick',[])
        set(gca,'yticklabel',[])
        set(gca,'ztick',[])
        set(gca,'zticklabel',[])
    
        switch j
            case 1
                %xy
                Util.setView(6);
            case 2
                %xz
                Util.setView(2);
                %set(get(a(j),'Children'),'Visible','off');
            case 3
                %yz
                Util.setView(5);
            case 4
                %yz
                Util.setView(4);
                %set(get(a(j),'Children'),'Visible','off');
        end
        axis equal 
        axis off
        box off
        a(j).XLim = bbox(1,:);
        a(j).YLim = bbox(2,:);
        a(j).ZLim = bbox(3,:);
    end
    Visualization.Figure.removeWhiteSpaceInSubplot(f,a);
    thisSkel.filename = 'panel_overlay_axons';
    outfile = fullfile(outDirPdf, ...
        sprintf('%s.eps', thisSkel.filename));
%     export_fig(outfile,'-q101', '-nocrop', '-transparent');
%     Util.log('Saving file %s.', outfile);
%     close(f);
    toc;
else
    for j = [1,3]
        tic;
        Util.log('Now plotting...')
        f = figure();
        f.Units = 'centimeters';
        f.Position = [1 1 21 29.7];
        hold on
        thisSkel.plot('',colors, true, tubeSize);
        synapseObjectSp = ...
                scatter3(somaLocsSp(:,1),somaLocsSp(:,2),somaLocsSp(:,3)...
                ,synBallSize,synShape, 'MarkerEdgeColor', synBallColorSp(1:3), ...
                'MarkerFaceColor', 'none');
        synapseObjectSh = ...
                scatter3(somaLocsSh(:,1),somaLocsSh(:,2),somaLocsSh(:,3)...
                ,synBallSize,synShape,'MarkerEdgeColor', synBallColorSh(1:3), ...
                'MarkerFaceColor', 'none');
        synapseObjectPC = ...
                scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
                ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
                'MarkerFaceColor', colorPC(1:3));
        %{
        synapseObjectCross = ...
                scatter3(nodesCross(:,1),nodesCross(:,2),nodesCross(:,3)...
                ,crossSize,'x','MarkerEdgeColor', colorCross(1:3), ...
                'MarkerFaceColor', colorCross(1:3));
        synapseObjectCircle = ...
                scatter3(nodesCircle(:,1),nodesCircle(:,2),nodesCircle(:,3)...
                ,crossSize,'o','MarkerEdgeColor', colorCross(1:3));
        %}
        synapseObjectRoot = ...
                scatter3(nodesRoot(:,1),nodesRoot(:,2),nodesRoot(:,3)...
                ,crossSize,'*','MarkerEdgeColor', colorRoot(1:3));
        switch j
            case 1
                %xy
                Util.setView(6, true);
            case 2
                %xz
                Util.setView(2, true);
            case 3
                %yz
                Util.setView(5, true);
            case 4
                %yz
                Util.setView(4, true);
        end
    thisSkel.filename = ['panel_overlay_axons_' num2str(j)];
    outfile = fullfile(outDirPdf, ...
        sprintf('%s.eps', thisSkel.filename));
%     export_fig(outfile,'-q101', '-nocrop', '-transparent');
%     Util.log('Saving file %s.', outfile);
%     close(f);
    toc;
    end
end
