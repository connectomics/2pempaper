% This script takes in a tree with diameter annotations
% and generates diameter values
% Author: Sahil Loomba <sahil.loombabrain.mpg.de>
%% measure axon diameter 

setConfig;
scale = config.scaleEM;
Util.log('Loading skeleton')
skelCffi = skeleton(config.cffi);
outfile = fullfile(outDir,'outData/figures','fig3',config.version,'tc_diameters.eps');
%% calculate and plot after you have done the annotations
diameter_paths = {fullfile(outDir, 'data/wholecells','TC1_diameters.nml'),...
                fullfile(outDir, 'data/wholecells','TC3_diameters.nml'),...
                fullfile(outDir, 'data/wholecells','TC5_diameters.nml')};
tcNames = {'TC axon 01', 'TC axon 03', 'TC axon 05'};
skels = {};
for i=1:numel(tcNames)
    curSkel = skelCffi.keepTreeWithName(tcNames{i});
    startComment = '_root';
    startpoint = curSkel.getNodesWithComment(startComment,1,'regexp');
    startIds(i) = str2double(curSkel.nodesAsStruct{1}(startpoint).id);
    skels{i} = curSkel;
    clear startpoint curSkel
end
Util.log('Calculating diameters...')
diameters = cell(numel(diameter_paths),1);
distances = cell(numel(diameter_paths),1);
myelin = cell(numel(diameter_paths),1);
for i=1:numel(diameter_paths)
    [diameters{i}, distances{i}, myelin{i}] = ...
        getDataFromAnnotation(skels{i}, startIds(i), diameter_paths{i});
end

Util.log('Plotting diameters vs distances...')
% colors = [0.929,0.694,0.125; 213/255,94/255,0; 0.75,0.75,0];
colors = repmat([0.5,0.5,0.5],3,1);
dist_interp = 1:400;
diam_interp = {};
for i=1:numel(tcNames)
    y = diameters{i};
    x = distances{i};
    m = [x,y];
    [~,idx] = unique(m(:,1));
    m = m(idx,:);
    [~,idx] = unique(m(:,2));
    m = m(idx,:);
    diam_interp{i} = interp1(m(:,1),m(:,2),dist_interp);
end

% measure placement of contours
d1 = sort(distances{1},'ascend');
diff1 = d1(2:end) - d1(1:end-1);
sprintf('Mean +- s.d. axon1:')
[mean(diff1), std(diff1)]
d2 = sort(distances{2},'ascend');
diff2 = d2(2:end) - d2(1:end-1);
sprintf('axon2:')
[mean(diff2), std(diff2)]
d3 = sort(distances{3},'ascend');
diff3 = d3(2:end) - d3(1:end-1);
sprintf('axon3:')
[mean(diff3), std(diff3)]
sprintf('Combined:')
d = vertcat(diff1, diff2, diff3);
[mean(d), std(d)]
sprintf('Total contours: %d, %d, %d',...
    numel(distances{1}),numel(distances{2}),numel(distances{3}))
sprintf('Total myelinated: %d, %d, %d',...
     cellfun(@sum, myelin))
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on
for i=1:numel(tcNames)
    plot(dist_interp, diam_interp{i},...
        'Color',colors(i,:),...
        'LineWidth',2);
end
% use real annotated data for scatter, not interpolated
for i=1:numel(tcNames)
    idxMyelin = myelin{i};
    scatter(distances{i}(idxMyelin), diameters{i}(idxMyelin),'o',...
        'MarkerFaceColor','r','MarkerEdgeColor','r');
end
diam_interp_all = vertcat(diam_interp{:});
plot(dist_interp, nanmean(diam_interp_all,1),'k','LineWidth',3);
cx_nel = sum(~isnan(diam_interp_all),2);
plot(dist_interp, nanmean(diam_interp_all,1) + ...
    nanstd(diam_interp_all,0,2)./sqrt(cx_nel),'color',[0,0,0],...
    'LineWidth',2);
plot(dist_interp, nanmean(diam_interp_all,1) - ...
    nanstd(diam_interp_all,0,2)./sqrt(cx_nel),'color',[0,0,0],...
    'LineWidth',2);
set(gca,'XLim',[0 500],'YLim',[0 1.5]);
%xlabel('Axonal path length(\mum) from root')
%ylabel('Axon diameter(\mum)')
xLimits = [0, 400];
xTicks = [0:200:400];
xMinorTicks = [0:50:400];

yLimits = [0, 1.5];
yTicks = [0:0.5:1.5];
yMinorTicks = [0:0.1:1.5];

ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = xMinorTicks;
ax.XAxis.TickValues = xTicks;
ax.XAxis.Limits = xLimits;

ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.LineWidth = 2;    

Util.setPlotDefault(gca,false,false);
box off;
%camroll(-90);
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

% histogram
hgram = histogram(nanmean(diam_interp_all,1),...
    'DisplayStyle','stairs','LineWidth',3,'EdgeColor','k');
%xlabel('Axon diameter(\mum)')
%ylabel('Path length to axon root')
Util.setPlotDefault(gca,false,false);
set(gca,'LineWidth',2,'XLim',[0,1],'xtick',[0,0.5,1],'ytick',0:50:200)
box off;
% export_fig(fullfile(outDir,'outData/figures','fig3',config.version,...
%     'tc_diameters_hist.eps'), '-q101', '-nocrop', '-transparent');
% close all

% inset scatter plot 
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on
for i=1:numel(tcNames)
    idxMyelin = myelin{i};
    scatter(distances{i}(idxMyelin), diameters{i}(idxMyelin),'o',...
        'MarkerFaceColor','r','MarkerEdgeColor','r');
    scatter(distances{i}(~idxMyelin), diameters{i}(~idxMyelin),'o',...
        'MarkerFaceColor','k','MarkerEdgeColor','k');
end

xLimits = [0, 400];
xTicks = [0:200:400];
xMinorTicks = [0:50:400];

yLimits = [0, 1.5];
yTicks = [0:0.5:1.5];
yMinorTicks = [0:0.1:1.5];

ax.XAxis.MinorTick = 'off';
ax.XAxis.MinorTickValues = xMinorTicks;
ax.XAxis.TickValues = xTicks;
ax.XAxis.Limits = xLimits;

ax.YAxis.MinorTick = 'off';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.LineWidth = 4;

Util.setPlotDefault(gca,false,false);
box off;
outfile = fullfile(outDir,'outData/figures','fig3',config.version,'tc_diameters_inset.eps');
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all


% calculate distance from root for two TC cross sections in TC1
skelTC1 = skeleton(diameter_paths{1});
skelTC1 = skelTC1.extractLargestTree;
rootNodeIdx = skelTC1.getNodesWithComment('TC_1',1,'regexp');
rootId = str2double(skelTC1.nodesAsStruct{1}(rootNodeIdx).id);
crossNodesIdx =  skelTC1.getNodesWithComment('crossx',1,'regexp');
for i=1:numel(crossNodesIdx)
    idX = str2double(skelTC1.nodesAsStruct{1}(crossNodesIdx(i)).id);
    [~,~,distPlot(i)] = skelTC1.getShortestPath( rootId, idX);
end

sprintf('Distances for cross sections: \n')
disp(distPlot./1e3)

dataText = cellfun(@(x,y) x(~y), diameters, myelin,'uni',0);
sprintf('Diameters (excluding myelin): \n%.02f um +- %.02f  (n=%d)\n', ...
    mean(vertcat(dataText{:})), std(vertcat(dataText{:})), numel(dataText))

sprintf('Diameters (mean+-s.d. excluding myelin): \nTC01 %.02f um +- %.02f \nTC03 %.02f um +- %.02f \nTC05 %.02f um +- %.02f',...
    mean(dataText{1}), std(dataText{1}), ...
    mean(dataText{2}), std(dataText{2}), ...
    mean(dataText{3}), std(dataText{3}))

function [diameters, distNodesAll, myelin] = getDataFromAnnotation(skel,...
                                                    startId, diameter_path)
    skelAnnotated = skeleton(diameter_path);
    % extract annotation trees of size nodes 2
    idxDel = arrayfun(@(x) size(skelAnnotated.nodes{x},1)~=2,1:skelAnnotated.numTrees);
    skelAnnotated = skelAnnotated.deleteTrees(idxDel);
    diameters  = skelAnnotated.pathLength./1e3; % um

    distNodesAll = zeros(skelAnnotated.numTrees,1);
    myelin = false(skelAnnotated.numTrees,1);
    for idxTree=1:skelAnnotated.numTrees
        % pick one node from annotation
        curPos = skelAnnotated.nodes{idxTree}(1,1:3);
        % get closest node in the skel
        axonNodeIdx = skel.getClosestNode(curPos,1,false);% only one tree here
        axonNodeId = str2double(skel.nodesAsStruct{1}(axonNodeIdx).id);
        % find path from annotation point to its _root
        [~,~,distPoint] = skel.getShortestPath(startId,axonNodeId);
        distNodesAll(idxTree) = distPoint./1e3; %um
        if ~isempty(skelAnnotated.getNodesWithComment('m',idxTree,'exact'))
            myelin(idxTree) = true;
        end
    end
end
