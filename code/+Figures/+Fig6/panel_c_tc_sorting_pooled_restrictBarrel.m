%% All TC axon with its post-syn targets sps/stp and IN and INs to Stp/Sps
% all INs
% ExN inside barrel

setConfig;
[cx_data,cx_datatxt,cx_dataraw] = xlsread(config.cffi_excel_SLmanual_pooled);
somaColors = Util.getSomaColors;
colorTC = Util.getTCColor;
restrictFlag = true; % restrict to exc cells within barrel
sortINFlag = false; % sort INs by first-hit synapses
debugFlag = true; % debug open and closed triads for manual reconstruction
debugFlagPlot = false;
markerSize = 10;

py3Color = Util.getPy3Color;

%%
Util.log('Reading data...')
rng(0)
% 2019-08-12_cffi_v1.xlsx
% NOTE : defined ignoring the first row with variable names
cx_tcCols = find(cellfun(@(x) any(regexp(x,'t\d+')),cx_dataraw(1,:)));
cx_StpRows = reshape(find(cellfun(@(x) any(regexp(x,'^p$')),cx_dataraw(:,1))) - 1,1,'');% defined ignoring first row
cx_SpsRows = reshape(find(cellfun(@(x) any(regexp(x,'^s$')),cx_dataraw(:,1))) - 1,1,'');
cx_INrows = reshape(find(cellfun(@(x) any(regexp(x,'i')),cx_dataraw(:,1))) - 1,1,''); % defined ignoring first row
cx_INCols = find(cellfun(@(x) any(regexp(x,'i\d+')),cx_dataraw(1,:))); % NOTE: Empty cols are thrown out
cx_INtypeCol = size(cx_dataraw,2); % col in cx_dataraw
cx_idsCol = 2;
cx_depthCol = 3;

rng(0)
cx_postsyn_depth = cx_data(:,cx_depthCol)+600;
cx_targetXPos = cx_postsyn_depth*0+1150+(rand(size(cx_postsyn_depth,1),1)-0.5)*50;
% cx_targetXPos(cx_StpRows) = cx_postsyn_depth(cx_StpRows)*0+1030+(rand(size(cx_StpRows,2),1)-0.5)*20;  % Stp
% cx_targetXPos(cx_SpsRows) = cx_postsyn_depth(cx_SpsRows)*0+1030+(rand(size(cx_SpsRows,2),1)-0.5)*20;  % Sps

% add jitter for visibility of stp and sps
r = -1 + (1+1)*rand(numel(cx_targetXPos),1);
cx_targetXPos(cx_StpRows) = 1000 + r(cx_StpRows);  % Stp
cx_targetXPos(cx_SpsRows) = 1000 + r(cx_SpsRows);  % Sps
cx_targetXPos(cx_INrows) = 980;  % INs
cx_INvertOffset = 0;% -50;%-200;
cx_tcXspacing = 0;
cx_tcXoffsetIN = 1000; %1400; TC to IN input from same TC start
cx_tcXoffsetExc = 1000;

%% load connectome and restrict to main IN types
idxTCPlot = 1:20;
load(config.connmat_SLmanual)

if restrictFlag
    [~,INStp] = Conn.restrictToBarrel(synMapINStp,'p',2);
    [~,INSps] = Conn.restrictToBarrel(synMapINSps,'s',2);
else
    INStp = true(countStp,1);
    INSps = true(countSps,1);
end

%% plot stp, sps, INs
%Util.log('Plotting somas...')
%fig = figure;
%fig.Color = 'white';
%hold on
cx_postType = []; % 1 for Ins, 2 for Stp, 3 for Sps
i_post_exc_plot = []; % List of stp sps plotted here
for i_tcc = idxTCPlot % TC01, etc.
    i_tc = cx_tcCols(i_tcc); % no for loop, works for just one TC
    for i_post = 1:size(cx_dataraw,1)-1
        cx_post_pos = cx_postsyn_depth(i_post);
        cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % synapse depths
        switch cx_datatxt{i_post+1,1}
            case 'i'
                curIN = str2double(cx_datatxt{i_post+1,2});
                 curINColor = somaColors{cellTypesAll(curIN)}(1:3);
                if sortINFlag
                    cx_post_pos = min(cx_thisPosVec);% lowest point of input synapse from tc
                    cx_postsyn_depth(i_post) = cx_post_pos; % update the IN square depth for later use
                end
                % plot(cx_targetXPos(i_post),cx_post_pos+cx_INvertOffset,'s',...
                %     'MarkerEdgeColor', curINColor, 'MarkerFaceColor', curINColor ,'MarkerSize',15);
                cx_postType(i_post) = 1;
            case 'p'
                if ~isempty(cx_thisPosVec)
    %                plot(cx_targetXPos(i_post),cx_post_pos,'om','MarkerSize', markerSize);
                    i_post_exc_plot(end+1) = i_post;
                end
                cx_postType(i_post) = 2;
            case 's'
                if ~isempty(cx_thisPosVec)
    %                plot(cx_targetXPos(i_post),cx_post_pos,'ob','MarkerSize',markerSize);
                    i_post_exc_plot(end+1) = i_post;
                end
                cx_postType(i_post) = 3;
            case 'shaft'
                cx_postType(i_post) = 4;
            case 'spine'
                cx_postType(i_post) = 5;
        end
    end
end

if restrictFlag % restrict exc to those within barrel
    idxStpHere = find(cx_postType(i_post_exc_plot)==2);
    stpInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxStpHere)+1, 2}});
    idxStpHereKeep = INStp(stpInnv);
    idxSpsHere = find(cx_postType(i_post_exc_plot)==3);
    spsInnv = cellfun(@str2num, {cx_datatxt{i_post_exc_plot(idxSpsHere)+1, 2}});
    idxSpsHereKeep = INSps(spsInnv);
 
    i_post_exc_plot = reshape(i_post_exc_plot([idxStpHereKeep(:); idxSpsHereKeep(:)]),1,'');
end

%% TC input
%Util.log('Plotting tc input...')
somaColors = Util.getSomaColors;
cx_postColors = {somaColors{1}(1:3), [1,0,1], [0,0,1], [138,43,226]./255, [0.5,0.5,0.5], py3Color}; % green, magenta, blue, purple, grey, yellow-orange
cx_TCsynPosVec_byType = cell(numel(unique(cx_postType)),1);
cx_TCinputPerCell = cell(numel(unique(cx_postType)),1);
postExcTCSynCount = []; % number of TC to Exc synapses
for i_tcc = idxTCPlot
    i_tc = cx_tcCols(i_tcc);
    cx_thisXposIN = i_tcc*cx_tcXspacing+cx_tcXoffsetIN;
    cx_thisXposExc = i_tcc*cx_tcXspacing+cx_tcXoffsetExc;
    cx_thisTCmaxpos = 0;
    for i_post = 1:size(cx_dataraw,1)-1
        cx_post_pos = cx_postsyn_depth(i_post);           
        if strcmp(cx_datatxt{i_post+1,1},'i')
            cx_post_pos = cx_post_pos+cx_INvertOffset;
        end
        if isempty(cx_datatxt{i_post+1,i_tc}) %no tc synapse but empty string will be non empty cell-array
            cx_thisPosVec = cx_dataraw{i_post+1,i_tc};
        else
            cx_thisPosVec = str2num(cx_dataraw{i_post+1,i_tc}); % empty string gives empty,  multiple numbers to array
        end
        if ~isempty(cx_thisPosVec)
            for iii=1:length(cx_thisPosVec)
                curLineWidth = 1;
                faceAlpha = 0.5;
                if ismember(i_post,cx_INrows) % if IN input
                    curLineWidth = 1; faceAlpha = 0.8;
%                        plot([cx_thisXposIN cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
%                            '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                        cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
%                         % < Marker at IN syn
%                         curMarkerSize = 5;
%                         plot(cx_thisXposIN,cx_thisPosVec(iii),'<','Color',...
%                             cx_postColors{cx_postType(i_post)},'MarkerSize',curMarkerSize);
                else % if exc input
                    if ismember(i_post, i_post_exc_plot) % only if exc is inside barrel
%                        plot([cx_thisXposExc cx_targetXPos(i_post)],[cx_thisPosVec(iii) cx_post_pos],...
%                            '-','Color', [cx_postColors{cx_postType(i_post)},faceAlpha],'LineWidth',curLineWidth);
                        cx_thisTCmaxpos= max(cx_thisTCmaxpos,cx_thisPosVec(iii));
                        if iii==length(cx_thisPosVec)
                            postExcTCSynCount = vertcat(postExcTCSynCount,length(cx_thisPosVec) ); % add only if this exc is plotted
                        end
                    end
                end
                cx_TCsynPosVec_byType{cx_postType(i_post)} = [cx_TCsynPosVec_byType{cx_postType(i_post)},...
                    cx_thisPosVec(iii)];                
            end
        end
        cx_TCinputPerCell{cx_postType(i_post)} = [cx_TCinputPerCell{cx_postType(i_post)} ,length(cx_thisPosVec)];
    end
end

%%
Util.log('Now plotting:')
fig = figure;
fig.Color = 'none';
ax = axes(fig);
cx_posBins = 0:25:500;
binWidth = 25;
yd=cell(3,1);
pd = cell(3,1);
for cx_typeC=[1 2 3 4]
    yData = cx_TCsynPosVec_byType{cx_typeC};
    [ ~, curpd] = Util.fitGaussianNormalized(yData, cx_posBins,'normal',cx_postColors{cx_typeC}, binWidth);
    %[~, ~, curpd] = Util.fitGaussian(yData, cx_posBins,'normal',cx_postColors{cx_typeC});
    yd{cx_typeC} = yData;
    pd{cx_typeC} = curpd;
    sprintf('Prob dist for %d \n',cx_typeC)
    disp(curpd)
end
box off
set(gca,'XDir','reverse','TickDir','out','YAxisLocation','right')
camroll(-90);
ax.XAxis.Limits = [-50 400];
ax.XAxis.TickValues = -50:50:400;

ax.YAxis.Limits = [0 0.4];
ax.YAxis.TickValues = 0:0.1:0.4;
ax.LineWidth = 2;
set(gca,'Color','none')
daspect([458, 1, 1]) % manual
Util.log('Saving fig panel...')
outfile = fullfile(outDir, 'outData/figures','fig3',config.version,['panel_tc_syn_num_restrictBarrel.eps']);
% export_fig(outfile, '-q101', '-nocrop', '-transparent');
% close all

% do tests: kmb: IN/Stp/Sps
[~,p1_km,~] = kstest2(yd{1},yd{2}); [p2_km,~,~] = ranksum(yd{1},yd{2});
[~,p1_kb,~] = kstest2(yd{1},yd{3}); [p2_kb,~,~] = ranksum(yd{1},yd{3});
[~,p1_mb,~] = kstest2(yd{2},yd{3}); [p2_mb,~,~] = ranksum(yd{2},yd{3});
sprintf(['IN-Stp: (KS)p=%.03e, (RS)p=%.03e\n ',...
    'IN-Sps: (KS)p=%.03e, (RS)p=%.03e\n ',...
    'Stp-Sps: (KS)p=%.03e, (RS)p=%.03e'],...
    p1_km,p2_km,p1_kb,p2_kb,p1_mb,p2_mb)
