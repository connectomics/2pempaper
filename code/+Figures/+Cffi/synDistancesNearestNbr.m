function [distSynPostSoma, distSynPost] = synDistancesNearestNbr(skel, preNames, preComments, postIds, postClass)
% This function uses skelCffi to extract distances of synapses from SOMA from preClass
% onto postClass
% Input
%   skel: skelCffi
%   preNames: {'TC axon 01', 'TC axon 02',..}
%   preComments: cell-array of strings of post name comments. eg. {'tc01',...,'tc20'}
%   postIds: IDs of dendrite cell trees. eg. [1,2,3,4,..43]
%   postClass: 'i','s','p'
% Output:
%   distSynPostSoma: Table with distances of pre syn in post targets from their somas
%   distSynPost: Table with distances between pairs of synapses on post targets nearest nbr for each syn from pre

Util.log('Extract cellSkel from skel')
preIdx = zeros(numel(preNames),1);
for i = 1:numel(preNames)
    preIdx(i) = skel.getTreeWithName(preNames{i},'exact');
end
cellSkel = skel.deleteTrees(preIdx); % contains only dendrites of INs and ExN

Util.log('Create post names...')
postNames = arrayfun(@(x) ['cell ' postClass num2str(x,'%02d')],postIds,'uni',0);

Util.log('Extracting syn distances...')
distSyn = cell(length(preComments),length(postNames));
distSynSoma = cell(length(preComments),length(postNames));
tic;
for preCommentIdx=1:length(preComments)
    for postNameIdx = 1:length(postNames)
        cellId = cellSkel.getTreeWithName(postNames{postNameIdx});
        somaIdx = cellSkel.getNodesWithComment('(cellbody)?(soma)?(_ss)?(_sp)?(_in)?',cellId,'regexp');
        if ~isempty(somaIdx)
            somaId = str2double(cellSkel.nodesAsStruct{cellId}(somaIdx).id);
            synIdx = cellSkel.getNodesWithComment(preComments{preCommentIdx},cellId,'exact');
            if ~isempty(synIdx)
                distSoma = []; % distance of syn from soma
                for k = 1:length(synIdx)
                    tcSynId = str2double(cellSkel.nodesAsStruct{cellId}(synIdx(k)).id);
                    [~,~,distSoma(k)] = cellSkel.getShortestPath(somaId,tcSynId);
                end
                % sort distance of synapse from soma
                [distSoma,indSortDistSoma] = sort(distSoma,'ascend');
                distSynSoma{preCommentIdx, postNameIdx} = distSoma./1e3;% um
                if length(synIdx)>1
                    curPaths = cellSkel.getShortestPaths(cellId);
                    tmpSyn = zeros(1, length(synIdx));
                    for curSynIdx = 1:numel(synIdx)
                        tmpSyn(curSynIdx) = min(nonzeros(curPaths(synIdx(curSynIdx),synIdx))); % nearest nbr dist to other input syn
                    end                    
                    distSyn{preCommentIdx,postNameIdx} = tmpSyn./1e3; % um
                else
                    % no pair of syn found, cant be zero
                    distSyn{preCommentIdx, postNameIdx} = NaN; 
                end
            else
                % tc synapse found
                distSyn{preCommentIdx, postNameIdx} = NaN;
                distSynSoma{preCommentIdx, postNameIdx} = NaN;
            end                    
        else
            error('No soma comment found!')
        end
    end
end
% distances from each other
distSynPost = cell2table(distSyn);
distSynPost.Properties.VariableNames = arrayfun(@(x) ...
    [postClass num2str(x,'%02d')], postIds,'uni',0);
% distances from soma
distSynPostSoma = cell2table(distSynSoma);
distSynPostSoma.Properties.VariableNames = arrayfun(@(x) ...
    [postClass num2str(x,'%02d')], postIds,'uni',0);
Util.log('Finished extracting syn distances!')
end