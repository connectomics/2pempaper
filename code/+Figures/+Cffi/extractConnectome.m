% extract IN/TC to IN/Stp/Sps connectome
% using '-' information because 'so p' etc. comments are not there for all targets
setConfig;
suffix = '_SLmanual';
outDirPdf = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version]);
Util.log('Parsing cffi skeleton...')
if isempty(suffix)
    skel = skeleton(config.cffi); % tcSyn dist. Contains TC, IN, ExN from YH
else    
    skel = skeleton(config.cffi_SLmanual); % tcSyn dist. Contains TC, IN, ExN manually found cffi by SL
end
skel = Figures.Fig2.IN.fixComments(skel);

Util.log('Parsing cell type skeleton...')
skelCellType = skeleton(config.cellTypesL4Fixed);
skelCellType = skelCellType.extractLargestTree;
c = skelCellType.getAllComments;
pIds = cellfun(@(x) regexp(x,'p(?<id>\d+)','names') ,c,'uni',0);
pIds = cellfun(@(x) str2double(x.id), pIds(~cellfun('isempty',pIds)));
countStp = max(pIds);
sIds = cellfun(@(x) regexp(x,'s(?<id>\d+)','names') ,c,'uni',0);
sIds = cellfun(@(x) str2double(x.id), sIds(~cellfun('isempty',sIds)));
countSps = max(sIds);
clear c
%countStp = 375;
%countSps = 965;

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);
%warning('Doing for 43 IN types only')
%cellTypesAll = cellTypesAll(1:43);
clear xlTable1 xlsfile xl1

treeNames = skel.names;
postClassNames = {'IN','s','p'};
postClassNamesSearchStr = {'i','s','p'}; 
postClassIds = struct('i',[],'iLocs',[],...
                        's',[],'sLocs',[],...
                        'p',[],'pLocs',[]);
INgroupNames = {'BIN1', 'BIN2','L4IN','NGFC','InvPyr','L4Other','nonL4','OtherBarrel','unclassified'};

% get dend Ids from cell names for each post class
for i=1:numel(postClassNames)
    dendLocs = find(cellfun(@(x) contains(x,['cell ' postClassNamesSearchStr{i}]),treeNames));
    dendIds = cellfun(@(x) regexp(x,['cell ' postClassNamesSearchStr{i} '(?<id>\w+)'],'names'),treeNames(dendLocs),'uni',0);
    dendIds = cellfun(@(x) x.id,dendIds,'uni',0);
    [dendIds, idxSort] = sort(dendIds);
    
    postClassIds.(postClassNamesSearchStr{i}) = cellfun(@(x) [postClassNames{i} x] ,dendIds,'uni',0);
    postClassIds.([postClassNamesSearchStr{i} 'Locs']) = dendLocs(idxSort);
    clear dendIds idxSort dendLocs
end

% extract cell types for INs that are present in cffi nml
postClassINIds = cellfun(@(x) regexp(x,'IN(\d+)','tokens'), postClassIds.i);
postClassINIds = cellfun(@(x) str2double(x{1}), postClassINIds);
cellTypesAll = cellTypesAll(postClassINIds);

% IN axons present in this cffi nml were fully traced and checked for output to INs dendrites
namesINFull = skel.names(contains(skel.names, 'IN axon'));
postClassIds.i_full = false(numel(postClassIds.i),1);
for idx = 1:numel(postClassIds.i)
    curId = postClassIds.i{idx};
    if any(contains(namesINFull,curId(3:end)))
        postClassIds.i_full(idx) = true;
    end
end

% pre classIds
preClassIds = struct('in',[],'inLocs',[],...
                        'tc',[],'tcLocs',[]);
preClassNames = {'in','tc'};
preClassIds.in = cellfun(@(x) ['IN axon ' x(end-1:end)],postClassIds.i,'uni',0); % IN axons names
preClassIds.inLocs = cellfun(@(x) skel.getTreeWithName(x,'exact'),preClassIds.in,'uni',0); % some are empty bc no pre axon
preClassIds.tc = arrayfun(@(x) ['TC axon ' num2str(x,'%02d')],1:20,'uni',0); % 20 tc axons names
preClassIds.tcLocs = cellfun(@(x) skel.getTreeWithName(x,'exact'),preClassIds.tc,'uni',0);

tcIdsInPost = arrayfun(@(x) ['tc' num2str(x,'%02d')], 1:20,'uni',0); % comments in post to look for tc input

%% Look from post side for tc comments
Util.log('tc to IN') % pre on y, post on x
[synMapTCIN, shaftMapTCIN, somaMapTCIN] = Conn.getPostConnectome(skel,tcIdsInPost, postClassIds.i, postClassIds.iLocs, 'tc','IN', outDirPdf, false);
synMapTCIN_grouped = Conn.groupByTypes(synMapTCIN,cellTypesAll,'x', preClassIds.tc, INgroupNames, 'tc', 'IN-class', 'full', outDirPdf);

Util.log('tc to Stp')
synMapTCStp = Conn.getPostConnectome(skel,tcIdsInPost, postClassIds.p, postClassIds.pLocs, 'tc','Stp', outDirPdf);

Util.log('tc to Sps')
synMapTCSps = Conn.getPostConnectome(skel,tcIdsInPost, postClassIds.s, postClassIds.sLocs, 'tc','Sps', outDirPdf);

Util.log('IN to IN')
[synMapININ, shaftMapININ,somaMapININ] = Conn.getPostConnectome(skel, postClassIds.i, postClassIds.i, postClassIds.iLocs, 'IN','IN', outDirPdf);
shaftMapININ_grouped = Conn.groupByTypes(shaftMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'shaftSyn',outDirPdf);
somaMapININ_grouped = Conn.groupByTypes(somaMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'somaSyn',outDirPdf);
synMapININ_grouped = Conn.groupByTypes(synMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'full', outDirPdf);

%% take care of '-' soma synapses
Util.log(' IN to Stp and Sps')
[synMapINStp, synMapINSps, somaMapINStp,somaMapINSps] = Conn.getPreConnectome(skel, skelCellType, preClassIds.in, preClassIds.inLocs, countStp, countSps, outDirPdf);

%shaftMapINStp_grouped = Conn.groupByTypes(shaftMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'shaftSyn',outDirPdf);
%somaMapINStp_grouped = Conn.groupByTypes(somaMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'somaSyn',outDirPdf);
synMapINStp_grouped = Conn.groupByTypes(synMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'full',outDirPdf);
%shaftMapINSps_grouped = Conn.groupByTypes(shaftMapINSps,cellTypesAll,'y',  INgroupNames, postClassIds.s, 'IN', 'Sps', 'shaftSyn',outDirPdf);
%somaMapINSps_grouped = Conn.groupByTypes(somaMapINSps,cellTypesAll,'y',  INgroupNames, postClassIds.s, 'IN', 'Sps', 'somaSyn',outDirPdf);
synMapINSps_grouped = Conn.groupByTypes(synMapINSps,cellTypesAll,'y', INgroupNames, postClassIds.s, 'IN', 'Sps', 'full',outDirPdf);

%% combine into whole connectome:
% append TC syn maps on post side with zeros
synMapTCStp_app = [synMapTCStp, zeros(size(synMapTCStp,1), countStp-size(synMapTCStp,2))];
synMapTCSps_app = [synMapTCSps, zeros(size(synMapTCSps,1), countSps-size(synMapTCSps,2))];

% do assertions before combining
assert(size(synMapININ,2) == size(synMapTCIN,2))
assert(size(synMapINStp,2) == size(synMapTCStp_app,2))
assert(size(synMapINSps,2) == size(synMapTCSps_app,2))
assert(size(synMapININ,1) == size(synMapINStp,1))
assert(size(synMapINStp,1) == size(synMapINSps,1))
assert(size(synMapTCIN,1) == size(synMapTCStp_app,1))
assert(size(synMapTCStp_app,1) == size(synMapTCSps_app,1))

wholeSynMap = cat(1, cat(2,synMapININ, synMapINStp, synMapINSps), cat(2,synMapTCIN, synMapTCStp_app, synMapTCSps_app));
idxPreIN = 1:numel(postClassIds.i);
idxPreTC = idxPreIN(end)+ (1:numel(tcIdsInPost));
idxPostIN = 1:numel(postClassIds.i);
idxPostStp = idxPostIN(end) + (1:countStp);
idxPostSps = idxPostStp(end) + (1:countSps);

%% grouped one whole syn map
wholeSynMap_grouped = cat(1, cat(2,synMapININ_grouped, synMapINStp_grouped, synMapINSps_grouped), cat(2,synMapTCIN_grouped, synMapTCStp_app, synMapTCSps_app));
idxPreIN_grouped = 1:numel(unique(cellTypesAll));
idxPreTC_grouped = idxPreIN_grouped(end) + (1 : numel(tcIdsInPost));
idxPostIN_grouped = 1:numel(unique(cellTypesAll));
idxPostStp_grouped = idxPostIN_grouped(end) + (1 : countStp);
idxPostSps_grouped = idxPostStp_grouped(end) + (1 : countSps);

Util.log('Saving data...')
save(fullfile(outDirPdf,['extractConnectome' suffix '.mat']));

%{
% look at specific IN inputs connections
idsIN = [2,8,3,4,29,40,5,6,17,24,25,31,41, 12, 14]; %IN ids
idxMain = arrayfun(@(x) find(contains(dendIds,num2str(x,'%02d'))),idsIN,'uni',0); % find occurence of IN id in the synMap
idxNotFound = cellfun(@isempty, idxMain);
sprintf('IN not found %d \n', idsIN(idxNotFound))

idxKeep = cell2mat(idxMain(~idxNotFound)); 
synMap = fullSynMap(idxKeep,:);
namesIN = dendIds(idxKeep);
namesTC = tcIds;
maxSyn = max(synMap(:));
figure
h = heatmap(namesTC, namesIN, synMap,'ColorLimits',[0 maxSyn]);
title('TC to IN synapses')
saveas(gcf,fullfile(outDirPdf,'heatmap_tc_to_in.png'));
close all

Util.save(fullfile(outDirPdf,'extractConnectome.mat'),synMap, fullSynMap)
%}
