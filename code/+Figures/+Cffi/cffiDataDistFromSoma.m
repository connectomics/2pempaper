% Collect data for 2018-12-01_cff_v3 required to run Fig3 graph-connectome
setConfig;
outDirPdf = fullfile(outDir,'data');
outfile = fullfile(outDirPdf,'2019-10-08_cffi_v1.xlsx');
Util.log('Loading skeleton...')
skel = skeleton(config.cffi); % tcSyn dist. Contains TC, IN, ExN
skel = Figures.Fig2.IN.fixComments(skel);

%% extract distances of synapses input
% get TC input on all post targets
countTC = numel(skel.getTreeWithName('TC axon(\s?)\d+','regexp'));
countIN = numel(skel.getTreeWithName('cell i(\s)?\d+','regexp'));
countSps = numel(skel.getTreeWithName('cell s(\s)?\d+','regexp'));
countStp = numel(skel.getTreeWithName('cell p(\s)?\d+','regexp'));

% tc comments on dendrites
tcComments = arrayfun(@(x) ['tc' num2str(x,'%02d')],1:countTC,'uni',0); % 20 tc axons comments
tcNames = arrayfun(@(x) ['TC axon ' num2str(x,'%02d')],1:countTC,'uni',0); % 20 tc axons names

[distSynINSoma, distSynIN] = Figures.Cffi.synDistances(skel, tcNames, tcComments,1:countIN, 'i');
[distSynPSoma, distSynP] = Figures.Cffi.synDistances(skel, tcNames, tcComments,1:countStp, 'p');
[distSynSSoma, distSynS] = Figures.Cffi.synDistances(skel, tcNames, tcComments,1:countSps, 's');

% soma synapses
inComments =  arrayfun(@(x) ['IN' num2str(x,'%02d')],1:countIN,'uni',0);
inCommentsForSoma =  arrayfun(@(x) ['IN' num2str(x,'%02d') ' so'],1:countIN,'uni',0);
preClassIds.in = arrayfun(@(x) ['IN axon ' num2str(x,'%02d')],1:countIN,'uni',0); % IN axons names
preClassIds.inLocs = cellfun(@(x) skel.getTreeWithName(x,'exact'),preClassIds.in,'uni',0); % some are empty bc no pre axon
distSynIN_INSoma = Figures.Cffi.synDistances(skel, preClassIds.in, inComments,1:countIN, 'i');
distSynIN_INSomaForSoma = Figures.Cffi.synDistances(skel, preClassIds.in, inCommentsForSoma,1:countIN, 'i');

% IN input on all dendrites of Stp/Sps (need to look from pre side)
[distSynIN_PSoma,distSynIN_SSoma] = Figures.Cffi.synDistancesForIN(skel, ...
    preClassIds.in, preClassIds.inLocs, countStp, countSps);

%% extract locations of soma and convert to 2P space
Util.log('Compiling soma locations ....')
inLocs = Figures.Cffi.somaLocations(skel,arrayfun(@(x)['cell i' ...
    num2str(x,'%02d')] ,1:countIN,'uni',0),'cellbody');
pLocs = Figures.Cffi.somaLocations(skel,arrayfun(@(x)['cell p' ...
    num2str(x,'%02d')] ,1:countStp,'uni',0),'_sp');
sLocs = Figures.Cffi.somaLocations(skel,arrayfun(@(x)['cell s' ...
    num2str(x,'%02d')] ,1:countSps,'uni',0),'_ss');

% convert to 2P space
m=load(config.barrelStateTform,'tform');
tform = m.tform;
% tilt correction hack and conversion
inLocsTemp = inLocs; sLocsTemp = sLocs; pLocsTemp = pLocs;
inLocsTemp(:,2) = round(inLocs(:,2)-.449*inLocs(:,3));
pLocsTemp(:,2) = round(pLocs(:,2)-.449*pLocs(:,3));
sLocsTemp(:,2) = round(sLocs(:,2)-.449*sLocs(:,3));
inLocsLM = TFM.applyTformToVertices(tform, inLocsTemp, 'forward');
pLocsLM = TFM.applyTformToVertices(tform, pLocsTemp, 'forward');
sLocsLM = TFM.applyTformToVertices(tform, sLocsTemp, 'forward');
Util.log('Finished soma locations ....')
%% compile data
allSomas = vertcat(inLocsLM, pLocsLM, sLocsLM);
allCellTypes = cellstr(vertcat(repelem('i',countIN,1),repelem('p',countStp,1),repelem('s',countSps,1)));
allIds = reshape(horzcat(arrayfun(@(x) num2str(x,'%02d'),1:countIN,'uni',0),...
                       arrayfun(@(x) num2str(x,'%02d'),1:countStp,'uni',0),...
                       arrayfun(@(x) num2str(x,'%02d'),1:countSps,'uni',0)),'',1);
data = horzcat(allCellTypes, ...
        allIds, ...
        num2cell(allSomas(:,1)), num2cell(allSomas(:,2)), num2cell(allSomas(:,3)),...                       
        [table2cell(distSynINSoma)'; table2cell(distSynPSoma)'; table2cell(distSynSSoma)']);
    
numCols = size(data,2);
t = cell2table(cell(size(data,1) , 5 + size(data,2)),...
    'VariableNames', [{'cell_type','ID','x','y','depth_i_um'},...
    arrayfun(@(x) ['t' num2str(x,'%02d')], 1:size(data,2),'uni',0)]);
for i=1:numCols
    t.(t.Properties.VariableNames{i}) = data(:,i);
end
Util.log('Writing outout excel data..')
if exist(outfile,'file')
    temp = xlsread(outfile);
    xlswrite(outfile,zeros(size(temp))*nan);
end
writetable(t,outfile);
% xlswrite(outfile,outData);
Util.log('Finished writing output excel data')