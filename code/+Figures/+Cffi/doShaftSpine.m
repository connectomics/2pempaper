function synDist = doShaftSpine(skel,preIds,preIdx, postIds, searchStr)
   synDist = zeros(numel(preIds),numel(postIds));
   for i=1:numel(preIds)
       thisPreIdx = preIdx{i};
       if isempty(thisPreIdx)
           disp(['tree ' preIds{i} ' is not present in pre']);
           continue;
       else
           axonComments = skel.getAllComments(thisPreIdx);
            for j=1:numel(axonComments)
                thisComment = axonComments{j};
                id = regexp(thisComment,[searchStr '(?<id>\w+)'],'names');
                if isempty(id)
                    clear id
                    continue;
                end
                id = str2double(id.id);
                % find distance of this synpase from post soma
                synDist(i,id) = synDist(i,id)+1;
            end
       end
   end
end

