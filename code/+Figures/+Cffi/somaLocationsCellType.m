function somaLocs = somaLocations(skel,count, class )
% extract soma locations from cell type
somaLocs = [];
for i=1:count
   somaLocs(i,:) = Util.getNodeCoordsWithComment(skel,[class num2str(i,'%02d')],'exact',1); 
end
assert(size(somaLocs,1)==count)
end
