% extract IN/TC to IN/Stp/Sps connectome
setConfig;
outDir = config.outDir;
outDirPdf = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version]);
Util.log('Parsing skeleton...')
skel = skeleton(config.cffi); % tcSyn dist. Contains TC, IN, ExN
skel = Figures.Fig2.IN.fixComments(skel);

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);

treeNames = skel.names;
postClassNames = {'IN','s','p'};
postClassNamesSearchStr = {'i','s','p'}; 
postClassIds = struct('i',[],'iLocs',[],...
                        's',[],'sLocs',[],...
                        'p',[],'pLocs',[]);
INgroupNames = {'BIN1', 'BIN2','L4IN','NGFC','InvPyr','L4Other','nonL4','OtherBarrel','unclassified'};

% get dend Ids from cell names for each post class
for i=1:numel(postClassNames)
    dendLocs = find(cellfun(@(x) contains(x,['cell ' postClassNamesSearchStr{i}]),treeNames));
    dendIds = cellfun(@(x) regexp(x,['cell ' postClassNamesSearchStr{i} '(?<id>\w+)'],'names'),treeNames(dendLocs),'uni',0);
    dendIds = cellfun(@(x) x.id,dendIds,'uni',0);
    [dendIds, idxSort] = sort(dendIds);
    
    postClassIds.(postClassNamesSearchStr{i}) = cellfun(@(x) [postClassNames{i} x] ,dendIds,'uni',0);
    postClassIds.([postClassNamesSearchStr{i} 'Locs']) = dendLocs(idxSort);
    clear dendIds idxSort
end

% get tc Ids from tree names
tcIds = {'tc1' 'tc2' 'tc3' 'tc4' 'tc5' 'tc6' 'tc7' 'tc8' 'tc9' 'tc10' 'tc11' 'tc12' ...
            'tc13' 'tc14' 'tc15' 'tc16' 'tc17' 'tc18' 'tc19' 'tc20'};
%%
Util.log('tc to IN') % pre on y, post on x
[synMapTCIN] = Conn.getThisConnectome(skel,tcIds, postClassIds.i, postClassIds.iLocs, 'tc','IN', outDirPdf);
synMapTCIN_grouped = Conn.groupByTypes(synMapTCIN,cellTypesAll,'x', tcIds, INgroupNames, 'tc', 'IN-class', 'full', outDirPdf);

Util.log('tc to Stp')
[synMapTCStp] = Conn.getThisConnectome(skel,tcIds, postClassIds.p, postClassIds.pLocs, 'tc','Stp', outDirPdf);

Util.log('tc to Sps')
[synMapTCSps] = Conn.getThisConnectome(skel,tcIds, postClassIds.s, postClassIds.sLocs, 'tc','Sps', outDirPdf);

Util.log('IN to IN')
[shaftMapININ,somaMapININ, synMapININ] = Conn.getThisConnectome(skel, postClassIds.i, postClassIds.i, postClassIds.iLocs, 'IN','IN', outDirPdf);
shaftMapININ_grouped = Conn.groupByTypes(shaftMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'shaftSyn',outDirPdf);
somaMapININ_grouped = Conn.groupByTypes(somaMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'somaSyn',outDirPdf);
synMapININ_grouped = Conn.groupByTypes(synMapININ,cellTypesAll,'both',INgroupNames, INgroupNames, 'IN', 'IN', 'full', outDirPdf);

Util.log(' IN to Stp')
[shaftMapINStp,somaMapINStp, synMapINStp] = Conn.getThisConnectome(skel, postClassIds.i, postClassIds.p, postClassIds.pLocs, 'IN','Stp', outDirPdf);
shaftMapINStp_grouped = Conn.groupByTypes(shaftMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'shaftSyn',outDirPdf);
somaMapINStp_grouped = Conn.groupByTypes(somaMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'somaSyn',outDirPdf);
synMapINStp_grouped = Conn.groupByTypes(synMapINStp,cellTypesAll,'y', INgroupNames, postClassIds.p, 'IN', 'Stp', 'full',outDirPdf);

Util.log('IN to Sps')
[shaftMapINSps,somaMapINSps, synMapINSps] = Conn.getThisConnectome(skel, postClassIds.i, postClassIds.s, postClassIds.sLocs, 'IN','Sps', outDirPdf);
shaftMapINSps_grouped = Conn.groupByTypes(shaftMapINSps,cellTypesAll,'y',  INgroupNames, postClassIds.s, 'IN', 'Sps', 'shaftSyn',outDirPdf);
somaMapINSps_grouped = Conn.groupByTypes(somaMapINSps,cellTypesAll,'y',  INgroupNames, postClassIds.s, 'IN', 'Sps', 'somaSyn',outDirPdf);
synMapINSps_grouped = Conn.groupByTypes(synMapINSps,cellTypesAll,'y', INgroupNames, postClassIds.s, 'IN', 'Sps', 'full',outDirPdf);

%% combine into whole connectome: do assertions before combining
assert(size(synMapININ,2) == size(synMapTCIN,2))
assert(size(synMapINStp,2) == size(synMapTCStp,2))
assert(size(synMapINSps,2) == size(synMapTCSps,2))
assert(size(synMapININ,1) == size(synMapINStp,1))
assert(size(synMapINStp,1) == size(synMapINSps,1))
assert(size(synMapTCIN,1) == size(synMapTCStp,1))
assert(size(synMapTCStp,1) == size(synMapTCSps,1))

wholeSynMap = cat(1, cat(2,synMapININ, synMapINStp, synMapINSps), cat(2,synMapTCIN, synMapTCStp, synMapTCSps));
idxPreIN = 1:numel(postClassIds.i);
idxPreTC = numel(idxPreIN) + 1 : numel(idxPreIN) + numel(tcIds);
idxPostIN = 1:numel(postClassIds.i);
idxPostStp = numel(idxPostIN) + 1 :  numel(idxPostIN) + numel(postClassIds.p);
idxPostSps = numel(idxPostIN) +  numel(idxPostStp) + 1 :  numel(idxPostIN) + numel(idxPostStp) +  numel(postClassIds.s);
%preIds = [postClassIds.i(:); tcIds(:)];
%postIds = [postClassIds.i(:); postClassIds.p(:); postClassIds.s(:)];
% make plot
maxSyn = max(wholeSynMap(:));
colorMap = colormap(jet);
colorMap(1,:) = [0,0,0];
climits = [0.1, maxSyn];
h = imagesc(wholeSynMap);
daspect([1 1 1])
colormap(colorMap)
set(gca,'CLim',climits, 'TickDir','out')
colorbar
set(gca,'ytick',[max(idxPreIN), max(idxPreTC)],'yticklabels','',...
        'xtick',[max(idxPostIN), max(idxPostStp), max(idxPostSps)],'xticklabels','')
set(gcf,'Position',get(0, 'Screensize'))
export_fig(fullfile(outDirPdf,'heatmap_wholeSynMap.fig'),...
                '-q101', '-nocrop', '-transparent');
close all

%% grouped one whole syn map
wholeSynMap_grouped = cat(1, cat(2,synMapININ_grouped, synMapINStp_grouped, synMapINSps_grouped), cat(2,synMapTCIN_grouped, synMapTCStp, synMapTCSps));
idxPreIN_grouped = 1:numel(unique(cellTypesAll));
idxPreTC_grouped = numel(idxPreIN_grouped) + 1 : numel(idxPreIN_grouped) + numel(tcIds);
idxPostIN_grouped = 1:numel(unique(cellTypesAll));
idxPostStp_grouped = numel(idxPostIN_grouped) + 1 :  numel(idxPostIN_grouped) + numel(postClassIds.p);
idxPostSps_grouped = numel(idxPostIN_grouped) +  numel(idxPostStp_grouped) + 1 : ...
                     numel(idxPostIN_grouped) + numel(idxPostStp_grouped) +  numel(postClassIds.s);
%preIds = [INgroupNames(:); tcIds(:)];
%postIds = [INgroupNames(:); postClassIds.p(:); postClassIds.s(:)];
% make plot
maxSyn = max(wholeSynMap_grouped(:));
colorMap = colormap(jet);
colorMap(1,:) = [0,0,0];
climits = [0.1, maxSyn];
h = imagesc(wholeSynMap_grouped);
daspect([1 1 1])
colormap(colorMap)
set(gca,'CLim',climits, 'TickDir','out')
colorbar
set(gca,'ytick',[max(idxPreIN_grouped), max(idxPreTC_grouped)],'yticklabels','',...
        'xtick',[max(idxPostIN_grouped), max(idxPostStp_grouped), max(idxPostSps_grouped)],'xticklabels','')
set(gcf,'Position',get(0, 'Screensize'));
export_fig(fullfile(outDirPdf,'heatmap_wholeSynMap_grouped.fig'),...
                '-q101', '-nocrop', '-transparent');
close all
%%
Util.log('Saving data...')
Util.save(fullfile(outDirPdf,'extractConnectome.mat'),synMapTCIN, synMapTCStp, synMapTCSps, ...
                                                   shaftMapININ,somaMapININ, synMapININ,...
                                                   shaftMapINStp,somaMapINStp, synMapINStp,...
                                                   shaftMapINSps,somaMapINSps, synMapINSps, ...
                                                   wholeSynMap, idxPreIN, idxPreTC, idxPostIN, idxPostStp, idxPostSps,...
                                                   synMapTCIN_grouped,... 
                                                   shaftMapININ_grouped,somaMapININ_grouped, synMapININ_grouped,...
                                                   shaftMapINStp_grouped,somaMapINStp_grouped, synMapINStp_grouped,...
                                                   shaftMapINSps_grouped,somaMapINSps_grouped, synMapINSps_grouped, ...
                                                   wholeSynMap_grouped, idxPreIN_grouped, idxPreTC_grouped,...
                                                   idxPostIN_grouped, idxPostStp_grouped, idxPostSps_grouped, ...
                                                   tcIds, postClassIds, cellTypesAll); 

%{
% look at specific IN inputs connections
idsIN = [2,8,3,4,29,40,5,6,17,24,25,31,41, 12, 14]; %IN ids
idxMain = arrayfun(@(x) find(contains(dendIds,num2str(x,'%02d'))),idsIN,'uni',0); % find occurence of IN id in the synMap
idxNotFound = cellfun(@isempty, idxMain);
sprintf('IN not found %d \n', idsIN(idxNotFound))

idxKeep = cell2mat(idxMain(~idxNotFound)); 
synMap = fullSynMap(idxKeep,:);
namesIN = dendIds(idxKeep);
namesTC = tcIds;
maxSyn = max(synMap(:));
figure
h = heatmap(namesTC, namesIN, synMap,'ColorLimits',[0 maxSyn]);
title('TC to IN synapses')
saveas(gcf,fullfile(outDirPdf,'heatmap_tc_to_in.png'));
close all

Util.save(fullfile(outDirPdf,'extractConnectome.mat'),synMap, fullSynMap)
%}
