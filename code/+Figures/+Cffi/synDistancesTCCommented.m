function skelOut = synDistancesTCCommented(skel, tcNames, inClass)
% look in each tc axon for comments "sp p/s" "sh i"
% distSynIN: table() with each var as tc01, tc02,...
% comment at pre axon the distance of synapse

Util.log('Extract tc axon trees indices...')
preIdx = zeros(numel(tcNames),1);
for i = 1:numel(tcNames)
    preIdx(i) = skel.getTreeWithName(tcNames{i},'exact');
end

inIdx = zeros(numel(inClass),1);
for i = 1:numel(inClass)
    inIdx(i) = skel.getTreeWithName(sprintf('cell i%02d',inClass(i)),'exact');
end

searchStr = arrayfun(@(x) sprintf('i %02d',x),inClass,'uni',0);

skelOut = doThis(skel, preIdx, numel(tcNames), max(inClass), searchStr);

skelOut = skelOut.deleteTrees([preIdx; inIdx],true);

end

function skelOut = doThis(skel, preIdx, countTC, countPost, searchStr)
    skelOut = skel;
    for idxStr = 1:numel(searchStr)
        curStr = searchStr{idxStr};
        distSynPost = cell(countPost,countTC);
        for idxTC=1:numel(preIdx)
            curTree = preIdx(idxTC);
            tcRootIdx = skel.getNodesWithComment('TC_root',curTree,'partial');
            tcRootId = str2double(skel.nodesAsStruct{curTree}(tcRootIdx).id);
            synIdx = skel.getNodesWithComment(['s[hp](\s)?' curStr],curTree,'regexp');
            if ~isempty(synIdx)
                for k = 1:length(synIdx)
                    postIdInSkel = str2double(skel.nodesAsStruct{curTree}(synIdx(k)).id);
                    [~,~,distSyn] = skel.getShortestPath(tcRootId,postIdInSkel);
                    synComment = skel.nodesAsStruct{curTree}(synIdx(k)).comment;
                    % update comment
                    skelOut.nodesAsStruct{curTree}(synIdx(k)).comment = [synComment sprintf(' dist_%.f_psd_',distSyn./1e3)];
                    % id of post dendrite
                    idxPost = regexp(synComment,['s[hp](\s)?' curStr(1) '(\s)?(?<id>\w+)'],'names');
                    idxPost = str2double(idxPost.id);
                    distSynPost{idxPost,idxTC} = [distSynPost{idxPost,idxTC},distSyn./1e3]; %um append all syn dist
                end
            else
                % leave the cell array empty
                sprintf('No output to %s found for TC %02d', curStr, idxTC);
            end
        end
    end
end
