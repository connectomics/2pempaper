function [distSynPAll, distSynSAll] = synDistancesFromINExcSoma(skel, skelCellType, ...
    inNames, countIN, countStp, countSps)

Util.log('Extract in axon trees indices...')
% some IN axons don't exist
preIdx = [];
inId = [];
for i = 1:numel(inNames)
    idx = skel.getTreeWithName(inNames{i},'exact');
    if ~isempty(idx)
        preIdx = cat(1,preIdx,idx); % where in skel
        inId = cat(1,inId,i); % which IN ID
    clear idx
    end
end

distSynP = doShaftSpine(skel, preIdx, inId, countIN, countStp, 'p');
distSynS = doShaftSpine(skel, preIdx, inId, countIN, countSps, 's');

[distSynPAll, distSynSAll] = doSoma(skel,skelCellType, preIdx, inId, distSynP, distSynS);

Util.log('Finished extracting syn distances!')
end

function distSynPost = doShaftSpine(skel, preIdx, inId, countIN, countPost, searchStr)
    assert(numel(preIdx) == numel(inId))
    distSynPost = cell(countPost,countIN);
    for idxIN=1:numel(preIdx)
        curTree = preIdx(idxIN);
        curINid = inId(idxIN);
        rootIdx = skel.getNodesWithComment('(cellbody)?(soma)?',curTree,'regexp');
        rootId = str2double(skel.nodesAsStruct{curTree}(rootIdx).id);
        % do shaft spine together
        synIdx = skel.getNodesWithComment(['s[hp](\s)?' searchStr '(\s)?\d+'],curTree,'regexp');
        if ~isempty(synIdx)
            for k = 1:length(synIdx)
                postIdInSkel = str2double(skel.nodesAsStruct{curTree}(synIdx(k)).id);
                [~,~,distSyn] = skel.getShortestPath(rootId,postIdInSkel);
                synComment = skel.nodesAsStruct{curTree}(synIdx(k)).comment;
                % id of post dendrite
                idxPost = regexp(synComment,['s[hp](\s)?' searchStr '(\s)?(?<id>\w+)'],'names');
                idxPost = str2double(idxPost.id);
                distSynPost{idxPost, curINid} = [distSynPost{idxPost,curINid},distSyn./1e3]; %um append all syn dist
            end
        else
            % leave the cell array empty
            disp(['No output to ' searchStr ' shaft/spine found for IN ' num2str(curINid,'%02d')]);
        end
    end
end

function [distSynP, distSynS] = doSoma(skel,skelCellType, preIdx, inId, distSynP, distSynS)
    for idxIN=1:numel(preIdx)
        curTree = preIdx(idxIN);
        curINid = inId(idxIN);
        rootIdx = skel.getNodesWithComment('(cellbody)?(soma)?',curTree,'regexp');
        rootId = str2double(skel.nodesAsStruct{curTree}(rootIdx).id);
        % do soma
        synIdx = skel.getNodesWithComment('-',curTree);
        if ~isempty(synIdx)
            for k = 1:length(synIdx)
                postIdInSkel = str2double(skel.nodesAsStruct{curTree}(synIdx(k)).id);
                [~,~,distSyn] = skel.getShortestPath(rootId,postIdInSkel);
                synPos = skel.nodes{curTree}(synIdx(k),1:3);
                somaIdx = skelCellType.getClosestNode(synPos,1,false);
                synComment = skelCellType.nodesAsStruct{1}(somaIdx).comment;% s%% or p%% or py3 or py5 or in or uc
                postType = synComment(1); % s or p or i or uc or others
                postId = str2double(synComment(2:end)); % will be NaN for non s or p comments
                if isnan(postId)
                    continue;
                end
                switch postType
                    case 'p'
                        distSynP{postId,curINid} = [distSynP{postId,curINid},distSyn./1e3];
                    case 's'
                        distSynS{postId,curINid} = [distSynS{postId,curINid},distSyn./1e3];
                    case 'i'
                        disp('This syn was on IN, skipped')
                    case 'u'
                        disp('This syn was on UC, skipped')
                    otherwise
                        error('Soma syn at non s or p cellbody!');
                end
            end
        else
            disp(['No output to soma found for IN ' num2str(curINid,'%02d')]);
        end
    end
end
