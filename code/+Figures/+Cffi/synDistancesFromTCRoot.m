function [distSynIN, distSynP, distSynS] = synDistancesFromTCRoot(skel, tcNames, countTC, countIN, countStp, countSps)
% look in each tc axon for comments "sp p/s" "sh i"
% distSynIN: table() with each var as tc01, tc02,...

Util.log('Extract tc axon trees indices...')
preIdx = zeros(numel(tcNames),1);
for i = 1:numel(tcNames)
    preIdx(i) = skel.getTreeWithName(tcNames{i},'exact');
end

distSynIN = doThis(skel, preIdx, countTC, countIN, 'i');
distSynP = doThis(skel, preIdx, countTC, countStp, 'p');
distSynS = doThis(skel, preIdx, countTC, countSps, 's');
Util.log('Finished extracting syn distances!')
end

function distSynPost = doThis(skel, preIdx, countTC, countPost, searchStr)
    distSynPost = cell(countPost,countTC);
    for idxTC=1:numel(preIdx)
        curTree = preIdx(idxTC);
        tcRootIdx = skel.getNodesWithComment('TC_root',curTree,'partial');
        tcRootId = str2double(skel.nodesAsStruct{curTree}(tcRootIdx).id);
        synIdx = skel.getNodesWithComment(['s[hp](\s)?' searchStr '(\s)?\d+'],curTree,'regexp');
        if ~isempty(synIdx)
            for k = 1:length(synIdx)
                postIdInSkel = str2double(skel.nodesAsStruct{curTree}(synIdx(k)).id);
                [~,~,distSyn] = skel.getShortestPath(tcRootId,postIdInSkel);
                synComment = skel.nodesAsStruct{curTree}(synIdx(k)).comment;
                % id of post dendrite
                idxPost = regexp(synComment,['s[hp](\s)?' searchStr '(\s)?(?<id>\w+)'],'names');
                idxPost = str2double(idxPost.id);
                distSynPost{idxPost,idxTC} = [distSynPost{idxPost,idxTC},distSyn./1e3]; %um append all syn dist
            end
        else
            % leave the cell array empty
            disp(['No output to ' searchStr '  found for TC ' num2str(idxTC,'%02d')]);
        end
    end
end
