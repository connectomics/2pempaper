function somaLocs = somaLocations(skel,treeNames, somaComment)
% extract soma locations from cffi file
treeIdx = cellfun(@(x) getTreeWithName(skel,x,'exact'),treeNames);    
somaLocs = [];
for i=1:numel(treeIdx)
   somaLocs(i,:) = Util.getNodeCoordsWithComment(skel,somaComment,'regexp',treeIdx(i)); 
end
assert(size(somaLocs,1)==numel(treeNames))
end
