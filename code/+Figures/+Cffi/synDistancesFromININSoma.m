function distSynAll = synDistancesFromININSoma(skel, countIN)

inCellNames = arrayfun(@(x) ['cell i' num2str(x,'%02d')],1:countIN,'uni',0);

idxTreePost = [];
for i = 1:numel(inCellNames)
    idx = skel.getTreeWithName(inCellNames{i},'exact');
    idxTreePost = cat(1,idxTreePost,idx);
    clear idx
end
Util.log('Extracting syn distances IN to IN...')
distSynAll = cell(countIN,countIN);
for idxIN=1:numel(idxTreePost)
    curTree = idxTreePost(idxIN);
    % do shaft soma together
    synIdx = skel.getNodesWithComment('IN(\s)?\d+(\s)?(so)?',curTree,'regexp');
    if ~isempty(synIdx)
        for k = 1:length(synIdx)
            synComment = skel.nodesAsStruct{curTree}(synIdx(k)).comment;
            % id of pre axon
            idPre = regexp(synComment,'IN(\s)?(?<id>\d+)(\sso)?','names');
            idPre = str2double(idPre.id);
            synPos = skel.nodes{curTree}(synIdx(k),1:3);
            idxTreePre = skel.getTreeWithName(['IN axon ' num2str(idPre,'%02d')]);
            if isempty(idxTreePre)
               error(['Input synpase from axon ' num2str(idPre) ' but no axon found!'])
            end
            % look for closest node in pre axon tree
            [axonSynNodeIdx, axonTreeIdxFound] = skel.getClosestNode(synPos,idxTreePre,false);
            assert(idxTreePre == axonTreeIdxFound)
            axonSynNodeId = str2double(skel.nodesAsStruct{idxTreePre}(axonSynNodeIdx).id);
            % find pre axons's soma by looking in corresponding dendrite?
            rootIdx = skel.getNodesWithComment('(cellbody)?(soma)?',idxTreePre,'regexp');
            rootId = str2double(skel.nodesAsStruct{idxTreePre}(rootIdx).id);
            
            % find path from output syn to its soma
            [~,~,distSyn] = skel.getShortestPath(rootId,axonSynNodeId);
            distSynAll{idxIN,idPre} = [distSynAll{idxIN,idPre},distSyn./1e3]; %um append all syn dist
        end
    else
        % leave the cell array empty
        disp(['No input found to IN' num2str(idxIN,'%02d')]);
    end
end
Util.log('Finished extracting syn distances!')
end