% Collect data for 2018-12-01_cff_v3 required to run Fig3 graph-connectome
setConfig;
outDirPdf = fullfile(outDir,'data');
annotationType = 'manual'; %'manual'; % ''
Util.log('Loading skeleton...')
if contains(annotationType,'manual')
    outfile = config.cffi_excel_SLmanual;
    skel = skeleton(config.cffi_SLmanual); % tcSyn dist. Contains TC, IN, ExN
else
    outfile = config.cffi_excel;
    skel = skeleton(config.cffi); % tcSyn dist. Contains TC, IN, ExN
end

skel = Figures.Fig2.IN.fixComments(skel);
skelCellType = skeleton(config.cellTypesL4Fixed);

Util.log('Read cell types manually assigned')
xlsfile = config.cellTypesIN;
[~,~,xl1] = xlsread(xlsfile);
xlTable1 = cell2table(xl1);
xlTable1(1,:) = [];
cellTypesAll = cell2mat(xlTable1.xl16);
clear xlTable1 xlsfile xl1


%% extract distances of TC synapses from TC root
% get TC input on all post targets
countTC = numel(skel.getTreeWithName('TC axon(\s?)\d+','regexp'));
countIN = numel(skel.getTreeWithName('cell i(\s)?\d+','regexp'));
countSps = numel(skel.getTreeWithName('cell s(\s)?\d+','regexp'));
countStp = numel(skel.getTreeWithName('cell p(\s)?\d+','regexp'));

% TC output in tc axons trees "sh i" "sp p/s"
tcNames = arrayfun(@(x) ['TC axon ' num2str(x,'%02d')],1:countTC,'uni',0); % 20 tc axons names
inNames = arrayfun(@(x) ['IN axon ' num2str(x,'%02d')],1:countIN,'uni',0); % axons names
[distSynIN_TC, distSynP_TC, distSynS_TC] = ...
    Figures.Cffi.synDistancesFromTCRoot(skel, tcNames, countTC, countIN, countStp, countSps);

% For IN to IN, look from post side
distSynIN_IN = Figures.Cffi.synDistancesFromININSoma(skel, countIN);

% for IN to Stp Sps
% NOTE: Now increase countStp and countSps based on all annotations
c = skelCellType.getAllComments;
pIds = cellfun(@(x) regexp(x,'p(?<id>\d+)','names') ,c,'uni',0);
pIds = cellfun(@(x) str2double(x.id), pIds(~cellfun('isempty',pIds)));
countStpApp = max(pIds);
sIds = cellfun(@(x) regexp(x,'s(?<id>\d+)','names') ,c,'uni',0);
sIds = cellfun(@(x) str2double(x.id), sIds(~cellfun('isempty',sIds)));
countSpsApp = max(sIds);
clear c
%countStpApp = 375;
%countSpsApp = 964;
[distSynP_IN, distSynS_IN] = ...
    Figures.Cffi.synDistancesFromINExcSoma(skel, skelCellType, inNames, countIN, countStpApp, countSpsApp);

%% choose whether to use whole version of partial version
distSynP_TC_app = [distSynP_TC;cell( size(distSynP_IN,1) - size(distSynP_TC,1) ,size(distSynP_TC,2))];
distSynS_TC_app = [distSynS_TC;cell( size(distSynS_IN,1) - size(distSynS_TC,1) ,size(distSynS_TC,2))];

%% extract locations of soma and convert to 2P space
Util.log('Compiling soma locations ....')
inLocs = Figures.Cffi.somaLocations(skel,arrayfun(@(x)['cell i' ...
    num2str(x,'%02d')] ,1:countIN,'uni',0),'_in'); % _in01
pLocs = Figures.Cffi.somaLocationsCellType(skelCellType, countStpApp,'p');
sLocs = Figures.Cffi.somaLocationsCellType(skelCellType, countSpsApp,'s');

% convert to 2P space
m=load(config.barrelStateTform,'tform');
tform = m.tform;
% tilt correction hack and conversion
inLocsTemp = inLocs; sLocsTemp = sLocs; pLocsTemp = pLocs;
inLocsTemp(:,2) = round(inLocs(:,2)-.449*inLocs(:,3));
pLocsTemp(:,2) = round(pLocs(:,2)-.449*pLocs(:,3));
sLocsTemp(:,2) = round(sLocs(:,2)-.449*sLocs(:,3));
inLocsLM = TFM.applyTformToVertices(tform, inLocsTemp, 'forward');
pLocsLM = TFM.applyTformToVertices(tform, pLocsTemp, 'forward');
sLocsLM = TFM.applyTformToVertices(tform, sLocsTemp, 'forward');
Util.log('Finished soma locations ....')

Util.log('Keep only IN axons pre traced by YH...') 
idxKeepINs = [1,2,3,4,5,6,7,8,12,14,17,18,24,25,27,28,29,26,40]; % keep only INs traced by YH
distSynIN_IN = distSynIN_IN(:,idxKeepINs);
distSynP_IN =  distSynP_IN(:,idxKeepINs);
distSynS_IN =  distSynS_IN(:,idxKeepINs);

%% compile data
allSomas = vertcat(inLocsLM, pLocsLM, sLocsLM);
allCellTypes = cellstr(vertcat(repelem('i',countIN,1),repelem('p',countStpApp,1),repelem('s',countSpsApp,1)));
allIds = reshape(horzcat(arrayfun(@(x) num2str(x,'%02d'),1:countIN,'uni',0),...
                       arrayfun(@(x) num2str(x,'%02d'),1:countStpApp,'uni',0),...
                       arrayfun(@(x) num2str(x,'%02d'),1:countSpsApp,'uni',0)),'',1);
                   
func = @(x) reshape(cellfun(@(y) mat2str(y),reshape(x,1,''),'uni',0),size(x,1),size(x,2));
data = horzcat(allCellTypes, ...
        allIds, ...
        num2cell(allSomas(:,1)), num2cell(allSomas(:,2)), num2cell(-1*allSomas(:,3)),...                       
        [func(distSynIN_TC),func(distSynIN_IN);...
        func(distSynP_TC_app),func(distSynP_IN);...
        func(distSynS_TC_app),func(distSynS_IN)]);
% add IN cell types: NOTE: Change from 43 to more later
data = [data, [cellstr(num2str(cellTypesAll(1:43))); repelem({''},countStpApp+countSpsApp,1)]];

numCols = size(data,2);
t = cell2table(cell(size(data,1) , size(data,2)),...
    'VariableNames', cat(2, {'cell_type','ID','x','y','depth_i_um'},...
    arrayfun(@(x) ['t' num2str(x,'%02d')], 1:countTC,'uni',0),...
    arrayfun(@(x) ['i' num2str(x,'%02d')], idxKeepINs,'uni',0),...
    {'cell_types_in'}));

for i=1:numCols
    t.(t.Properties.VariableNames{i}) = data(:,i);
end
Util.log('Writing outout excel data..')
if exist(outfile,'file')
    temp = xlsread(outfile);
    xlswrite(outfile,zeros(size(temp))*nan);
end
writetable(t,outfile);
% xlswrite(outfile,outData);
Util.log('Finished writing output excel data')
