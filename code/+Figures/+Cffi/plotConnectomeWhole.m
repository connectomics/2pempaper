setConfig
outDir = config.outDir;
outDirPdf = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version]);
load(config.connmat)

fullINs = [1,2,3,4,5,6,7,8,12,14,17,18,24,25,27,28,29,36,40];
restrictINFull = true;

%% 
data = wholeSynMap_grouped;
outfile = fullfile(outDirPdf,'heatmapWithHist_WholeSyn_grouped.eps');
plotConnWithHist(data,idxPreIN_grouped,idxPreTC_grouped,idxPostIN_grouped, idxPostStp_grouped,idxPostSps_grouped,true, outfile);

data = wholeSynMap;
% randomize in post groups
rng(0)
indIN = randperm(numel(idxPostIN));
indStp = randperm(numel(idxPostStp));
indSps = randperm(numel(idxPostSps));

if restrictINFull
    Util.log('Restricting to full INs only:')
    assert(max(fullINs)<=numel(idxPreIN))
    idxPreIN = fullINs;
end
data = wholeSynMap([idxPreIN,idxPreTC],...
                     [idxPostIN(indIN), idxPostStp(indStp), idxPostSps(indSps)] );

outfile = fullfile(outDirPdf,'heatmapWithHist_WholeSyn.eps');
maxSyn = plotConnWithHist(data,idxPreIN,idxPreTC,idxPostIN(indIN), idxPostStp(indStp),idxPostSps(indSps),true, outfile);

outfile = fullfile(outDirPdf,'heatmapWithHist_WholeSyn_colorbar.eps');
Util.plotColorbar(outfile, maxSyn);

function maxSyn = plotConnWithHist(data,idxPreIN,idxPreTC,idxPostIN, idxPostStp,idxPostSps, histFlag, outfile)
Util.log('Plotting conn...')
dataY = sum(data,2);
dataX = sum(data,1);
minSynCount = 0;

plotType = 'scatter'; % 'imagesc'
fig = figure();
fig.Color = 'white';
fig.Position(1:2) = 50;
fig.Position(3:4) = 900;
ax = axes(fig);
switch plotType
    case 'imagesc'
        maxSyn = max(data(:));
        colorMap = colormap(jet);
        colorMap(1,:) = [0,0,0];
        climits = [0.1, maxSyn];
        h = imagesc(ax, data);
        % daspect([1 1 1])
        colormap(colorMap)
        set(gca,'CLim',climits, 'TickDir','out')
        colorbar('Location','westoutside')
    case 'scatter'
        maxSyn = 5; % max(data(:));
        colorMap = parula(maxSyn);
        dataMat = data';
        [x,y] = find(dataMat);
        h = scatter(ax, x, y,...
            10,'o','filled');
        idxNonZero = find(dataMat);
        h.CData = colorMap( min(dataMat(idxNonZero), size(colorMap, 1)),:);
        xlim([1,size(dataMat,1)])
        ylim([1,size(dataMat,2)])
end
ax.Color = 'black';
ax.TickDir = 'out';
ax.YDir = 'reverse';
ax.XAxisLocation = 'top';
set(gca,'ytick',[max(idxPreIN), max(idxPreTC)],'yticklabels','',...
        'xtick',[max(idxPostIN), max(idxPostStp), max(idxPostSps)],'xticklabels','')
ax.Position = [0.15, 0.15, 0.7, 0.7];
%%
if histFlag
    % Histogram over incoming synapses
    Util.log('Plotting histogram post...')
    axPost = axes(fig);
    axPost.Position = ax.Position;
    axPost.Position(2:2:4) = [ax.Position(2)+ax.Position(4), 0.05]; % top
    axPost.Position(2:2:4) = [0.05, ax.Position(2)-0.05]; % bottom

    postSynCount = dataX;

    histogram(axPost, ...
        'BinCount', postSynCount, ...
        'BinEdges', 0:1:numel(postSynCount), ...
        'EdgeColor', ax.ColorOrder(1, :), ...
        'FaceAlpha', 1);

    axPost.YDir = 'reverse';
    axPost.TickDir = 'out';
    axPost.YMinorTick = 'off';
    % axPost.YScale = 'log';
    % axPost.YLim(2) = max(postSynCount);
    % yTicks = 0:axPost.YLim(end);
    % yticks(axPost, yTicks);
    % yticklabels(axPost, arrayfun( ...
    %     @(d) sprintf('%d', d), ...
    %     yTicks, 'UniformOutput', false));

    xlim(axPost, [0, numel(postSynCount)]);
    xticks(axPost, [xticks(axPost), axPost.XLim(2)]);
    ylim(axPost, [0, max(postSynCount)]);
    yticks(axPost, [yticks(axPost), axPost.YLim(2)]);
    ylabel(axPost, 'Synapses');

    % Histogram over outgoing synapses
    Util.log('Plotting histogram pre...')
    axPre = axes(fig);
    axPre.Position = ax.Position;
    axPre.Position(1) = sum(ax.Position(1:2:end));
    axPre.Position(3) = 0.95 - axPre.Position(1);

    axonSynCount = dataY;
    histogram(axPre, ...
        'BinCounts', axonSynCount, ...
        'BinEdges', 0:1:numel(axonSynCount), ...
        'Orientation', 'horizontal', ...
        'EdgeColor', ax.ColorOrder(1, :), ...
        'FaceAlpha', 1);

    axPre.TickDir = 'out';
    % axPre.XScale = 'log';
    axPre.XAxisLocation = 'top';
    axPre.XMinorTick = 'off';
    axPre.YDir = 'reverse';
    axPre.YAxisLocation = 'right';

    % axPre.XLim(1) = minSynCount;
    % axPre.XLim(2) = max(axonSynCount);
    % xTicks = log10(axPre.XLim);
    % xTicks = 10 .^ (xTicks(1):xTicks(2));
    % xticks(axPre, xTicks);
    % xticklabels(axPre, arrayfun( ...
    %     @(d) sprintf('%d', d), ...
    %     xTicks, 'UniformOutput', false));

    xlim(axPre, [0, max(axonSynCount)]);
    xticks(axPre, [xticks(axPre), axPre.XLim(2)]);
    ylim(axPre, [0, numel(axonSynCount)]);
    yticks(axPre, [yticks(axPre), axPre.YLim(2)]);
    xlabel('Synapses');
end
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all
end
