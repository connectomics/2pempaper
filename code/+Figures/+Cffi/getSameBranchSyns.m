function [synIdxClusters, labels, distSomaClusters] = getSameBranchSyns(paths, synIdx, distSoma)
% Input: paths from syns to soma. Cell array of row vectors
%        synIds of synapses
% Output: Cell array with clusters of syn indices that lie on the same branch
%         labels of agglos

% remove soma node in the path
paths = cellfun(@(x) x(2:end), paths,'uni',0);

% max id
maxSegId = max(horzcat(paths{:}));

% edges
edges = cellfun(@(x) sort([x(1:end-1)', x(2:end)'],2), paths,'uni',0);
edges = vertcat(edges{:});

[labels, ~] = Agglo.mergeAgglosAtEdges(paths, edges, maxSegId);

synIdxClusters = {};
distSomaClusters = {};
for i=1:max(labels)
    curGroup = labels == i;
    if sum(curGroup)>1
        synIdxClusters = cat(2,synIdxClusters, {synIdx(curGroup)});
        distSomaClusters = cat(2, distSomaClusters, {distSoma(curGroup)});
    end
end

end

