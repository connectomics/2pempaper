% plot all somata across barrel somas from YH's cffi data

if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
    addpath(genpath('/home/loombas/mnt/gaba/u/repos/loombas/L4'));
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
    addpath(genpath('/u/sahilloo/repos/loombas/L4'));
end
outDir = config.outDir;

somaColors = {[230/255, 159/255, 0/255, 1],... % 1a orange
              [213/255, 94/255, 0/255, 1],... %1b vermillion
              [0, 0, 0, 1],... % 2 % black
              [0.75, 0.75, 0, 1],... % 3 dark yellow
              [204/255, 121/255, 167/255, 1],... % 4 reddish purple
              [0.3010, 0.7450, 0.933,1],... % 5 black
              [1, 0, 1, 1],... %magenta
              [0,0,1,1]}; % blue

synBallSize = 25; tubeSize = 2; dotSize= 3;
somaBallColorIN = somaColors{3}; %black
somaBallColorStp = somaColors{7}; % magenta 
somaBallColorSps = somaColors{8}; % blue
somaBallSize = 25;

outDirPdf = fullfile(config.outDir,'outData/figures/cffi/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

skelPC = skeleton(fullfile(outDir,'outData/pointClouds/skelSomaIn_vOct.nml'));
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

Util.log('Read LM sps and stp soma coords...')
xlsfile = fullfile(outDir,'data/2018-12-01_cffi_v2.xlsx');
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);
xlTable = xlTable(2:end,:);

% cffi nml file

skelcffi = skeleton(fullfile(outDir,'data/2018-12-05_cffi.nml'));
names = skelcffi.names;

% cell i##
idxIN = cellfun(@(x) any(regexp(x,'cell i')),names);
skelIN = skelcffi.deleteTrees(~idxIN);
somaLocsIN = [];
somaLocsIN = Util.getNodeCoordsWithComment(skelIN, {'soma', '_in'}, 'partial'); 
[namesIN,idxSort] = sort(names(idxIN));
somaLocsIN = somaLocsIN(idxSort,:); % sort names and their soma positions

% cell p## : NOTE:p33 is missing
idxStp = cellfun(@(x) any(regexp(x,'cell p')),names);
skelStp = skelcffi.deleteTrees(~idxStp);
somaLocsStp = [];
somaLocsStp = Util.getNodeCoordsWithComment(skelStp, {'soma', '_sp'}, 'partial');
namesStp = names(idxStp);
ids = cellfun(@(x) str2double(regexp(x,'\d+','match','once')),namesStp);
[~,idxSort] = sort(ids);
namesStp = namesStp(idxSort);
somaLocsStp = somaLocsStp(idxSort,:); % sort names and their soma positions

% cell s##
idxSps = cellfun(@(x) any(regexp(x,'cell s')),names);
skelSps = skelcffi.deleteTrees(~idxSps);
somaLocsSps = [];
somaLocsSps = Util.getNodeCoordsWithComment(skelSps, {'soma', '_ss'}, 'partial');
namesSps = names(idxSps);
ids = cellfun(@(x) str2double(regexp(x,'\d+','match','once')),namesSps);
[~,idxSort] = sort(ids);
namesSps = namesSps(idxSort);
somaLocsSps = somaLocsSps(idxSort,:); % sort names and their soma positions

%% Cell ## ??
%idxOther = cellfun(@(x) any(regexp(x,'Cell ')),names);

% change scale of somaLocs
scale = skelPC.scale./1000;
nodesPC = skelPC.setScale(nodesPC,scale);
somaLocsIN = skelPC.setScale(somaLocsIN,scale);
somaLocsStp = skelPC.setScale(somaLocsStp,scale);
somaLocsSps = skelPC.setScale(somaLocsSps,scale);

Util.log('Now plotting figure....');
bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;
objectPC = ...
            scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
            ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
            'MarkerFaceColor', colorPC(1:3));
objectSomaIN = ...
             scatter3(somaLocsIN(:,1),somaLocsIN(:,2),somaLocsIN(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColorIN(1:3), ...
            'MarkerFaceColor', somaBallColorIN(1:3));

objectSomaStp = ... 
             scatter3(somaLocsStp(:,1),somaLocsStp(:,2),somaLocsStp(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColorStp(1:3), ...
            'MarkerFaceColor', somaBallColorStp(1:3));
objectSomaSps = ...
             scatter3(somaLocsSps(:,1),somaLocsSps(:,2),somaLocsSps(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColorSps(1:3), ...
            'MarkerFaceColor', somaBallColorSps(1:3));

%    xlabel('x ');
%    ylabel('y ');
%    zlabel('z ');
%    grid on
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
set(gca,'ztick',[])
set(gca,'zticklabel',[])
axis equal 
axis off
box off
a.XLim = bbox(1,:);
a.YLim = bbox(2,:);
a.ZLim = bbox(3,:);
%yz
view([90, 0]);
camroll(-90);
camroll(270);
outfile = fullfile(outDirPdf,['all_somata.eps']);
export_fig(outfile,'-q101', '-nocrop', '-transparent');
Util.log('Saving file %s.', outfile);
close(f);




