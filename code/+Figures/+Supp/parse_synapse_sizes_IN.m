% annotated tc to IN synapse PSD sizes before
% extract syn distances from TC root and add to comment. 
% Parse the tc root distance and psd size now

setConfig;

inType = 'WB';
Util.log('Doing for %s',inType)
skelAnnotate = skeleton(fullfile(outDir,'outData','figures','fig3',config.version, ...
    'groundTruth', ['syn_distances_annotate_' inType '_to_parse.nml']));
[psdWB, plTCWB] = parseThis(skelAnnotate);

inType = 'LB';
Util.log('Doing for %s',inType)
skelAnnotate = skeleton(fullfile(outDir,'outData','figures','fig3',config.version, ...
    'groundTruth', ['syn_distances_annotate_' inType '_to_parse.nml']));
[psdLB, plTCLB] = parseThis(skelAnnotate);

inType = 'L4';
Util.log('Doing for %s',inType)
skelAnnotate = skeleton(fullfile(outDir,'outData','figures','fig3',config.version, ...
    'groundTruth', ['syn_distances_annotate_' inType '_to_parse.nml']));
[psdL4, plTCL4] = parseThis(skelAnnotate);

%%
somaColors = Util.getSomaColors;
Util.log('Scatter:')
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on;
plot(psdWB, plTCWB,'o','MarkerFaceColor',somaColors{1}(1:3),'MarkerEdgeColor',somaColors{1}(1:3))
plot(psdLB, plTCLB,'o','MarkerFaceColor',somaColors{2}(1:3),'MarkerEdgeColor',somaColors{2}(1:3))
plot(psdL4, plTCL4,'o','MarkerFaceColor',somaColors{3}(1:3),'MarkerEdgeColor',somaColors{3}(1:3))

xlabel('TC synapse PSD size(nm)')
ylabel('Pathlength of TC axon (nm)')

ax.XAxis.TickValues = 0:200:1200;
ax.XAxis.Limits = [0,1200];

ax.YAxis.Limits = [0,3.5e5];

ax.LineWidth = 4;
set(gca,'FontSize',18)
Util.setPlotDefault(gca,'','');
% daspect([1,100,1])
outfile = fullfile(outDir,'outData/figures','fig2',config.version,'synapse_size_tc_IN_psd.eps');
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all

%%
Util.log('box plot')
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on;
x1 = psdWB(:);
x2 = psdLB(:);
x3 = psdL4(:);

x = [x1; x2; x3];
curIdx1 = 1:numel(x1);
curIdx2 = curIdx1(end)+(1:numel(x2));
curIdx3 = curIdx2(end)+(1:numel(x3));

g = cell(numel(x),1);
g(curIdx1) = {'WB'}; % 1st box
g(curIdx2) = {'LB'}; % 2nd box
g(curIdx3) = {'L4'}; % 3rd box

pVal12 = ranksum(x(curIdx1),x(curIdx2));
pVal13 = ranksum(x(curIdx1),x(curIdx3));
pVal23 = ranksum(x(curIdx2),x(curIdx3));
rng(0)
colors = cat(1,somaColors{1}(1:3),... % wb
               somaColors{2}(1:3),... % lb
               somaColors{3}(1:3)); % l4

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
scatter(widthS.*rand(numel(curIdx1),1)+posS(1), x(curIdx1),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdx2),1)+posS(2), x(curIdx2),...
    100,colors(2,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdx3),1)+posS(3), x(curIdx3),...
    100,colors(3,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
set(h,{'linew'},{4});
sprintf('p-value %f, #WB = %d, #LB = %d', pVal12,numel(x1),numel(x2))
sprintf('p-value %f, #WB = %d, #L4 = %d', pVal13,numel(x1),numel(x3))
sprintf('p-value %f, #LB = %d, #L4 = %d', pVal23,numel(x2),numel(x3))
ylabel('TC synapse PSD size(nm)')

ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:100:1200;
ax.YAxis.TickValues = 0:200:1200;
ax.YAxis.Limits = [0,1200];
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
set(gca,'FontSize',24)
Util.setPlotDefault(gca,'','');
daspect([1,100,1])
outfile = fullfile(outDir,'outData/figures','fig2',config.version,'synapse_size_IN_psd.eps');
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all

function [psd, d] = parseThis(skel)
    % keep only TC axons
    skelTC = skel.keepTreeWithName('TC axon','regexp');
    
    % keep psd trees
    skelPSD = skel.keepTreeWithName('explorative','regexp');
    
    % path lengths and nearest TC axon comment for axonal distance
    numPSD = skelPSD.numTrees;
    psd = zeros(numPSD,1);
    d = zeros(numPSD,1);
    for curTree = 1:numPSD
        psd(curTree) = skelPSD.pathLength(curTree); % nm
    
        curNodes = skelPSD.getNodes(curTree);
        assert(size(curNodes,1) == 2)
        for i=1:2
            [nodeIdx, treeIdx] = skelTC.getClosestNode(curNodes(i,:));
            
            rNodes = skelTC.reachableNodes(treeIdx,nodeIdx, 2, 'up_to'); % 2 steps
            curComment = {skelTC.nodesAsStruct{treeIdx}(rNodes).comment};
            idxKeep = find(contains(curComment, 'dist_'));
            curComment = curComment{idxKeep};
            if idxKeep % comment with dist is found
                break;
            end
        end
        assert(contains(curComment, 'dist_'))
        temp = regexp(curComment, 'dist_(?<distance>\d+)_psd_','names');
        d(curTree) = str2double(temp.distance)*1e3; % nm
        clear curComment temp
    end
end
