% plot the distributions along Z
% real endings
% artificial endings
% https://webknossos.brain.mpg.de/annotations/Explorational/5bbd4593010000e91ed6a06a#9585,22931,8944,0,0.64,341985
version = '04_07_2019';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
    addpath(genpath('/home/loombas/mnt/gaba/u/repos/loombas/L4'));
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
    addpath(genpath('/u/sahilloo/repos/loombas/L4'));
end
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3',version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
nml = fullfile(config.outDir,'data',['skel_tc_v_' version '.nml']); % annotated endings
skel = skeleton(nml);
skel = skel.keepTreeWithName('TC axon','regexp');
skel.scale = [11.24,11.24,30];

Util.log('find real/artifical endings and synapses...')
nodesCross1 = Util.getNodeCoordsWithComment(skel,'out','partial');
nodesCross2 = Util.getNodeCoordsWithComment(skel,'gg','partial');
nodesCross = vertcat(nodesCross1,nodesCross2);
nodesCircle = Util.getNodeCoordsWithComment(skel,'end','partial');
nodesBP = [];
for i =1:skel.numTrees
    bp = getBranchpoints(skel,i);       
    nodesBP = vertcat(nodesBP, skel.nodes{i}(bp,1:3));
end

scale = skel.scale./1000;
nodesCross = skel.setScale(nodesCross,scale);
nodesCircle = skel.setScale(nodesCircle,scale);
nodesBP = skel.setScale(nodesBP,scale);

bboxWk = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bboxWk, [11.24; 11.24; 30]./1000);

Util.log('Now plotting...')
binWidth = 30;
% analysis of real endings distribution
dataZ = nodesCircle(:,3);
name = 'tc-real-endings';
plotForThisData(dataZ,binWidth, bbox, outDirPdf, name);
% analysis of artificial endings distribution
dataZ = nodesCross(:,3);
name = 'tc-artificial-endings';
plotForThisData(dataZ,binWidth, bbox, outDirPdf, name);
% analysis of branchpoints
dataZ = nodesBP(:,3);
name = 'tc-branchpoints';
plotForThisData(dataZ,binWidth, bbox, outDirPdf, name);

function plotForThisData( dataZ, binWidth, bbox, outDirPdf, name)
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
axis on
box on
set(gca,'xtick',[])
set(gca,'ytick',[])
h = histogram(dataZ,'DisplayStyle','stairs','BinWidth',binWidth);
xlim([bbox(3,1) bbox(3,2)]);
title(name)
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', name));
export_fig(outfile,'-q101', '-nocrop', '-transparent');
close(f)
end
