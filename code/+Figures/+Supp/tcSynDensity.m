% plot the distributions along Z
% tc synapse density
% tc axon length
% tc synapses
% https://webknossos.brain.mpg.de/annotations/Explorational/5bbd4593010000e91ed6a06a#9585,22931,8944,0,0.64,341985
version = '04_07_2019';
if Util.isLocal()
    config.outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';
    addpath(genpath('/home/loombas/mnt/gaba/u/repos/loombas/L4'));
else
    config.outDir = '/tmpscratch/sahilloo/barrel/';
    addpath(genpath('/u/sahilloo/repos/loombas/L4'));
end
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3/',version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
nml = fullfile(config.outDir,'data',['skel_tc_v_' version '.nml']); % annotated endings
skel = skeleton(nml);
skel.scale = [11.24,11.24,30];

Util.log('find synapses...')
bboxWk = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bboxWk, [11.24; 11.24; 30]./1000);

Util.log('Find tc axon lengths...')
dimension = 3; binSize = 1000;
[synDensity, synCount, synLengths] = Util.calculateSynDensity(skel,{'sp','sh'},'partial', bboxWk, dimension, binSize);

Util.log('Now plotting...')
boxWidth = 0.9;
plotForThisType(synDensity, boxWidth, 'v', 'tcSynDensity', outDirPdf);
plotForThisType(synCount, boxWidth, 'v', 'tcSynCount', outDirPdf);
plotForThisType(synLengths, boxWidth, 'v', 'tcSynLengths', outDirPdf);
plotForThisTypeSimple(synCount, 'tc_syn_counts_line', outDirPdf);
plotForThisTypeSimple(synLengths, 'tc_syn_lengths_line', outDirPdf);

function plotForThisType(dataType, boxWidth, markerType, name, outDirPdf)
% dataType: 1xN cell array
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;
dataMax = max(cellfun(@(x) numel(x),dataType));
data = nan(dataMax,length(dataType));
for i=1:length(dataType)
    thisData = dataType{i};
    if ~isempty(thisData)
        thisDataSize = length(thisData);
        data(1:thisDataSize,i) = thisData(:);
    end
end
boxplot(data,'Colors','k','Widths',boxWidth, 'Whisker',0);
for i=1:length(dataType)
    scatter(i - boxWidth/2 + boxWidth.*rand(size(data(:,i))),data(:,i),...
        markerType,'MarkerFaceColor',[0,0,0],'MarkerEdgeColor',[0,0,0])
end
medianPoints = NaN(length(dataType),1);
for i=1:length(dataType)
   idxNonNan = ~isnan(data(:,i));
   if any(idxNonNan)
       medianPoints(i) = median(data(idxNonNan,i));
   end
end
plot(medianPoints,'LineWidth',1);
axis square
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', name));
export_fig(outfile,'-q101','-transparent');
%export_fig(outfile,'-q101', '-nocrop', '-transparent');
close(f)
end


function plotForThisTypeSimple(dataType, name, outDirPdf)
% only plot summed numbers
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
dataPoints  = cellfun(@sum,dataType);
plot(dataPoints,'LineWidth',1);
axis square
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', name));
export_fig(outfile,'-q101','-transparent');
%export_fig(outfile,'-q101', '-nocrop', '-transparent');
close(f)
end
