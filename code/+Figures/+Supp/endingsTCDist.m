% plot the distribution of real endings along Z
% https://webknossos.brain.mpg.de/annotations/Explorational/5bbd4593010000e91ed6a06a#9585,22931,8944,0,0.64,341985
setConfig;
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/fig3/v1/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

somaColors = {[230/255, 159/255, 0/255, 1],... % 1a orange
              [213/255, 94/255, 0/255, 1],... %1b vermillion
              [0, 0, 0,1],... % 2 % black
              [0.75, 0.75, 0, 1],... % 3 dark yellow
              [204/255, 121/255, 167/255, 1],... % 4 reddish purple
              [0.3010, 0.7450, 0.933,1],... % 5 skyblue
              [0,158/255, 115/255, 1],... % bluish green
              [182/255,109/255,1,1],...% magenta
              [36/255,1,36/255,1],... % green
              [0.5,0.5,0.5,1]};
% red orange
rng(0)
Rmin = 0.9;
Rmax = 1;
Gmin = 0;
Gmax = 0.6;
Bmin = 0;
Bmax = 0.5;
dotSize= 3;

colorEndings = somaColors{9};
sizeEndings = 5;
nml = config.skelTCCombined; % annotated endings
skel = skeleton(nml);
totalTrees = skel.numTrees;
treeNames = skel.names;
numTrees = skel.numTrees;
skel.scale = [11.24,11.24,30];

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];


Util.log('find real endings')
thisSkel = skel;
nodesReal = getNodeCoordsWithComment(skel,'end','partial');

scale = skel.scale./1000;
nodesReal = skel.setScale(nodesReal,scale);
nodesPC = skel.setScale(nodesPC,scale);

bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);

tic;
Util.log('Now plotting...')
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
for j = 1:4
    a(j) = subplot(2, 2, j);
    hold on;
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])
    set(gca,'ztick',[])
    set(gca,'zticklabel',[])

    objRE = ...
      scatter3(nodesReal(:,1),nodesReal(:,2),nodesReal(:,3)...
      ,sizeEndings,'MarkerEdgeColor', colorEndings(1:3), ...
      'MarkerFaceColor', colorEndings(1:3));

    objectPC = ...
            scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
            ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
            'MarkerFaceColor', colorPC(1:3));

%    xlabel('x ');
%    ylabel('y ');
%    zlabel('z ');
%    grid on
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])
    set(gca,'ztick',[])
    set(gca,'zticklabel',[])

    switch j
        case 1
            %xy
            view([0, -90]);
            camroll(-90);
        case 2
            %xz
            view([0, 180]);
            camroll(-90);
        case 3
            %yz
            view([90, 0]);
            camroll(-90);
            camroll(270);
        case 4
            %yz
            view([90,0]);
            camroll(-90);
            camroll(270);
    end
    axis equal 
    axis on
    box on
    a(j).XLim = bbox(1,:);
    a(j).YLim = bbox(2,:);
    a(j).ZLim = bbox(3,:);

end

thisSkel.filename = 'real_endings_location';
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', thisSkel.filename));
export_fig(outfile,'-q101', '-nocrop', '-transparent');

Util.log('Saving file %s.', outfile);
close(f);    
toc;

% analysis of real endings distribution
dataZ = nodesReal(:,3);
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
h = histogram(dataZ,'DisplayStyle','stairs','BinWidth',30);
xlim([bbox(3,1) bbox(3,2)]);
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'real_endings_hist'));
export_fig(outfile,'-q101', '-nocrop', '-transparent');
close(f)

function coords = getNodeCoordsWithComment(skel,comment,type)
coords = []; somaCoord = [];
for i=1:skel.numTrees
    m = skel.getNodesWithComment(comment,i,type,true);
    mm = ~cellfun(@isempty,m);
    loc = find(mm);
    if ~isempty(loc)
        somaCoord = skel.nodes{i}(m{loc(1)},1:3);
    end
    coords = vertcat(coords,somaCoord);
end

end
