% synapse size from TC to Stp/Sps is similar
setConfig;
uiopen(fullfile(outDir,'data/synSize/TC_syn_1.fig'),1)
outfile = fullfile(outDir,'outData/figures/fig3/','synapse_size.eps');

Util.log('Extracting data from fig:')
fig = gcf;
axObjs = fig.Children;
dataObjs = axObjs.Children;

dataStp = nan(numel(dataObjs(1).XData),2);
dataStp(:,1)  = dataObjs(1).XData;
dataStp(:,2) = dataObjs(1).YData;

dataSps = nan(numel(dataObjs(2).XData),2);
dataSps(:,1)  = dataObjs(2).XData;
dataSps(:,2) = dataObjs(2).YData;

close all

psdStp = dataStp(:,1);
plTCStp = dataStp(:,2);
psdSps = dataSps(:,1);
plTCSps = dataSps(:,2);
%%
Util.log('Scatter:')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
plot(psdStp, plTCStp,'o','MarkerFaceColor','m','MarkerEdgeColor','m')
plot(psdSps, plTCSps,'o','MarkerFaceColor','b','MarkerEdgeColor','b')

xlabel('TC synaspe PSD size(nm)')
ylabel('Pathlength of TC axon (nm)')

xLimits = [0,1200];
xTicks = 0:200:1200;
ax.XAxis.TickValues = xTicks;
ax.XAxis.Limits = xLimits;
ax.LineWidth = 4;
set(gca,'FontSize',18)
Util.setPlotDefault(gca,'','');
% daspect([1,100,1])
outfile = fullfile(outDir,'outData/figures','fig3',config.version,'synapse_size_tc_psd.eps');
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all

%%
Util.log('box plot')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
x1 = psdStp(:);
x2 = psdSps(:);

x = [x1; x2];
curIdx1 = 1:numel(x1);
curIdx2 = curIdx1(end)+(1:numel(x2));

g = cell(numel(x),1);
g(curIdx1) = {'StP'}; % 1st box
g(curIdx2) = {'SpS'}; % 2nd box

pVal = ranksum(x(curIdx1),x(curIdx2));

rng(0)
colors = cat(1,[1,0,1],... % stp
               [0,0,1]); % sps

startS = 1; widthS = 2;
[pos, posS] = Util.getBoxPlotPos(2, startS);
scatter(widthS.*rand(numel(curIdx1),1)+posS(1), x(curIdx1),...
    100,colors(1,1:3),'filled','o');
scatter(widthS.*rand(numel(curIdx2),1)+posS(2), x(curIdx2),...
    100,colors(2,1:3),'filled','o');

h=boxplot(x,g,'Colors','k','Widths',2,'Positions',pos, 'Symbol','r+');
set(h,{'linew'},{4});
sprintf('p-value %f, #StP = %d, #SpS = %d', pVal,numel(x1),numel(x2))
ylabel('TC synaspe PSD size(nm)')

yLimits = [0,1200];
yTicks = 0:200:1200;
yMinorTicks = 0:100:1200;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.Limits = yLimits;
ax.XAxis.Color = 'none';
ax.LineWidth = 4;
set(gca,'FontSize',24)
Util.setPlotDefault(gca,'','');
daspect([1,100,1])
outfile = fullfile(outDir,'outData/figures','fig3',config.version,'synapse_size_psd.eps');
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all