% figure 6 rastor plots
setConfig;
names = {'fig_ij_C03', 'fig_ij_C08','fig_ij_C22','fig_ij_C27'};

for i=1:numel(names)
uiopen(fullfile(config.outDir,'outData/figures/fig6/functional_data/', [names{i} '.fig']),1)
outfile = fullfile(config.outDir,'outData/figures/fig6/functional_data/', [names{i} '.eps']);

Util.setPlotDefault(gca,'','');
ax = gca;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = -0.5:0.1:1.5;
ax.XAxis.TickValues = -0.5:0.5:1.5;
ax.XAxis.Limits = [-0.5, 1.5];

ax.YAxis.TickValues = 0:10:70;
ax.YAxis.Limits = [0, 70];
ax.LineWidth = 2;
ax.Title.String = '';
set(ax,'FontSize',30)

Util.log('make data more visible')
fig = gcf;
axObjs = fig.Children;
dataObjs = axObjs.Children;
for i=1:numel(dataObjs)
    x = dataObjs(i).XData;
    y = dataObjs(i).YData;
    plot(x,y,'-k','LineWidth',2)
end

export_fig(outfile,'-q101', '-nocrop','-transparent')
close all
end

