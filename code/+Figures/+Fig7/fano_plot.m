% scatter plot of FanoFactor data with soma syn, cortical depth, primary dendrite
setConfig;
outDir = config.outDir;

outDirPdf = fullfile(config.outDir,'outData/figures/fig6',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

%%
Util.log('Read ff data')
[fano_fac, so, ct, c_depth, num_d, idxStp, idxSps] = Figures.Fig6.loadFunctionalData();

cellCount = 59;
colorStp = 'm';
colorSps = 'b';

Util.log('Now plotting...')
%% 
%{
ss_count = 0;
sp_count = 0;
for i = 1:59
    if ~isnan(so(i))
        if ct(i)
            sp_count = sp_count + 1;
            sp_so(sp_count) = so(i);
            sp_spont(sp_count) = fano_fac.spont(i);
            sp_on(sp_count) = fano_fac.on(i);
            sp_off(sp_count) = fano_fac.off(i);
        else
            ss_count = ss_count + 1;
            ss_so(ss_count) = so(i);
            ss_spont(ss_count) = fano_fac.spont(i);
            ss_on(ss_count) = fano_fac.on(i);
            ss_off(ss_count) = fano_fac.off(i);
        end
    end
end
%}
%% ff with soma syn
sprintf('FF vs soma synapses')
xlimit = [0, 2.5];
ylimit = [0,100];
yTicks = 0:50:100;
xTicks = 0:1:2;
yMinorTicks = 10:10:100;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
doThis(fano_fac.on,so,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (on)', '# soma synapses',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
doThis(fano_fac.off,so,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (off)', '',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';

ax = subplot(1,3,3);
doThis(fano_fac.spont,so,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (spont)', '',ax, xTicks, yTicks, yMinorTicks);
ax.YAxis.Label.String = '';
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';
export_fig(fullfile(outDirPdf,'ff_vs_soma_syn.eps'),'-q101', '-nocrop', '-transparent');
close all

%% ff with cortical depth
sprintf('FF vs cortical depth')
xlimit = [0, 2.5];
ylimit = [0,400];
yTicks = 0:200:400;
xTicks = 0:1:2;
yMinorTicks = 0:50:400;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
doThis(fano_fac.on,c_depth,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit, ...
    'FF (on)', 'Cortical depth(\mum)',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
doThis(fano_fac.off,c_depth,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (off)', '',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';

ax = subplot(1,3,3);
doThis(fano_fac.spont,c_depth,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (spont)', '',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';
export_fig(fullfile(outDirPdf,'ff_vs_depth.eps'),'-q101', '-nocrop', '-transparent');
close all

%% ff with num_d
fontSize = 24;
lineWidth = 4;
yTicks = 0:4:8;
xTicks = 0:1:2;
yMinorTicks = 0:1:8;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
hold on
x = fano_fac.on;
y = num_d;
s=scatter(x(idxStp),y(idxStp),'o','filled');
thisColor = colorStp;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor;
s=scatter(x(idxSps),y(idxSps),'o','filled');
thisColor = colorSps;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor; 
xlabel('FF (on)')
ylabel('# primary dendrites')
xlim([0, 2.5])
ylim([0,8])
hold off;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
hold on
x = fano_fac.off;
y = num_d;
s=scatter(x(idxStp),y(idxStp),'o','filled');
thisColor = colorStp;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor;
s=scatter(x(idxSps),y(idxSps),'o','filled');
thisColor = colorSps;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor; 
xlabel('FF (off)')
ylabel('')
xlim([0, 2.5])
ylim([0,8])
hold off;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.TickLabels = '';
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,3);
hold on
x = fano_fac.spont;
y = num_d;
s=scatter(x(idxStp),y(idxStp),'o','filled');
thisColor = colorStp;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor;
s=scatter(x(idxSps),y(idxSps),'o','filled');
thisColor = colorSps;
s.LineWidth = 0.6;
s.MarkerEdgeColor = thisColor;
s.MarkerFaceColor = thisColor; 
xlabel('FF (spont)')
ylabel('')
xlim([0, 2.5])
ylim([0,8])
hold off;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = yMinorTicks;
ax.YAxis.TickValues = yTicks;
ax.YAxis.TickLabels = '';
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);
export_fig(fullfile(outDirPdf,'ff_vs_numd.eps'),'-q101', '-nocrop', '-transparent');
close all

%%
function doThis(x,y,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit, xname, yname,...
            ax, xTicks, yTicks, yMinorTicks)
    hold on
    s=scatter(x(idxStp),y(idxStp),'o','filled');
    thisColor = colorStp;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor;
    s=scatter(x(idxSps),y(idxSps),'o','filled');
    thisColor = colorSps;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor; 
    % % corr coef
    [rStp,pStp] = corr(x(idxStp)',y(idxStp)','rows','complete');
    [rSps, pSps] = corr(x(idxSps)',y(idxSps)','rows','complete');
    %line fit colors reverse
    h=lsline;
    h(1).Color = colorSps;
    h(2).Color = colorStp;
    %{
    lm = fitlm(x(idxStp),y(idxStp));
    pStp = lm.Coefficients.pValue(2);
    mStp = lm.Coefficients.Estimate(2);
    lm = fitlm(x(idxSps),y(idxSps));
    pSps = lm.Coefficients.pValue(2);
    mSps = lm.Coefficients.Estimate(2);
%     title(sprintf('slope ,%.03f, p %.02f \n slope %.03f, p %.02f',mSps,pSps,mStp,pStp),'FontSize',14)
    %}
    legend off
    xlabel(xname)
    ylabel(yname)
    xlim(xlimit)
    ylim(ylimit)
    Util.log([xname ' rStp:' num2str(rStp) ' p:' num2str(pStp)])
    Util.log([xname ' rSps:' num2str(rSps) ' p:' num2str(pSps)])
    hold off

    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = yMinorTicks;
    ax.YAxis.TickValues = yTicks;
    ax.XAxis.TickValues = xTicks;
    ax.LineWidth = 4;
    ax.FontSize = 24;
end
