function [excId, excClass] = createMapping(cellIDs,skelCffi)
% create a mapping from recodred cells 1 to 59 to stp/sps from cffi file
setConfig;
if nargin<2
    Util.log('Loading cffi skel:')
    skelCffi = skeleton(config.cffi);
end
[comments, treeIdx] = skelCffi.getAllComments;

excId = zeros(numel(cellIDs),1);
excClass = nan(numel(cellIDs),1); % true if stp
for i=1:numel(cellIDs)
    curId = cellIDs(i);
    curComment = ['cell' sprintf('%d',curId) '$'];
    curCommentLoc = find(cellfun(@(x) ~isempty(regexp(x,curComment,'once')),comments));
    if ~isempty(curCommentLoc)
        curTreeName = skelCffi.names{treeIdx(curCommentLoc)};
        if ~isempty(regexp(curTreeName, 'cell [sp]', 'once'))
            id = regexp(curTreeName,['cell [ps]' '(?<id>\w+)'],'names');
            excId(i) = str2double(id.id);
            switch curTreeName(6) % s or p
                case 'p'
                    excClass(i) = true; % stp
                case 's'
                    excClass(i) = false; %sps
            end
            clear curComment id curTree cutTreeName
        else
            error('Not stp or sps tree')
        end
    else
        warning(sprintf('No comment %s found!',curComment))
    end
end
end
