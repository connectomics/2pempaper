% reproduce VPM PSTHs from Yu, Svoboda 2016, 2019
setConfig
somaColors = Util.getSomaColors(true);
%outDirPdf = 'I:\tmpscratch\sahilloo\barrel\outData\figures\figure7\';
outDirPdf = fullfile(outDir, 'outData','figures','figure7');

%% VPM: Latency 2.6 ms, 1 bin = 1ms, -5 to 25 ms range
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,200,200,450,450,190,190,100,100,90,90,20,20,40,40,0,0,0,20,20,0,0,30,0];
curWeight = 1;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor','c','FaceColor','c','LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = []; % -5:5:25;

ax.YAxis.Limits = [0,500];
ax.YAxis.TickValues = [0,500];
ax.YAxis.TickLabels = [];

ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20*3.33,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_tc'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop', '-transparent')
close all

%% W-BIN (copy of TC), Latency 4.3 ms, Spikerate 1/3rd
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
%curPSTH = [0,0,0,0,0,...
%    0,0,0,0,200,200,450,450,190,190,100,100,90,90,20,20,40,40,0,0,0,20,20,0,0];
% add baseline activity of 25*3
curPSTH = [0,0,0,0,0,...
    71,63,75,200,250,450,450,190,190,100,100,90,90,80,85,80,60,75,61,75,65,70,75,65];
curWeight = 1/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(1,:),'FaceColor',somaColors(1,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,150];
ax.YAxis.TickValues = [0,150];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_wb_copy_of_tc'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% W-BIN (FFI), Latency 4.3 ms, Spikerate 1/3rd
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    71,63,75,200,250,450,200,150,50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
curWeight = 1/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(1,:),'FaceColor',somaColors(1,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,150];
ax.YAxis.TickValues = [0,150];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
daspect([1,20,1])
plot([5,5],ylim,'--k')
outfile = fullfile(outDirPdf, sprintf('PSTH_wb_ffi'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% L-BIN (copy of TC) Latency 4.3 ms,
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
%curPSTH = [0,0,0,0,0,...
%    0,0,0,0,200,200,450,450,190,190,100,100,90,90,20,20,40,40,0,0,0,20,20,0,0];
% add baseline activity of 25*3
curPSTH = [0,0,0,0,0,...
    71,63,75,200,250,450,450,190,190,100,100,90,90,80,85,80,60,75,61,75,65,70,75,65];
curWeight = 1/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(2,:),'FaceColor',somaColors(2,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,150];
ax.YAxis.TickValues = [0,150];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_lb_copy_of_tc'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% Stp (copy of TC) Latency 7.8 ms,
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,200,200,450,450,190,190,100,100,90,90,20,20,40,40,0,0,0];
curWeight = 1/3/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end-1,:),'FaceColor',somaColors(end-1,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_stp_copy_of_tc'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% Stp (Inh by WB) Latency 7.8 ms,
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,40,40,200,200,250,250,190,190,100,100,90,90,20,20,40,40,0,0,0];
curWeight = 1/3/3/5;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end-1,:),'FaceColor',somaColors(end-1,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_stp_inh_by_wbin'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all


%% Stp (Disinh) Latency 7.8 ms,
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,0,0,150,200,450,250,140,20,10,10,0,0,0,0,0,0,0,0];
curWeight = 1/3/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end-1,:),'FaceColor',somaColors(end-1,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_stp_disinh'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% Sps (copy of TC) Latency 7.8 ms,
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,200,200,450,450,190,190,100,100,90,90,20,20,40,40,0,0,0];
curWeight = 1/3/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end,:),'FaceColor',somaColors(end,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_sps_copy_of_tc'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% Sps (FFI single) Latency 7.8 ms
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,200,200,450,200,0,0,0,0,0,0,0,0,0,0,0,0,0];
curWeight = 1/3/3;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end,:),'FaceColor',somaColors(end,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_sps_ffi_single'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all

%% Sps (FFI double both wb and lb) Latency 7.8 ms
fig = figure;
fig.Color = 'none';
ax = axes(fig);
hold on
curPSTH = [0,0,0,0,0,...
    0,0,0,0,0,0,0,0,200,200,450,200,0,0,0,0,0,0,0,0,0,0,0,0,0];
curWeight = 1/3/5;
bar(curPSTH*curWeight,'BarWidth', 1,'EdgeColor',somaColors(end,:),'FaceColor',somaColors(end,:),'LineWidth',2);
Util.setPlotDefault(gca);

ax.XAxis.Limits = [0,30];
ax.XAxis.TickValues = 0:5:30;
ax.XAxis.TickLabels = -5:5:25;
ax.XAxis.TickLabels = [];

ax.YAxis.Limits = [0,50];
ax.YAxis.TickValues = [0,50];
ax.YAxis.TickLabels = [];
ax.TickLength = [0.025,0.025];
set(ax,'LineWidth',2)
plot([5,5],ylim,'--k')
daspect([1,20/3,1])
outfile = fullfile(outDirPdf, sprintf('PSTH_sps_ffi_double'));
export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
close all
