function [fano_factor, so, ct, c_depth, num_d, idxStp, idxSps, cffiIds, mtType, somaCoords] = loadFunctionalData() 
setConfig
% from /data/2019_01_30_2p_summary_v3.xlsx
% load(fullfile(outDir,'data','fano_fac.mat')); % latest data from Bonn

% soma = [59 52 59 50 76 77 54 63 57 54 ...
%     33 72 49 73 61 NaN NaN NaN 38 55 ...
%     64 54 NaN NaN NaN NaN 36 41 NaN NaN ...
%     55 45 80 74 65 NaN NaN 64 77 65 ...
%     62 81 NaN NaN NaN NaN 72 50 44 NaN ...
%     49 NaN 53 47 80 56 41 48 60];

% ct = [1 1 1 1 1 1 1 1 1 0 ...
%     0 1 0 1 1 1 0 0 0 0 ...
%     1 0 1 0 0 0 0 0 0 0 ...
%     1 0 1 1 1 1 0 1 1 1 ...
%     0 1 0 0 0 0 1 0 0 0 ...
%     0 0 0 0 1 1 0 0 0];

num_d = [6 4 5 4 6 5 7 4 5 5 ...
        5 5 5 4 5 6 NaN NaN 6 5 ...
        5 4 4 NaN NaN NaN 5 5 NaN NaN ...
        6 5 6 4 4 4 NaN 5 4 5 ...
        5 7 NaN NaN NaN NaN 5 6 6 4 ...
        6 NaN 5 6 5 4 4 4 4];

xlsfile = config.functionalExcel;
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);

stpIds = vertcat(xlTable.xl1{2:28});
spsIds = vertcat(xlTable.xl1{31:49});

cellCount = 59;

ct = nan(1,cellCount);
ct(stpIds) = 1;
ct(spsIds) = 0;
idxStp = find(ct==1);
idxSps = find(ct==0);

fano_factor = struct('pre',nan(1,cellCount),'on',nan(1,cellCount),'off',nan(1,cellCount),'spont',nan(1,cellCount));
fano_factor.pre([stpIds;spsIds]) = vertcat(xlTable.xl2{2:28}, xlTable.xl2{31:49});
fano_factor.on([stpIds;spsIds]) = vertcat(xlTable.xl3{2:28}, xlTable.xl3{31:49});
fano_factor.off([stpIds;spsIds]) = vertcat(xlTable.xl4{2:28}, xlTable.xl4{31:49});
fano_factor.spont([stpIds;spsIds]) = vertcat(xlTable.xl5{2:28}, xlTable.xl5{31:49});

c_depth = nan(1,cellCount);
c_depth([stpIds;spsIds]) =  cellfun(@(x) str2double(x(end-2:end)),{xlTable.xl8{2:28}, xlTable.xl8{31:49}});

so = nan(1,cellCount);
so([stpIds;spsIds]) = vertcat(xlTable.xl10{2:28}, xlTable.xl10{31:49});

cffiIds = nan(1,cellCount);
cffiIds([stpIds; spsIds]) = vertcat(xlTable.xl19{2:28}, xlTable.xl19{31:49});
% 
% % excel table has spike rates
% fano_factor.on = fano_factor.on + fano_factor.pre;
% fano_factor.off = fano_factor.off + fano_factor.pre;

% MT type
mtType = nan(1,cellCount);
mtType([stpIds; spsIds]) = vertcat(xlTable.xl9{2:28}, xlTable.xl9{31:49});

% soma coords EM
temp = cell(1,cellCount);
temp([stpIds; spsIds]) = vertcat(xlTable.xl7(2:28), xlTable.xl7(31:49));
somaCoords = nan(cellCount,3);
for idx = 1:numel(temp) 
    curCell = temp{idx};
    if ~isempty(curCell)
        
        coord = regexp( curCell,...
            '(?<x>\d+)_(?<y>\d+)_(?<z>\d+)','names');
        coord = structfun(@str2double,coord);
        coord = reshape(coord,1,'');
    else
        coord = [nan, nan, nan];
    end
    somaCoords(idx,:) = coord;
end


end
