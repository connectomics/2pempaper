% read: https://www.theanalysisfactor.com/factor-analysis-1-introduction/
% Factor analysis to explain observed AP wrt cell type of cell depth

% load('E:\tmpscratch\sahilloo\barrel\data\loadedFunctionalData.mat')
[fano_factor, so, ct, c_depth, num_d, idxStp, idxSps, cffiIds, mtType, somaCoords] = Figures.Fig6.loadFunctionalData;

%% factoran
var_names = {'FF_on', 'FF_spont', 'cell_type', 'cell_depth'};
X = [fano_factor.on(:), fano_factor.spont(:), ct(:), c_depth(:)]; % n x d observable variables
X = X(all(~isnan(X),2),:);

numFactors = 1; % m: choice of possible latent factors

% lambda or w = d x m: Factor loading (weights that relate observables to factors or
% correlations between factors and observables)
[Lambda, Psi, T, stats, F] = factoran(X,numFactors,...
    'xtype','data',...
    'rotate', 'none',...
    'normalize',false,...
    'scores', 'regression');

sprintf('Factor loadings or weights with %d factor(s) for observed variables', numFactors)
sprintf('%s ',var_names{:})
disp(Lambda)

%% multivariate regression
% determine whether the mean of a variable differs significantly among groups. 
% Often there are multiple response variables, and you are interested in determining
% whether the entire set of means is different from one group to the next.

% The first output, d, is an estimate of the dimension of the group means. 
% If the means were all the same, the dimension would be 0, indicating that 
% the means are at the same point. If the means differed but fell along a line,
% the dimension would be 1. In the example the dimension is 2, indicating that the
% group means fall in a plane but not along a line. This is the largest possible dimension
% for the means of three groups
X = [fano_factor.on(:), fano_factor.spont(:), c_depth(:)];
gplotmatrix(X,[], ct(:),[],'+x')

[d,p,stats] = manova1(X,ct(:))

% canonical variables
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
figure
gscatter(c2,c1,ct(:),[],'os')

stats.gmdist