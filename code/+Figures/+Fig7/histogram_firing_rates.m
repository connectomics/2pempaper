% histogram of firing rates
setConfig;
outDir = config.outDir;
outDirPdf = fullfile(config.outDir,'outData/figures/fig6/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
[~,~,xl] = xlsread(fullfile(config.outDir,'data/2p_response.xlsx')); 

stp = cell2mat(xl(2:23,2:5));
sps = cell2mat(xl(26:43,2:5));

Util.log('Now plotting stp on and off rate scatter')
figure
hold on
scatter(stp(:,2), stp(:,3),70,'x',...
    'MarkerFaceColor','m', 'MarkerEdgeColor','m','LineWidth',2);
scatter(sps(:,2), sps(:,3),70,'x',...
    'MarkerFaceColor','b', 'MarkerEdgeColor','b','LineWidth',2);
axis([0 4 0 4]);
set(gca,'xtick',0:2:4,'ytick',0:2:4);
set(gca,'TickDir','out');
xlabel('spiking rate (on) Hz');
ylabel('spiking rate (off) Hz');
outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'panel_h'));
export_fig(outfile,'-q101','-transparent');
close all


Util.log('Now plotting density curves over histogram...')
figure
subplot(1,2,1)
hold on
xbins = 0:0.4:4;
nbins = numel(xbins);
binWidth = 0.4;
% stp_on = histogram(stp(:,2),xbins,'DisplayStyle','stairs','FaceColor','none','EdgeColor','m','LineWidth',2);
% sps_on = histogram(sps(:,2),xbins,'DisplayStyle','stairs','FaceColor','none','EdgeColor','b','LineWidth',2);
% h_stp = histfit(stp(:,2),numel(xbins));
% h_stp(1).FaceColor ='none';
% h_stp(1).EdgeColor ='m';
% h_stp(1).LineWidth = 2;
% % h_stp(1).Style = 'hist';
% h_stp(2).Color ='m';
% h_stp(2).LineWidth = 2;
% h_sps = histfit(sps(:,2),numel(xbins));
% h_sps(1).FaceColor ='none';
% h_sps(1).EdgeColor ='b';
% h_sps(1).LineWidth = 2;
% % h_sps(1).Style = 'hist';
% h_sps(2).Color ='b';
% h_sps(2).LineWidth = 2;
stp_on = hist(stp(:,2),xbins);
sps_on = hist(sps(:,2),xbins);
bar(stp_on,'FaceColor','none','EdgeColor','m','LineWidth',2,'BarWidth',1);
bar(sps_on,'FaceColor','none','EdgeColor','b','LineWidth',2,'BarWidth',1);
% axis([0 4 0 10]);
% set(gca,'xtick',0:1:4,'ytick',0:5:10);
x = stp_on;
area = sum(x) * binWidth;
xx = linspace(-3,15);
plot(xx,area*normpdf(xx,mean(x),std(x)),'m-')
f = ksdensity(x,xx);
plot(xx,area*f,'m--')
x = sps_on;
area = sum(x) * binWidth;
xx = linspace(-3,15);
plot(xx,area*normpdf(xx,mean(x),std(x)),'b-')
f = ksdensity(x,xx);
plot(xx,area*f,'b--')

hold off
legend({'stp-on','sps-on','normpdf','kdensity'})
set(gca,'TickDir','out');
title('on');
hold off

subplot(1,2,2)
hold on
% stp_off = histogram(stp(:,3),xbins,'DisplayStyle','stairs','FaceColor','none','EdgeColor','m','LineWidth',2);
% sps_off = histogram(sps(:,3),xbins,'DisplayStyle','stairs','FaceColor','none','EdgeColor','b','LineWidth',2);
% barh(stp_off,'FaceColor','none','EdgeColor','m','LineWidth',2);
% barh(sps_off,'FaceColor','none','EdgeColor','b','LineWidth',2);
% h_stp = histfit(stp(:,3),numel(xbins));
% h_stp(1).FaceColor ='none';
% h_stp(1).EdgeColor ='m';
% h_stp(1).LineWidth = 2;
% % h_stp(1).style = 'hist';
% h_stp(2).Color ='m';
% h_stp(2).LineWidth = 2;
% h_sps = histfit(sps(:,3),numel(xbins));
% h_sps(1).FaceColor ='none';
% h_sps(1).EdgeColor ='b';
% h_sps(1).LineWidth = 2;
% % h_sps(1).Style = 'hist';
% h_sps(2).Color ='b';
% h_sps(2).LineWidth = 2;
stp_off = hist(stp(:,3),xbins);
sps_off = hist(sps(:,3),xbins);
bar(stp_off,'FaceColor','none','EdgeColor','m','LineWidth',2,'BarWidth',1);
bar(sps_off,'FaceColor','none','EdgeColor','b','LineWidth',2,'BarWidth',1);
% axis([0 4 0 10]);
% set(gca,'xtick',0:1:4,'ytick',0:5:10);
x = stp_off;
area = sum(x) * binWidth;
xx = linspace(-3,15);
plot(xx,area*normpdf(xx,mean(x),std(x)),'m-')
f = ksdensity(x,xx);
plot(xx,area*f,'m--')
x = sps_off;
area = sum(x) * binWidth;
xx = linspace(-3,15);
plot(xx,area*normpdf(xx,mean(x),std(x)),'b-')
f = ksdensity(x,xx);
plot(xx,area*f,'b--')

hold off
legend({'stp-off','sps-off','normpdf','kdensity'})
set(gca,'TickDir','out');
title('off');

outfile = fullfile(outDirPdf, ...
    sprintf('%s.eps', 'panel_h_hist'));
export_fig(outfile,'-q101','-transparent');
close all
