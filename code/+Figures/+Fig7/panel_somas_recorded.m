% panel e showing stp vs sps colored recorded LM wholecell somas
setConfig;
outDir = config.outDir;

somaColors = Util.getSomaColors;
synBallSize = 25; tubeSize = 2; dotSize= 3;
somaBallColorStp = somaColors{end-1}; %magenta
somaBallColorSps = somaColors{end}; %blue
somaBallSize = 25;

outDirPdf = fullfile(config.outDir,'outData/figures/fig6/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

Util.log('Read LM sps and stp soma coords...')
%xlsfile = fullfile(outDir,'data/2018_12_12_2p_summary_v2.xlsx');
xlsfile = config.functionalExcel;
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);

[fano_factor, so, ct, c_depth, num_d, idxStp, idxSps, cffiIds, mtType, somas] = Figures.Fig6.loadFunctionalData();

idx4 = mtType==4;
idx2 = mtType==2;

% for MT2
cellIdsMT2 = reshape(find(idx2),'',1);
typesMT2 = reshape(ct(idx2),'',1); % 1 or 0
somasMT2 = somas(idx2,:);

idxStpMT2 = reshape(typesMT2 == 1,'',1);
idxSpsMT2 = reshape(typesMT2 == 0, '', 1);

% transform MT2 to LM to MT4
m=load(fullfile(outDir,'barrelStateTformMT2_vOct.mat'),'tform');
tformMT2 = m.tform;
%{
% tilt correction
newNodes = somasMT2;
newNodes(:,2) = round(newNodes(:,2)-.449*newNodes(:,3));
%}
newNodes = somasMT2;
somasMT2_LM = TFM.applyTformToVertices(tformMT2, newNodes, 'forward'); %EM to LM dataset

scaleEM = [11.24,11.24,30];
scaleLM = [1172 1172 1000];
Util.log('point cloud');
points = somasMT2_LM;
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(points(idxStpMT2,:),'','',repelem(somaBallColorStp,size(points(idxStpMT2),1),1)); % m color
skel = skel.addNodesAsTrees(points(idxSpsMT2,:),'','',repelem(somaBallColorSps,size(points(idxSpsMT2),1),1)); % blue color
skel.write(fullfile(outDir,'outData/pointClouds/somasRecordedMT2_LM.nml'));

m=load(fullfile(outDir,'barrelStateTform_vOct.mat'),'tform');
tformMT4 = m.tform;
somasMT2InMT4 =  TFM.applyTformToVertices(tformMT4, somasMT2_LM, 'backward'); %LM to EM dataset

points = somasMT2InMT4;
names = arrayfun(@(x) ['cellId_' num2str(x)],cellIdsMT2,'uni',0);
skel = skeleton();
skel  = skel.setParams('YH_st126_MT4_updated_1708',scaleEM,[1 1 1]);
skel = skel.addNodesAsTrees(points(idxStpMT2,:),names(idxStpMT2),'',repelem(somaBallColorStp,size(points(idxStpMT2),1),1)); % m color
skel = skel.addNodesAsTrees(points(idxSpsMT2,:),names(idxSpsMT2),'',repelem(somaBallColorSps,size(points(idxSpsMT2),1),1)); % blue color
skel.write(fullfile(outDir,'outData/pointClouds/somasRecordedMT2_EM.nml'));

% for MT4
cellIdsMT4 = reshape(find(idx4),'',1);
somasMT4 = somas(idx4,:);
typesMT4 = reshape(ct(idx4),'',1);

idxStpMT4 = reshape(typesMT4 == 1,'',1);
idxSpsMT4 = reshape(typesMT4 == 0,'',1);

points =  somasMT4;
names = arrayfun(@(x) ['cellId_' num2str(x)],cellIdsMT4,'uni',0);
skel = skeleton();
skel  = skel.setParams('YH_st126_MT4_updated_1708',scaleEM,[1 1 1]);
skel = skel.addNodesAsTrees(points(idxStpMT4,:),names(idxStpMT4),'',repelem(somaBallColorStp,size(points(idxStpMT4),1),1)); % m color
skel = skel.addNodesAsTrees(points(idxSpsMT4,:),names(idxSpsMT4),'',repelem(somaBallColorSps,size(points(idxSpsMT4),1),1)); % blue color
skel.write(fullfile(outDir,'outData/pointClouds/somasRecordedMT4_EM.nml'));

% transform MT4 to LM
% tilt correction
newNodes = somasMT4;
newNodes(:,2) = round(newNodes(:,2)-.449*newNodes(:,3));
somasMT4_LM = TFM.applyTformToVertices(tformMT4, newNodes, 'forward'); 
points = somasMT4_LM;
skel = skeleton();
skel  = skel.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skel = skel.addNodesAsTrees(points(idxStpMT4,:),'','',repelem(somaBallColorStp,size(points(idxStpMT4),1),1)); % m color
skel = skel.addNodesAsTrees(points(idxSpsMT4,:),'','',repelem(somaBallColorSps,size(points(idxSpsMT4),1),1)); % blue color
skel.write(fullfile(outDir,'outData/pointClouds/somasRecordedMT4_LM.nml'));

allSomas = vertcat(somasMT4,somasMT2InMT4);
allTypes = vertcat(typesMT4, typesMT2);
allIds = vertcat(cellIdsMT4, cellIdsMT2);

% classify based on sps stp
idxStp = allTypes == 1;
idxSps = allTypes == 0;
somasStp = allSomas(idxStp,:);
somasSps = allSomas(idxSps,:);

%scale all spheres
thisSkel = skelPC;
thisSkel.scale = [11.24, 11.24, 30];
scale = thisSkel.scale./1000;
somaLocsStp = thisSkel.setScale(somasStp,scale);
somaLocsSps = thisSkel.setScale(somasSps,scale);
nodesPC = thisSkel.setScale(nodesPC,scale);

Util.log('Now plotting figure....');
bbox = [-8000,35000;-8000,35000;-6000,14000];
bbox = bsxfun(@times, bbox, [11.24; 11.24; 30]./1000);
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;
objectPC = ...
            scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
            ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
            'MarkerFaceColor', colorPC(1:3));
objectSomaStp = ... 
             scatter3(somaLocsStp(:,1),somaLocsStp(:,2),somaLocsStp(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColorStp(1:3), ...
            'MarkerFaceColor', somaBallColorStp(1:3));
objectSomaSps = ...
             scatter3(somaLocsSps(:,1),somaLocsSps(:,2),somaLocsSps(:,3)...
            ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColorSps(1:3), ...
            'MarkerFaceColor', somaBallColorSps(1:3));
%yz
Util.setView(5,true)
outfile = fullfile(outDirPdf,['panel_somas_recorded.eps']);
export_fig(outfile,'-q101', '-nocrop', '-transparent');
Util.log('Saving file %s.', outfile);
close(f);
Util.save(fullfile(outDirPdf,'somasRecordedData.mat'), somasMT2, somasMT4, somasMT2_LM, somasMT4_LM)
