% scatter plot of FanoFactor data with soma syn, cortical depth, primary dendrite
setConfig;
outDir = config.outDir;

outDirPdf = fullfile(config.outDir,'outData/figures/fig6',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

%%
Util.log('Read ff data')
[fano_fac, so, ct, c_depth] = Figures.Fig6.loadFunctionalData(outDir); % sorted based on cellID % total 59

pathDir = fullfile(outDir,'data','wholecells','skels_recording_data');
nmls = dir(fullfile(pathDir,'*.nml'));
%skels = arrayfun(@(x) skeleton(fullfile(pathDir,x.name)), nmls,'uni',0);
dataIE = [];
cellIDs = [];
for i=1:numel(skels)
    thisSkel = skels{i};
    % extract cellID from skeleton name
    id = regexp(thisSkel.filename,'cell(?<id>\d+)','names');
    cellIDs(i) = str2double(id.id);
end

% keep only cellIDs found
fano_fac.on = fano_fac.on(cellIDs);
fano_fac.off = fano_fac.off(cellIDs);
fano_fac.spont = fano_fac.spont(cellIDs);
ct = ct(cellIDs);
so = so(cellIDs);

assert(numel(so) == numel(skels))
for i=1:numel(skels)
    thisSkel = skels{i};
    dataIE(i) = getStructuralDataForSkel(thisSkel, so(i));
end

% colorcode = ['b' 'm'];
cellCount = 59;
colorStp = 'm';
colorSps = 'b';
idxStp = find(ct); %magenta
idxSps = find(~ct); %blue

Util.log('Now plotting...')
%% ff with soma syn
xlimit = [0, 2.5];
ylimit = [0,1.5];
yTicks = 0:0.5:1.5;
xTicks = 0:1:2;
yMinorTicks = 0:0.1:1.5;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
doThis(fano_fac.on,dataIE,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (on)', 'Structural i/e',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
doThis(fano_fac.off,dataIE,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (off)', '',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';

ax = subplot(1,3,3);
doThis(fano_fac.spont,dataIE,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit,...
    'FF (spont)', '',ax, xTicks, yTicks, yMinorTicks);
ax.YAxis.Label.String = '';
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';
export_fig(fullfile(outDirPdf,'ff_vs_i_e.eps'),'-q101', '-nocrop', '-transparent');
close all

function doThis(x,y,idxStp, idxSps, colorStp, colorSps, xlimit, ylimit, xname, yname,...
            ax, xTicks, yTicks, yMinorTicks)
    hold on
    s=scatter(x(idxStp),y(idxStp),'o','filled');
    thisColor = colorStp;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor;
    s=scatter(x(idxSps),y(idxSps),'o','filled');
    thisColor = colorSps;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor; 
    % % corr coef
    [rStp,pStp] = corr(x(idxStp)',y(idxStp)','rows','complete');
    [rSps, pSps] = corr(x(idxSps)',y(idxSps)','rows','complete');
    %line fit colors reverse
    h=lsline;
    h(1).Color = colorSps;
    h(2).Color = colorStp;
    lm = fitlm(x(idxStp),y(idxStp));
    pStp = lm.Coefficients.pValue(2);
    mStp = lm.Coefficients.Estimate(2);
    lm = fitlm(x(idxSps),y(idxSps));
    pSps = lm.Coefficients.pValue(2);
    mSps = lm.Coefficients.Estimate(2);
%     title(sprintf('slope ,%.03f, p %.02f \n slope %.03f, p %.02f',mSps,pSps,mStp,pStp),'FontSize',14)
    legend off
    xlabel(xname)
    ylabel(yname)
    xlim(xlimit)
    ylim(ylimit)
    Util.log([xname ' rStp:' num2str(rStp) ' p:' num2str(pStp)])
    Util.log([xname ' rSps:' num2str(rStp) ' p:' num2str(pSps)])
    hold off

    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = yMinorTicks;
    ax.YAxis.TickValues = yTicks;
    ax.XAxis.TickValues = xTicks;
    ax.LineWidth = 4;
    ax.FontSize = 24;
end

function [dataIE, shaft, spine] = getStructuralDataForSkel(skel, so)
    % extract shaft and spine synapses
    idxDend = skel.getTreeWithName('cell','partial');
    
    spineIdx = skel.getNodesWithComment('sp_root',idxDend,'partial'); % comments with spines
    
    idxShaft = skel.getTreeWithName({'axon','cell'},'partial',true); % trees with shaft and soma syn nodes
    dataIE = (numel(idxShaft)+so) / numel(spineIdx);
end
