% scatter plot of FanoFactor data with soma syn, cortical depth, primary dendrite
setConfig;
load(config.connmat,'somaMapINStp','somaMapINSps')

skelCffi = skeleton(config.cffi);
skelCellType = skeleton(config.cellTypesL4Fixed);
skelCellType = skelCellType.extractLargestTree;

outDirPdf = fullfile(config.outDir,'outData/figures/fig6',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end
restrictExc = true;
restrictINFull = true;
onlyNmlsFlag = false; % restrict to those that have nmls in folder
%%
Util.log('Read ff data')
[fano_fac, so, ct, c_depth, num_d,~,~, cffiIds] = Figures.Fig6.loadFunctionalData();

pathDir = fullfile(outDir,'data','wholecells','skels_recording_data');
nmls = dir(fullfile(pathDir,'*.nml'));
skels = arrayfun(@(x) skeleton(fullfile(pathDir,x.name)), nmls,'uni',0);
dataIE = [];
cellIDs = [];
for i=1:numel(skels)
    thisSkel = skels{i};
    % extract cellID from skeleton name
    id = regexp(thisSkel.filename,'cell(?<id>\d+)','names');
    cellIDs(i) = str2double(id.id);
end

% keep only cellIDs found for LM tracings
fano_fac.on = fano_fac.on(cellIDs);
fano_fac.off = fano_fac.off(cellIDs);
fano_fac.spont = fano_fac.spont(cellIDs);
ctLM = ct(cellIDs);
soLM = so(cellIDs);

assert(numel(soLM) == numel(skels))
for i=1:numel(skels)
    thisSkel = skels{i};
    dataIE(i) = getStructuralDataForSkel(thisSkel, soLM(i));
end

cellCount = numel(so);
colorStp = 'm';
colorSps = 'b';

%% ff with soma syn
Util.log('Now plotting...')
xlimit = [0, 2.5];
ylimit = [0,1.5];
yTicks = 0:0.5:1.5;
xTicks = 0:1:2;
yMinorTicks = 0:0.1:1.5;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
doThis(fano_fac.on,dataIE,ctLM, colorStp, colorSps, xlimit, ylimit,...
    'FF (on)', 'Structural i/e',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
doThis(fano_fac.off,dataIE,ctLM, colorStp, colorSps, xlimit, ylimit,...
    'FF (off)', '',ax, xTicks, yTicks, yMinorTicks);
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';

ax = subplot(1,3,3);
doThis(fano_fac.spont,dataIE,ctLM, colorStp, colorSps, xlimit, ylimit,...
    'FF (spont)', '',ax, xTicks, yTicks, yMinorTicks);
ax.YAxis.Label.String = '';
Util.setPlotDefault(gca,false,false);
ax.YAxis.TickLabels = '';
export_fig(fullfile(outDirPdf,'ff_vs_i_e.eps'),'-q101', '-nocrop', '-transparent');
close all

%% How many soma synapses have we found in connectome
% for analysis of soma syn coverage
if onlyNmlsFlag
    stpLMIds = cellIDs(ctLM==1);
    spsLMIds = cellIDs(ctLM==0);
    somaSynStp = soLM(ctLM==1);
    somaSynSps = soLM(ctLM==0);
else
    stpLMIds = find(cffiIds>0 & ct==1);
    spsLMIds = find(cffiIds>0 & ct==0);
    somaSynStp = so(stpLMIds);
    somaSynSps = so(spsLMIds);
end

Util.log('Do this only for the above cellIDs (data is messy)')
fullINs = [1,2,3,4,5,6,7,8,12,14,17,18,24,25,27,28,29,36,40];
if restrictINFull
   Util.log('restrict INs to fully reconstructed:')
   somaMapINStp = somaMapINStp(fullINs,:);
   somaMapINSps = somaMapINSps(fullINs,:);
end
% stp
if onlyNmlsFlag
    stpIds =  Figures.Fig6.createMapping(stpLMIds,skelCffi);
else
    stpIds = cffiIds(stpLMIds);
end

somaSynStpFound = sum(somaMapINStp(:,stpIds),1);
assert(numel(somaSynStpFound) == numel(somaSynStp))
IN = true(numel(stpIds),1);
if restrictExc
    Util.log('restrict Stp to those within barrel:')
    ids = stpIds;
    somaLocs = [];
    for i = 1:numel(ids)
        curId = ids(i);
        somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['p' num2str(curId,'%02d')] ,'exact');
    end
    [IN, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');
end
stpTable = table(reshape(stpIds(IN),'',1), reshape(stpLMIds(IN),'',1),....
    reshape(somaSynStp(IN),'',1), reshape(somaSynStpFound(IN),'',1),...
    'VariableNames',{'p', 'LM','somaSynStp','found'});

% sps
if onlyNmlsFlag
    spsIds =  Figures.Fig6.createMapping(spsLMIds,skelCffi);
else
    spsIds = cffiIds(spsLMIds);
end

somaSynSpsFound = sum(somaMapINSps(:,spsIds),1);
assert(numel(somaSynSpsFound) == numel(somaSynSps))
IN = true(numel(spsIds),1);
if restrictExc
    Util.log('restrict Sps to those within barrel:')
    ids = spsIds;
    somaLocs = [];
    for i = 1:numel(ids)
        curId = ids(i);
        somaLocs(i,:) = Util.getNodeCoordsWithComment(skelCellType, ['s' num2str(curId,'%02d')] ,'exact');
    end
    [IN, ~, ~] = Tform.insideOutsideBarrel(somaLocs,'EM');
end
spsTable = table(reshape(spsIds(IN),'',1), reshape(spsLMIds(IN),'',1),...
    reshape(somaSynSps(IN),'',1), reshape(somaSynSpsFound(IN),'',1),...
    'VariableNames',{'s', 'LM','somaSynStp','found'});

% output text
disp(stpTable)
idxKeep = stpTable.found>0;
d = stpTable{:,end}./stpTable{:,end-1};
sprintf('Stp soma syns found: %.02f +- %.02f, (n=%d)', nanmean(d), nanstd(d), sum(~isnan(d)))
sprintf('Stp soma syns found (>0): %.02f +- %.02f, (n=%d)', nanmean(d(idxKeep)), nanstd(d(idxKeep)), sum(~isnan(d(idxKeep))))

disp(spsTable)
idxKeep = spsTable.found>0;
d = spsTable{:,end}./spsTable{:,end-1};
sprintf('Sps soma syns found: %.02f +- %.02f, (n=%d)', nanmean(d), nanstd(d), sum(~isnan(d)))
sprintf('Sps soma syns found (>0): %.02f +- %.02f, (n=%d)', nanmean(d(idxKeep)), nanstd(d(idxKeep)), sum(~isnan(d(idxKeep))))

% # soma syn from INs to somas within barrel
[~,stpIN] = Conn.restrictToBarrel(somaMapINStp,'p',2);
[~,spsIN] = Conn.restrictToBarrel(somaMapINSps,'s',2);
sprintf('Stp Inside: %d \nSps Inside: %d \nIN soma syn to Stp Inside: %d \nIN soma syn to Sps Inside: %d ',...
            sum(stpIN), sum(spsIN), sum(sum(somaMapINStp(:,stpIN))), sum(sum(somaMapINSps(:,spsIN))))

%%
function doThis(x,y, ctLM, colorStp, colorSps, xlimit, ylimit, xname, yname,...
            ax, xTicks, yTicks, yMinorTicks)
    assert(numel(x) == numel(ctLM))
    idxStp = ctLM==1;
    idxSps = ctLM==0;
    hold on
    s=scatter(x(idxStp),y(idxStp),'o','filled');
    thisColor = colorStp;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor;
    s=scatter(x(idxSps),y(idxSps),'o','filled');
    thisColor = colorSps;
    s.LineWidth = 0.6;
    s.MarkerEdgeColor = thisColor;
    s.MarkerFaceColor = thisColor; 
    % % corr coef
    [rStp,pStp] = corr(x(idxStp)',y(idxStp)','rows','complete');
    [rSps, pSps] = corr(x(idxSps)',y(idxSps)','rows','complete');
    %line fit colors reverse
    h=lsline;
    h(1).Color = colorSps;
    h(2).Color = colorStp;
    lm = fitlm(x(idxStp),y(idxStp));
    pStp = lm.Coefficients.pValue(2);
    mStp = lm.Coefficients.Estimate(2);
    lm = fitlm(x(idxSps),y(idxSps));
    pSps = lm.Coefficients.pValue(2);
    mSps = lm.Coefficients.Estimate(2);
    sprintf('Stp: slope ,%.03f, p %.02f \nSps slope %.03f, p %.02f',mStp,pStp,mSps,pSps)
    legend off
    xlabel(xname)
    ylabel(yname)
    xlim(xlimit)
    ylim(ylimit)
    Util.log([xname ' rStp:' num2str(rStp) ' p:' num2str(pStp)])
    Util.log([xname ' rSps:' num2str(rSps) ' p:' num2str(pSps)])
    hold off

    ax.YAxis.MinorTick = 'on';
    ax.YAxis.MinorTickValues = yMinorTicks;
    ax.YAxis.TickValues = yTicks;
    ax.XAxis.TickValues = xTicks;
    ax.LineWidth = 4;
    ax.FontSize = 24;
end

function dataIE = getStructuralDataForSkel(skel, so)
    % extract shaft and spine synapses
    idxDend = skel.getTreeWithName('cell','partial');
    
    spineIdx = skel.getNodesWithComment('sp_root',idxDend,'partial'); % comments with spines
    
    idxShaft = skel.getTreeWithName({'axon','cell'},'partial',true); % trees with shaft and soma syn nodes
    dataIE = (numel(idxShaft)+so) / numel(spineIdx);
end
