% determine whether there are any differences between independent groups
% on more than one continuous dependent variable
% Null Hypothesis: 
% Samples come from populations with identical multivariate mean

% Data from Kay
m = load('E:\tmpscratch\sahilloo\barrel\data\onoff_manova_10_09_2020.mat');
% continous dependent variables
% data_?? mean AP number within 200ms around an on/off stimulus 
% depth_?? depths for each of the N cells of type stp/sps

ap = [m.data_sp, m.data_ss];
c_depth = [m.depth_sp, m.depth_ss];
X = [ap(:), c_depth(:)];

% group / independent variable
ct = [ones(1,numel(m.data_sp)), zeros(1,numel(m.data_ss))]; % 1-StP, 0-SpS

gplotmatrix(X,[], ct(:),[],'+x')
saveas(gcf, 'E:/tmpscratch/sahilloo/barrel/outData/figures/fig6/04_07_2019/manova.png')

% do one-way manova test
[d,p,stats] = manova1(X,ct(:))

% canonical variables instead of original variables
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
figure
gscatter(c2,c1,ct(:),[],'oo')

stats.gmdist

% d=1: At least one dependent var has different means for independent var

%% Matlab fit
% "R squared", proportion of the variance in the dependent variable that is
% predictable from the independent variable
% R2 coef. of determination between APs and cell_depth
mdl = fitlm(c_depth(:), ap(:))
mdl.plot
saveas(gcf, 'E:/tmpscratch/sahilloo/barrel/outData/figures/fig6/04_07_2019/r2-ap-cell-depth.png')

% R2 coef. of determination between APs and cell_type
mdl = fitlm(ct(:), ap(:))
mdl.plot
saveas(gcf, 'E:/tmpscratch/sahilloo/barrel/outData/figures/fig6/04_07_2019/r2-ap-cell-type.png')

% R2 coef. of determination between APs and both cell_depth + cell_type
tbl = table(c_depth(:), ct(:), ap(:),'VariableNames',{'c_depth','ct','ap'});
mdl = fitlm(tbl, 'ap~c_depth+ct')
mdl.plot
saveas(gcf, 'E:/tmpscratch/sahilloo/barrel/outData/figures/fig6/04_07_2019/r2-ap-cell-depth-cell-type.png')

%% Repeated measures model
% used by when same is measured repeatedly
t = table(ct(:),ap(:), c_depth(:),'VariableNames',{'cell_types','APs','cell_depth'});
Meas = table([1 2]','VariableNames',{'Measurements'});
rm = fitrm(t,'APs-cell_depth~cell_types','WithinDesign',Meas);
manova(rm)
manova(rm,'By','cell_types')
