% figure 7 extract data from ON/OFF response figures and plot as hitogram
setConfig;

nameType = 'ON';
name = sprintf('%s_rate_whisker',lower(nameType));

uiopen(fullfile(config.outDir,'outData/figures/figure7/inferred_aps/inferred_aps', [name,'.fig']),1)
outfile = fullfile(config.outDir,'outData/figures/figure7/inferred_aps/inferred_aps', [name '_histogram.eps']);

%%
fig = gcf;
axObjs = fig.Children(3); % 2nd subplot
dataObjs = axObjs.Children(1); % scatter
sps = dataObjs.YData;
axObjs = fig.Children(5); % 1st subplot
dataObjs = axObjs.Children(1); % scatter
stp = dataObjs.YData;

%% all responses
curStp = stp;
curSps = sps;
pValue = ranksum(curStp,curSps);
sprintf('%s responses (all) median, range: Stp (%.3f, [%.3f to %.3f], N=%d) vs Sps (%.3f, [%.3f to %.3f], N=%d). Factor %.2f, ranksum pValue = %e',...
    nameType,...
    median(curStp), min(curStp), max(curStp), numel(curStp),...
    median(curSps), min(curSps), max(curSps), numel(curSps),...    
    median(curStp)/ median(curSps), pValue)

%% remove non-responsive stp and sps with 325th percentile
curStp = stp;
curSps = sps;

thrStp = prctile(curStp,25);
curStp(curStp<thrStp) = [];

thrSps = prctile(curSps,25);
curSps(curSps<thrSps) = [];

pValue = ranksum(curStp,curSps);
sprintf('%s responses(thr>25th perc) median, range: Stp (%.3f, [%.3f to %.3f], N=%d) vs Sps (%.3f, [%.3f to %.3f], N=%d). Factor %.2f, ranksum pValue = %e',...
    nameType, ...
    median(curStp), min(curStp), max(curStp), numel(curStp),...
    median(curSps), min(curSps), max(curSps), numel(curSps),...    
    median(curStp)/ median(curSps), pValue)

%% top 50% cells comparison only
curStp = stp;
curSps = sps;

thrStp = prctile(curStp,50);
curStp(curStp<thrStp) = [];

thrSps = prctile(curSps,50);
curSps(curSps<thrSps) = [];

pValue = ranksum(curStp,curSps);
sprintf('%s responses (thr>50th perc)  median, range: Stp (%.3f, [%.3f to %.3f], N=%d) vs Sps (%.3f, [%.3f to %.3f], N=%d). Factor %.2f, ranksum pValue = %e',...
    nameType, ...
    median(curStp), min(curStp), max(curStp), numel(curStp),...
    median(curSps), min(curSps), max(curSps), numel(curSps),...    
    median(curStp)/ median(curSps), pValue)

%% spont. activity based thr
curStp = stp;
curSps = sps;

thrStp = 0.366; % median
curStp(curStp<thrStp) = [];

thrSps = 0.276; % median
curSps(curSps<thrSps) = [];

pValue = ranksum(curStp,curSps);
sprintf('%s responses (thr>median spont activity)  median, range: Stp (%.3f, [%.3f to %.3f], N=%d) vs Sps (%.3f, [%.3f to %.3f], N=%d). Factor %.2f, ranksum pValue = %e',...
    nameType, ...
    median(curStp), min(curStp), max(curStp), numel(curStp),...
    median(curSps), min(curSps), max(curSps), numel(curSps),...    
    median(curStp)/ median(curSps), pValue)

% %%
% figure;
% hold on;
% ax = gca;
% histogram(curStp, 'BinWidth',0.05','DisplayStyle','stairs','EdgeColor','m','Normalization','probability')
% histogram(curSps, 'BinWidth',0.05','DisplayStyle','stairs','EdgeColor','b','Normalization','probability')
% 
% %%
% figure; hold on;
% binWidth = 0.25;
% cx_posBins = 0:binWidth:4;
% Util.fitGaussianNormalized(curStp, cx_posBins,'normal','m', binWidth);
% Util.fitGaussianNormalized(curSps, cx_posBins,'normal','b', binWidth);

% %% save
% export_fig(outfile,'-q101', '-nocrop','-transparent')
% close all