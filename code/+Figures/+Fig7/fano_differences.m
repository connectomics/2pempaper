% scatter plot of FanoFactor data with soma syn, cortical depth, primary dendrite
setConfig;

outDirPdf = fullfile(config.outDir,'outData/figures/fig6/', config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

%%
Util.log('Read ff data')
[fano_fac, so, ct, c_depth, num_d, idxStp, idxSps] = Figures.Fig6.loadFunctionalData();

%% one way ANOVA: groups Stp/Sps and dependent variable fano factor
% Whether the samples across two groups are draw from population with unequal means
Util.log('Now plotting...')
yTicks = 0:1:3;
lineWidth = 4;
fontSize = 24;
fig = figure;
fig.Color = 'white';
ax = subplot(1,3,1);
hold on
x = [fano_fac.on(idxStp), fano_fac.on(idxSps)];
doThis(x,1:numel(idxStp), numel(idxStp)+(1:numel(idxSps)), 'Spike rate (on)');
hold off
ax.YAxis.TickValues = yTicks;
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,2);
hold on
x = [fano_fac.off(idxStp), fano_fac.off(idxSps)];
doThis(x,1:numel(idxStp), numel(idxStp)+(1:numel(idxSps)), 'Spike rate (off)');
hold off
ax.YAxis.TickValues = yTicks;
ax.YAxis.TickLabels = '';
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);

ax = subplot(1,3,3);
hold on
x = [fano_fac.spont(idxStp), fano_fac.spont(idxSps)];
doThis(x,1:numel(idxStp), numel(idxStp)+(1:numel(idxSps)), 'Spike rate (spont)');
hold off
ax.YAxis.TickValues = yTicks;
ax.YAxis.TickLabels = '';
ax.LineWidth = lineWidth;
ax.FontSize = fontSize;
Util.setPlotDefault(gca,false,false);
% export_fig(fullfile(outDirPdf,'ff_differences.eps'),'-q101', '-nocrop', '-transparent');
% close all

function doThis(x,idxStp, idxSps, yname)
    g = cell(numel(x),1);
    g(idxStp) = {'Stp'}; % first box
    g(idxSps) = {'Sps'}; % second box
    boxplot(x,g,'Colors','k','Widths',2,'Positions',[5,10]);
    scatter((6-4).*rand(numel(idxStp),1)+4, x(idxStp),...
        'o','filled', 'MarkerFaceColor','m','MarkerEdgeColor','m');
    scatter((11-9).*rand(numel(idxSps),1) + 9,x(idxSps),...
        'o','filled', 'MarkerFaceColor','b','MarkerEdgeColor','b');
    ylim([0,3]) % hard coded
%     pValue1 = anova1(x,g,'off');
    [~, pValue1] = ttest2(x(idxStp),x(idxSps));
    pValue2 = ranksum(x(idxStp),x(idxSps));
    title(sprintf('p ttest2: %.03f\np rs: %.03f',pValue1, pValue2))
    sprintf('p ttest2: %.03f\np rs: %.03f',pValue1, pValue2)
    ylabel(yname);
    box off,set(gca,'TickDir','out');
%     legend({'Stp','Sps'},'Location','best')
end
