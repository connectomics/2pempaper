% panel f with example stp and sps example denrite reconstructions with barrel somata
%stp: https://webknossos.brain.mpg.de/annotations/Explorational/5bd8646a01000046288cd92c#14832,15900,1958,0,1.25,17406
%sps: https://webknossos.brain.mpg.de/annotations/Explorational/5bd86be801000096298cdab1#16198,16558,4674,0,8.35,10009
setConfig;
outDir = config.outDir;

somaColors = Util.getSomaColors;
tubeSize = 1; dotSize= 3;
somaBallSize = 100;
somaBallColorStp = somaColors{end-1}; % magenta
somaBallColorSps = somaColors{end}; % blue

outDirPdf = fullfile(config.outDir,'outData/figures/fig1/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

Util.log('Read tracings and synapse data...')
% nml = fullfile(config.outDir,['data/wholecells/stp_example_dendrite_axon.nml']);
nml = fullfile(config.outDir,['data/wholecells/stp_example_dendrite_with_apical_split.nml']);
skelStp = skeleton(nml);
skelStp.scale = [11.24,11.24,30];

nml = fullfile(config.outDir,['data/wholecells/sps_four_examples.nml']);
skelSpsAll = skeleton(nml);
skelSpsAll.scale = [11.24,11.24,30];

% scale nodesPC
scale = skelStp.scale./1000;
nodesPC = skelStp.setScale(nodesPC, scale);

Util.log('Now plotting figure....');

%% plot Stp
f = figure();
f.Units = 'centimeters';
f.Position = [1 1 21 29.7];
hold on;
objectPC = ...
        scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
        ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
        'MarkerFaceColor', colorPC(1:3));
plotDendriteWithSoma(skelStp,somaBallColorStp,somaBallSize, tubeSize);
hold off;
% save output figure
% outfile = fullfile(outDirPdf,'panel_examples_stp.eps');
% export_fig(outfile,'-q101', '-nocrop','-transparent');
% Util.log('Saving file %s.', outfile);
% close(f);

% plot all exmaples sps separately
for i = 1:skelSpsAll.numTrees
    skelSps = skelSpsAll.keepTreeWithName(skelSpsAll.names{i});

    f = figure();
    f.Units = 'centimeters';
    f.Position = [1 1 21 29.7];
    hold on;
    objectPC = ...
            scatter3(nodesPC(:,1),nodesPC(:,2),nodesPC(:,3)...
            ,dotSize,'filled', 'MarkerEdgeColor', colorPC(1:3), ...
            'MarkerFaceColor', colorPC(1:3));
    plotDendriteWithSoma(skelSps,somaBallColorSps,somaBallSize, tubeSize);
    hold off;
    % save output figure
%     outfile = fullfile(outDirPdf,['panel_examples_' skelSpsAll.names{i} '.eps']);
%     export_fig(outfile,'-q101', '-nocrop','-transparent');
%     Util.log('Saving file %s.', outfile);
%     close(f);
end

function plotDendriteWithSoma(thisSkel, somaBallColor, somaBallSize, tubeSize)
    idxAxon  = cellfun(@(x) any(regexpi(x,'axon')),thisSkel.names); % exclude axon, keep dend + spines
    idxApical = cellfun(@(x) any(regexpi(x,'apical')),thisSkel.names); % exclude apical first
    idxPlot = ~(idxAxon|idxApical);
    thisSoma = Util.getNodeCoordsWithComment(thisSkel,{'soma','_ss'},'partial');
    %scale all spheres
    scale = thisSkel.scale./1000;
    somaLocs = thisSkel.setScale(thisSoma,scale);
    thisCellColor = somaBallColor(1:3);
    thisSkel.plot(idxPlot,thisCellColor, true, tubeSize);
    thisSkel.plot(idxApical, 'k', true, 2*tubeSize);
    objectSoma = ...
                 scatter3(somaLocs(1,1),somaLocs(1,2),somaLocs(1,3)...
                ,somaBallSize,'filled', 'MarkerEdgeColor', somaBallColor(1:3), ...
                'MarkerFaceColor', somaBallColor(1:3));
    %yz
    Util.setView(3, true);
end
