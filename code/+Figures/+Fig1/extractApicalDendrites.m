function skelFound = extractApicalDendrites(somaStpCoords)
    setConfig

    if iscell(somaStpCoords)
        somaStpCoords = vertcat(somaStpCoords{:});
    end
    
    % for fig1. the apicals were extacted from skel cellTypesL4Updated_py5 nml and written out in allForHeiko\data_1g_EM
    sprintf('Loading stp apicals from: %s', config.apicalNmlEM)
    skelApicals = skeleton(config.apicalNmlEM);
    skelApicals.scale = config.scaleEM;

    idxFound = [];
    for i=1:size(somaStpCoords,1)
        curSoma = somaStpCoords(i,:);
        [~,tree] = skelApicals.getClosestNode(curSoma,1:skelApicals.numTrees,0);
        if isempty(tree)
            error(sprintf('No Apical found for soma coord: %d,%d,%d',curSoma(1), curSoma(2), curSoma(3)))
        end
        idxFound = vertcat(idxFound, tree);
    end
    
    skelFound = skelApicals.deleteTrees(idxFound, true);
end
