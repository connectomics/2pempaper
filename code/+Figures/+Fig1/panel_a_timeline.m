% make the timeline axis for the mouse

%P0: birth day 30.03
%P1: 1 day old 31.03
%P65 - injection  3.06 (1 March day + 30 April + 31 May + 3 June)
%P79 - 2p+perfusion 17.06 (1 March day + 30 April + 31 May + 17 June)

setConfig;
outDirPdf = fullfile(config.outDir,'outData/figures/fig1/',config.version);
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

f = figure();
f.Color = 'none';
set(gca,'LineWidth',3,'ycolor','none', 'xtick',[0,65,79], 'xticklabel','')
xlim([0,85])

% save output figure
% outfile = fullfile(outDirPdf,'panel_timeline.eps');
% export_fig(outfile,'-q101', '-nocrop','-transparent');
% Util.log('Saving file %s.', outfile);
% close(f);
