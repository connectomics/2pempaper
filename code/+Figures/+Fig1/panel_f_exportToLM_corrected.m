% panel g with pyr/stp + apicals vs sps along barrel in background
% NOTE that this was not used because types of cells above border were updated for 1g plot exclusively
setConfig;

Rmin = 0; Rmax = 0.2;
Gmin = 0.5; Gmax = 0.9;
Bmin = 0.3; Bmax = 0.75;

somaColors = Util.getSomaColors;
tubeSize = 2; dotSize= 3;
somaBallSize = 5;
somaStpColor = somaColors{end-1};
somaSpsColor = somaColors{end};
somaInColor = [0,0,0];

scaleLM = [1172,1172,1000];
scaleEM = [11.24,11.24,30];

outDirPdf = fullfile(config.outDir,'outData/figures/fig1/v1/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

outVersion = '2020_10_26_';

l4BorderUp = 326; % from sigmoid fit of 2p soma data
l4BorderDown = 515;

skelPC = skeleton(config.skelIn);
nodesPC = skelPC.getNodes;
colorPC = [0.5,0.5,0.5];

skelStp = skeleton(config.stpAllApicals); % stp and pyr cells with apicals
skelStp.scale = [11.24, 11.24, 30];

%skelAll= skeleton(fullfile(outDir,'data/2018_10_23_cell_type_L4_v2.nml')); % 1600 trees of cells
%skelAll = skeleton(fullfile(outDir,'data/2019_01_22_cell_type_L4.nml')); % only use largest tree that has somas commented
skelAll = skeleton(config.cellTypesL4Fixed);
skelAll = skelAll.extractLargestTree;
skelAll.scale = [11.24, 11.24, 30];

%skelIn = skeleton(fullfile(outDir,'data/cell_type_inhibitory_cells.nml')); % in soma locations
%skelIn.scale = [11.24, 11.24, 30];

skelLM = skeleton(config.somasClickedLM); % somas clicked in LM

% all pyr/stp somas
somaLocsMinusOne = Util.getNodeCoordsWithComment(skelLM, '-1', 'partial'); % somas clicked black holes in LM

somaLocsStp = Util.getNodeCoordsWithComment(skelAll, 'p\d+', 'regexp'); % L3 py, L4 sp, L5 py were commented with ???sp??? or ???py??? : NEW: py3, py5, p,s
somaLocsSps = Util.getNodeCoordsWithComment(skelAll, 's\d+', 'regexp'); % ss: spiny-stellate ss or ss +
somaLocsIn = Util.getNodeCoordsWithComment(skelAll, 'in', 'partial'); %
somaLocsL3pyr = Util.getNodeCoordsWithComment(skelAll, 'py3', 'partial'); %
somaLocsL5pyr = Util.getNodeCoordsWithComment(skelAll, 'py5', 'partial'); %
somaLocsUC = Util.getNodeCoordsWithComment(skelAll, 'uc', 'partial'); %

Util.log(' debug into tilt issues')
% no correction EM
skelOut = skeleton;
skelOut = skelOut.setParams('YH_st126_MT4_updated_1708',scaleEM,[0,0,0]);
skelOut = addSomasAsGroup(skelOut,somaLocsStp,'stp',[1,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsSps, 'sps',[0,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsIn, 'IN',[0,0,0,1]);
skelOut.write(fullfile(outDir,'allForHeiko', [outVersion 'somasAll_EM.nml']));

m=load(config.barrelStateTform,'tform');
tform = m.tform;
% tilt correction hack
skelStpNew = skeleton();
skelStpNew = skelStpNew.setParams('YH_st126_MT4_updated_1708', [11.24,11.24,30],[1 1 1]);
for i = 1:skelStp.numTrees
    newNodes = skelStp.nodes{i}(:,1:3);
    newNodes(:,2) = round(newNodes(:,2)-.449*newNodes(:,3));
    skelStpNew = skelStpNew.addTree(skelStp.names{i},newNodes, skelStp.edges{i},[1,0,1,1]); % magenta
end
somaLocsStp(:,2) = round(somaLocsStp(:,2)-.449*somaLocsStp(:,3));
somaLocsSps(:,2) = round(somaLocsSps(:,2)-.449*somaLocsSps(:,3));
somaLocsIn(:,2) = round(somaLocsIn(:,2)-.449*somaLocsIn(:,3));
somaLocsUC(:,2) = round(somaLocsUC(:,2)-.449*somaLocsUC(:,3));
nodesPC(:,2) = round(nodesPC(:,2)-.449*nodesPC(:,3));
somaLocsL3pyr(:,2) = round(somaLocsL3pyr(:,2)-.449*somaLocsL3pyr(:,3));
somaLocsL5pyr(:,2) = round(somaLocsL5pyr(:,2)-.449*somaLocsL5pyr(:,3));

%debug tilt corrected EM
skelOut = skeleton;
skelOut = skelOut.setParams('YH_st126_MT4_updated_1708',scaleEM,[0,0,0]);
skelOut = addSomasAsGroup(skelOut,somaLocsStp,'stp',[1,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsSps, 'sps',[0,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsIn, 'IN',[0,0,0,1]);
skelOut.write(fullfile(outDir,'allForHeiko', [outVersion 'somasAll_corrected_EM.nml']));

Util.log('transform now to LM space')
skelStpLM = TFM.applyTformToSkeleton(tform, skelStpNew, scaleLM, 'forward'); %EM to LM dataset
somaLocsStpLM = TFM.applyTformToVertices(tform, somaLocsStp, 'forward'); 
somaLocsSpsLM =  TFM.applyTformToVertices(tform, somaLocsSps, 'forward');
somaLocsInLM = TFM.applyTformToVertices(tform, somaLocsIn, 'forward');
nodesPCLM = TFM.applyTformToVertices(tform, nodesPC, 'forward');
somaLocsUCLM = TFM.applyTformToVertices(tform, somaLocsUC, 'forward');
somaLocsL3pyrLM = TFM.applyTformToVertices(tform, somaLocsL3pyr, 'forward');
somaLocsL5pyrLM =  TFM.applyTformToVertices(tform, somaLocsL5pyr, 'forward');

% debug tilt corrected EM to LM
skelOut = skeleton;
skelOut = skelOut.setParams('m150517_2p_03',scaleLM,[0,0,0]);
skelOut = addSomasAsGroup(skelOut,somaLocsStpLM,'stp',[1,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsSpsLM, 'sps',[0,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsInLM, 'IN',[0,0,0,1]);
skelOut.write(fullfile(outDir,'allForHeiko', [outVersion 'somasAll_corrected_EM_to_LM.nml']));

% color orange for stp that are within 5 um of minusOne & closest
distance = pdist2(bsxfun(@times, somaLocsStpLM,scaleLM),...
                  bsxfun(@times, somaLocsMinusOne,scaleLM),'euclidean'); % in nm in LM
distThr = 5000; %nm
idxStpToOrange = [];
idxMinusOneCaptured = [];
for i=1:size(somaLocsStpLM,1)
    thisDist = distance(i,:);
    if any(thisDist<=distThr)
        [~,idxMinusOne] = min(thisDist);% which minuOne soma is nearest
        idxStpToOrange = [idxStpToOrange;i];
        idxMinusOneCaptured = [idxMinusOneCaptured; idxMinusOne];
    end
end
somaLocsL3pyrLM = vertcat(somaLocsL3pyrLM, somaLocsStpLM(idxStpToOrange,:));
somaLocsL3pyr = vertcat(somaLocsL3pyr, somaLocsStp(idxStpToOrange,:));
somaLocsStpLM(idxStpToOrange,:) = [];
somaLocsStp(idxStpToOrange,:) = [];

skelOut = skeleton;
skelOut = skelOut.setParams('m150517_2p_03',scaleLM,[0,0,0]);
skelOut = addSomasAsGroup(skelOut,somaLocsStpLM,'stp',[1,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsL3pyrLM, 'L3pyr', [0.9290, 0.6940, 0.1250,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsL5pyrLM, 'L5pyr', [0.8, 0.6940, 0.1250,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsMinusOne, 'minusOne',[0.3010, 0.7450, 0.9330,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsSpsLM, 'sps',[0,0,1,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsInLM, 'IN',[0,0,0,1]);
skelOut = addSomasAsGroup(skelOut,somaLocsUCLM, 'UC',[0.5,0.5,0.5,1]);
skelOut.write(fullfile(outDir,'allForHeiko',[outVersion 'somasAll.nml']));

info = Util.runInfo();
save(fullfile(outDir,'allForHeiko', [outVersion 'fig1_panelg_dataEM.mat']),'somaLocsStp','somaLocsSps',...
            'somaLocsIn','nodesPC','somaLocsL3pyr','somaLocsL5pyr','somaLocsUC','info')
save(fullfile(outDir,'allForHeiko', [outVersion 'fig1_panelg_dataLM.mat']),'somaLocsStpLM','somaLocsSpsLM',...
            'somaLocsInLM','nodesPCLM','somaLocsL3pyrLM','somaLocsL5pyrLM','somaLocsUCLM','info')
skelStpLM  = skelStpLM.setParams('m150517_2p_03',scaleLM,[1 1 1]);
skelStpLM.write(fullfile(outDir,'allForHeiko',[outVersion 'stp_all_apicals_LM_hacked.nml']))

Util.log('write coordinates of somas')
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_Stp_magenta.txt']),somaLocsStpLM,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_Sps_blue.txt']),somaLocsSpsLM,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_In_black.txt']),somaLocsInLM,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_PC_grey.txt']),nodesPCLM,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_L3pyr_orange.txt']),somaLocsL3pyr,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_L5pyr_orange.txt']),somaLocsL5pyr,'\t');
dlmwrite(fullfile(outDir,'allForHeiko',[outVersion 'somaCoords_UC_grey.txt']),somaLocsUCLM,'\t');

%  additionally separate out trees of ADs that belong are above L3 border
[somaLocsAD, treeIndAD] = Util.getNodeCoordsWithComment(skelStp, 'sp', 'partial');
somaLocsAD(:,2) = round(somaLocsAD(:,2)-.449*somaLocsAD(:,3)); % tilt correction
somaLocsADLM = TFM.applyTformToVertices(tform, somaLocsAD, 'forward'); % convert to LM
idxOr1 = somaLocsADLM(:,3) < l4BorderUp;
idxOr2 = somaLocsADLM(:,3) > l4BorderDown;
idxOr = idxOr1 | idxOr2;
orangeAD = treeIndAD(idxOr);
magentaAD = setdiff(1:skelStpLM.numTrees, orangeAD);
skelStpLM.write(fullfile(outDir,['allForHeiko/' outVersion 'stp_all_apicals_LM_hacked_magenta.nml']), magentaAD);
skelStpLM.write(fullfile(outDir,['allForHeiko/' outVersion 'stp_all_apicals_LM_hacked_orange.nml']), orangeAD);
treeNames = skelStp.names;
orangeNames = treeNames(orangeAD);

function skel = addSomasAsGroup(skel, points, groupName, color )
    [skel, id] = skel.addGroup(groupName);
    skel = skel.addNodesAsTrees(points,'','',repelem(color, size(points,1),1));
    treesNew = skel.numTrees - size(points,1) + 1 : skel.numTrees;
    skel = skel.addTreesToGroup(treesNew , id);
end
