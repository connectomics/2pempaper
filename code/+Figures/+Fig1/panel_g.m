% plot the distributions along Z
% soma density
% https://webknossos.brain.mpg.de/annotations/Explorational/5be198150100006e02ebd657#141,124,345,0,0.86,32
setConfig;
info = Util.runInfo();
Util.showRunInfo(info);

dataFromUser = 'sl';
useUCSoma = true;
ticklen = [0.025, 0.025];
skel = skeleton(config.somasClickedLM); % somas clicked LM

Util.log('extract all comments 0,1,2,3: fluo. somas')
somaCoordsMinusOne = Util.getNodeCoordsWithComment(skel,{'-1'},'partial');
somaCoordsFluo = Util.getNodeCoordsWithComment(skel,{'0','1','2','3'},'exact'); % within bbox
idxCell = skel.getTreeWithName('cell','regexp');
somaCoordsCells = skel.getNodes(idxCell);
somaCoords = vertcat(somaCoordsFluo, somaCoordsCells);

%%
dataPointsFluo = [];
dimension = 3; binSize = 15;
lowerLim = 285; % manual
upperLim = 420; % manual: dont bin in region with no data
numBins = floor((upperLim - lowerLim)/binSize);
somaCounts = zeros(numBins-1,1); %OK
for i = 1:numBins
    thisBinUpper = lowerLim + i*binSize;
    somaCounts(i) = sum(somaCoords(:,dimension) <= thisBinUpper);
end
dataPointsFluo = somaCounts;
clear somaCounts numBins

%% load YH panel h data & plot stp/sps/in/py3/py5/fluo. curves
% NOTE: data and bins go from lower towards upper layers
switch dataFromUser
    case 'yh'
        % load soma locations and get distribution of L5pyr
        m = load(fullfile(outDir,'allForHeiko/fig1_panelg_dataLM.mat'));
        somaCoords = m.somaLocsL5pyrLM;
        dimension = 3; binSize = 15; % also chosen by data from YH
        bins = upperLim:binSize:lowerLim;
        somaCounts = zeros(1,numel(bins));
        for i = 1:numel(bins)
            thisBinLower = lowerLim - (i-1)*binSize;
            thisBinUpper = lowerLim - i*binSize;
            somaCounts(i) = sum(somaCoords(:,dimension) >= thisBinUpper & ...
                                somaCoords(:,dimension) < thisBinLower);
        end
        py5 = somaCounts; clear m somaCounts
        % data from yh, missing py5
        m = load(fullfile(outDir,'data/panel_h_yh_data.mat'));
        ss = m.ss;
        sp = m.sp;
        in = m.in;
        py3 = m.py;
        xbins = m.xbins;
        avg_f = m.avg_f;
    case 'sl'
        m = load(fullfile(outDir,'data/panel_h_sl_data.mat')); %data from skelCellType EM annotation file
        ss = m.ss;
        sp = m.sp;
        in = m.in;
        py3 = m.py3;
        py5 = m.py5;
        uc = m.uc;
        xbins = m.xbins;
        avg_f = m.avg_f;     
        clear m
        Util.log('Removing last 0 to -15 bin')
        ss = ss(1:end-1);
        sp = sp(1:end-1);
        in = in(1:end-1);
        py3 = py3(1:end-1);
        py5 = py5(1:end-1);
        uc = uc(1:end-1);
        xbins = xbins(1:end-1);
end

%%
fig = figure;
fig.Color = 'white';
hold all
%plot((ss+sp+in+py3+py5)./sum(ss+sp+py3+in+py5),xbins,'k--', 'LineWidth',4); % all somata dashed line
if useUCSoma
    yTemp = (ss+sp+in+py3+py5+uc);
else
    yTemp = (ss+sp+in+py3+py5);
end
shaded = patch([yTemp,yTemp],[xbins,zeros(1,numel(xbins))],[0.9,0.9,0.9]);
shaded.EdgeColor = [0.9,0.9,0.9];
idx = find(ss);
plot(ss(idx),xbins(idx),'b', 'LineWidth',4);
idx = find(sp);
plot(sp(idx),xbins(idx),'m', 'LineWidth',4);
idx = find(in);
plot(in(idx),xbins(idx),'k', 'LineWidth',4);
idx = find(py3);
plot(py3(idx),xbins(idx), 'Color',[230/255,159/255,0], 'LineWidth',4);
idx = find(py5);
plot(py5(idx),xbins(idx), 'Color',[0.5,0.5,0.5], 'LineWidth',4);
clear idx

Util.figPlot('','');
ax1 = gca;
ax1.XAxis.Limits = [-120, 120]; % center it
ax1.XAxis.TickValues = 50:50:100;
ax1.YAxis.Limits = [-600,0];
set(ax1,'LineWidth',4, 'TickLength',ticklen)

%% new axis for fluo.
axF_pos = ax1.Position;
axF_pos(3) = axF_pos(3)/2; % left half
axF = axes('Position',axF_pos,...
    'XAxisLocation','top',...
    'Color','none');
% plot scnn1 curve
avg_f0 = avg_f-mean(avg_f(35:40));
xbins_fluo = -600:15:-15;
plot(avg_f0./max(avg_f0)/-10,xbins_fluo,'g', 'LineWidth',4, 'Parent', axF);

axF.XAxis.Limits = [-0.10, 0.001];
set(gca,'xtick',-0.1:0.05:0,'xticklabel',[1 0.5 0],'tickDir','out')
set(gca,'xticklabel','')
set(gca,'LineWidth',4, 'TickLength',ticklen)
% ylabel('Cortical depth(\mum)');
% xlabel('Norm. fluorescence                  Fraction of Soma')
% legend({'L4 SpS','L4 StP','IN','L3 Pyr','all Somata','scnn1a'});
% legend boxoff

%% Normalize to the max of soma counts
dataPointsFluo = dataPointsFluo ./max(dataPointsFluo);

%% Left half of figure: overlay my soma counts curve with sigm fit
ax2_pos = axF.Position; 
ax2 = axes('Position',ax2_pos,...
    'XAxisLocation','top',...
    'Color','none');
line(-1*dataPointsFluo,-1*(binSize*(0:numel(dataPointsFluo)-1)),'Parent',ax2,'LineWidth',4,'color',[89, 124, 43]./255);

hold on;
[p,stat] = Util.sigm_fit((0:numel(dataPointsFluo)-1),dataPointsFluo,'','',false);
ypred = stat.ypred;
xpred =  binSize*(0:numel(ypred)-1);
line(-1*ypred,-1*xpred,'Parent',ax2,'LineWidth',4,'color','k','LineStyle','--');

% Change # soma counts to normalized 0 to 1
% change negative to positive labels
ax2.XAxis.Limits = [-1,0]; % normalize % xlim([-180,0])
ax2.XAxis.TickValues = -1:0.5:0;

xticklabels = get(ax2,'xticklabel');
newlabels = cellfun(@(x) num2str(str2double(x)*-1),xticklabels,'uni',0);
set(ax2, 'XTicklabel',newlabels(:)'); % change negative to positive soma counts
set(ax2,'TickDir','out','LineWidth',4, 'TickLength',ticklen)
% xlabel('Cumulative count of somas');
% legend({'# fluo. cell','sigmfit'})

% align y axis with rest of the panels
ylimits = ax2.YAxis.Limits;
lowerLimForAlignment = lowerLim+binSize - 600;
upperLimForAlignment = abs(ylimits(1))+ (600-upperLim);
ax2.YAxis.Limits = [lowerLimForAlignment, upperLimForAlignment];

% turn off axis labels
set(ax2,'yticklabel',[])

%% add lines at 10, 5, 90th percentile. NOTE these are wrt raw data which overlaps with sigm fit
% find 10th, 50th, 90th %
% param = p;
% fsigm = @(param,xval) param(1)+(param(2)-param(1))./(1+10.^((param(3)-xval)*param(4)));
counts_10 = 0.1* (max(dataPointsFluo));
counts_10_z =  p(3) - log10((p(2) - counts_10)./(counts_10 - p(1)))./p(4); 
counts_50 = 0.5* (max(dataPointsFluo));
counts_50_z =  p(3) - log10((p(2)-counts_50)./(counts_50 - p(1)))./p(4);
counts_90 = 0.9* (max(dataPointsFluo));
counts_90_z =  p(3) - log10((p(2)-counts_90)./(counts_90 - p(1)))./p(4);

z_10_mapped = binSize*counts_10_z;
z_50_mapped = binSize*counts_50_z;
z_90_mapped = binSize*counts_90_z;
z_10 = lowerLim +  binSize + z_10_mapped;
z_50 = lowerLim + binSize + z_50_mapped;
z_90 = lowerLim + binSize + z_90_mapped;

fprintf('Percentiles are as follows:\n')
disp([z_10, z_50, z_90])

leftEndCyan = max(dataPointsFluo);

hold on;
line([-1*0 -1*leftEndCyan], [-1*z_10_mapped -1*z_10_mapped],...
        'Parent',ax2,'LineWidth',0.75,'color','c','LineStyle','--');
line([-1*0 -1*leftEndCyan], [-1*z_50_mapped -1*z_50_mapped],...
        'Parent',ax2,'LineWidth',0.75,'color','c','LineStyle','--');
line([-1*0 -1*leftEndCyan], [-1*z_90_mapped -1*z_90_mapped],...
        'Parent',ax2,'LineWidth',0.75,'color','c','LineStyle','--');
set(gca,'LineWidth',4, 'TickLength',ticklen)

%% plot sigmoid fit for L45 border
% Copied from +Figures/+Fig1/layer4BorderDown_LM_non_cumul.m
switch dataFromUser
    case 'yh'
        m = load(fullfile(outDir,'allForHeiko/fig1_panelg_dataLM.mat'));
        somaLocsIn = m.somaLocsInLM;
        somaLocsSps = m.somaLocsSpsLM;
        somaLocsStp = m.somaLocsStpLM;
        somaLocsL5pyr = m.somaLocsL5pyrLM;
        somaLocsL3pyr = m.somaLocsL3pyrLM;
        somaLocsUC = m.somaLocsUCLM;
        somaCoords2 = vertcat(somaLocsIn,somaLocsSps,somaLocsStp,...
            somaLocsL5pyr,somaLocsL3pyr,somaLocsUC);
    case 'sl'
        m = load(fullfile(outDir,'allForHeiko/fig1_panelg_somaCountsLM.mat')); % from skelCellTypesL4
        if useUCSoma
            somaCoords2 = vertcat(m.somaLocsStp_in,m.somaLocsStp_out,...
                m.somaLocsSps_in, m.somaLocsSps_out, ...
                m.somaLocsIn_in, m.somaLocsIn_out, ...
                m.somaLocsL3pyr_in, m.somaLocsL3pyr_out,...
                m.somaLocsL5pyr_in, m.somaLocsL5pyr_out,...
                m.somaLocsUC_in, m.somaLocsUC_out);
        else
            somaCoords2 = vertcat(m.somaLocsStp_in,m.somaLocsStp_out,...
                m.somaLocsSps_in, m.somaLocsSps_out, ...
                m.somaLocsIn_in, m.somaLocsIn_out, ...
                m.somaLocsL3pyr_in, m.somaLocsL3pyr_out,...
                m.somaLocsL5pyr_in, m.somaLocsL5pyr_out);
        end
        clear m
end

%%
clear somaCounts dataPoints
dimension = 3; binSize = 10;
upperLim = 420; % manual
lowerLim = 540; % manual restrict to last bin whrer L5pyr cells are there
numBins = floor((lowerLim - upperLim)/binSize);
somaCounts = zeros(numBins-1,1); %OK
for i = 1:numBins
    thisBinUpper = lowerLim - i*binSize;
    thisBinLower =  lowerLim - (i-1)*binSize;
    somaCounts(i) = sum(somaCoords2(:,dimension) >= thisBinUpper ...
                        & somaCoords2(:,dimension) < thisBinLower);
end
dataPoints = somaCounts;

%% Normalize to the max of soma counts
dataPoints = dataPoints ./max(dataPoints);

%% add l4 and l5 somas density (non cumul) curve
ax2_pos = ax2.Position; % position of second axes
ax3_pos = ax2_pos;
ax3_pos(3) = ax3_pos(3); % left half
ax3 = axes('Position',ax3_pos,...
    'XAxisLocation','top',...
    'Color','none','ytick',[],'yticklabels',[]);
line(-1*dataPoints, binSize*(0:numel(dataPoints)-1),'Parent',ax3,'LineWidth',4,'color',[0.5,0.5,0.5]);

ax3.YAxis.Limits = [0, lowerLim - upperLim]; % match upper and lower limits
ylimits = ax3.YAxis.Limits;
% ax3.YAxis.Limits = [0 ylimits(2)];
% bias = lowerLim - ylimits(2);
% yticklabels = get(gca,'yticklabel');
% newlabels = cellfun(@(x) num2str((str2double(x)+bias)*-1),yticklabels,'uni',0);
% set(gca, 'YTicklabel',fliplr(newlabels(:)'));

[p,stat] = Util.sigm_fit((0:numel(dataPoints)-1),dataPoints,'','',false);
ypred = stat.ypred;
xpred =  binSize*(0:numel(ypred)-1);
line(-1*ypred,xpred,'LineWidth',4,'color','k','LineStyle','-.');

% align y axis with rest of the panels
lowerLimForAlignment = lowerLim-600;
upperLimForAlignment = ylimits(2)+upperLim;
ax3.YAxis.Limits = [lowerLimForAlignment, upperLimForAlignment];

% adjust x axis
ax3.XAxis.Limits = [-1,0]; % normalize % xlim([-180,0]) % same as other # somata axis
ax3.XAxis.TickValues = -1:0.5:0;

% turn off axis labels
set(ax3,'xticklabel',[],'yticklabel',[])

%% Percentiles of the sigmoid fit
% param = p;
% fsigm = @(param,xval) param(1)+(param(2)-param(1))./(1+10.^((param(3)-xval)*param(4)));
counts_10 = 0.1* (max(ypred));
counts_10_z =  p(3) - log10((p(2) - counts_10)./(counts_10 - p(1)))./p(4); 
counts_50 = 0.5* (max(ypred));
counts_50_z =  p(3) - log10((p(2)-counts_50)./(counts_50 - p(1)))./p(4);
counts_90 = 0.9* (max(ypred));
counts_90_z =  p(3) - log10((p(2)-counts_90)./(counts_90 - p(1)))./p(4);

z_10_mapped = binSize*counts_10_z + binSize;
z_50_mapped = binSize*counts_50_z + binSize;
z_90_mapped = binSize*counts_90_z + binSize;

z_10 = lowerLim - z_10_mapped;
z_50 = lowerLim - z_50_mapped;
z_90 = lowerLim - z_90_mapped;
fprintf('Percentiles are as follows:\n')
disp([z_10, z_50, z_90])
% title(sprintf('lowerLim %d, upperLim %d, binSize %d \n 10th:%.2f, 50th %.2f, 90th %.2f ', ...
%     lowerLim, upperLim, binSize,z_10,z_50,z_90))

hold on;
line([-1*0 -1*leftEndCyan], [z_10_mapped z_10_mapped],...
        'Parent',ax3,'LineWidth',0.75,'color','c','LineStyle','-.');
line([-1*0 -1*leftEndCyan], [z_50_mapped z_50_mapped],...
        'Parent',ax3,'LineWidth',0.75,'color','c','LineStyle','-.');
line([-1*0 -1*leftEndCyan], [z_90_mapped z_90_mapped],...
        'Parent',ax3,'LineWidth',0.75,'color','c','LineStyle','-.');
set(ax3,'xcolor','none','ycolor','none') % turn off top axis and left axis

%% turn off depth labelled axis
set(ax1,'ycolor','none','box','off'); % remove depth labels axis
set(ax2,'ycolor','none','box','off'); % remove depth labels axis
set(axF,'ycolor','none','box','off')
set(ax3,'ycolor','none','box','off')

%%
% Util.log('Saving figures...')
% outfile = fullfile(config.outDir,'outData/figures/fig1/',config.version, ...
%     sprintf('%s.eps', 'panel_h_yh_v4'));
% export_fig(outfile,'-q101','-transparent');
% outfile = fullfile(config.outDir,'outData/figures/fig1/',config.version, ...
%     sprintf('%s.fig', 'panel_h_yh_v4'));
% export_fig(outfile,'-q101','-transparent');
% close all

%% error margin for text

m = load(fullfile(outDir,'allForHeiko/fig1_panelg_somaCountsLM.mat')); % from skelCellTypesL4

data = rmfield(m,'info');
data = rmfield(data,'outTable');

borderL45 = 515;
outTable = table();

allTypes = fieldnames(data);
for i=1:numel(allTypes)
    curName = allTypes{i};
    x = data.(curName);
    above = sum(x(:,3) <= borderL45);
    curTable = table;
    curTable.name = {curName}; 
    curTable.total = size(x,1);
    curTable.aboveBorder = above;
    
    outTable = [outTable; curTable]; 
end
disp(outTable)
