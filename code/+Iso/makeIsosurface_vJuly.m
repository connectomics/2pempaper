% Description:
% start script: load LM imagedata and create isosurface of the barrel
setConfig
% execute only once to load the image data and save it
%{
sourceFolder = '/run/user/1000/gvfs/smb-share:server=storage.hest.corp.brain.mpg.de,share=data/Data/huay/forSahil/m150617_raw/';
outDir = '/home/loombas/mnt/gaba/tmpscratch/barrel/';

files = dir(fullfile(sourceFolder,'*.tif'));
NF = length(files);
images = cell(NF,1);
for k = 1 : NF
  images{k} = imread(fullfile(sourceFolder, files(k).name));
end
%}
% load images
outDir = config.outDir;
m = load(fullfile(outDir,'images.mat'));
raw = cat(3,m.images{:});
clear m
visualize = false;

%% do stuff
sizeRaw = size(raw);
barrelRegion = false(size(raw));
barrelRegionPost = false(size(raw));

% Create all green RGB image for overlay
green = cat(3, zeros([sizeRaw(1) sizeRaw(2)]), ...
    ones([sizeRaw(1) sizeRaw(2)]), zeros([sizeRaw(1) sizeRaw(2)]));
tic;

firstStep = 320;
endStep =  520;
maskOL = triu(ones(256,256),-160);

superRelaxingSliceRange = [400,450]; % from yunfeng to allow correct outline due to lack of fluo.

for z = firstStep:endStep
    % do the steps of processing
%    thisImage = raw(:,:,z);
    thisImage = uint8(mean(raw(:,:,z-5:z+5),3));
%    figure
%    subplot(4,4,1); imshow(thisImage); title('raw');
    mask = adapthisteq(thisImage);
%    subplot(4,4,2);imshow(mask); title('mask');

    marker = imerode(mask,strel('disk',12));

%    subplot(4,4,3);imshow(marker); title('marker');
    imgRec = imreconstruct(marker,mask);
%    subplot(4,4,4); imshow(imgRec); title('reconstructed');
   
    imgBW = imbinarize(imgRec);
%    subplot(4,4,5); imshow(imgBW); title('binarized');

    imgM = bwmorph(imgBW,'majority');
%    subplot(4,4,6); imshow(imgM); title('majority');

    imgFl = bwmorph(imgM,'fill');
%    subplot(4,4,7); imshow(imgFl); title('fill');

    imgTk = bwmorph(imgFl,'thicken');    
%    subplot(4,4,8); imshow(imgTk); title('thicken');

    imgMasked = imgTk & maskOL;
    %imgEr = imerode(imgTk,diag(ones(5,1)));
    %imgEr = imerode(imgEr,fliplr(diag(ones(5,1))));
%    subplot(4,4,9); imshow(imgMasked);title('MaskOL');
    
    imgFl = imfill(imgMasked,'holes');
%    subplot(4,4,10); imshow(imgFl); title('fill holes');

    %imgTk = bwmorph(imgFl,'thicken');
    %subplot(4,4,11); imshow(imgTk); title('thicken');

    imgO = imerode(imgFl,fliplr(diag(ones(3,1))));
%    subplot(4,4,12); imshow(imgO); title('Eroded');
    % extract biggest cc
    cc = bwconncomp(imgO);
    numPixels = cellfun(@numel,cc.PixelIdxList);
    [~,idxBiggest] = max(numPixels);
    imgF = false(size(thisImage));
    imgF(cc.PixelIdxList{idxBiggest}) = true;
%    subplot(4,4,13); imshow(imgF); title('big CC');
    % post processing
    imgF1 = imdilate(imgF,strel('disk',3));

%    subplot(4,4,14); imshow(imgF1); title('disk dilate');
    
    imgF2 = imclose(imgF,strel('disk',5));
%    subplot(4,4,15); imshow(imgF2); title('disk close');
    
    %imgF3 = imclose(imgF2,strel('disk',5));
    %subplot(4,4,16); imshow(imgF3); title('disk close');

    % Save for output
    thisBarrelRegion = imgF2;    
    barrelRegion(:,:,z) = thisBarrelRegion;
    barrelRegionPost(:,:,z) = bwconvhull(thisBarrelRegion); 
    if visualize
        figure('Position', [3841 1 1920 999],'visible','on');
        subplot(1,2,1); imshow(thisImage);
        hold off;
        subplot(1,2,2); imshow(thisImage);
        hold on;
        h = imshow(green); set(h, 'AlphaData', thisBarrelRegion.*0.2);
        hold off;
        drawnow;
    end
    close all
    Util.progressBar(z,endStep);
end

% smooth3
barrelRegion = logical(smooth3(barrelRegion,'gaussian',3,2));
barrelRegionPost = logical(smooth3(barrelRegionPost,'gaussian',3,2));

%% 
Util.log('Making isosurface of barrelRegion...')
% temp = barrelRegionPost(1:5:end,1:5:end,:); % downsample
temp = barrelRegion;
figure
hold on
iso = isosurface(temp,0.5);
p = patch(iso);
p.FaceColor = 'red';
p.EdgeColor = 'none';
% daspect([28 28 11.24]);
daspect([1 1 1]);
view(-60,35);
axis on
% axis([0 1000 0 900 0 1200])
% view(3);
camlight 
lighting gouraud
hold off
saveas(gcf,fullfile(outDir,'barrelIso_vJuly.fig'));
close all

temp = barrelRegionPost;
figure
hold on
isoPost = isosurface(temp,0.5);
p = patch(isoPost);
p.FaceColor = 'red';
p.EdgeColor = 'none';
% daspect([28 28 11.24]);
daspect([1 1 1]);
view(-60,35);
axis on
% axis([0 1000 0 900 0 1200])
% view(3);
camlight 
lighting gouraud
hold off
saveas(gcf,fullfile(outDir,['barrelPostIso_vJuly.fig']));
close all

info = Util.runInfo(false);
save(fullfile(outDir,'barrelState_vJuly.mat'),'barrelRegion','barrelRegionPost','iso','isoPost','info');

Util.log('Making seg movie...')
makeSegMovie(barrelRegion, raw, fullfile(outDir,'zzzMovie_vJuly.avi'),...
    fullfile(outDir,'barrelMovie_vJuly.avi'));

