% This script takes in a tree with diameter annotations
% and generates diameter values
% Author: Sahil Loomba <sahil.loombabrain.mpg.de>
%% measure axon diameter 
setConfig;
Util.log('Loading skeleton')
skelCffi = skeleton(config.cffi);
outfile = fullfile(outDir,'outData/figures','fig3',config.version,'tc_diameters.eps');
%% calculate and plot after you have done the annotations
diameter_paths = {fullfile(outDir, 'data/wholecells','tc_axon1_diameters.nml'),...
                fullfile(outDir, 'data/wholecells','tc_axon3_diameters.nml'),...
                fullfile(outDir, 'data/wholecells','tc_axon5_diameters.nml')};
tcNames = {'TC axon 01', 'TC axon 03', 'TC axon 05'};
skels = {};
for i=1:numel(tcNames)
    curSkel = skelCffi.keepTreeWithName(tcNames{i});
    startComment = '_root';
    startpoint = curSkel.getNodesWithComment(startComment,1,'regexp');
    startIds(i) = str2double(curSkel.nodesAsStruct{1}(startpoint).id);
    skels{i} = curSkel;
    clear startpoint curSkel
end
Util.log('Calculating diameters...')
diameters = cell(numel(diameter_paths),1);
distances = cell(numel(diameter_paths),1);
for i=1:numel(diameter_paths)
    [diameters{i}, distances{i}] = getDataFromAnnotation(skels{i}, startIds(i), diameter_paths{i});
end

Util.log('Plotting diameters vs distances...')
colors = [0.929,0.694,0.125; 213/255,94/255,0; 0.75,0.75,0];
dist_interp = 1:400;
diam_interp = {};
for i=1:numel(tcNames)
    y = diameters{i};
    x = distances{i};
    m = [x,y];
    [~,idx] = unique(m(:,1));
    m = m(idx,:);
    [~,idx] = unique(m(:,2));
    m = m(idx,:);
    diam_interp{i} = interp1(m(:,1),m(:,2),dist_interp);
end

fig = figure;
fig.Color = 'white';
hold on
for i=1:numel(tcNames)
    plot(dist_interp, diam_interp{i},'Color',colors(i,:));
end
diam_interp_all = vertcat(diam_interp{:});
plot(dist_interp, nanmean(diam_interp_all,1),'k','LineWidth',3);
cx_nel = sum(~isnan(diam_interp_all),2);
plot(dist_interp, nanmean(diam_interp_all,1) + nanstd(diam_interp_all,0,2)./sqrt(cx_nel),'k','LineWidth',3);
plot(dist_interp, nanmean(diam_interp_all,1) - nanstd(diam_interp_all,0,2)./sqrt(cx_nel),'k','LineWidth',3);
set(gca,'XLim',[0 400],'YLim',[0 2.5]);
%xlabel('Axonal path length(\mum) from root')
%ylabel('Axon diameter(\mum)')
Util.setPlotDefault(gca,false,false);
box off;
set(gca,'LineWidth',2,'YAxisLocation','left')
%camroll(-90);
export_fig(outfile, '-q101', '-nocrop', '-transparent');
close all

% histogram
hgram = histogram(nanmean(diam_interp_all,1),'DisplayStyle','stairs','LineWidth',3,'EdgeColor','k');
%xlabel('Axon diameter(\mum)')
%ylabel('Path length to axon root')
Util.setPlotDefault(gca,false,false);
set(gca,'LineWidth',2,'XLim',[0,1],'xtick',[0,0.5,1],'ytick',[0:50:200])
box off;
export_fig(fullfile(outDir,'outData/figures','fig3',config.version,'tc_diameters_inset.eps'), '-q101', '-nocrop', '-transparent');
close all


function [diameters, distNodesAll] = getDataFromAnnotation(skel, startId, diameter_path)
skelAnnotated = skeleton(diameter_path);
% extract annotation trees of size nodes 2
idxDel = arrayfun(@(x) size(skelAnnotated.nodes{x},1)~=2,1:skelAnnotated.numTrees);
skelAnnotated = skelAnnotated.deleteTrees(idxDel);
diameters  = skelAnnotated.pathLength./1e3; % um

distNodesAll = zeros(skelAnnotated.numTrees,1);
for idxTree=1:skelAnnotated.numTrees
    % pick one node from annotation
    curPos = skelAnnotated.nodes{idxTree}(1,1:3);
    % get closest node in the skel
    axonNodeIdx = skel.getClosestNode(curPos,1,false);% only one tree here
    axonNodeId = str2double(skel.nodesAsStruct{1}(axonNodeIdx).id);
    % find path from annotation point to its _root
    [~,~,distPoint] = skel.getShortestPath(startId,axonNodeId);
    distNodesAll(idxTree) = distPoint./1e3; %um
end
end
