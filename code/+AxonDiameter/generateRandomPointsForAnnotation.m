function [skelOut, coords, points] = generateRandomPointsForAnnotation(skelPath,...
    startComment, endComment, delta, scale, outfile)
% This script generates coordinates on an axon between its startpoint (eg. soma)
% and end point (any synapse location, say last one) seprated by delta
% distances through its stretch
% INPUT:
%   skelPath: Path to nml with one tree with axon as the single tree
%   startComment: Comment at the root or soma of the axon ('soma','tc_root')
%   endComment: Comment at the last synapse 
%   delta: [�m] step size between measuring points eg. 25
%   scale: dataset scale
%   outfile: Path to output skeleton file with random points
% OUTPUT:
%   skelOut: skel with random points and axon tree
%   coords: locations of those points
%
% Author: Helene Schmidt <helene.schimdt@brain.mpg.de>
% Modified by: Sahil Loomba <sahil.loombabrain.mpg.de>
%% measure axon diameter 
skel = skeleton(skelPath);
startpoint = skel.getNodesWithComment(startComment,1,'regexp');

% attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
comments = {skel.nodesAsStruct{1}.comment}';
assert(startpoint == find(cellfun(@(x) any(regexp(x,startComment)),comments)))

% find the comment to be the end point
sy_node = find(cellfun(@(x) any(regexp(x,endComment)),comments));

%% make axonogram and generate random points for annotations
baum = Axonogram.axonogram(skel, '', startpoint, scale);
[coord, points] = AxonDiameter.measure_axon_diameter(sy_node,baum,skel,delta,scale);

Util.log('Writing out coordinates for annotations...')
skelOut  = skel;
skelOut = skelOut.addNodesAsTrees(coord);
if nargin>5
    skelOut.write(outfile);
end
end
