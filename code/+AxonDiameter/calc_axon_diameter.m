function [y, dist] = calc_axon_diameter(curTree,skel,Baum,scale)
% Author: Helene Schmidt <helene.schimdt@brain.mpg.de>
% Modified by: Sahil Loomba <sahil.loombabrain.mpg.de>
% diameters
one = curTree.nodes{1}(1:2:end, 1:3).* repmat(scale, size(curTree.nodes{1}(1:2:end, 1:3),1), 1);
two = curTree.nodes{1}(2:2:end, 1:3).* repmat(scale, size(curTree.nodes{1}(1:2:end, 1:3),1), 1);
y = sqrt(sum((one-two).^2,2))./1000;

nodes = [];
for i = 1:size(one,1)
    D = pdist([skel.nodes{1}(:,1:3).* repmat(scale, size(skel.nodes{1}(:,1:3),1),1); one(i,:)]);
    DM = squareform(D);
    s = size(skel.nodes{1}(:,1:3),1);
    d = DM(1:s,s+1:end);
    d = d./1000; %�m
    idx_proxi = find(d ==min(d));
    [i_d, i_s] = ind2sub(size(d),idx_proxi);
    node = skel.nodes{1}(i_d,1:3);
    
    nodes(end+1) = intersect(intersect(find(skel.nodes{1}(:,1)== node(1,1)),...
        find(skel.nodes{1}(:,2)== node(1,2))),find(skel.nodes{1}(:,3)== node(1,3)));
    
end
dist = AxonDiameter.pathlength_dist(nodes, skel, scale, Baum);
end