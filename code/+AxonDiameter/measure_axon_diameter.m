function [coord, m_p] = measure_axon_diameter(sy,Baum,tree,delta,scale)
% Author: Helene Schmidt <helene.schimdt@brain.mpg.de>
% Modified by: Sahil Loomba <sahil.loombabrain.mpg.de>
% get all path points
path_points = [];

found = false;
c = 1;
while found == false
    idx = find(Baum(c).points == sy);
    if ~isempty(idx)
        found = true;
    else
        c = c+1;
    end
end

path_points = Baum(c).points(1:idx);
tn = Baum(c).treename;

while tn ~= 1
    for i = 1:length(Baum)
        if ismember(tn,Baum(i).children)
            tn = i;
        end
    end
    path_points = [Baum(tn).points path_points];
end
% path_points = [Baum(1).points path_points];
pl_pp = AxonDiameter.pathlength_dist(path_points, tree, scale, Baum);

% get points for axon diameter measurement + coordinates
m = floor(pl_pp(end)-floor(floor(pl_pp(end))/delta)*delta):delta:pl_pp(end);

m_p = [];
for i = 1:length(m)
   diff = abs(pl_pp-m(i));
   idx = find(diff == min(diff));
   m_p(end+1) = path_points(idx(end));
end
coord = tree.nodes{1}(m_p,1:3);

end