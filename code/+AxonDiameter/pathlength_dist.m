function dist = pathlength_dist(idx, skel, scale, Baum)
% Author: Helene Schmidt <helene.schimdt@brain.mpg.de>
% Modified by: Sahil Loomba <sahil.loombabrain.mpg.de>
% Returns the path length from idx nodes to soma in �m
dist = [];
for i = 1:length(idx)
    start = idx(i);
    found = false;
    c = 1;
    while found == false
        ind = find(Baum(c).points == start);
        if ~isempty(ind)
            found = true;
        else
            c = c+1;
        end
    end
    nodes = skel.nodes{1}(Baum(c).points(1:ind),1:3).* ...
        repmat(scale, size(skel.nodes{1}(Baum(c).points(1:ind),1:3),1), 1);
    one = nodes((1:size(nodes,1)-1)',:);
    two = nodes((2:size(nodes,1))',:);
   
    dist = [dist; sum(sqrt(sum((one-two).^2,2)))/1000 + Baum(c).xy_start(1)];

end
end