function f = adaptPaperPosition( f )
%ADAPTPAPERPOSITION Adjust the paper position after resizing figure window.
% INPUT f: (Optional) Figure handle.
%          (Default: f = gcf)
% OUTPUT f: Figure handle to updated figure.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('f','var')
    f = gcf;
end

set(f,'Units','Centimeters');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Centimeters', ...
    'PaperSize',[pos(3), pos(4)]);

end

