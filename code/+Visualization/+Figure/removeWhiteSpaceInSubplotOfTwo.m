function f = removeWhiteSpaceInSubplotOfTwo(f,a, type)
% f: figure handle
% a: subplot handles
% type: subplots type 'row' or 'col'
if ~exist('f','var') || isempty(f)
    f = gcf;
    a = f.Children;
end

if ~exist('type','var') || isempty(type)
    type = 'row';
end

switch type
    case 'row'
        a(1).Units = 'normalized';
        a(2).Units = 'normalized';
        
        a(1).Position(1:2) = [0,-0.17];
        a(1).Position(3:4) = a(1).Position(3:4)+ [0.45,0.45];
        set(a(1),'color','none');
        
        a(2).Position(1:2) = [0.40,-0.14];
        a(2).Position(3:4) = a(2).Position(3:4)+ [0.45,0.45];
        set(a(2),'color','none');
        
    case 'col'
        a(1).Units = 'normalized';
        a(2).Units = 'normalized';

        a(1).Position(1:2) = [0,0.4];
        a(1).Position(3:4) = [0.3347,0.3412] + [0.40,0.40]; % from subplot 2,2 dims
        set(a(1),'color','none');

        a(2).Position(1:2) = [0,0.1];
        a(2).Position(3:4) = [0.3347,0.3412] + [0.40,0.40]; % from subplot 2,2 dims
        set(a(2),'color','none');
end
end
