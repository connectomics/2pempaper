function f = removeWhitespaceFromPlot(f, border)
%REMOVEWHITESPACEFROMPLOT Remove surrounding whitespace from plot.
% INPUT f: (Optional) Figure handle.
%          (Default: f = gcf)
%       border: (Optional) [4x1] double specifying the modification from
%           exact tight inset.
% OUTPUT f: Figure handle to updated figure.
%
% NOTE Set axes tick labels to [] to get only get remove whitespace arond
%      actual plot.
% NOTE Saving files as eps allow to delete the whitespace directly in
%      illustrator.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('f','var') || isempty(f)
    f = gcf;
end
ax = gca;
if ~exist('border','var') || isempty(f)
    if strcmp(ax.TickDir,'out')
        border = [0.015, 0.015, 0.02, 0.02];
    else
        border = [0, 0, 0, 0];
    end
end

%make current axis tight
set(ax,'Units','normalized');
ti = get(ax,'TightInset');
set(ax,'Position',[ti(1)+border(1) ti(2)+border(2) ...
    1-ti(3)-ti(1)-border(3)  1-ti(4)-ti(2)-border(4) ]);

Visualization.Figure.adaptPaperPosition(f);

end

