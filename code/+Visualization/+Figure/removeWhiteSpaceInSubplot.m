function f = removeWhiteSpaceInSubplot(f,a)
% works for 4 subplots where f.Position = [1 1 21 29.7] in centimeters

if ~exist('f','var') || isempty(f)
    f = gcf;
    a = f.Children;
end

a(1).Units = 'normalized';
a(2).Units = 'normalized';
a(3).Units = 'normalized';
a(4).Units = 'normalized';

a(1).Position(1:2) = [0,0.35];
a(1).Position(3:4) = a(1).Position(3:4)+ [0.30,0.30];
set(a(1),'color','none');
a(2).Position(1:2) = [0.40,0.35];
a(2).Position(3:4) = a(2).Position(3:4)+ [0.30,0.30];
set(a(2),'color','none');
a(3).Position(1:2) = [0,0.03];
a(3).Position(3:4) = a(3).Position(3:4)+ [0.30,0.30];
set(a(3),'color','none');
a(4).Position(1:2) = [0.40,0.03];
a(4).Position(3:4) = a(4).Position(3:4)+ [0.30,0.30];
set(a(4),'color','none');
end
