function fvt = applyTformToIso(tform,fv,direction)
% apply affine transformation to in isosurfaces
% tform = 4x4 matrix
% iso: f,v
% fvt: transformed fv
% direction: forward (default) or backward

if strcmp(direction,'backward')
    tform = inv(tform);
end
    
vertices = fv.vertices;

% append 1 to vertices
vertices = horzcat(vertices,ones(size(vertices,1),1));

% transpose vertices to 4xN
vertices = transpose(vertices);

% apply tform
v = tform*vertices;

% transpose v
v = transpose(v);

% remove extra 1's
v = v(:,1:3);

% build output
fvt.vertices = round(v);
fvt.faces = fv.faces;
end