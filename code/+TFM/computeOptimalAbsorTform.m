function [ tform, regParams, stat] = computeOptimalAbsorTform(...
    movingPoints, fixedPoints, scaleMovingPoints, scaleFixedPoints, relativeSearchRange )
stat = struct;
if nargin < 5
    relativeSearchRange = 0.1;
end

scaleVector = [scaleFixedPoints(1)/scaleMovingPoints(1),...
               scaleFixedPoints(2)/scaleMovingPoints(2),...
               scaleFixedPoints(3)/scaleMovingPoints(3)].^-1;

% if nargout > 2
    lsqsBefore = absorWrapper(movingPoints, fixedPoints, scaleVector);
    stat.lsqsBefore = lsqsBefore;
% end

% optimal scale vector
lossFunc = @(scaleVector) absorWrapper( movingPoints, fixedPoints, scaleVector );

tform = [];
b = [];
Aeq = [];
beq = [];
lb = scaleVector-scaleVector*relativeSearchRange;
ub = scaleVector+scaleVector*relativeSearchRange;

scaleVectorOpt = patternsearch(lossFunc, scaleVector, tform, b, Aeq, beq, lb, ub);

% optimal tform
[ stat.lsqsOpt, tform, regParams, stat.Bfit, stat.ErrorStats ] = absorWrapper(...
    movingPoints, fixedPoints, scaleVectorOpt );

end

function [ lsqsOpt, tform, regParams, Bfit, ErrorStats] = absorWrapper( movingPoints, fixedPoints, scaleVector )
if ~exist('scaleVector','var') || isempty(scaleVector)
    [regParams,Bfit,ErrorStats] = TFM.absor(movingPoints',fixedPoints','doScale',1);
    regParams.s = repmat(regParams.s,[1 3]);
    tform = regParams.M;
else
    tform_scale = [
                    scaleVector(1)  0               0               0
                    0               scaleVector(2)  0               0
                    0               0               scaleVector(3)  0
                    0               0               0               1];
    [ movingPointsT ] = TFM.applyTformToVertices(tform_scale, movingPoints);
    [regParams,Bfit,ErrorStats] = TFM.absor(movingPointsT',fixedPoints','doScale',0);
    regParams.s = scaleVector;
    tform_rot_trans = [
                    regParams.R     regParams.t
                    0   0   0       1];
    tform =  tform_rot_trans * tform_scale;
end

[ movingPointsT ] = TFM.applyTformToVertices(tform, movingPoints);
lsq = (sum(((fixedPoints - movingPointsT).^2),2)).^0.5;
lsqsOpt = sum(lsq);
end

