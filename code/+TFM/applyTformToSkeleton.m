function skel = applyTformToSkeleton(tform, skel, newScale, direction )
% tform: 4x4 transformation matrix
% direction: forward (default) or backward
if ~exist('direction','var') || isempty(direction)
    direction = 'forward';
end
if strcmp(direction,'backward')
    tform = inv(tform);
end

numToStrCellArray = @(vals) arrayfun( ...
    @(v) sprintf('%d', v), vals, 'UniformOutput', false);

endStep = length(skel.nodes);
for treeIdx = 1:endStep  
    
    curNodes = skel.nodes{treeIdx}(:,1:3);
    tformNodes = round(TFM.applyTformToVertices(tform,curNodes));
    
    % update skel
    skel.nodes{treeIdx}(:,1:3) = tformNodes;
    
    skel.nodesNumDataAll{treeIdx}(:,3:5) = tformNodes;
    
    temp = numToStrCellArray(reshape(tformNodes(:, 1), 1, []));
    [skel.nodesAsStruct{treeIdx}.x] = temp{:};
    temp = numToStrCellArray(reshape(tformNodes(:, 2), 1, []));
    [skel.nodesAsStruct{treeIdx}.y] = temp{:};
    temp = numToStrCellArray(reshape(tformNodes(:, 3), 1, []));
    [skel.nodesAsStruct{treeIdx}.z] = temp{:};

end

if ~exist('newScale','var') || isempty(newScale)
    skel.scale = skel.scale ./ [tform(1,1) tform(2,2) tform(3,3)];
else
    skel.scale = newScale;
end
end
