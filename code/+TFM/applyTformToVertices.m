function verticesT = applyTformToVertices(tform,vertices,direction)
% apply affine transformation to in isosurfaces
% tform = 4x4 matrix
% vertices: Nx3
% verticesT: transformed vertices Nx3
% direction: forward (default) or backward
if ~exist('direction','var') || isempty(direction)
    direction = 'forward';
end
if strcmp(direction,'backward')
    tform = inv(tform);
end
    
% append 1 to vertices, transpose vertices to 4xN
vertices = transpose(horzcat(vertices,ones(size(vertices,1),1)));

% apply tform
verticesT = transpose(tform*vertices);

% remove extra 1's
verticesT = verticesT(:,1:3);

% build output
% verticesT = round(verticesT);
end
