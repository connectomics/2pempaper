% set configuration parameters for 2pEM barrel analysis
config = struct;
config.outDir = '../outDir'; % set relative path

outDir = config.outDir;
config.version = '04_07_2019';
config.scaleEM = [11.24, 11.24, 30];
config.scaleLM = [1172,1172,1000];
config.skelIn = fullfile(config.outDir,'outData/pointClouds/skelSomaIn_vMay.nml');
config.barrelStateTform = fullfile(outDir,'barrelStateTformIso_vOct.mat');
config.stpAllApicals = fullfile(outDir,'data','2018_10_23_stp_all_apicals.nml');
config.somasClickedLM = fullfile(outDir,'data','m150617_2p_somas.nml');
config.skelINCombined = fullfile(outDir,'data','skel_in_combined_01_09_2021.nml');

% TC input mapping
config.skel_tc_input_complete = fullfile(outDir,'outData','tracings','hiwiProjects', 'proofread_finished','tc_readout_v20.nml');
config.skelMerged = fullfile(outDir, 'outData','tracings','hiwiProjects','proofread_finished_readout','readout_tc_20210729T073021.nml');
config.readoutTCData = fullfile(outDir, 'outData','tracings','hiwiProjects','proofread_finished_readout','readout_tc_20210729T073021_pooled_distance_data.mat');

config.skelTCCombined = fullfile(outDir,'data','skel_tc_combined_2019-08-14.nml');
config.cellTypesIN = fullfile(outDir, 'data',['cell_types_v_26_08_2021.xlsx']);
config.IN_syn =  fullfile(outDir,'data','IN_syn_v7.xlsx');
config.cffi = fullfile(outDir,'data','cffi_20191217.nml');
config.cffi_SLmanual = fullfile(outDir,'data','cffi_20211104.nml');

config.cffi_excel = fullfile(outDir,'data','2020-02-06_cffi_v1.xlsx');
config.cffi_excel_SLmanual = fullfile(outDir,'data','2020-02-19_cffi_v1_SLmanual.xlsx');% update when triads finished
config.cffi_excel_SLmanual_pooled = fullfile(outDir,'data','2020-11-04_cffi_v1_SLmanual_pooled.xlsx');% added sh + spine distances as well
config.cellTypesL4 = fullfile(outDir, 'data','2019_05_03_cell_type_L4.nml');
config.cellTypesL4Fixed = fullfile(outDir, 'data','2021_08_27_cell_type_L4_fixed.nml'); 
config.cellTypesL4Updated_py5 = fullfile(outDir,'data','2019_05_03_cell_type_L4_updated_py5.nml');
config.apicalNmlEM = fullfile(outDir,'allForHeiko','data_1g_EM','2020_10_28_stp_all_apicals_hacked_magenta.nml');
config.ais = fullfile(outDir,'data','YH_st126_MT4_updated_1708_ais_15_01_2020.nml');

config.skelDendContactome = fullfile(outDir,'data','skel_in_in_dendrite_contacts_1um.nml'); % proof read for reach contacts

config.connmat = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version],'extractConnectome.mat');
config.connmat_SLmanual = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version],'extractConnectome_SLmanual.mat');
config.connmat_dend = fullfile(outDir,'outData','figures','IN_gallery',['skel_in_combined_v_' config.version],'extractININDendriteConnectome.mat');


config.functionalExcel = fullfile(outDir,'data','2019_01_30_2p_summary_v3.xlsx');

% WB pre syn axons
config.skelPreSynAxonsForClustering = fullfile(outDir,'data','skelPreSynAxonsForClustering.nml');
