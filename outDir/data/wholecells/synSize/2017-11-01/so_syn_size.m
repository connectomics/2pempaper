clear all
fold = 'C:\Users\huay\Documents\MATLAB\2017-11-01\';
file = dir([fold 'sp\*.nml']);
for i = 1:5
    skel = skeleton([fold 'sp\' file(i).name]);
    skel = skel.sortTreesByLength;
    for j = 2:11
        points(1,1:3) = skel.nodes{j}(1,1:3);
        points(2,1:3) = skel.nodes{j}(2,1:3);
        points(3,1:3) = skel.nodes{j}(3,1:3);
        a = (((points(1,1)-points(2,1))*11.24)^2+((points(1,2)-points(2,2))*11.24)^2 ...
            +((points(1,3)-points(2,3))*30)^2)^.5;
        b = (((points(1,1)-points(3,1))*11.24)^2+((points(1,2)-points(3,2))*11.24)^2 ...
            +((points(1,3)-points(3,3))*30)^2)^.5;
        c = (((points(2,1)-points(3,1))*11.24)^2+((points(2,2)-points(3,2))*11.24)^2 ...
            +((points(2,3)-points(3,3))*30)^2)^.5;
        s = 1/2*(a+b+c);
        A = (s*(s-a)*(s-b)*(s-c))^.5;
        AZ_size_sp(j+(i-1)*10-1) = a;
        d_AZ_sp(j+(i-1)*10-1) = 2*A/a;
    end
end

file = dir([fold 'ss\*.nml']);
for i = 1:5
    skel = skeleton([fold 'ss\' file(i).name]);
    skel = skel.sortTreesByLength;
    for j = 2:11
        points(1,1:3) = skel.nodes{j}(1,1:3);
        points(2,1:3) = skel.nodes{j}(2,1:3);
        points(3,1:3) = skel.nodes{j}(3,1:3);
        a = (((points(1,1)-points(2,1))*11.24)^2+((points(1,2)-points(2,2))*11.24)^2 ...
            +((points(1,3)-points(2,3))*30)^2)^.5;
        b = (((points(1,1)-points(3,1))*11.24)^2+((points(1,2)-points(3,2))*11.24)^2 ...
            +((points(1,3)-points(3,3))*30)^2)^.5;
        c = (((points(2,1)-points(3,1))*11.24)^2+((points(2,2)-points(3,2))*11.24)^2 ...
            +((points(2,3)-points(3,3))*30)^2)^.5;
        s = 1/2*(a+b+c);
        A = (s*(s-a)*(s-b)*(s-c))^.5;
        AZ_size_ss(j+(i-1)*10-1) = a;
        d_AZ_ss(j+(i-1)*10-1) = 2*A/a;
    end
end