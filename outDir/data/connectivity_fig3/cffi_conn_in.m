clear conn;
skel_cffi = skeleton('D:\lm_em\data\cffi_2019_10_25.nml');
skel_cffi.scale = [0.01124, 0.01124, 0.030];
for i = 1:20
    tc = skel_cffi.getTreeWithName(sprintf('TC axon %02d',i));
    tc_root = skel_cffi.getNodesWithComment('TC_root',tc);
    tc_root_id = str2num(skel_cffi.nodesAsStruct{tc}(tc_root).id);
    for n = 1:43
        if n<100
            tc_input = skel_cffi.getNodesWithComment(sprintf('sh i %02d',n),tc);
        else
            tc_input = skel_cffi.getNodesWithComment(sprintf('sh i %d',n),tc);
        end
        L=[];
        if ~isempty(tc_input)
            for nn = 1:length(tc_input)
                tc_input_id = str2num(skel_cffi.nodesAsStruct{tc}(tc_input(nn)).id);
                [~,tree,l] = skel_cffi.getShortestPath(tc_root_id,tc_input_id);
                L = [L,l];
            end
        end
        conn{n,i} = L;    
    end
end

INs = [1 2 3 4 5 6 7 8 12 14 17 24 25 27 28 29 40];
for i = 1:length(INs)
    in = skel_cffi.getTreeWithName(sprintf('IN axon %02d',INs(i)));
    in_soma = skel_cffi.getNodesWithComment('cellbody',in);
    in_soma_id = str2num(skel_cffi.nodesAsStruct{in}(in_soma).id);
    for n = 1:43
        if n<100
            in_input_so = skel_cffi.getNodesWithComment(sprintf('so i %02d',n),in);
            in_input_sh = skel_cffi.getNodesWithComment(sprintf('sh i %02d',n),in);
        else
            in_input_so = skel_cffi.getNodesWithComment(sprintf('sh i %d',n),in); 
            in_input_sh = skel_cffi.getNodesWithComment(sprintf('so i %02d',n),in);
        end
        L=[];
        in_input = [in_input_so;in_input_sh];
        if ~isempty(in_input)
            for nn = 1:length(in_input)
                in_input_id = str2num(skel_cffi.nodesAsStruct{in}(in_input(nn)).id);
                [~,tree,l] = skel_cffi.getShortestPath(in_soma_id,in_input_id);
                L = [L,l];
            end
        end
        conn{n,i+20} = L;    
    end
end