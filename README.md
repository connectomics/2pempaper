# Code for analysis done in "Connectomic analysis of thalamus-driven disinhibition in cortical layer 4", Hua, Loomba et al 2022 Cell Reports
## Getting Started
The code in this repository was primarily developed for the R2021a release of [MATLAB](https://www.mathworks.com/products/matlab.html).

The latest version of the code can be found at https://gitlab.mpcdf.mpg.de/connectomics/2pempaper.git

If you have git installed, you can clone this repository by entering
```
$ git clone https://gitlab.mpcdf.mpg.de/connectomics/2pempaper.git
```
into your terminal.

Alternatively, you can download the code as a compressed file via the gitlab frontend.


Here's a list of the packages and folders with a short description of the contents:
* **+Figures/+Fig1**: Contains scripts to generate figures used in Fig. 1 of the publication
* **+Figures/+Fig2**: Same as above
* **+Figures/+Fig3**: Same as above
* **+Figures/+Fig4**: Same as above
* **+Figures/+Fig5**: Same as above
* **+Figures/+Fig6**: Same as above
* **+Figures/+Fig7**: Same as above
* **+Iso**: To process light microscopy data to generate isosurface for barrel
* **+TFM**: To compute transformation between LM and EM reference space
* **+Tform**: To apply computed transformations to skeleton and isosurface reconstructions
* **+Util**: All the utility functionalities specific to this repository
* **setConfig**: List of NML annotation files and excel data used in analysis
* **auxiliaryMethods**: Other utilities used for the analysis.

## Authors

This package was developed by
* **Sahil Loomba** 

with contribution from 
* **Yunfeng Hua**

under scientific supervision by
* **Moritz Helmstaedter**

Skeleton parser for WebKnossos (.nml) neurite skeleton files makes use of an efficient .nml parser developed by 
* **Alessandro Motta**

The Matlab class used to represent single neurite skeletons was developed by
* **Benedikt Staffler**
* **Alessandro Motta**
* **Manuel Berning**
* **Florian Drawitsch**
* **Ali Karimi**
* **Kevin Boergens**
* **Sahil Loomba**
* **Martin Schmidt**
* **Marcel Beining**

Volumetric data used to generate surfaces uses the [Webknossos-wrapper](https://github.com/scalableminds/webknossos-wrap) file format. You can visit their website for a complete list of authors.


## Acknowledgements
We thank
* **Matt Jacobson** for providing the “Absolute Orientation – Horn’s method” Matlab central package we used in our affine registration workflow
* **R Pavao** for providing Matlab central package for optimization of parameters of the [sigmoid function](https://www.mathworks.com/matlabcentral/fileexchange/42641-sigm_fit)
* **Bastian Bechtold** for providing Violin Plots for Matlab, [Github Project](https://github.com/bastibe/Violinplot-Matlab)
* **Jason D. Yeatman** and **D. Kroon** for providing smoothpatch function for Matlab, [Github Project](https://github.com/yeatmanlab/AFQ/blob/master/3Dmesh/smoothpatch_version1b/smoothpatch.m)
* **Arseny Kapoulkine** for pugixml, a XML parser library for C++
  https://pugixml.org/
* **Oliver J. Woodford** and **Yair M. Altman** for providing Matlab central package for exporting figures [export_fig](https:www.mathworks.com/matlabcentral/fileexchange/23629-export_fig)
## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2022 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany
